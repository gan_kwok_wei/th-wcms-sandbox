    <link rel="stylesheet" href="/assets/extend/fws/css/fwslider.css" media="all">
    
    <!-- Fonts 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->
    <script src="/assets/extend/fws/js/jquery.min.js"></script>
    <script src="/assets/extend/fws/js/jquery-ui.min.js"></script>

    <script src="/assets/extend/fws/js/fwslider.js"></script>

    <script type="text/javascript">
<!--

$(window).load(function() {
	//BIG SLIDER BANNER
	$('#slider_home').nivoSlider({
		directionNav: false,
		controlNav: true,
		controlNavThumbs:true,
		effect : 'fade'
	});

	//WHATS HOT BANNER
	$('#slider_hot').nivoSlider({
		directionNav: true,
		controlNav: true,
		effect : 'fade'
	});

	//SELECT UNIFORM
	$(".myselect").uniform();
});

//-->
</script>

<div class="row-slider black clearfix">
<div id="trace" class="black">
			<section id="billboard" class="slider-wrapper theme-default">
				<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/billboard-slider-promo', $this->output_data);
				?>
				<!-- END BIG BANNER -->
			</section>
		</div>
</div>

<div class="clear"></div>

<div id="container">
	<div class="six clearfix">
				<?php
					$this->output_data['page_id'] = 0;
					$this->load->view('component/promotion_list', $this->output_data);
				?>
		
	</div>
</div>

<div class="gutter"></div>