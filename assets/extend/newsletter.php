<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
 
<style>
 
h3#enew-signup {
        font: normal 900 20px Arial, Helvetica, sans-serif;
}
 
div#signup-wrap {
        width: 400px;
        background-color: #f3f3f3;
        border: 1px solid #d4d4d4;
        padding: 40px 0;
        margin: 0 auto;
}
 
table.signupframe {
        width: 228px;
        margin: 0 auto;
}
 
table.signupframe td {
        text-align: left;
        font: normal normal 12px Arial, Helvetica, sans-serif;
        color: #616060;
        padding-bottom: 8px;
}
 
 
table.signupframe input, table.signupframe select {
        margin: 3px 0 0;
        width: 226px;
}
 
table.signupframe td.radio input {
        width: auto;
}
 
table.signupframe td.radio label {
        margin-right: 12px;
}
 
table.signupframe td.submit input {
        width: auto;
}
 
table.signupframe td.submit {
        text-align: right;
}
 
table.signupframe span.man {
        color: #f00;
}
 
table.signupframe td.note {
        font-size: 10px;
}
 
 
</style>
</head>
 
<body>
<center>
<h3 id="enew-signup">4e-Newsletter Sign Up</h3>
</center>
 
<form method=post action="https://app.icontact.com/icp/signup.php" name="icpsignup" id="icpsignup264" accept-charset="UTF-8" onsubmit="return verifyRequired264();" >
<input type=hidden name=redirect value="http://www.tunehotels.com/eblast/user/register-success.html" />
<input type=hidden name=errorredirect value="http://www.tunehotels.com/eblast/user/register-fail.html" />
    <input type=hidden name="listid" value="14623">
    <input type=hidden name="specialid:14623" value="D2H8">
 
    <input type=hidden name=clientid value="773175">
    <input type=hidden name=formid value="264">
    <input type=hidden name=reallistid value="1">
    <input type=hidden name=doubleopt value="1">
<div id="signup-wrap">
        <center>
        <table class="signupframe" cellpadding="0" cellspacing="0">
               <tr>
               <td>
               <span class="man">*</span> <strong>Email</strong><br />
                <input type=text name="fields_email">
            </td>
        </tr>
        <tr>
               <td>
               <span class="man">*</span> <strong>First Name</strong><br />
                <input type=text name="fields_fname">
            </td>
        </tr>
        <tr>
               <td>
               <span class="man">*</span> <strong>Last Name</strong><br />
                <input type=text name="fields_lname">
            </td>
        </tr>
        <tr>
               <td class="radio">
               <span class="man">*</span> <strong>Gender</strong><br />
                <input type=radio id="options:gender:M" name="fields_gender" value="M"> <label for="options:gender:M">Male</label>
                      <input type=radio id="options:gender:F" name="fields_gender" value="F"> <label for="options:gender:F">Female</label>
            </td>
        </tr>
        <tr>
               <td>
               <span class="man">*</span> <strong>Country</strong><br />
                <select name="fields_country">
                  <option></option>
                  <option value="MY">Malaysia</option>
                  <option value="ID">Indonesia</option>
                  <option value="GB">United Kingdom</option>
                  <option value="CH">China</option>
                  <option value="SG">Singapore</option>
                  <option value="AU">Australia</option>
                  <option value="HK">Hong Kong</option>
                  <option value="TH">Thailand</option>
                  <option value="IN">India</option>
                  <option value="OA">Other Asia Pacific Countries</option>
                  <option value="OE">Other European Countries</option>
                  <option value="OT">Other Continents</option>
                </select>
            </td>
        </tr>
        <tr>
               <td>
               <span class="man">*</span> <strong>Date of Birth</strong> (dd/mm/yyyy)<br />
                <input type=text name="fields_dob" maxlength="10">
            </td>
        </tr>
        <tr>
               <td>
               <strong>Phone</strong><br />
                <input type=text name="fields_phone">
            </td>
        </tr>
        <tr>
               <td>
                <input type=text name="fields_channel" value="website" style="display: none" readonly="readonly">
            </td>
        </tr>
        <tr>
               <td class="note">
                <span class="man">* Mandatory Field</span>
            </td>
        </tr>
        <tr>
               <td class="submit"><input type="submit" name="Submit" value="Submit"></td>
        </tr>
        </table>
    </center>
</div>
 
</form>
<script type="text/javascript">
/*
//Auto transfer field
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

var email = document.getElementsByName('fields_email')[0]
var fname = document.getElementsByName('fields_fname')[0]
var lname = document.getElementsByName('fields_lname')[0]

email.value = getUrlVars()["email"];
fname.value = getUrlVars()["firstname"];
lname.value = getUrlVars()["lastname"];
*/

//end of auto transfer field

var icpForm264 = document.getElementById('icpsignup264');
 
if (document.location.protocol === "https:")
 
        icpForm264.action = "https://app.icontact.com/icp/signup.php";
function verifyRequired264() {
  if (icpForm264["fields_email"].value == "") {
    icpForm264["fields_email"].focus();
    alert("The Email field is required.");
    return false;
  }
  else if(icpForm264["fields_email"].value != "") {
        emailStr = icpForm264["fields_email"].value;
        var emailPat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var matchEmail = emailStr.match(emailPat);
        if (matchEmail == null) {
               alert("Please enter a valid email.")
               return false;
        }
  }
  if (icpForm264["fields_fname"].value == "") {
    icpForm264["fields_fname"].focus();
    alert("The First Name field is required.");
    return false;
  }
  if (icpForm264["fields_lname"].value == "") {
    icpForm264["fields_lname"].focus();
    alert("The Last Name field is required.");
    return false;
  }
  for (filled=i=0; i<document.icpsignup["fields_gender"].length; i++)
    if (icpForm264["fields_gender"][i].checked) {
      filled=1;
      break;
    }
  if (!filled) {
    alert("The Gender field is required.");
    return false;
  }
  if (icpForm264["fields_country"].value == "") {
    icpForm264["fields_country"].focus();
    alert("The Country field is required.");
    return false;
  }
  if (icpForm264["fields_dob"].value == "") {
    icpForm264["fields_dob"].focus();
    alert("The Date of Birth field is required.");
    return false;
  }
  else if (icpForm264["fields_dob"].value != "") {
        var currentTime = new Date()
        var currMonth = currentTime.getMonth() + 1
        var currDay = currentTime.getDate()
        var currYear = currentTime.getFullYear()
        var legalAge = currYear - 13;
 
        var dateStr = icpForm264["fields_dob"].value;
        var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{2}|\d{4})$/;
        // To require a 4 digit year entry, use this line instead:
        // var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
        var matchArray = dateStr.match(datePat); // is the format ok?
        if (matchArray == null) {
               alert("The date you'd entered is not in a valid format.")
               return false;
        }
        day = matchArray[1]; // parse date into variables
        month = matchArray[3];
        year = matchArray[4];
        if (month < 1 || month > 12) { // check month range
               alert("Month must be between 1 and 12.");
               return false;
        }
        if (day < 1 || day > 31) {
               alert("Day must be between 1 and 31.");
               return false;
        }
        if ((month==4 || month==6 || month==9 || month==11) && day==31) {
               alert("Month "+month+" doesn't have 31 days!")
               return false
        }
        if (month == 2) { // check for february 29th
               var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
               if (day>29 || (day==29 && !isleap)) {
                       alert("February " + year + " doesn't have " + day + " days!");
                       return false;
               }
        }
        if(year > currYear) {
               alert("Please enter the correct date.");
               return false;
        }
       
        if(year > legalAge) {
               alert("You must be at least 13 years old to sign up.");
               return false;
        }
       
        return true;
  }
 
return true;
}
</script>
 
</body>
</html>