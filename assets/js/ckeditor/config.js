﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.contentsCss = '/tune_media/wysiwyg.css'
	config.toolbar = [
	                  ['Source','-','Preview','Print','Image','Table'],
	                  ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo'],
	                  ['Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat']
	                 ];
	config.enterMode = CKEDITOR.ENTER_BR;
};
