
  WebFontConfig = {
    google: { families: [ 'Roboto:400,900,300:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); 
  
  /* menu */
$(document).ready(function(){

	// Remove the class of child and grandchild
	// This removes the CSS 'falback'
	$("#ul_top_menu ul.child").removeClass("child");
	$("#ul_top_menu ul.grandchild").removeClass("grandchild");
	
	// When a list item that contains an unordered list
	// is hovered on
	$("#ul_top_menu li").has("ul").hover(function(){

		//Add a class of current and fade in the sub-menu
		$(this).addClass("current").children("ul").fadeIn();
	}, function() {

		// On mouse off remove the class of current
		// Stop any sub-menu animation and set its display to none
		$(this).removeClass("current").children("ul").stop(true, true).css("display", "none");
	});
	
	
	//LIGHTBOX
	$('#this_country_lang').click(function(){
		
		
		var screenTop = $(document).scrollTop();
	
		var doc_tinggi = $(document).height();
		var tinggi = $(window).height();
		var lebar = $(window).width();
		var tinggi_box = $('.box1').height();
		var lebar_box = $('.box1').width();
		var top = ((tinggi/2)-(tinggi_box/2))+screenTop;
		var left = (lebar/2)-(lebar_box/2);
		
		$('.backdrop').css({'height' : doc_tinggi + 'px'});
		$('.backdrop, .box1').animate({'opacity':'.50'}, 300, 'linear');
		$('.box1').animate({'opacity':'1.00'}, 300, 'linear');
		$('.backdrop, .box1').css({'display' : 'block'});
		/* $('.box1').css({'top':top + 'px', 'left':left + 'px'}); */
		$('.box1').css({'top':'93px', 'left':left + 'px'});
	});

	$('.close1').click(function(){
		close_box1();
	});

});

function close_box1()
{
	$('.backdrop, .box1').animate({'opacity':'0'}, 300, 'linear', function(){
		$('.backdrop, .box1').css('display', 'none');
	});
}

$(function () {

	$("a#menu_hotel").click(function () {
		
		$("#sub_promotion").slideUp("slow");
		$("#sub_corporate").slideUp("slow");
		$("#sub_about_us").slideUp("slow");
		$("#sub_help_info").slideUp("slow");
		$("#sub_hotel").slideToggle("slow");
	});
	
	$("a#menu_promotion").click(function () {

		$("#sub_hotel").slideUp("slow");
		$("#sub_corporate").slideUp("slow");
		$("#sub_about_us").slideUp("slow");
		$("#sub_help_info").slideUp("slow");
		$("#sub_promotion").slideToggle("slow");
	});
	
	$("a#menu_corporate").click(function () {

		$("#sub_hotel").slideUp("slow");
		$("#sub_promotion").slideUp("slow");
		$("#sub_about_us").slideUp("slow");
		$("#sub_help_info").slideUp("slow");
		$("#sub_corporate").slideToggle("slow");
	});
	
	$("a#menu_about_us").click(function () {

		$("#sub_hotel").slideUp("slow");
		$("#sub_promotion").slideUp("slow");
		$("#sub_corporate").slideUp("slow");
		$("#sub_help_info").slideUp("slow");
		$("#sub_about_us").slideToggle("slow");
	});
	
	$("a#menu_help_info").click(function () {

		$("#sub_hotel").slideUp("slow");
		$("#sub_promotion").slideUp("slow");
		$("#sub_corporate").slideUp("slow");
		$("#sub_about_us").slideUp("slow");
		$("#sub_help_info").slideToggle("slow");
	});
	
	//BOOKING SELECT
//	$('.black_light').selectbox();
	
	//LANGUAGE & COUNTRY SELECT
/*	$('#for_lang').ddslick({
		width:200
	});
	$('#for_country').ddslick({
		width:200
	});
*/	
	//gallery
	$('.hotel_gallery_ul>li>a').click(function(){
		//$('.detail_image').empty();
		$('.hotel_gallery_ul>li>a').removeClass('active');
		$(this).addClass('active');
		var rel = $(this).attr('rel');
		
		
//		$('.detail_image').html('<p style="margin-top:200px;margin-left:200px;" ><img src="./images/assets/ajax-loader.gif" width="31" height="31" /></p>');
		$('.detail_image img').hide();
//		alert(rel);
		$('#' + rel).fadeIn('slow');
//		$('.detail_image').hide().html('<img src="' + rel + '" style="width:705px;" />').fadeIn('slow');
	});

});
