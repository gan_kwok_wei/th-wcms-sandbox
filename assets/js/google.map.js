/**
 * 
 */
	
	//map
	var map;
	var gIcon;
//	var points = new Array();
	 
	function addLoadEvent(func) { 
		var oldonload = window.onload; 
		if (typeof window.onload != 'function'){ 
			window.onload = func
		} else { 
			window.onload = function() {
				oldonload();
				func();
			}
		}
	}
/*	
	addLoadEvent(loadMap);
	addLoadEvent(addPoints);
*/	
	function loadMap(element, centerLat, centerLong) {
		map = new GMap2(document.getElementById(element));
		map.addControl(new GLargeMapControl());
		map.addControl(new GMapTypeControl());
		map.setCenter(new GLatLng(centerLat, centerLong), 16);
		map.setMapType(G_NORMAL_MAP);
	 
		gIcon = new GIcon();
		gIcon.image = "http://www.google.com/mapfiles/marker.png";
		gIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
		gIcon.iconSize = new GSize(20, 34);
		gIcon.shadowSize = new GSize(37, 34);
		gIcon.iconAnchor = new GPoint(9, 34);
		gIcon.infoWindowAnchor = new GPoint(9, 2);
		gIcon.infoShadowAnchor = new GPoint(18, 25);
	}
	  
	function createMarker(point, icon, popuphtml) {
		var popuphtml = "<div id=\"popup\">" + popuphtml + "<\/div>";
		var marker = new GMarker(point, icon);
		GEvent.addListener(marker, "click", function() {
			marker.openInfoWindowHtml(popuphtml);
		});
		return marker;
	}

	function addPoints(points) {
		for(var i = 0; i < points.length; i++) {
			var point = new GPoint(points[i][1], points[i][0]);
			var popuphtml = points[i][4] ;
			var marker = createMarker(point, points[i][2], popuphtml);
			map.addOverlay(marker);
		}
	}
