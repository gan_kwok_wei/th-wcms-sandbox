<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel extends GE_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		
		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir);

		$this->output_head['class'] = strtolower(__CLASS__);
		$this->output_head['module'] = strtolower($module);
		$this->output_head['style_extras'] = array(//$this->theme_dir . 'css/reset.css',
													//$this->theme_dir . 'css/jquery-ui-1.8.17.custom.css',
													//$this->theme_dir . 'css/jquery.selectbox.css',
													//$this->theme_dir . 'css/flight-calendar.css',
													//$this->theme_dir . 'css/uni_form.css',
													$this->theme_dir . 'css/bootstrap.css',
													$this->theme_dir . 'css/page_hotel.css',
													$this->theme_dir . 'css/main.css',
													$this->theme_dir . 'css/default.css',
													$this->theme_dir . 'css/~homepage.css',
													$this->theme_dir . 'css/~global.css',
													$this->theme_dir . 'css/menu_nav.css'
													);
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/jquery.tools.min.js',
												//base_url() . 'assets/js/jquery.uniform.js',
												//base_url() . 'assets/js/jquery.dateFormat-1.0.js',
												base_url() . 'assets/js/bootstrap.min.js',
												base_url() . 'assets/js/jquery-1.8.18.min.js');

	}

	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function index() {
		$this->hotels();
	}

	/**
	 * Enter description here ...
	 * @param unknown_type $country_id
	 */
	function hotels($country_id = NULL){
		$title = current($this->system_model->get_system_config('home_title'));
		
		$this->output_head['popup_newsletter'] = FALSE;
		$this->output_head['popup_room'] = FALSE;
		
		$this->output_head['title'] = $title;
		$page_name = (in_array($this->uri->segment(1), $this->system_model->get_registered_country(TRUE))) ?  $this->uri->segment(3) : $this->uri->segment(1);
		if(!$page_name){ $page_name = "home"; }
		$this->output_head['page_name'] = $page_name;
		
		$this->load->view('header', $this->output_head);

		$this->output_data['country_id'] = $country_id;
		
		$this->load->view('hotel', $this->output_data);
		
		$this->load->view('footer', $this->output_head);
	}

	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 * @param unknown_type $hotel_name
	 * @param unknown_type $tab_hotel
	 */
	function hotel_detail($hotel_id = NULL, $hotel_name = NULL, $tab_hotel = NULL) {
		if($hotel_id == NULL){
			$this->hotels();
		} else {
			$result= $this->pages_model->get_hotel($hotel_id);
			if($result->num_rows() > 0){

				$hotel = $result->row();

				$this->output_head['title'] = ($hotel->text_title) ? $hotel->text_title : $hotel->def_title;
				$page_name = (in_array($this->uri->segment(1), $this->system_model->get_registered_country(TRUE))) ?  $this->uri->segment(3) : $this->uri->segment(1);
				if(!$page_name){ $page_name = "home"; }
				$this->output_head['page_name'] = $page_name;
				
				$this->output_head['keywords'] = $hotel->meta_keywords;
				$this->output_head['description'] = $hotel->meta_description;
				$this->load->view('header', $this->output_head);

				$this->output_data['hotel_id'] = $hotel_id;
				$this->output_data['hotel_name'] = $hotel_name;
				$this->output_data['tab_hotel'] = $tab_hotel;

				$this->load->view('hotel_detail', $this->output_data);
				
				$this->load->view('footer', $this->output_head);
			} else {
				//$this->hotels();
				redirect('hotel/', 'refresh');
			}
		}
	}

	function hotel_by_name($hotel_name = NULL, $tab_hotel = NULL) {
		if($hotel_name == NULL){
			$this->hotels();
		} else {
			$result= $this->pages_model->get_hotel_by_name($hotel_name);
			if($result->num_rows() > 0) {

				$hotel = $result->row();
				$this->pages_model->set_hotel_hits($hotel->hotel_id);

				$this->output_head['title'] = ($hotel->text_title) ? $hotel->text_title : $hotel->def_title;
				$page_name = (in_array($this->uri->segment(1), $this->system_model->get_registered_country(TRUE))) ?  $this->uri->segment(3) : $this->uri->segment(1);
				if(!$page_name){ $page_name = "home"; }
				$this->output_head['page_name'] = $page_name;
				
				$this->output_head['popup_newsletter'] = FALSE;
				$this->output_head['popup_room'] = ($tab_hotel == NULL || $tab_hotel == 'hotel_information') ? TRUE : FALSE;
				$this->output_head['popup_promotion'] = TRUE;
				
				$this->output_head['keywords'] = $hotel->meta_keywords;
				$this->output_head['description'] = $hotel->meta_description;
				$this->load->view('header', $this->output_head);

				$this->output_data['hotel_id'] = $hotel->hotel_id;
				$this->output_data['hotel_name'] = $hotel_name;
				$this->output_data['tab_hotel'] = $tab_hotel;
				
				$this->load->view('hotel_detail', $this->output_data);
				$this->load->view('footer', $this->output_head);
			} else {
				//$this->hotels();
				redirect('hotel/', 'refresh');
			}
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */