<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends GE_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    function __construct() {
        parent::__construct();
        $this->load->model(array('system_model', 'cms_model', 'user_model', 'pages_model'));

        $dir = explode('/', str_replace('\\', '/', __DIR__));
        $module = end($dir);

        $this->output_head = array('class' => strtolower(__CLASS__),
                                    'module' => strtolower($module),
                                    'style_extras' => array(//$this->theme_dir . 'css/xtra-base.css',
                                                          //$this->theme_dir . 'css/jquery-ui-1.8.17.custom.css',
                                                          //$this->theme_dir . 'css/nivo/themes/default/default.css',
                                                          //$this->theme_dir . 'css/nivo/themes/light/light.css',
                                                          //$this->theme_dir . 'css/nivo/nivo-slider.css',
                                                          //$this->theme_dir . 'css/jquery.selectbox.css',
                                                         // $this->theme_dir . 'css/flight-calendar.css',
                                                         // $this->theme_dir . 'css/uni_form.css',
                                                          $this->theme_dir . 'css/default.css',
                                                          $this->theme_dir . 'css/bootstrap.css',

	                                                   
	                                                   $this->theme_dir . 'css/~homepage.css',
	                                                   $this->theme_dir . 'css/menu_nav.css',
	                                                   $this->theme_dir . 'css/~global.css'
	                                                   ),


                                    'js_extras' => array(//base_url('assets/js') . 'jquery-1.9.1.min.js',
                                                        //base_url('assets/js') . 'jquery.nivo.slider.js',
                                                        //base_url('assets/js') . 'jquery.ddslick.js',
                                                        //base_url('assets/js') . 'jquery.selectbox-0.2.js',
                                                        //base_url('assets/js') . 'bootstrap-modal.js',
                                                        base_url('assets/js') . 'html5shiv.min.js',
                                                        base_url('assets/js') . 'bootstrap.min.js',
                                                        'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js')

                                                   
                            );

    }

    /**
     * Enter description here ...
     * @param string $mode
     */
    function index($mode = 'show') {
        $this->output_head['function'] = __FUNCTION__;
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');
        $this->ckfinder->SetupCKEditor($this->ckeditor);

        $title = current($this->system_model->get_system_config('home_title'));
        $this->output_head['title'] = $title;
        $this->load->view('header', $this->output_head);

        $this->output_data['mode'] = $mode;
        $this->load->view('home', $this->output_data);
        $this->load->view('footer');

    }


    /**
     * Enter description here ...
     * @param string menu_id
     */
    function page($menu_id) {
        $this->output_head['function'] = __FUNCTION__;
        $this->load->library('ckeditor');
        $this->load->library('ckFinder');

        $title = current($this->system_model->get_system_config('home_title'));
        $this->output_head['title'] = $title;
        $this->load->view('header', $this->output_head);

        $this->output_data['menu_id'] = $menu_id;
        $this->load->view('pages', $this->output_data);
        $this->load->view('global/footer');

    }

}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */