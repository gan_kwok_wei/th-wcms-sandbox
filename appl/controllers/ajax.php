<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2013, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ajax.php
 * @copyright Copyright 2011-2013, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Jan 22, 2013
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Ajax extends GE_Controller {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir);
		$this->output_head['class'] = strtolower(__CLASS__);
		$this->output_head['module'] = strtolower($module);
		
	}

	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function index() {
		if(isset($_POST['act'])){
			if($_POST['act'] == "get_title_promo"){
				if(isset($_POST['promo_id'])){
					$get_promo_by_id = $this->pages_model->get_promo_by_id($_POST['promo_id']);
					$data_get_promo_by_id = $get_promo_by_id->row();
					echo (($data_get_promo_by_id->text_title) ? $data_get_promo_by_id->text_title : $data_get_promo_by_id->def_title);
					$set_promo_hits = $this->pages_model->set_promo_hits($_POST['promo_id']);
				}
			}
			
			if($_POST['act'] == "get_content_promo"){
				if(isset($_POST['promo_id'])){
					$get_promo_by_id = $this->pages_model->get_promo_by_id($_POST['promo_id']);
					$data_get_promo_by_id = $get_promo_by_id->row();
					echo (($data_get_promo_by_id->text_content) ? $data_get_promo_by_id->text_content : $data_get_promo_by_id->def_content);
				}
			}
			
			if($_POST['act'] == "get_image_promo"){
				if(isset($_POST['promo_id'])){
					$get_promo_by_id = $this->pages_model->get_promo_by_id($_POST['promo_id']);
					$data_get_promo_by_id = $get_promo_by_id->row();
					echo '<img src="' . (($data_get_promo_by_id->text_image_1) ? $data_get_promo_by_id->text_image_1 : $data_get_promo_by_id->def_image_1) . '" class="box_promo_image" />';
				}
			}

			if($_POST['act'] == "get_city_by_country"){
				if(isset($_POST['country_id'])){
					$query_hotel_by_country = $this->pages_model->get_hotel_by_country($_POST['country_id']);
					if($query_hotel_by_country->num_rows() > 0){
						echo '<option value="">Select City</option>';
						foreach($query_hotel_by_country->result() as $data_hotel_by_country){
							echo '<option value="' . $data_hotel_by_country->hotel_id . '" data-foo="' . $data_hotel_by_country->hotel_name . '" >' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
						}
					}
				}
			}

			if($_POST['act'] == "insert_inquiry"){
				$sql = mysql_query("INSERT INTO hotel_inquiry (hotel_inquiry_type,
										company_name,
										contact_person,
										telephone,
										mobile,
										address,
										postcode,
										country_id,
										fax,
										email,
										hotel_id,
										checkin,
										checkout,
										no_of_night,
										no_of_room,
										no_of_pax,
										room_type,
										remarks,
										status)
									VALUES ('" . $_POST['hotel_inquiry_type'] . "',
										'" . $_POST['company_name'] . "',
										'" . $_POST['contact_person'] . "',
										'" . $_POST['telephone'] . "',
										'" . $_POST['mobile'] . "',
										'" . $_POST['address'] . "',
										'" . $_POST['postcode'] . "',
										'" . $_POST['country_id'] . "',
										'" . $_POST['fax'] . "',
										'" . $_POST['email'] . "',
										'" . $_POST['hotel_id'] . "',
										'" . $_POST['in_year'] . "-" . $_POST['in_month'] . "-" . $_POST['in_day'] . "',
										'" . $_POST['out_year'] . "-" . $_POST['out_month'] . "-" . $_POST['out_day'] . "',
										'" . $_POST['no_of_night'] . "',
										'" . $_POST['no_of_room'] . "',
										'" . $_POST['no_of_pax'] . "',
										'" . $_POST['room_type'] . "',
										'" . $_POST['remarks'] . "',
										'0')") or die(mysql_error());
				$affect = mysql_affected_rows();
				if($affect > 0){
					echo "1";
				} else {
					echo "0";
				}
			}

			if($_POST['act'] == "get_room"){
				if(isset($_POST['hotel_id'])){
					$get_hotel_room = $this->pages_model->get_hotel_room($_POST['hotel_id']);
					if($get_hotel_room->num_rows() > 0){
						echo '<option value="">Select Room</option>';
						foreach($get_hotel_room->result() as $data_get_hotel_room){
							echo '<option value="' . $data_get_hotel_room->room_id . '" data-foo="' . $data_get_hotel_room->room_id . '" >' . $data_get_hotel_room->title . '</option>';
						}
					}
				}
			}

			if($_POST['act'] == "insert_subscribe"){
				$sql = mysql_query("INSERT INTO users (role_id,
										email,
										first_name,
										last_name,
										dob,
										sex,
										phone_mobile,
										country)
									VALUES ('8',
										'" . $_POST['news_email'] . "',
										'" . $_POST['news_fname'] . "',
										'" . $_POST['news_lname'] . "',
										'" . $_POST['news_birth'] . "',
										'" . $_POST['news_gender'] . "',
										'" . $_POST['news_phone'] . "',
										'" . $_POST['news_country_id'] . "')") or die(mysql_error());
				$affect = mysql_affected_rows();
				if($affect > 0){
					echo "1";
				} else {
					echo "0";
				}
			}
		}
	}
	
	/**
	 * Enter description here ...
	 */
	function room_detail() {
		$result = $this->pages_model->get_room_detail($_POST['room_id']);
		if($result->num_rows() > 0) {
			$data = $result->row();
			$this->output
			    ->set_content_type('application/json')
			    ->set_output(json_encode(array('result' => 1, 'title' => $data->title, 'image' => $data->image, 'feature_text' => $data->feature_text)));
		} else {
			$this->output
			    ->set_content_type('application/json')
			    ->set_output(json_encode(array('result' => 0)));
		}
	}

}

/**
 * End of file ajax.php 
 * Location: ./.../.../.../ajax.php 
 */