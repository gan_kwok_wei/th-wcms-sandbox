﻿    <link rel="stylesheet" href="/assets/extend/fws/css/fwslider.css" media="all">
    
    <!-- Fonts 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->
    <script src="/assets/extend/fws/js/jquery.min.js"></script>
    <script src="/assets/extend/fws/js/jquery-ui.min.js"></script>

    <script src="/assets/extend/fws/js/fwslider.js"></script>

    <script type="text/javascript">
<!--

$(window).load(function() {
	//BIG SLIDER BANNER
	$('#fwslider').slide({
		directionNav: false,
		controlNav: true,
		controlNavThumbs:true,
		effect : 'fade'
	});

	//WHATS HOT BANNER
	$('#slider_hot').nivoSlider({
		directionNav: true,
		controlNav: true,
		effect : 'fade'
	});


});

//-->
</script>

<!--start /fwslider -->
	<div class="promo-slider">
		<div class="container clearfix">
			<section id="booker">
			<?php
			$this->load->view('component/booking_search');
			?>
			</section>

		</div>

		<section id="trace">
				<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/billboard-slider-promo', $this->output_data);
				?>
					
		</section>

	</div>
<!--end / fwslider -->


<div class="clear"></div>

<div class="row" id="container">
	<div class="six clearfix">
		<div class="bread_crumb f_left">
		
			</div>
				<div class="list_promotion" style="margin-top:30px;">
					<ul id="will_paginate">
					<table>
					
						<?php
						
							$sql_get_all_promo = $this->pages_model->get_all_promo_deal($promo_id);
							if($sql_get_all_promo->num_rows() > 0){
								foreach($sql_get_all_promo->result() as $data_get_all_promo){
						?>
						<tr>
							<td></td><td><?php //echo '<div style="padding-left:15px;"><a href="' . base_url('home') . '">' . strtoupper(lang('home')) . '</a> > <a href="' . base_url('promotion') . '">' . strtoupper(lang('promotions')) . '</a> > ' .$data_get_all_promo->text_title;?></div></td></tr>
						<tr>
								<td><img src="<?php echo (($data_get_all_promo->text_icon_1) ? $data_get_all_promo->text_icon_1 : $data_get_all_promo->def_icon_1); ?>" style="float:left;"></td>
								

								<td><div class="gw_dd_content_stack_offer">
									<a href="#"><h4 class="thick"><?php echo (($data_get_all_promo->text_title) ? $data_get_all_promo->text_title : $data_get_all_promo->def_title);?></h4></a>
									<p><?php echo (($data_get_all_promo->text_teaser) ? $data_get_all_promo->text_teaser : $data_get_all_promo->def_teaser);?></p>
								</div></td>
						</tr>
						<?php
								}
							}
							else{
								echo '<p style="padding:20px 0px;">' . lang('promo_no_found') . '</p>';
							}
						?>
					
					</table>
					</ul>
					<div class="clean"></div>
				</div>
		
	</div>
</div>

<div class="gutter"></div>