<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends GE_Controller {

	var $output_data;

	function __construct() {
		parent::__construct();
		$this->load->model('system_model');
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir); 

		$this->output_data = array('class' => __CLASS__, 'module' => $module);

	}

	function index() {
		$this->authenticate();
	}

	function lang($lang) {
		$this->session->save_enc('lang', $lang);

		//		clear language
		$this->lang->is_loaded = array();

		$this->lang->load('ge', $this->session->get_dec('lang'));
		$this->lang->load('date', $this->session->get_dec('lang'));
		$this->lang->load('form_validation', $this->session->get_dec('lang'));

		$this->authenticate();
	}

	function check_login() {
		if(!is_logged()) {
			$this->output->set_output('TRUE');
		} else {
			$this->output->set_output('FALSE');
		}
	}

	function authenticate() {
		$this->output_data['function'] = __FUNCTION__;

		if (is_logged()) {
			$this->home();

		} else {
			$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|md5');

			$valid_user = FALSE;
			$msg = "";

			if ($this->form_validation->run() != FALSE){//	echo 'valid';
				$valid_user = $this->auth->trylogin($this->input->post("email"), $this->input->post("password")); 

				if(!$valid_user) {
					$msg = '<div class="alert">' . lang('login_error') . '</div>';                                    
				} else {
					$this->home();

				}
			}

			$this->output_data['msg'] = $msg;
			$this->load->view("login", $this->output_data);

		}

	}

	/**
	* Enter description here ...
	*/
	function logout() {     
		$this->auth->logout();
		redirect('login');
		exit;
	}

	/**
	* Enter description here ...
	*/
	function home() {
		$item = (get_role() <= $this->config->item('ge_max_admin_role')) ? 'ge_admin_url' : 'ge_home_url';
//		redirect($this->config->item($item));
		redirect('system/dashboard');
		exit;
	}

	/**
	* Enter description here ...
	*/
	function forgot_password() {
		$this->output_data['function'] = __FUNCTION__;
		$msg = "";
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|xss_clean');
		if ($this->form_validation->run() != FALSE) {//	echo 'valid';
			$this->load->model(array('system_model'));

			$valid_user = $this->system_model->get_forgotten_user($this->input->post("email")); 

			if(!$valid_user) {
				$msg = "<div class=\"alert\">".lang('alert_user_undefined')."</div>";                                    
			} else {
				$this->system_model->reset_password($valid_user->user_id);
				$msg = "<div class=\"alert\">".lang('success_reset_password')."</div>"; 

			}
		}
		$this->output_data['msg'] = $msg;

		$this->load->view('forgot_password', $this->output_data);
	}

}

/* End of file login.php */
/* Location: ./appl/controllers/login.php */