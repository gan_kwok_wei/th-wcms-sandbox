<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource pages.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 16, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Pages extends GE_Controller {
	
	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
		if(!is_logged()) {
			redirect($this->config->item('ge_redirect_login'));
			exit;
		}
		
		$this->load->model(array('system_model', 'user_model', 'cms_model'));
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir); 

		$this->output_head = array('class' => strtolower(__CLASS__), 'module' => strtolower($module));

	}
	
	function index() {
		$this->page_list();
	}
	
	/**
	 * Enter description here ...
	 */
	function page_list() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});',
													'$(\'#form_list\').submit( function() { if(confirm("Save list?")) { return true; } else { return false; } });',
													'var asInitVals = new Array();',
													'$("thead input").keyup( function () { oTable.fnFilter( this.value, $("thead input").index(this) + 1 ); } );',
													'$("thead input").each( function (i) { asInitVals[i] = this.value; } );',
													'$("thead input").focus( function () { if( this.className == "search_init" ) { this.className = ""; this.value = ""; } } );',
													'$("thead input").blur( function (i) { if( this.value == "" ) { this.className = "search_init"; this.value = asInitVals[$("thead input").index(this)]; } } );');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['title'] = 'Custom Page Manager';
		$this->output_data['content_category_id'] = 1;
		$this->load->view('system/pages/list_page', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function content_add($ref, $content_category_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Add New ' . humanize($ref);
		$this->output_data['mode'] = 'add';
		$this->output_data['content_category_id'] = $content_category_id;
		$this->output_data['ref'] = $ref;
		switch ($ref) {
			case 'help_info_faq' :
				$view = 'system/pages/form_faq';
			break;
			case 'page_list' :
				$view = 'system/pages/form_pages';
			break;
			default :
				$view = 'system/pages/form_content';
			break;				
		}
		$this->load->view($view, $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $content_id
	 */
	function content_edit($ref, $content_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Edit ' . humanize($ref);
		$this->output_data['mode'] = 'edit';
		$this->output_data['content_id'] = $content_id;
		$this->output_data['ref'] = $ref;
		switch ($ref) {
			case 'help_info_faq' :
				$view = 'system/pages/form_faq';
			break;
			case 'page_list' :
				$view = 'system/pages/form_pages';
			break;
			default :
				$view = 'system/pages/form_content';
			break;				
		}
		$this->load->view($view, $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function corporate() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/ui.spinner.min.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js",
												base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
												
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css',
													base_url() . "assets/js/jqueryui/ui.spinner.css");
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Pages Manager';
		$this->output_data['content_id'] = 39;
		$this->load->view('system/pages/form_corporate', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function about_us_management_team() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js',
												base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['menu'] = 'about_us_menu';
		$this->output_data['sub_about'] = 'management_team';
		$this->output_data['content_category_id'] = 7;
		$this->output_data['content_id'] = 41;
		$this->load->view('system/pages/list_content', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function about_us_shareholders() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js',
												base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['menu'] = 'about_us_menu';
		$this->output_data['sub_about'] = 'shareholders';
		$this->output_data['content_category_id'] = 8;
		$this->output_data['content_id'] = 42;
		$this->load->view('system/pages/list_content', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function about_us_career() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js',
												base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['title'] = 'Pages Manager';
		$this->output_data['menu'] = 'about_us_menu';
		$this->output_data['sub_about'] = 'career';
		$this->output_data['content_category_id'] = 9;
		$this->output_data['content_id'] = 43;
		$this->load->view('system/pages/list_content', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function about_us_concept() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js',
												base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['menu'] = 'about_us_menu';
		$this->output_data['sub_about'] = 'concept';
		$this->output_data['content_category_id'] = 10;
		$this->output_data['content_id'] = 44;
		$this->load->view('system/pages/list_content', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function about_us_media() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js',
												base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['menu'] = 'about_us_menu';
		$this->output_data['sub_about'] = 'media';
		$this->output_data['content_category_id'] = 11;
		$this->output_data['content_id'] = 45;
		$this->load->view('system/pages/list_media', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function help_info_contact_us() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['sub_about'] = 'contact_us';
		$this->output_data['content_id'] = 30;
		$this->load->view('system/pages/form_contact_us', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function help_info_booking_cancelation() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['sub_about'] = 'booking_cancelation';
		$this->output_data['content_id'] = 31;
		$this->load->view('system/pages/form_booking_cancelation', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function help_info_terms_condition() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['sub_about'] = 'terms_condition';
		$this->output_data['content_id'] = 32;
		$this->load->view('system/pages/form_terms_condition', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function help_info_fee_hotel_schedule() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js',
												base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['menu'] = 'help_info_menu';
		$this->output_data['sub_about'] = 'fee_hotel_schedule';
		$this->output_data['content_category_id'] = 19;
		$this->output_data['content_id'] = 46;
		$this->load->view('system/pages/list_content', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function help_info_faq() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js',
												base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['ref'] = __FUNCTION__;
		$this->output_data['menu'] = 'help_info_menu';
		$this->output_data['sub_about'] = 'faq';
		$this->output_data['content_category_id'] = 20;
		$this->output_data['content_id'] = 47;
		$this->load->view('system/pages/list_content', $this->output_data);
		$this->load->view('system/footer');
	}
	
}

/**
 * End of file pages.php 
 * Location: ./.../.../.../pages.php 
 */