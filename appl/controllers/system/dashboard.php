<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource dashboard.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Mar 15, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Dashboard extends GE_Controller {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
		if(!is_logged()) {
			redirect($this->config->item('ge_redirect_login'));
			exit;
		}
		
		$this->load->model(array('system_model', 'user_model'));
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir); 

		$this->output_head = array('class' => strtolower(__CLASS__), 'module' => strtolower($module));

	}
	
	/**
	 * Enter description here ...
	 */
	function index() {
		$this->show_data();
	}

	/**
	 * Enter description here ...
	 */
	function show_data() {
		$this->output_head['function'] = __FUNCTION__;

		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
												
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hits_hotel'] = $this->system_model->get_hotel_hits();
		$this->output_data['hits_promo'] = $this->system_model->get_promo_hits();
		$this->output_data['hits_content'] = $this->system_model->get_content_hits();
		
		$this->output_data['outstanding_content'] = $this->system_model->get_outstanding_content();
		$this->output_data['outstanding_hotel'] = $this->system_model->get_outstanding_hotel();
		$this->output_data['outstanding_promo'] = $this->system_model->get_outstanding_promo();
		$this->output_data['outstanding_banner'] = $this->system_model->get_outstanding_banner();
		
		$this->load->view('system/dashboard', $this->output_data);
		$this->load->view('system/footer');
	}

	/**
	 * Enter description here ...
	 */
	function config() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});',
													'$(\'#form_config\').submit( function() { if(confirm("Save configuration?")) { return true; } else { return false; } });');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'System Configuration';
		$this->load->view('system/form_config', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function country() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});',
													'$(\'#form_config\').submit( function() { if(confirm("Save configuration?")) { return true; } else { return false; } });',
													'var asInitVals = new Array();',
													'$("thead input").keyup( function () { oTable.fnFilter( this.value, $("thead input").index(this) + 1 ); } );',
													'$("thead input").each( function (i) { asInitVals[i] = this.value; } );',
													'$("thead input").focus( function () { if( this.className == "search_init" ) { this.className = ""; this.value = ""; } } );',
													'$("thead input").blur( function (i) { if( this.value == "" ) { this.className = "search_init"; this.value = asInitVals[$("thead input").index(this)]; } } );');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Country Configuration';
		$this->load->view('system/list_country', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function country_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Add Country';
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_country', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 */
	function country_edit($country_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Edit Country';
		$this->output_data['country_id'] = $country_id;
		$this->output_data['mode'] = 'edit';
		$this->load->view('system/form_country', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function country_add_language($country_id, $language_id, $default = 0) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->load->model('system_model');
		$this->system_model->country_add_language($country_id, $language_id, $default);
		
		set_success_message(sprintf(lang('success_add'), 'role permission'));
		redirect("system/dashboard/country_edit/$country_id");
		exit;
				
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function country_remove_language($country_id, $language_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->load->model('system_model');
		$this->system_model->country_remove_language($country_id, $language_id);
		
		set_success_message(sprintf(lang('success_add'), 'role permission'));
		redirect("system/dashboard/country_edit/$country_id");
		exit;
				
	}
	
	/**
	 * Enter description here ...
	 */
	function meta_tags() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});',
													'$(\'#form_tags\').submit( function() { if(confirm("Save tags?")) { return true; } else { return false; } });');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Meta Tags';
		$this->load->view('system/form_tags', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function menu() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});',
													'$(\'#form_list_menu\').submit( function() { if(confirm("Save configuration?")) { return true; } else { return false; } });',
													'var asInitVals = new Array();',
													'$("thead input").keyup( function () { oTable.fnFilter( this.value, $("thead input").index(this) + 1 ); } );',
													'$("thead input").each( function (i) { asInitVals[i] = this.value; } );',
													'$("thead input").focus( function () { if( this.className == "search_init" ) { this.className = ""; this.value = ""; } } );',
													'$("thead input").blur( function (i) { if( this.value == "" ) { this.className = "search_init"; this.value = asInitVals[$("thead input").index(this)]; } } );');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Menu Manager';
		$this->load->view('system/list_menu', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function menu_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(
			base_url() . "assets/js/ckeditor/ckeditor.js",
			base_url() . "assets/js/ckfinder/ckfinder.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_add_menu');
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_menu', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $menu_id
	 */
	function menu_edit($menu_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(
			base_url() . "assets/js/ckeditor/ckeditor.js",
			base_url() . "assets/js/ckfinder/ckfinder.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_edit_menu');
		$this->output_data['menu_id'] = $menu_id;
		$this->output_data['mode'] = 'edit';
		$this->load->view('system/form_menu', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function menu_category() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});',
													'$(\'#form_list_menu\').submit( function() { if(confirm("Save configuration?")) { return true; } else { return false; } });',
													'var asInitVals = new Array();',
													'$("thead input").keyup( function () { oTable.fnFilter( this.value, $("thead input").index(this) + 1 ); } );',
													'$("thead input").each( function (i) { asInitVals[i] = this.value; } );',
													'$("thead input").focus( function () { if( this.className == "search_init" ) { this.className = ""; this.value = ""; } } );',
													'$("thead input").blur( function (i) { if( this.value == "" ) { this.className = "search_init"; this.value = asInitVals[$("thead input").index(this)]; } } );');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Category Manager';
		$this->output_data['table'] = 'menu';
		$this->load->view('system/list_menu_category', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function menu_category_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
	
		$this->output_head['js_extras'] = array(
			base_url() . "assets/js/ckeditor/ckeditor.js",
			base_url() . "assets/js/ckfinder/ckfinder.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_add_category');
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_menu_category', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function menu_category_edit($menu_category_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_edit_category');
		$this->output_data['mode'] = 'edit';
		$this->output_data['menu_category_id'] = $menu_category_id;
		$this->load->view('system/form_menu_category', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Function to handle request from javascript ajax
	 * do nothing 
	 */
	function ajax_handler() {
		
	}
	
}
/**
 * End of file dashboard.php 
 * Location: ./.../.../.../dashboard.php 
 */