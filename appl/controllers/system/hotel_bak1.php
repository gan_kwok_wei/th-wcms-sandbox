<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource hotel.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 8, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Hotel extends GE_Controller {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
		if(!is_logged()) {
			redirect($this->config->item('ge_redirect_login'));
			exit;
		}
		
		$this->load->model(array('system_model', 'user_model', 'hotel_model'));
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir); 

		$this->output_head = array('class' => strtolower(__CLASS__), 'module' => strtolower($module));

	}
	
	function index() {
		$this->show_list();
	}

	/**
	 * Enter description here ...
	 */
	function show_list() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Hotels Manager';
		$this->load->view('system/list_hotel', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function hotel_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/ui.spinner.min.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js",
												base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
												
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css',
													base_url() . "assets/js/jqueryui/ui.spinner.css");
													
//		$this->output_head['js_function'] = array('featureTable = $(\'#feat_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->load->view('system/form_hotel_add', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $content_id
	 */
	function hotel_edit($hotel_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/ui.spinner.min.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js",
												base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
												
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css',
													base_url() . "assets/js/jqueryui/ui.spinner.css");
													
//		$this->output_head['js_function'] = array('featureTable = $(\'#feat_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hotel_id'] = $hotel_id;
		$this->load->view('system/form_hotel_edit', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function hotel_room_edit($hotel_id) {
		if(!$this->has_access('hotel_edit', __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
											
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hotel_id'] = $hotel_id;
		$this->load->view('system/form_hotel_room_edit', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function hotel_gallery_edit($hotel_id) {
		if(!$this->has_access('hotel_edit', __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckFinder');
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckfinder/ckfinder.js");

		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hotel_id'] = $hotel_id;
		$this->load->view('system/form_hotel_gallery_edit', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function hotel_feature_edit($hotel_id) {
		if(!$this->has_access('hotel_edit', __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
												
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
													
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hotel_id'] = $hotel_id;
		$this->load->view('system/form_hotel_feature_edit', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function hotel_package_edit($hotel_id) {
		if(!$this->has_access('hotel_edit', __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
									
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hotel_id'] = $hotel_id;
		$this->load->view('system/form_hotel_package', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function hotel_package_detail_add($hotel_id) {
		if(!$this->has_access('hotel_edit', __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/ui.spinner.min.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js",
												base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css',
													base_url() . "assets/js/jqueryui/ui.spinner.css");
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hotel_id'] = $hotel_id;
		$this->output_data['hotel_package_id'] = 0;
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_hotel_package_detail', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function hotel_package_detail_edit($hotel_id, $hotel_package_id) {
		if(!$this->has_access('hotel_edit', __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
									
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
		
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/ui.spinner.min.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js",
												base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css',
													base_url() . "assets/js/jqueryui/ui.spinner.css");
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['hotel_id'] = $hotel_id;
		$this->output_data['hotel_package_id'] = $hotel_package_id;
		$this->output_data['mode'] = 'edit';
		$this->load->view('system/form_hotel_package_detail', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function feature_ref_list() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Features Manager';
		$this->load->view('system/list_feature_ref', $this->output_data);
		$this->load->view('system/footer');
	}

	/**
	 * Enter description here ...
	 */
	function feature_ref_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Add New Feature';
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_feature_ref', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $content_id
	 */
	function feature_ref_edit($feature_ref_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Edit Feature';
		$this->output_data['mode'] = 'edit';
		$this->output_data['feature_ref_id'] = $feature_ref_id;
		$this->load->view('system/form_feature_ref', $this->output_data);
		$this->load->view('system/footer');
	}
	
}

/**
 * End of file city.php 
 * Location: ./.../.../.../city.php 
 */