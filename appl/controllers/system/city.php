<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource city.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 8, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class City extends GE_Controller {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
		if(!is_logged()) {
			redirect($this->config->item('ge_redirect_login'));
			exit;
		}
		
		$this->load->model(array('system_model', 'user_model', 'city_model'));
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir); 

		$this->output_head = array('class' => strtolower(__CLASS__), 'module' => strtolower($module));

	}
	
	function index() {
			$this->show_list();
	}

	/**
	 * Enter description here ...
	 */
	function show_list() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});',
													'var asInitVals = new Array();',
													'$("thead input").keyup( function () { oTable.fnFilter( this.value, $("thead input").index(this) + 1 ); } );',
													'$("thead input").each( function (i) { asInitVals[i] = this.value; } );',
													'$("thead input").focus( function () { if( this.className == "search_init" ) { this.className = ""; this.value = ""; } } );',
													'$("thead input").blur( function (i) { if( this.value == "" ) { this.className = "search_init"; this.value = asInitVals[$("thead input").index(this)]; } } );');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Cities Manager';
		$this->load->view('system/list_city', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function city_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Add New City';
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_city', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $content_id
	 */
	function city_edit($city_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Edit City';
		$this->output_data['mode'] = 'edit';
		$this->output_data['city_id'] = $city_id;
		$this->load->view('system/form_city', $this->output_data);
		$this->load->view('system/footer');
	}
	
}

/**
 * End of file city.php 
 * Location: ./.../.../.../city.php 
 */