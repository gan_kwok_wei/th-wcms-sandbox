<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource user.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Mar 15, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class User extends GE_Controller {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
		if(!is_logged()) {
			redirect($this->config->item('ge_redirect_login'));
			exit;
		}
		$this->load->model('system_model');
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir); 

		$this->output_head = array('class' => strtolower(__CLASS__), 'module' => strtolower($module));

	}
	
	/**
	 * Enter description here ...
	 */
	function index() {
		$this->show_list();
	}

	/**
	 * Enter description here ...
	 */
	function show_list() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->output_head['js_function'] = array('oTable = $(\'#data_table\').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'User Manager';
		$this->load->view('system/list_user', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function user_add($role = NULL, $view = NULL) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		if($role == NULL) {
			$this->output_head['function'] = __FUNCTION__;
			$this->load->model('user_model');
			
			$this->load->view('system/header', $this->output_head);
			$this->load->view('system/menu');
			
			$this->output_data['title'] = lang('title_add_user');
			$this->output_data['mode'] = 'add';
			$this->load->view('system/form_user', $this->output_data);
			$this->load->view('system/footer');
		} else {
			
		}
	}
	
	/**
	 * Enter description here ...
	 * @param int $user_id
	 */
	function user_edit($user_id, $view = NULL) {
/*		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
*/		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_edit_user');
		$this->output_data['user_id'] = $user_id;
		$this->output_data['mode'] = 'edit';
		$this->load->view('system/form_user', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * @return Page Redirect
	 */
	function account() {
		$this->user_edit(get_user_id());
	}
	
	/**
	 * Enter description here ...
	 * @param int $user_id
	 */
	function user_delete($user_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output->set_header($this->config->item('ajax_header'));
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_data['module'])) {
			set_error_message(lang('alert_no_permission'));
			$this->user_edit($user_id);
			return;
		} elseif($user_id == get_user_id()){
			set_error_message(lang('alert_del_own_account'));
			$this->user_edit($user_id);
			return;
		} else { 
			$this->user_model->delete_user($user_id);
			set_success_message(sprintf(lang('success_delete'), $user_id));
			$this->show_list();
		}
	}
	
	/**
	 * @return Page
	 */
	function change_password() {
/*		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
*/		
		$this->output_head['function'] = __FUNCTION__;
    	
		/* @var unknown_type */
		$this->output_data['user_id'] = get_user_id();
		
		$this->load->helper('form');
		/* @var JavaScript Functions */
		$this->output_head['js_func'] = array();
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		/* @title unknown_type */
		$this->output_data['title'] = lang('label_change_password') . ' ' . get_user_data('name');
		
		
		/* @action form action */
		$this->load->view('system/password_form', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * AJAX process
	 * @param int $user_id
	 */
	function reset_password($user_id = NULL) {
		$this->output_head['function'] = __FUNCTION__;
    	
//		$user_id = $_POST['user_id'];
		$this->output->set_header($this->config->item('ajax_header'));
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			$this->output->set_output(lang('alert_no_permission'));
			return;
		}
		
		$this->system_model->reset_password($user_id);
		$this->output->set_output(sprintf(lang('success_reset_password'), $user_id));
		
	}
	
	/**
	 * @param $str
	 * @return Callback Process
	 */
	function old_password_check($str) {
		
		$user_data = $this->user_model->get_user(get_user_id())->row();
		echo "<!--" . $str  . " = " . $user_data->password;
		if ($str != $user_data->password) {
			echo " failed -->";
			$this->form_validation->set_message('old_password_check', 'the %s not match');
			return false;
		} else {
			echo " ok -->";
			return true;
		}
	}
	
}
/**
 * End of file user.php 
 * Location: ./.../.../.../user.php 
 */