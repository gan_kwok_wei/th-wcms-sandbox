<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource security.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Mar 15, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Security extends GE_Controller {

	function __construct() {
		parent::__construct();
		if(!is_logged()) {
			redirect($this->config->item('ge_redirect_login'));
			exit;
		}
		$this->load->model('system_model');
		$this->load->helper(array('form'));
		$this->load->library(array('form_validation'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir); 

		$this->output_head = array('class' => strtolower(__CLASS__), 'module' => strtolower($module));

	}
	
	function index() {
		$this->security_config();
	}
	
	function security_config() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = 'Role & Security Manager';
		$this->load->view('system/security_panel', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 */
	function role_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_add_role');
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_role', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 */
	function role_edit($role_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->output_head['style_extras'] = array(base_url() . 'assets/js/datatables/media/css/demo_table.css',
													base_url() . 'assets/js/datatables/media/css/demo_table_jui.css',
													base_url() . 'assets/js/datatables/media/css/demo_page.css');
		$this->output_head['js_extras'] = array(base_url() . 'assets/js/datatables/media/js/jquery.dataTables.min.js');
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_edit_role');
		$this->output_data['role_id'] = $role_id;
		$this->output_data['mode'] = 'edit';
		$this->load->view('system/form_role', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function role_add_permission($role_id, $permission_id, $allow = 1) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		
		$this->load->model('user_model');
		$this->user_model->role_add_permission($role_id, $permission_id, $allow);
		
		set_success_message(sprintf(lang('success_add'), 'role permission'));
		redirect("system/security/role_edit/$role_id");
		exit;
				
	}
	
	/**
	 * Enter description here ...
	 */
	function permission_add() {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_add_permission');
		$this->output_data['mode'] = 'add';
		$this->load->view('system/form_permission', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $permission_id
	 */
	function permission_edit($permission_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		$this->output_head['function'] = __FUNCTION__;
		$this->load->model('user_model');
		
		$this->load->view('system/header', $this->output_head);
		$this->load->view('system/menu');
		
		$this->output_data['title'] = lang('title_edit_permission');
		$this->output_data['permission_id'] = $permission_id;
		$this->output_data['mode'] = 'edit';
		$this->load->view('system/form_permission', $this->output_data);
		$this->load->view('system/footer');
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 */
	function role_permission($role_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		
	}
	
	/**
	 * Enter description here ...
	 * @param int $user_id
	 */
	function user_permission($user_id) {
		if(!$this->has_access(__FUNCTION__, __CLASS__, $this->output_head['module'])) {
			set_warning_message(lang('alert_no_permission'));
			redirect($this->config->item('ge_redirect_default'));
			exit;
		}
		
		
	}
	
}
/**
 * End of file security.php 
 * Location: ./.../.../.../security.php 
 */