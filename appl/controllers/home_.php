<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends GE_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	function __construct() {
		parent::__construct();
		$this->load->model(array('system_model', 'cms_model', 'user_model'));
		
		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir);

		$this->output_head = array('class' => strtolower(__CLASS__), 'module' => strtolower($module));
		
	}
	
	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function index($mode = 'show') {
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);

		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);
		
		$this->output_data['mode'] = $mode;
		$this->load->view('home', $this->output_data);
		$this->load->view('footer');

	}
	
	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function intro($mode = 'show') {
		$this->output_data['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_data['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_data['title'] = $title;
		$this->output_data['mode'] = $mode;
		$this->load->view('intro', $this->output_data);
		$this->load->view('footer');
	}
	
	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function contact($mode = 'show') {
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);
	
		$this->output_head['js_extras'] = array(base_url() . "assets/js/ckeditor/ckeditor.js",
												base_url() . "assets/js/ckfinder/ckfinder.js",
												base_url() . "assets/js/jqueryui/jquery.validate.js"
		);
		$this->load->view('header', $this->output_head);
		
		$this->output_data['mode'] = $mode;
		$this->load->view('contact', $this->output_data);
		$this->load->view('footer');

	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */