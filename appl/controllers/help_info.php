<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help_info extends GE_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->model(array('system_model', 'cms_model', 'user_model', 'pages_model'));

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir);

		$this->output_head['class'] = strtolower(__CLASS__);
		$this->output_head['module'] = strtolower($module);
		$this->output_head['style_extras'] = array($this->theme_dir . 'css/reset.css',
													$this->theme_dir . 'css/~global.css',
													$this->theme_dir . 'css/page_hotel.css',
													$this->theme_dir . 'css/corporate.css',
													$this->theme_dir . 'css/about.css',
													$this->theme_dir . 'css/help_info.css',
													$this->theme_dir . 'css/promotion.css',
													$this->theme_dir . 'css/menu_nav.css',
													$this->theme_dir . 'css/default.css',
													$this->theme_dir . 'css/jquery-ui-1.8.17.custom.css',
													$this->theme_dir . 'css/font.css',
													$this->theme_dir . 'css/jquery.selectbox.css',
													$this->theme_dir . 'css/flight-calendar.css',
													$this->theme_dir . 'css/uni_form.css');
		$this->output_head['js_extras'] = array(base_url('assets/js') . 'jquery.tools.min.js',
														base_url('assets/js') . 'jquery.ddslick.js',
														base_url('assets/js') . 'jquery.selectbox-0.2.js',
														base_url('assets/js') . 'jquery.uniform.js',
														base_url('assets/js') . 'jquery.dateFormat-1.0.js',
														'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js');

	}

	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function index(){
		$this->contact_us();
	}

	function contact_us() {
		$this->output_head['function'] = __FUNCTION__;

		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['sub_about'] = "contact_us";
		$this->load->view('help_info', $this->output_data);
		$this->load->view('footer');

	}

	function booking_cancelation() {
		$this->output_head['function'] = __FUNCTION__;
		
		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['sub_about'] = "booking_cancelation";
		$this->load->view('help_info', $this->output_data);
		$this->load->view('footer');

	}

	function terms_condition() {
		$this->output_head['function'] = __FUNCTION__;
		
		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['sub_about'] = "terms_condition";
		$this->load->view('help_info', $this->output_data);
		$this->load->view('footer');

	}

	function fee_hotel_schedule() {
		$this->output_head['function'] = __FUNCTION__;
		
		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['sub_about'] = "fee_hotel_schedule";
		$this->load->view('help_info', $this->output_data);
		$this->load->view('footer');

	}

	function faq() {
		$this->output_head['function'] = __FUNCTION__;
		
		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['sub_about'] = "faq";
		$this->load->view('help_info', $this->output_data);
		$this->load->view('footer');
	}

	/**
	 * Enter description here ...
	 * @param string menu_id
	 */
	function page($menu_id) {
		$this->output_head['function'] = __FUNCTION__;
		
		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['menu_id'] = $menu_id;
		$this->load->view('pages', $this->output_data);
		$this->load->view('footer');

	}

	/**
	 * Enter description here ...
	 * @param string menu_id
	 */
	function news_sign_up($country_id = NULL){
		$title = current($this->system_model->get_system_config('home_title'));
		//$this->output_head['title'] = $title;
		//$this->load->view('header', $this->output_head);

		$this->output_data['country_id'] = $country_id;
		$this->load->view('news_sign_up', $this->output_data);
		//$this->load->view('footer');
	}
	
	/**
	 * Enter description here ...
	 * @param string menu_id
	 */
	function frame_sign_up(){
		$title = current($this->system_model->get_system_config('home_title'));
		$this->load->view('frame_sign_up', $this->output_data);
	}
	
	/**
	 * Enter description here ...
	 * @param string menu_id
	 */
	function news_sign_up_response($response = 'ok'){
		if($response = 'ok') {
			$this->output->set_output('<center>' . lang('e_news_success') . '</center>');
		} else {
			$this->output->set_output('<center>' . lang('e_news_failed') . '</center>');
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */