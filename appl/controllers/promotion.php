<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promotion extends GE_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();

		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir);

		$this->output_head['class'] = strtolower(__CLASS__);
		$this->output_head['module'] = strtolower($module);
		$this->output_head['style_extras'] = array($this->theme_dir . 'css/reset.css',
													//$this->theme_dir . 'css/page_hotel.css',
													$this->theme_dir . 'css/default.css',
													$this->theme_dir . 'css/jquery-ui-1.8.17.custom.css',
													//$this->theme_dir . 'css/nivo/themes/default/default.css',
													//$this->theme_dir . 'css/nivo/themes/light/light.css',
													//$this->theme_dir . 'css/nivo/nivo-slider.css',
													//$this->theme_dir . 'css/jquery.selectbox.css',
													//$this->theme_dir . 'css/flight-calendar.css',
													//$this->theme_dir . 'css/uni_form.css',
													$this->theme_dir . 'css/menu_nav.css',
													$this->theme_dir . 'css/~global.css',
													$this->theme_dir . 'css/promotion.css');

		$this->output_head['js_extras'] = array(base_url('assets/js') . 'jquery.tools.min.js',
												//('assets/extend/fws/js') . 'fwslider.js',
												//base_url('assets/js') . 'jquery.nivo.slider.js',
												//base_url('assets/js') . 'jquery.ddslick.js',
												//base_url('assets/js') . 'jquery.selectbox-0.2.js',
												//base_url('assets/js') . 'jquery.uniform.js',
												//base_url('assets/js') . 'jquery.dateFormat-1.0.js',
												'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
												'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js');

	}

	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function index() {
		$this->promo_hotel();
	}

	function promo_hotel($country_id = NULL, $country_name = NULL, $city_id = NULL, $city_name = NULL){
		$this->output_head['function'] = __FUNCTION__;
		
		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['country_id'] = $country_id;
		$this->output_data['country_name'] = $country_name;
		$this->output_data['city_id'] = $city_id;
		$this->output_data['city_name'] = $city_name;

		$this->load->view('promotion', $this->output_data);
		$this->load->view('footer');
	}

   	/**
	 * Enter description here ...
	 * @param string menu_id
	 */
	function page($menu_id) {
		$this->output_head['function'] = __FUNCTION__;

		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);

		$this->output_data['menu_id'] = $menu_id;
		$this->load->view('pages', $this->output_data);
		$this->load->view('footer');

	}

	/**
	 * Enter description here ...
	 * @param unknown_type $promo_name
	 */
	function deals($promo_name) {
		$result = $this->pages_model->get_promo_by_name($promo_name);
		if($result->num_rows() == 0) {
			$this->promo_hotel();
		} else {
			$promo = $result->row();
			$this->output_head['function'] = __FUNCTION__;
		
			$title = current($this->system_model->get_system_config('home_title'));
			$this->output_head['title'] = $title;
			$this->load->view('header', $this->output_head);
	
			$this->output_data['promo_id'] = $promo->promo_id;
	
			$this->load->view('deals', $this->output_data);
			$this->load->view('footer');
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */