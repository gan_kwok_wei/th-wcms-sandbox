<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends GE_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	function __construct() {
		parent::__construct();
		$this->load->model(array('system_model', 'cms_model', 'user_model', 'pages_model'));
		
		$dir = explode('/', str_replace('\\', '/', __DIR__));
		$module = end($dir);

		$this->output_head = array('class' => strtolower(__CLASS__), 
									'module' => strtolower($module),
									'style_extras' => array($this->theme_dir . 'css/reset.css',
															$this->theme_dir . 'css/page_hotel.css',
															$this->theme_dir . 'css/corporate.css',
															$this->theme_dir . 'css/about.css',
															$this->theme_dir . 'css/promotion.css',
															$this->theme_dir . 'css/style.css',
															$this->theme_dir . 'css/jquery-ui-1.8.17.custom.css',
															$this->theme_dir . 'css/font.css',
															$this->theme_dir . 'css/jquery.selectbox.css',
															$this->theme_dir . 'css/flight-calendar.css',
															$this->theme_dir . 'css/uni_form.css'),
									'js_extras' => array(base_url('assets/js/jquery.tools.min.js'),
														base_url('assets/js/jquery.ddslick.js'),
														base_url('assets/js/jquery.selectbox-0.2.js'),
														base_url('assets/js/jquery.uniform.js'),
														base_url('assets/js/jquery.dateFormat-1.0.js'),
														'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js')
							);
		
	}
	
	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function index() {
		redirect('home/', 'refresh');
	}
	
	/**
	 * Enter description here ...
	 * @param string $mode
	 */
	function page($menu_id) {
		$this->output_head['function'] = __FUNCTION__;
		$this->load->library('ckeditor');
		$this->load->library('ckFinder');
		$this->ckfinder->SetupCKEditor($this->ckeditor);

		$title = current($this->system_model->get_system_config('home_title'));
		$this->output_head['title'] = $title;
		$this->load->view('header', $this->output_head);
		
		$this->output_data['menu_id'] = $menu_id;
		$this->load->view('pages', $this->output_data);
		$this->load->view('footer');

	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */