public function star()
    {
        $rate = $this->input->post("rate_val", true);
        $video_url = $this->input->post("video", true);
        $this->load->model("Model Name");
        $video_id = $this->model_name->get_video_id($video_url);
        unset($this->layout); //Block template Layout
        if (get_user_id())   //get_user_id() return login user id
        {
            if (!$this->model_name->is_video_rated(get_user_id(), $video_id))
            {
                $data = array("video_id" => $video_id,
                    "user_id" => get_user_id(),
                    "videos_rating_value" => $rate,
                    "videos_rating_date" => "" . date("Y-m-d H:i:s") . ""
                    );
                if ($this->model_name->insert_rating($data, $video_id, $rate))
                {
                    echo json_encode(array("code" => "Success", "msg" => "Your Video Rating has been posted"));
                }
                else
                {
                    echo json_encode(array("code" => "Error", "msg" => "There was a problem rating your video"));
                }
            }
            else
            {
                echo json_encode(array("code" => "Error", "msg" => "You have already rated this video"));
            }
        }
        else
        {
            echo json_encode(array("code" => "Error", "msg" => "You have to login to rate the video"));
        }
        exit(0);
    }