<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/********************************************************************/
/* File Name		: ./appl/config/grease.php						*/
/* Module			: 												*/
/* Revise Number	: 01											*/
/* Created By		: Budi Laxono									*/
/* Created At		: 												*/
/********************************************************************/
/* Modified By		: Unknown										*/
/* Modified At		: Unknown										*/
/* Modification		: Unknown										*/
/********************************************************************/

/*
|--------------------------------------------------------------------------
| 
|--------------------------------------------------------------------------
|
| 
*/
$config['site_logo'] = '';

$config['site_title'] = 'Welcome to Tune Hotels, Best Value Hotels. Great Value Great Savings';

$config['site_name'] = 'Tune Hotels';

$config['company_name'] = 'Tune Hotels';

$config['company_address'] = '';

$config['company_phone'] = '';

$config['company_fax'] = '';

$config['ge_footer_text'] = '&copy; 2012 Tune Hotels';

$config['ge_home_url'] = 'home';

$config['ge_admin_url'] = 'system/dashboard';


/*
|--------------------------------------------------------------------------
| 
|--------------------------------------------------------------------------
|
| 
*/
$config['json_header'] = array(
"Expires: Wed, 12 Sep 1979 07:00:00 GMT",
"Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT",
"Cache-Control: no-cache, must-revalidate",
"Pragma: no-cache",
"Content-type: text/x-json");


/*
|--------------------------------------------------------------------------
| 
|--------------------------------------------------------------------------
|
| 
*/
$config['ajax_header'] = array(
"Expires: Wed, 12 Sep 1979 07:00:00 GMT",
"Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT",
"Cache-Control: no-cache, must-revalidate",
"Pragma: no-cache",
"Content-type: text/plain");

/*
|--------------------------------------------------------------------------
| 
|--------------------------------------------------------------------------
|
| 
*/
$config['ge_error_msg'] = NULL;
$config['ge_warning_msg'] = NULL;
$config['ge_success_msg'] = NULL;

/*
|--------------------------------------------------------------------------
| 
|--------------------------------------------------------------------------
|
| default
*/
$config['ge_def_password'] = 'password';

$config['ge_max_admin_role'] = 5;

$config['ge_upload_path'] = 'assets/media/';

/*
|--------------------------------------------------------------------------
| 
|--------------------------------------------------------------------------
|
| redirect
*/
$config['ge_redirect_login'] = 'login/authenticate';

$config['ge_redirect_default'] = 'home';

/*
|--------------------------------------------------------------------------
| 
|--------------------------------------------------------------------------
|
| options
*/
$config['ge_page'] = array(
						0 => 'Home',
						1 => 'Hotels',
						2 => 'Hotel Detail',
						3 => 'Promotions',
						4 => 'Corporate',
						5 => 'About Us',
						6 => 'Help & Info'
						);
						
$config['ge_tag_tables'] = array('content', 'content_category');

$config['ge_language'] = array(
						'en' => 'english', 
						'id' => 'indonesia', 
						'th' => 'thailand', 
						'ch' => 'chinese');
$config['ge_def_language'] = 'en';

$config['ge_country'] = array(
						'gb' => 'united kingdom', 
						'id' => 'indonesia', 
						'my' => 'malaysia', 
						'ph' => 'philippines', 
						'th' => 'thailand');
$config['ge_def_country'] = array('my');

$config['ge_im_type'] = array(
						'' => '---', 
						'Yahoo' => 'Yahoo', 
						'Skype' => 'Skype', 
						'MSN' => 'MSN', 
						'AOL' => 'AOL');

$config['ge_currency'] = array(
						'IDR' => array('name' => 'Rupiah',
										'code' => 'Rp.',
										'country' => 'Indonesia'), 
						'USD' => array('name' => 'Dollar',
										'code' => '$.',
										'country' => 'United State')); 

$config['ge_def_currency'] = 'IDR';
