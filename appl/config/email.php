<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource email.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Feb 7, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

$config['protocol'] = 'mail';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'iso-8859-1';
$config['wordwrap'] = TRUE;

$config['smtp_host'] = 'ssl://smtp.googlemail.com';
$config['smtp_port'] = 465;
$config['smtp_user'] = 'budi.laksono@greenlabgroup.com';
$config['smtp_pass'] = '';

$config['mailtype'] = "html";
$config['newline'] = "\r\n";

/**
 * End of file email.php 
 * Location: ./.../.../.../email.php 
 */