<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ip2location_lite {
	protected $errors = array();
	protected $service = 'api.ipinfodb.com';
	protected $version = 'v3';
	protected $apiKey = '1b20c8a76190f20fc9a202e54a3d158889bd8dd7b1f4ac63982ca18e279e7b2b';
	public function __construct(){		ini_set('allow_url_fopen', '1');	}	public function __destruct(){}
	public function setKey($key){		if(!empty($key)) $this->apiKey = $key;	}
	public function getError(){		return implode("\n", $this->errors);	}
	public function getCountry($host){		return $this->getResult($host, 'ip-country');	}
	public function getCity($host){		return $this->getResult($host, 'ip-city');	}
	private function getResult($host, $name){		$ip = @gethostbyname($host);		if(preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $ip)){			$srv = 'http://' . $this->service . '/' . $this->version . '/' . $name . '/?key=' . $this->apiKey . '&ip=' . $ip . '&format=xml';			$xml = file_get_contents($srv);			try{				$response = @new SimpleXMLElement($xml);				foreach($response as $field=>$value){					$result[(string)$field] = (string)$value;				}				return $result;			}			catch(Exception $e){				$this->errors[] = $e->getMessage();				return;			}		} 		$this->errors[] = '"' . $host . '" is not a valid IP address or hostname.';		return;	}}

?>