<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/********************************************************************/
/* File Name		: ./appl/libraries/aims_Controller.php			*/
/* Module			: 												*/
/* Revise Number	: 01											*/
/* Created By		: Budi Laxono									*/
/* Created At		: 13-06-2010									*/
/********************************************************************/
/* Modified By		: Unknown										*/
/* Modified At		: Unknown										*/
/* Modification		: Unknown										*/
/********************************************************************/

class GE_Controller extends CI_Controller {
	
	var $output_head;
	var $output_data;
	var $theme_dir;
	
	/**
	 * Enter description here ...
	 */
	function __construct() {
		
		parent::__construct();
		if (!defined('__DIR__')) { define('__DIR__', ''); }
		
		$this->load->model('system_model');
		
		$countries = $this->system_model->get_registered_country();
		$languages = $this->system_model->get_registered_lang();
		$country = $this->system_model->_init_country();
		$language = $this->system_model->_init_language();
		
//		var_dump($_SESSION);
//		var_dump($languages);
		
		$this->load->model(array('cms_model', 'user_model', 'pages_model'));
		
		$this->lang->load('pages', $languages[$language]);
		$this->lang->load('ge', $languages[$language]);
//		$this->lang->load('date', $languages[$language]);
		$this->lang->load('form_validation', $languages[$language]);
		
		$country_list = array();
//		$list = $this->system_model->get_country_list();
		$list = $this->pages_model->get_country();
		foreach($list->result() as $row) {
			$language_list = array();
			$list_lang = $this->system_model->get_country_language($row->country_id);
			foreach($list_lang->result() as $row_lang) {
				$language_list[$row_lang->key] = $row_lang->val;
			}
			
			$hotel_list = array();
			$list_hotel = $this->pages_model->get_hotel_by_country($row->country_id);
			foreach($list_hotel->result() as $row_hotel) {
				$title = (($row_hotel->text_title) ? $row_hotel->text_title : $row_hotel->def_title);
				$hotel_list[] = array('id' => $row_hotel->hotel_id, 'name' => $row_hotel->hotel_name, 'title' => $title);
			}
			
			$name = (($row->text_title) ? $row->text_title : $row->def_title);
			$country_list[] = array('country_id' => $row->country_id, 'letter_code' => $row->letter_code, 'name' => $name, 'letter_code' => $row->letter_code, 'language_list' => $language_list, 'hotel_list' => $hotel_list);
		}
		
		$menu_list = array();
		$sub_menus = array();
		$list = $this->pages_model->get_menu_list();
		foreach($list->result() as $row){
			$title = (($row->text_title) ? $row->text_title : $row->def_title);
			$sub_list = $this->pages_model->get_menu_list($row->menu_id);
			if(($sub_list->num_rows() > 0) || ($row->menu_name == 'hotel')) {
				$menu_list[] = array('id' => $row->menu_id, 'name' => $row->menu_name, 'title' => $title, 'link' => '#', 'icon_class' => 'i_child');
				$sub_menus[$row->menu_name] = $sub_list->result();
			} else {
				$menu_list[] = array('id' => $row->menu_id, 'name' => $row->menu_name, 'title' => $title, 'link' => $row->link, 'icon_class' => '');
			}
		}
		
		$this->output_head['country_list'] = $country_list;
		$this->output_head['menu_list'] = $menu_list;
		$this->output_head['sub_menus'] = $sub_menus;
		$this->output_head['country'] = $countries[$country];
		$this->output_head['language'] = $languages[$language];
		
		$this->output_data = array('data_status' => array('disable', 'enable'));
		
		if(isset($_POST['action'])) {
			list($model, $action) = explode('.', $_POST['action']);
			unset($_POST['action']);
			$this->load->model($model);
			$this->$model->$action();
		}
		
		$this->theme_dir = base_url('assets/themes/publish/' . $this->system_model->get_publish_theme() . '/');
		
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $function
	 * @param unknown_type $class
	 * @param unknown_type $module
	 * @return number
	 */
	function get_permission_id($function = '*', $class = '*', $module = '*') {
		
		$function = strtolower($function);
		$class = strtolower($class);
		$module = strtolower($module) == 'controllers' ? '*' : strtolower($module);
		
		$this->db->select('permission_id')->from('security_permission')->where(array('module' => $module, 'class' => $class, 'function' => $function));
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			$row = $result->row();
			return $row->permission_id;
		} else {
			return 0;
		}
	}
	
	function has_access($function = '*', $class = '*', $module = '*') {
		return has_permission($this->get_permission_id($function, $class, $module));
		
	}
	
	/**
	 * @param $data
	 * @return unknown_type
	 */
	function before_insert_process($data){
		$data->add_field("created_id", get_user_id());	
	} 
	
	/**
	 * @param $data
	 * @return unknown_type
	 */
	function before_update_process($data){
		$data->add_field("modified_id", get_user_id());
		$data->add_field("modified_time", date('Ymd H:i:s'));
	} 
	
	function valid_from_check($date) {
//		echo $date;
		if(!$date) {
			$this->form_validation->set_message('valid_from_check', 'The first date can not be empty');
			return FALSE;
		}
		list($j, $n, $Y) = explode('-', $date);
		$today = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
		$valid_from = mktime(0, 0, 0, (int) $n, (int) $j, (int) $Y);
		if($today >= $valid_from) {
			$this->form_validation->set_message('valid_from_check', 'The first date can not be set today or day before');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
}

/* End of file aims_controller.php */
/* Location: ./appl/libraries/aims_controller.php */