<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/********************************************************************/
/* File Name		: ./appl/libraries/aims_Controller.php			*/
/* Module			: 												*/
/* Revise Number	: 01											*/
/* Created By		: Budi Laxono									*/
/* Created At		: 13-06-2010									*/
/********************************************************************/
/* Modified By		: Unknown										*/
/* Modified At		: Unknown										*/
/* Modification		: Unknown										*/
/********************************************************************/

class GE_Sys_Controller extends CI_Controller {
	
	var $output_head;
	var $output_data;
	var $theme_dir;
	
	/**
	 * Enter description here ...
	 */
	function __construct() {
		
		parent::__construct();
		$this->load->model('system_model');
		$this->output_data = array('data_status' => array('disable', 'enable'));
		
//		var_dump($_SESSION);
//		var_dump($languages);
		
		$this->lang->load('ge', 'english');
		$this->lang->load('form_validation', 'english');
		
		if(isset($_POST['action'])) {
			list($model, $action) = explode('.', $_POST['action']);
			unset($_POST['action']);
			$this->load->model($model);
			$this->$model->$action();
		}
		
		$this->theme_dir = base_url('assets/themes/publish/' . $this->system_model->get_publish_theme() . '/');
		
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $function
	 * @param unknown_type $class
	 * @param unknown_type $module
	 * @return number
	 */
	function get_permission_id($function = '*', $class = '*', $module = '*') {
		
		$function = strtolower($function);
		$class = strtolower($class);
		$module = strtolower($module) == 'controllers' ? '*' : strtolower($module);
		
		$this->db->select('permission_id')->from('security_permission')->where(array('module' => $module, 'class' => $class, 'function' => $function));
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			$row = $result->row();
			return $row->permission_id;
		} else {
			return 0;
		}
	}
	
	function has_access($function = '*', $class = '*', $module = '*') {
		return has_permission($this->get_permission_id($function, $class, $module));
		
	}
	
	/**
	 * @param $data
	 * @return unknown_type
	 */
	function before_insert_process($data){
		$data->add_field("created_id", get_user_id());	
	} 
	
	/**
	 * @param $data
	 * @return unknown_type
	 */
	function before_update_process($data){
		$data->add_field("modified_id", get_user_id());
		$data->add_field("modified_time", date('Ymd H:i:s'));
	} 
	
	function valid_from_check($date) {
//		echo $date;
		if(!$date) {
			$this->form_validation->set_message('valid_from_check', 'The first date can not be empty');
			return FALSE;
		}
		list($j, $n, $Y) = explode('-', $date);
		$today = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
		$valid_from = mktime(0, 0, 0, (int) $n, (int) $j, (int) $Y);
		if($today >= $valid_from) {
			$this->form_validation->set_message('valid_from_check', 'The first date can not be set today or day before');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
}

/* End of file aims_controller.php */
/* Location: ./appl/libraries/aims_controller.php */