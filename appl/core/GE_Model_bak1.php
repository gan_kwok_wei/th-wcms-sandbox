<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/********************************************************************/
/* File Name		: ./appl/libraries/aims_Model.php			*/
/* Module			: 												*/
/* Revise Number	: 01											*/
/* Created By		: Budi Laxono									*/
/* Created At		: 13-06-2010									*/
/********************************************************************/
/* Modified By		: Unknown										*/
/* Modified At		: Unknown										*/
/* Modification		: Unknown										*/
/********************************************************************/

class GE_Model extends CI_Model {
	
	var $status;
	
	/**
	 * Constructor
	 * @return unknown_type
	 */
	function __construct() {
		parent::__construct();		

	}
	
	/**
	 * @param $to
	 * @param $subj
	 * @param $str
	 * @param $attach
	 * @param $mail_type
	 * @return unknown_type
	 */
	function _send_mail_notification($to, $subj, $str, $attach, $mail_type = 'Task Notification'){		
		$this->load->library('email');       
        $this->load->helper('file');
		$this->email->clear();
		$this->email->from('webmin@tunehotels.com', 'Tune Hotels');
		$this->email->to($to);
		$this->email->cc('budi.laksono@greenlabgroup.com');
		$this->email->subject($subj);
		$this->email->message($str);
//		echo $this->email->print_debugger().'xxx<br/>';	
//		echo "<pre>".print_r($attach,1)."</pre>";
		for($i=0;$i<count($attach);$i++) {
			$this->email->attach($attach[$i]);
		}
//		var_dump($this->email);
		if (!defined('LOGEMAIL_PATH')) define("LOGEMAIL_PATH", APPPATH."/logs/log_".date('Y-m-d')."_email.txt");
		if (!$this->email->send()) {
			$str .= "==========================================\n";
			$str .= "$mail_type Email failed to be sent to $to..\n";
			$str .= $this->email->print_debugger()."\n";		
			$str .= "==========================================\n";	
			write_file(LOGEMAIL_PATH, $str, 'at');
		} else {
			$str = "\n$mail_type Email has been sent to $to at " . date('Y-m-d h:i:s');
//			echo $this->email->print_debugger();			
			write_file(LOGEMAIL_PATH, $str, 'at');
		}
	}
	
	/**
	 * @param $sequence
	 * @return unknown_type
	 */
	function _next_val($sequence){
		$sql = "SELECT nextval('$sequence'::regclass)";
		$out = $this->db->query($sql)->row();
		return $out->nextval;
	}
	
	/**
	 * @param 'd-m-Y H:i:s' $date
	 * @return 'Y-m-d H:i:s' $date
	 */
	function _db_date($date) {
		$time = '';
		if(strpos($date, ' ') !== FALSE) {
			list($date, $time) = explode(' ', $date);
		}
		list($d, $m, $y) = explode('-', $date);
		
		return trim("$y-$m-$d $time");
	}
	
	function _mail_template($tittle = "", $name = "", $content = "") {
		
		$mail = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
	
<body>
	<div style="background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 5px;
    font-size: 70%;
    width: 480px;
    margin: 10px auto;
    min-height: 300px;
    padding: 5px 15px;">
		
		<div style="">
			<img src="' . base_url('assets/media/images/') . 'mail-logo.jpg" alt="' . $this->config->item('site_name') . '" />
		</div>
		<div style="background: url(' . base_url('assets/themes/default/css/images/') . 'ui-bg_glass_35_dddddd_1x400.png) repeat-x scroll 50% 50% #DDDDDD;
	    border: 1px solid #C5C5C5;
	    border-top-left-radius: 5px;
	    border-top-right-radius: 5px;
	    font-weight: bold;
	    line-height: normal;
	    padding: 7px 7px 1px;">
			' . $tittle . '
		</div>
				
		<div style="padding: 10px; min-height: 150px">
			Dear ' . $name . ',<br/>
			<br/>
			' . $content . '
		</div>
		
			Thanks,<br />
			The ' . $this->config->item('site_name') . ' Admin.
		<div style="clear: both;"></div>
	</div>
	<br/><br/>This email was automatically sent. Don\'t reply this massage.<br/>
</body>
</html>';
		
		return $mail;
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $name
	 * @return mixed
	 */
	function _set_clean_name($name) {
		$void = array('& ', '/ ', '.', ',', '"', "'", "(", ")");
		return underscore(str_replace($void, '', $name));
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $rev
	 * @return multitype:
	 */
	function _registered_language($rev = FALSE) {
		$return = array();
		$this->db->order_by('key');
		/*
		 * custom
		 */
		$result = $this->db->get_where('system_config', array('type' => 'language', 'key <>' => 'en'));
		/* * */
		if($rev) {
			$return['english'] = 'en';
			foreach($result->result() as $row) {
				$return[$row->val] = $row->key;
			}
		} else {
			$return['en'] = 'english';
			foreach($result->result() as $row) {
				$return[$row->key] = $row->val;
			}
		}
	//	var_dump($result);
		return $return;
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $rev
	 * @return multitype:
	 */
	function _registered_country($rev = FALSE) {
		$return = array();
		$this->db->order_by('letter_code');
		$result = $this->db->get('country');
		
		if($rev) {
			foreach($result->result() as $row) {
				$return[$row->name] = strtolower($row->letter_code);
			}
		} else {
			foreach($result->result() as $row) {
				$return[strtolower($row->letter_code)] = $row->name;
			}
		}
		return $return;
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $name
	 * @return mixed
	 */
	function _init_country() {
		$array_country = $this->_registered_country();
//		echo "<!-- x : " . $this->session->get_dec('country') . " | -->";
		if($this->uri->segment(1) != '' && in_array($this->uri->segment(1), array_keys($array_country))) {
//			echo "<!-- a -->";
			$this->session->save_enc('country', $this->uri->segment(1));
			return $this->uri->segment(1);
		} else {
//			echo "<!-- b: " . $this->session->get_dec('country') . " | -->";
			if($this->session->get_dec('country') && $this->session->get_dec('country') != '') {
//				echo "<!-- b1 -->";
				return $this->session->get_dec('country');
			} else {
//				echo "<!-- b2 -->";
				$locations = $this->ip2location_lite->getCity($_SERVER['REMOTE_ADDR']);
//				echo 'ip : ' . $_SERVER['REMOTE_ADDR'] . $this->ip2location_lite->getError();
//				echo $_SERVER['REMOTE_ADDR'];
//				var_dump($locations);
				$countryCode = strtolower($locations['countryCode']);
//				var_dump($expression)
				if(!in_array($countryCode, $array_country)) {
//					echo "<!-- b2x -->";
					$def = $this->config->item('ge_def_country');
					$this->session->save_enc('country', $def[0]);
					return $def[0];
				} else {
//					echo "<!-- b2y -->";
					$this->session->save_enc('country', $countryCode);
					return $countryCode;
				}
			}
		}
	}
	
	/**
	 * Enter description here ...
	 */
	function _get_country() {
		return $this->session->get_dec('country');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $name
	 * @return mixed
	 */
	function _init_language() {
		$array_lang = $this->_registered_language();
		if($this->uri->segment(2) != '' && in_array($this->uri->segment(2), array_keys($array_lang))) {
			
			$this->session->save_enc('lang', $this->uri->segment(2));
			return $this->uri->segment(2);
		} else {
			if($this->session->get_dec('lang') && $this->session->get_dec('lang') != '') {
				return $this->session->get_dec('lang');
			} else {
				$country = $this->_get_country();
				$sql = "SELECT * FROM country_language WHERE `default` = 1 AND country_id = (SELECT country_id FROM country WHERE letter_code = '$country') ";
				$result = $this->db->query($sql);
				if($result->num_rows() > 0) {
					$lang = $result->row()->language_id;
					$this->session->save_enc('lang', $lang);
					return $lang;
				} else {
					$this->session->save_enc('lang', $this->config->item('ge_def_country'));
					return $this->config->item('ge_def_language');
				}
				
			}
		}
	}
	
	/**
	 * Enter description here ...
	 */
	function _get_language() {
		return $this->session->get_dec('lang');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $name
	 * @return mixed
	 */
	function _get_def_language() {
		/*
		$array_lang = $this->_registered_language();
		$country = $this->_get_country();
		
		$this->db->join('country', 'country.country_id = country_language.country_id');
		$result = $this->db->get_where('country_language', array('country.letter_code' => $country, 'default' => 1));
		if($result->num_rows() > 0) {
			return $result->row()->language_id;
		} else {
		*/	return $this->config->item('ge_def_language');
//		}
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $file
	 * @param unknown_type $save
	 * @param unknown_type $maxwidth
	 * @param unknown_type $maxheight
	 */
	function _resize_hi($file,$save,$maxwidth,$maxheight) {
		list($width, $height) = getimagesize($file) ; 
		$imgInfo = getimagesize($file) ; 
	
		if($width>$height) {
			if($width>$maxwidth) {
				$modwidth = $maxwidth;
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
			} else {
				$modwidth = $width;
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
			}
			
			if($modheight>$maxheight) {
				if($height>$maxheight) {
					$modheight = $maxheight;
					$diff = $height / $modheight;
					$modwidth = $width / $diff;
				} else {
					$modheight = $height;
					$diff = $height / $modheight;
					$modwidth = $width / $diff;
				}
			}
		} else if($width==$height) {
			if($width>$maxwidth) {
				$modwidth = $maxwidth;	 
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
			} else {
				$modwidth = $width;
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
			}
			
			if($modheight>$maxheight) {
				if($height>$maxheight) {
					$modheight = $maxheight;
					$diff = $height / $modheight;
					$modwidth = $width / $diff;
				} else {
					$modheight = $height;
					$diff = $height / $modheight;
					$modwidth = $width / $diff;
				}
			}
		} else if($width<$height) {
			if($height>$maxheight) {
				$modheight = $maxheight;
				$diff = $height / $modheight;
				$modwidth = $width / $diff;
			} else {
				$modheight = $height;
				$diff = $height / $modheight;
				$modwidth = $width / $diff;
			} if($modwidth>$maxwidth) {
				if($width>$maxwidth) {
					$modwidth = $maxwidth;
					$diff = $width / $modwidth;
					$modheight = $height / $diff;
				} else {
					$modwidth = $width;
					$diff = $width / $modwidth;
					$modheight = $height / $diff;
				}
			}
		}
		
		
		switch ($imgInfo[2]) {
			case 1: $image = imagecreatefromgif($file); break;		
			case 2: $image = imagecreatefromjpeg($file);  break;		
			case 3: $image = imagecreatefrompng($file); break;		
		}
	
		//$image=imagecreatefromjpeg($file);
		
		$tn=imagecreatetruecolor($modwidth,$modheight);
		
		if(($imgInfo[2] == 1) or ($imgInfo[2]==3)) {		
		  imagealphablending($tn, false);
		  imagesavealpha($tn,true);
		  $transparent = imagecolorallocatealpha($tn, 255, 255, 255, 127);
		  imagefilledrectangle($tn, 0, 0, $modwidth, $modheight, $transparent);
		}
		 
		imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height);
		
		
		switch ($imgInfo[2]) {
			case 1: imagegif($tn,$save); break;		
			case 2: imagejpeg($tn,$save);  break;		
			case 3: imagepng($tn,$save); break;
		
		}
	
	}
		
	/**
	 * Enter description here ...
	 * @param unknown_type $file
	 * @param unknown_type $save
	 * @param unknown_type $maxwidth
	 * @param unknown_type $maxheight
	 */
	function _resize_lo($file, $save, $maxwidth, $maxheight) {
		list($width, $height) = getimagesize($file) ;  
		$imgInfo = getimagesize($file) ; 
	
		if($width > $height) {
			if($width > $maxwidth) {				
				$modheight = $maxheight;
				$diff = $height / $modheight;
				$modwidth = $width / $diff;
				
				if($modwidth < $maxwidth) {	
					$modwidth = $maxwidth;
					$diff = $width / $modwidth;
					$modheight = $height / $diff;							
				}
			} else {
				$modwidth = $width;
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
			}
		} else if($width == $height) {
			if($width > $maxwidth) {
				$modwidth = $maxwidth;	 
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
			} else {
				$modwidth = $width;
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
			}
		} else if($width < $height) {
			if($height > $maxheight) {
				$modwidth = $maxwidth;
				$diff = $width / $modwidth;
				$modheight = $height / $diff;
				
				if($modheight < $maxheight) {
					$modheight = $maxheight;
					$diff = $height / $modheight;
					$modwidth = $width / $diff;							
				}
			} else {
				$modheight = $height;
				$diff = $height / $modheight;
				$modwidth = $width / $diff;
			}
		}
		
		switch ($imgInfo[2]) {		
			case 1: $image = imagecreatefromgif($file); break;		
			case 2: $image = imagecreatefromjpeg($file);  break;		
			case 3: $image = imagecreatefrompng($file); break;		
		}
	
		//$image=imagecreatefromjpeg($file);
		
		$tn = imagecreatetruecolor($modwidth, $modheight);
		
		
		if(($imgInfo[2] == 1) or ($imgInfo[2]==3)) {
			imagealphablending($tn, false);
			imagesavealpha($tn, true);
			$transparent = imagecolorallocatealpha($tn, 255, 255, 255, 127);
			imagefilledrectangle($tn, 0, 0, $modwidth, $modheight, $transparent);
		}
		//echo ("<!-- $modwidth, $modheight -->");
		imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height);
		
		switch ($imgInfo[2]) {
		
			case 1: imagegif($tn,$save); break;		
			case 2: imagejpeg($tn,$save);  break;		
			case 3: imagepng($tn,$save); break;
		
		}
	}	
		
}
		

/* End of file aims_Model.php */
/* Location: ./appl/libraries/aims_Model.php */