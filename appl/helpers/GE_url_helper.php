<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource GE_url_helper.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Sep 28, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */


/**
 * Site URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access	public
 * @param	string
 * @return	string
 */
	function site_url($uri = '') {
		$CI =& get_instance();
	
		if(in_array($CI->uri->segment(1), array_keys($CI->config->item('ge_country'))) && in_array($CI->uri->segment(2), array_keys($CI->config->item('ge_language')))) {
			$uri = $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/' . $uri;
		}
		return $CI->config->site_url($uri) . '/';
	}

// ------------------------------------------------------------------------

/**
 * Base URL
 * 
 * Create a local URL based on your basepath.
 * Segments can be passed in as a string or an array, same as site_url
 * or a URL to a file can be passed in, e.g. to an image file.
 *
 * @access	public
 * @param string
 * @return	string
 */
	function base_url($uri = '') {
		$CI =& get_instance();
		
		if(in_array($CI->uri->segment(1), array_keys($CI->config->item('ge_country'))) && in_array($CI->uri->segment(2), array_keys($CI->config->item('ge_language')))) {
			$uri = $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/' . $uri;
		}
		return $CI->config->base_url($uri) . '/';
	}


/**
 * End of file GE_url_helper.php 
 * Location: ./.../.../.../GE_url_helper.php 
 */