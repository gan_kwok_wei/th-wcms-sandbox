<?php
	$set_hotel_hits = $this->pages_model->set_hotel_hits($hotel_id);
?>
	<div id="content-container">
		<div id="home_banner">
			<!-- START BIG BANNER -->
			<?php
				$this->output_data['page_id'] = 2;
				$this->load->view('component/big_slider', $this->output_data);
			?>
			<!-- END BIG BANNER -->
			
			<div id="container-book">
				<!-- START BOOKING VIEW -->
				<?php
					$this->load->view('component/booking_search');
				?>
				<!-- END BOOKING VIEW -->
			</div>
		</div>
		<div class="clear"></div>
		<div id="sidekicks-wrap" class="six clearfix">
			<div id="hotel_container_left" class="f_left">
				<div id="detail_hotel_container">
					<!-- START DETAIL HOTEL -->
					<?php
						$this->load->view('component/detail_hotel');
					?>
					<!-- END DETAIL HOTEL -->
				</div>
				<div class="clear"></div>
			</div>
		
			<div id="hotel_container_right" class="f_left">
				<div class="trip_advisor_hotel_container">
					<!-- START TRIP ADVISOR HOTEL -->
					<?php
						$this->output_data['hotel_id'] = $hotel_id;
						$this->output_data['page_id'] = 1;
						$this->output_data['position'] = 'right_1';
						$this->load->view('component/trip_advisor_hotel', $this->output_data);
					?>
					<!-- END TRIP ADVISOR HOTEL -->
			</div>
		</div>	
			<div class="whats_hot_hotel_container">
				<!-- START WHATS HOT HOTEL -->
				<?php
					$this->output_data['page_id'] = 2;
					$this->output_data['position2'] = 'right_2';
					$this->load->view('component/whats_hot_hotel', $this->output_data);
				?>
				<!-- END WHATS HOT HOTEL -->
			</div>
			<div class="container_newsletter_small">
				<!-- START NEWSLETTER SMALL -->
				<?php
					$this->load->view('component/newsletter_small');
				?>
				<!-- END NEWSLETTER SMALL -->
			</div>
		</div>
		<div class="clear"></div>
		
	</div>
</div>
<!-- end body container -->