<?php
	$get_page_banner_page = $this->pages_model->get_page_banner_page($page_id);
	$data_get_page_banner_page = $get_page_banner_page->row();
?>
<div class="bgc-lightGrey box-shade round" style="display:block; height:204px; margin:0; padding:0;">
	<?php
		if($get_page_banner_page->num_rows() > 0){
			if($data_get_page_banner_page->text_link){
				$link = $data_get_page_banner_page->text_link;
			}
			else{
				$link = $data_get_page_banner_page->def_link;
			}
			
			if($link == ""){
				echo '<img src="' . (($data_get_page_banner_page->text_image) ? $data_get_page_banner_page->text_image : $data_get_page_banner_page->def_image) . '" />';
			}
			else{
				echo '<a href="' . $link . '"><img src="' . (($data_get_page_banner_page->text_image) ? $data_get_page_banner_page->text_image : $data_get_page_banner_page->def_image) . '" /></a>';
			}
		}
	?>
</div>