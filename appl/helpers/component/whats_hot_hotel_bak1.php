<div class="whats_hot_hotel">
	<script>
		$(document).ready(function(){
			$('.btn_see_detail').click(function(){
				var alt = $(this).attr('alt');
				var screenTop = $(document).scrollTop();
				
				$('.box_booking_search').hide();
				
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>ajax",
					data: { act: "get_title_promo", promo_id: alt }
				}).done(function( msg ) {
					$('.box_content_text_title h2').html(msg);
				});
				
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>ajax",
					data: { act: "get_content_promo", promo_id: alt }
				}).done(function( msg ) {
					$('.this_content_promo').html(msg);
				});
				
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>ajax",
					data: { act: "get_image_promo", promo_id: alt }
				}).done(function( msg ) {
					$('.this_image_promo').html(msg);
				});
				
				var doc_tinggi = $(document).height();
				var tinggi = $(window).height();
				var lebar = $(window).width();
				var tinggi_box = $('.box').height();
				var lebar_box = $('.box').width();
				var top = ((tinggi/2)-(tinggi_box/2))+screenTop;
				var left = (lebar/2)-(lebar_box/2);
				
				$('.backdrop').css({'height' : doc_tinggi + 'px'});
				$('.backdrop, .box').animate({'opacity':'.50'}, 300, 'linear');
				$('.box').animate({'opacity':'1.00'}, 300, 'linear');
				$('.backdrop, .box').css({'display' : 'block'});
				$('.box').css({'top':top + 'px', 'left':left + 'px'})
			});

			$('.close').click(function(){
				close_box();
			});

			$('.backdrop').click(function(){
				close_box();
			});
			
			$('.box_booking_search').hide();
			
			$('.btn_book_box').click(function(){
				$('.box_promo_image').hide();
				$('.box_booking_search').show();
			});
			
			$('.box_booking_close').click(function(){
				$('.box_promo_image').show();
				$('.box_booking_search').hide();
			});
			
		});

		function close_box()
		{
			$('.backdrop, .box').animate({'opacity':'0'}, 300, 'linear', function(){
				$('.backdrop, .box').css('display', 'none');
			});
		}
	</script>
	<h1><?php echo lang('whats_whats_hot');?></h1>
	
	<?php
//		$get_page_promo = $this->pages_model->get_page_banner_pos($page_id, $position2);
		$get_page_promo = $this->pages_model->get_page_promo($page_id, $position2);
		if($get_page_promo->num_rows() > 0){
			foreach($get_page_promo->result() as $data_get_page_promo){
	?>
				<div class="whats_hot_hotel_content">
					<img src="<?php echo (($data_get_page_promo->text_image) ? $data_get_page_promo->text_image : $data_get_page_promo->def_image); ?>"/>
					<div class="whats_hot_hotel_text">
						<h3><?php echo (($data_get_page_promo->text_title) ? $data_get_page_promo->text_title : $data_get_page_promo->def_title);?></h3>
						<span><?php echo (($data_get_page_promo->text_teaser) ? $data_get_page_promo->text_teaser : $data_get_page_promo->def_teaser);?></span>
						<br/><br/>
						<input type="button" class="btn_see_detail" value="<?php echo lang('book_now');?>" alt="<?php echo $data_get_page_promo->promo_id;?>" />
					</div>
				</div>
	<?php
			}
		}
	?>
</div>