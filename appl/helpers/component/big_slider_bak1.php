<script type="text/javascript">
<!--

$(window).load(function() {
	//BIG SLIDER BANNER
	$('#slider_home').nivoSlider({
		directionNav: false,
		controlNav: true,
		controlNavThumbs:true
	});
	
	//WHATS HOT BANNER
	$('#slider_hot').nivoSlider({
		directionNav: false,
		controlNav: true,
		effect : 'fade'
	});
	
	//SELECT UNIFORM
	$(".myselect").uniform();
});

//-->
</script>
<div class="slider-wrapper theme-default">
	<div id="slider_home" class="nivoSlider">
<?php
	
	if(isset($hotel_id)) {
		$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top", $hotel_id);
	} else {
		$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top");
	}
//	echo $this->db->last_query();
	
	if($get_page_banner->num_rows() > 0){
		foreach($get_page_banner->result() as $data_get_page_banner){
			if($data_get_page_banner->text_link){
				$link_2 = $data_get_page_banner->text_link;
			} else {
				$link_2 = $data_get_page_banner->def_link;
			}
			$icon = (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon);
			if($link_2 == ""){
				echo '<img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($icon) ? $icon : '/tune_media/images/banner/thumb/ui-billboard-bullets.png') . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"/>';
			} else {
				echo '<a href="' . $link_2 . '"><img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon) . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"/></a>';
			}
		}
	}
	
//	echo $this->db->last_query();
?>
	</div>
</div>