
		<div id="top_menu">
		<div class="container">
				<?php
	$this_page = (in_array($this->uri->segment(1), $this->system_model->get_registered_country(TRUE))) ?  $this->uri->segment(3) : $this->uri->segment(1);
	if(!$this_page){
		$this_page = "home";
	}
?>
				<ul id="ul_top_menu">
						<?php
	$sub_menus = array();
	$list = $this->pages_model->get_menu_list();
	foreach($list->result() as $main_menu){
		$sub_list1 = $this->pages_model->get_menu_list($main_menu->menu_id);
		if(($sub_list1->num_rows() > 0) || ($main_menu->menu_name == 'hotel')) {
			$link = '#';
			$icon_class = 'i_child';
			$sub_menus[$main_menu->menu_name] = $sub_list1->result();
		} else {
			$icon_class = '';
			$link = base_url($main_menu->link);
		}
?>
						<li><a href="<?php echo $link; ?>" id="menu_<?php echo $main_menu->menu_name; ?>"
	class="<?php echo $icon_class . ' ' . $main_menu->menu_name . ' a_first ' . (($this_page == $main_menu->menu_name) ? ($main_menu->menu_name . '_hover') : ""); ?> "> <?php echo (($main_menu->text_title) ? $main_menu->text_title : $main_menu->def_title); ?></a></li>
						<?php
	}

?>
				</ul>
				</div>
		</div>

<!--top menu ends-->

<!--start hotel submenu pushdown div-->
<div id="sub_hotel" class="nav_drop" style="display: none; width:100%;">
		<?php
	$res_country = $this->pages_model->get_country();
	foreach($res_country->result() as $data_country) {
?><div class="container">
		<div class="nav_drop_model" > <a href="<?php echo base_url('hotel/hotels/' . $data_country->country_id . '/' . underscore(strtolower($data_country->name))); ?>"  class="nav_drop_tab">
				<h2><?php echo (($data_country->text_title) ? $data_country->text_title : $data_country->def_title); ?></h2>
				</a>
				<?php
		$res_country_hotel = $this->pages_model->get_hotel_by_country($data_country->country_id);
		if($res_country_hotel->num_rows() > 0){
			echo '<ul class="nav_drop_list">';
			foreach($res_country_hotel->result() as $data_hotel) {
?>
				<!-- li><a href="<?php echo base_url('our-hotel/' . $data_hotel->hotel_name); ?>" onclick="#"><?php echo (($data_hotel->text_title) ? $data_hotel->text_title : $data_hotel->def_title); ?></a></li-->
				<li> <a href="<?php echo base_url('hotel/hotel_detail/' . $data_hotel->hotel_id . '/' . $data_hotel->hotel_name); ?>" onclick="#"> <?php echo (($data_hotel->text_title) ? $data_hotel->text_title : $data_hotel->def_title); ?> </a> </li>
				<?php
			}
			echo '</ul>';
		}
?>
		</div>
		<?php
	}
?>
</div>
</div>

<!--end hotel submenu pushdown div-->
<?php
	if(count($sub_menus) > 0) {
		unset($sub_menus['hotel']);
		foreach ($sub_menus as $sub_key => $sub_menu) {
?>

<div id="sub_<?php echo $sub_key; ?>" class="nav_drop yellow" style="display:none">

		<div class="nav_drop_model" >
				<ul class="nav_drop_list">
						<?php
			foreach($sub_menu as $menu_data) {
			?>
				<li><a href="<?php echo base_url($menu_data->link); ?>" onclick="#"><?php echo (($menu_data->text_title) ? $menu_data->text_title : $menu_data->def_title); ?></a></li>
						<?php
			}
?>
				</ul>
		</div>

</div>
<?php
		}
	}
?>
</div>

<!--header closing-->