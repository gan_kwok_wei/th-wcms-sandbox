	<div id="content-container">

		<div id="corporate_container_left" class="f_left">

			<div id="contact_us_medium_container" style="margin-top:15px;">

				<?php

					$this->load->view('component/contact_us_medium.php');

				?>

			</div>

			

			<!-- start promo -->

			<div id="container_promo_medium">

				<?php

					$this->output_data['page_id'] = 7;

					$this->output_data['position1'] = 'left_1';

					$this->output_data['position2'] = 'left_2';

					$this->load->view('component/image_promo_medium.php', $this->output_data);

				?>

			</div>

			<!-- end promo -->

			<div class="container_newsletter_medium">

				<!-- START NEWSLETTER MEDIUM -->

				<?php

					$this->load->view('component/newsletter_medium.php');

				?>

				<!-- END NEWSLETTER MEDIUM -->

			</div>

		</div>

		

		<div id="corporate_container_right" class="f_left">

			<div class="top_page">

				<div class="bread_crumb f_left">

					<?php

						echo '<a href="' . base_url() . '">' . lang('home') . '</a> > ';

						echo lang('title_404');

					?>

				</div>

				<div class="clear"></div>

				

				<div id="corporate_content_container">

					<div class="page_content">

						<h1><?php echo lang('page_not_found')?></h1>

						<p style="font-size:12px;"><?php echo lang('404_text')?></p>

						<!--div class="inquiry_form_content">

							

						</div-->

					</div>

				</div>

			</div>

		</div>

		<div class="clear"></div>

		

	</div>

</div>

<!-- end body container -->