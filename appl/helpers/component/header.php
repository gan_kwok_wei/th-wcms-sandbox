<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


		<!--<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/font.css" />-->


		<link rel="stylesheet" href="css/nivo/themes/default/default.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/nivo/themes/light/light.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/nivo/nivo-slider.css" type="text/css" media="screen" />


		<!-- date -->
		<link rel="stylesheet" href="css/flight-calendar.css" type="text/css" media="screen" />



		<link rel="stylesheet" type="text/css" href="css/uni_form.css" />


		<title></title>



	</head>

	<body>
	<!-- start body container -->
	<div id="th-header">
		<div class="wrapper">
			<div id="header_top">
					<div id="logo"></div>


				<div id="top_menu">
				<ul>
					<li>
						<a href="index.php" class="home">#</a>
					</li>
					<li>
						<a href="hotel.php" class="our_hotels">Our Hotels</a>
					</li>
					<li>
						<a href="promotion.php" class="promotions">Promotions</a>
					</li>
					<li>
						<a href="corporate.php" class="corporate">Business</a>
					</li>
					<li>
						<a href="about_us.php" class="about_us">About Us</a>
					</li>
					<li>
						<a href="help_info.php" class="help_info">Help &amp; Info</a>
					</li>
				</ul>
			</div>
		</div>

	</div>
