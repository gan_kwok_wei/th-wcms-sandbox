<link rel="stylesheet" href="/assets/extend/fws/css/fwslider.css" media="all">
<script src="/assets/extend/fws/js/fwslider.js"></script>
<!--div id="slider_home" class="nivoSlider" style="display:block; width:980px; height:420px; padding:0; margin: 0;"-->
    <div id="fwslider" style="width:100%;">
        <div class="slider_container">
			<div id="" style="width:100%; ">
			<div class="slider_container">
			 <div class="slide" style="width:100%;"> 
		<?php 
			
			if(isset($hotel_id)) {
				$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top", $hotel_id);
			} else {
				$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top");
			}

			if($get_page_banner->num_rows() > 0){
				foreach($get_page_banner->result() as $data_get_page_banner){
					if($data_get_page_banner->text_link){
						$link_2 = $data_get_page_banner->text_link;
					}
					else{
						$link_2 = $data_get_page_banner->def_link;
					}

					if($link_2 == ""){
						echo '<img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon) . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"/>';
					}
					else{
						echo '<a href="' . $link_2 . '"><img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon) . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"/></a>';
					}
				}
			} 

			//echo $this->db->last_query();
		?>
<!--/div-->
   

            <!-- ****************** STEP 3: ADD SLIDES ****************** -->
            
            <!-- Duplicate to create more slides -->
           
                
                <!-- Slide image -->
                    <!--img src="/assets/extend/fws/img/slide.jpg">
                <!-- /Slide image -->
                
                <!-- Texts container -->
                <!--div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <!--h4 class="title">REVEAL THE MAGIC OF LEGOLAND, MALAYSIA</h4>
                        <!-- /Text title -->
                        
                        <!-- Text description -->
                        <!--p class="description">Enter a chance to win a stay in Tune Hotel Danga Bay, Johor</p>
                        <!-- /Text description -->
                        
                        <!-- Learn more button -->
                        <!--a class="readmore" href="#">LET'S GO!</a>
                        <!-- /Learn more button -->
                    <!--/div>
                <!--/div-->
                 <!-- /Texts container -->
                 
            </div>
            <!-- /Duplicate to create more slides -->
            
            <!-- ****************** /STEP 3: ADD SLIDES ****************** -->

            <!--div class="slide">
                <img src="/assets/extend/fws/img/slide2.jpg">
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h4 class="title">Get away this weekend to Edinburgh</h4>
                        <p class="description">Save up to GBP 20 and earn 15,000 extra BIG points</p>
                        <a class="readmore" href="#">LET'S GO!</a>
                    </div>
                </div>
            </div><!--/slide -->

            <!--div class="slide">
                <img src="/assets/extend/fws/img/slide3.jpg">
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h4 class="title">Escape to the blissful beaches of Bali</h4>
                        <p class="description">Save up to Rp 2000 per night booking...</p>
                        <a class="readmore" href="#">LETS GO!</a>
                    </div>
                </div>
            </div><!--/slide -->

 

        </div>
	 <div class="timers"></div>

        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>

    </div>
