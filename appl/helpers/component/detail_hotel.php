<section>

	<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.idTabs.min.js"></script>

	<!--script type="text/javascript" src="js/jquery.ad-gallery.js"></script-->
	<?php
	$query_hotel_detail = $this->pages_model->get_hotel($hotel_id);
	$data_hotel_detail = $query_hotel_detail->row();

	if($tab_hotel == ""){
		$tab_hotel = 'hotel_information';
	}

	?>

	<link rel="stylesheet" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/nivo/themes/dark/dark.css" type="text/css" media="screen" />

	<script type="text/javascript">
	$(function(){
		//$("#ul_tab_detail_hotel").idTabs();
	});

	</script>
	<!--tab-->

		<h5><?php echo lang('map_hotel_tune_hotel');?></h5>
			<h4 style="margin-bottom:36px;"><?php echo (($data_hotel_detail->text_title) ? $data_hotel_detail->text_title : $data_hotel_detail->def_title);?></h4>
		

		<div id="hotel-info">
			<div class="nav-sections tab">
				<ul id="page-nav">
					<li>
						<a href="<?php echo base_url('hotel/hotel_detail/' . $hotel_id . '/' . $hotel_name . '/hotel_information');?>" class="<?php echo ($tab_hotel == 'hotel_information' ? 'selected' : '');?>"><?php echo lang('det_hotel_hotel_information');?></a>
					</li>
					
					<li>
						<a href="<?php echo base_url('hotel/hotel_detail/' . $hotel_id . '/' . $hotel_name . '/hotel_gallery');?>" class="<?php echo ($tab_hotel == 'hotel_gallery' ? 'selected' : '');?>"><?php echo lang('det_hotel_gallery');?></a>
					</li>
						<li>
			<a href="<?php echo base_url('hotel/hotel_detail/' . $hotel_id . '/' . $hotel_name . '/nearby_attraction');?>" class="<?php echo ($tab_hotel == 'nearby_attraction' ? 'selected' : '');?>"><?php echo lang('det_hotel_nearby_attraction');?></a>
		</li>


				</ul>
			</div>
		</div>
		<!--tabs-->

		<div class="clear"></div>
		<!--box-->
		

			<?php
			$this->output_data2['hotel_id'] = $hotel_id;
			$this->output_data2['city_id'] = $data_hotel_detail->city_id;

			$this->load->view('component/' . $tab_hotel, $this->output_data2);
			?>
			

		<!--box-->

	</section>
