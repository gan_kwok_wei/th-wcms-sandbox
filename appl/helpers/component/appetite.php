<?php
	$get_culinary = $this->pages_model->get_culinary($city_id);
	if($get_culinary->num_rows() > 0) {
		$data_get_culinary = $get_culinary->row();
		echo "<!-- debug " . $this->db->last_query() . "  -->";
		
		if($data_get_culinary->text_teaser){
			$appetite_text = $data_get_culinary->text_teaser;
		}
		else{
			$appetite_text = $data_get_culinary->def_teaser;
		}
	
		if($appetite_text != ""){
?>

<div class="appetite">
	<h2><?php echo lang('nearby_appetite');?></h2>
	<div class="appetite_content">
		<table>
			<tr>
				<td style="vertical-align:top;"><img class="f_left" src="<?php echo (($data_get_culinary->text_icon) ? $data_get_culinary->text_icon : $data_get_culinary->def_icon); ?>"/></td>
				<td style="vertical-align:top;padding-left:10px;">
					<p><?php echo $appetite_text;?></p>
				</td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
</div>
<?php
		}
	}
?>