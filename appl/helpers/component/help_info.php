<!--<div class="six red clearfix" style="height:80px;">
	Place an image here.....its up to you!
	</div>
</div>-->

<div class="page contact-us">

<div id="sticky-nav">
		<div class="sticky-nav-wrapper">
	
			<div class="page-title">
					
				<h3>Contact Us</h3>
			
				 <!-- <a id="back-to-top" href="#th-header">Back to top</a>-->
			
			</div> <!-- /.page-title -->
	
		</div> <!-- /.sticky-nav-wrapper -->
	</div> <!-- /#sticky-nav -->

<div id="container" class="clearfix" role="main">
		
		<div class="six clearfix">
		
			<div class="col4 first">
		
				<!--<h2 class="alpha">Talk to us</h2>-->
				<p class="intro">
					"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
				</p>

			</div>
			
			<div class="col2">
				<div class="blue-col2-bg tech-issues">
					<div class="inner">
						<h3 class="alpha">Website technical issues</h3>
						<p>If you're experienci`ng technical issues on <span>Tune Hotel <br>
Tune Hotels.com, please <a href="#">report it</a>.</p>
					</div>

				</div>	
		
			</div>
			
		</div>	
		
		<div class="six clearfix">
		
			<div class="col2 first tele-numbers">
				<div class="tel">
					<h6><span class="type">Malaysia</span></h6> 
					<p><span class="value">XXX-XXXX-XXXX-<span><br>
					Monday - Friday 9:00am - 9:00pm<br>Public Holidays 9.00 am – 6.00pm<br>
					</span></p>
				</div>
				
				<div class="tel">
					<h6><span class="type">Indonesia</span></h6> 
					<p><span class="value">XXX-XXXX-XXXX-<span><br>
					Monday - Friday 9:00am - 9:00pm<br>Public Holidays 9.00 am – 6.00pm<br>
					</span></p>
				</div>
				
				<div class="tel">
					<h6><span class="type">Thailand</span></h6> 
					<p><span class="value">XXX-XXXX-XXXX-<span><br>
					Monday - Friday 9:00am - 9:00pm<br>Public Holidays 9.00 am – 6.00pm<br>
					</span></p>
				</div>

				<div class="tel">
					<h6><span class="type">Phillipines</span></h6> 
					<p><span class="value">XXX-XXXX-XXXX-<span><br>
					Monday - Friday 9:00am - 9:00pm<br>Public Holidays 9.00 am – 6.00pm<br>
					</span></p>
				</div>  
                      				
			</div>
			
			<div class="col2 tele-numbers">			 
				
				<div class="tel">
					<h6><span class="type">India</span></h6> 
					<p><span class="value">XXX-XXXX-XXXX-<span><br>
					Monday - Friday 9:00am - 9:00pm<br>Public Holidays 9.00 am – 6.00pm<br>
					</span></p>
				</div> 
				<div class="tel">
					<h6><span class="type">Australia</span></h6> 
					<p><span class="value">XXX-XXXX-XXXX-<span><br>
					Monday - Friday 9:00am - 9:00pm<br>Public Holidays 9.00 am – 6.00pm<br>
					</span></p>
				</div>   

				<div class="tel">
					<h6><span class="type">United Kingdom</span></h6> 
					<p><span class="value">XXX-XXXX-XXXX-<span><br>
					Monday - Friday 9:00am - 9:00pm<br>Public Holidays 9.00 am – 6.00pm<br>
					</span></p>
				</div>   

                
                <div class="tel">
					<h6><span class="type">Deaf or hard of hearing customers</span></h6> 
					<p><span>(TTY/TDD) 1-800-336-5530</span></p>
				</div>
			</div>
			
			<div class="col2" style="margin-top:-36px;">
				<div class="blue-col2-bg faq">
					<div class="inner">
						<h3 class="alpha">Most popular topics</h3>
						<ul class="first">
							<li>Name your list</li>
							<li>Name your list</li>
							<li>Name your list</li>
							<li>Name your list</li>
							<li>Name your list</li>
							<li>Name your list</li>
							<li>Name your list</li>
						</ul>
					</div>
				</div>	
			</div>
			
		</div>
		
		<div class="six clearfix">
		
			<div class="col4 first just-ask">
		
				<!--<h2>Just Ask</h2>-->
				<p>Please email us with questions, concerns or to give compliments.<br>
				For immediate assistance or to book a reservation please call one of the numbers above.</p>
				<div class="tab-content"> 
				


 

  <!-- main Help Landing table starts -->
  <table cellspacing="0" cellpadding="0" border="0" align="center">
   <tbody><tr>
      <!-- Help Landing left td starts -->
      <td valign="top">

          <div id="tdMainStage">
            <ul id="tdTabs"> <!-- BEGIN MENU TABS -->
              <li><a href="#" id="tab1" class="selected">Tab 01</a></li>
              <li><a href="#" id="tab2" class="">Tab 02</a></li>
              <li><a href="#" id="tab3" class="">Tab 03</a></li>
              <li><a href="#" id="tab4">Tab 04</a></li>
            </ul> <!-- END MENU TABS -->

            <div id="tab1Content">





<img src="ir" border="0" id="welcome">

<p><span>Tune Hotel <br>
Tune Hotels's Help Site was designed with your needs in mind. Chances are that the answer to your question can be found here. Plus, it is much easier than picking up the phone to ask.</p>



<div id="help">
<a href="http://www.<span>Tune Hotel <br>
Tune Hotels.com/help/">Click here to get started</a>
</div>




			  </div>


            <div id="tab2Content"></div>

            <div id="tab3Content"></div>

            <div id="tdMainStageBottom">&nbsp;</div>
          </div>
        </td>
        <!--  Help Landing left td ends -->

        <!--  Help Landing right td starts -->
        <td valign="top" align="right">

      </td>
      <!--  Help Landing right td ends -->
    </tr>
    <tr>
      <td colspan="2" height="30"></td>
    </tr>
  </tbody></table>


 

	
				</div>
			</div>
			
			
		</div>	
			
	</div>
	</div>