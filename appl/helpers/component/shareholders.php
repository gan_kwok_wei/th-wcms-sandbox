<div class="shareholders">
	<?php
		$get_content = $this->pages_model->get_content('42');
		$data_get_content = $get_content->row();
		
		//update hits
		$set_content_hits = $this->pages_model->set_content_hits('42');
	?>
	<!--h1><?php echo lang('about_shareholders');?></h1-->
	<h1><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></h1>
	
	<?php
		if($data_get_content->text_content){
			$head_text = $data_get_content->text_content;
		}
		else{
			$head_text = $data_get_content->def_content;
		}
		if($head_text != ""){
			echo '<p style="padding:10px; font-size:12px;border-top: 1px solid #E1E1E1;">' . $head_text . '</p>';
		}
	?>
	
	<ul>
		<?php
			$get_content_list = $this->pages_model->get_content_list('8');
			if($get_content_list->num_rows() > 0){
				foreach($get_content_list->result() as $data_get_content_list){
					echo "<li>" . (($data_get_content_list->text_title) ? $data_get_content_list->text_title : $data_get_content_list->def_title) . "</li>";
				}
			}
		?>
	</ul>
</div>