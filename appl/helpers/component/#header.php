<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
		<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript" charset="utf-8"></script-->
		<script type="text/javascript" src="js/jquery.tools.min.js"></script>

		<!--<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/font.css" />-->

		<!-- nivo -->
		<script type="text/javascript" src="js/jquery.nivo.slider.js"></script>

		<link rel="stylesheet" href="css/nivo/themes/default/default.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/nivo/themes/light/light.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/nivo/nivo-slider.css" type="text/css" media="screen" />

		<!-- form -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/jquery.ddslick.js"></script>

		<!-- select box -->
		<script type="text/javascript" src="js/jquery.selectbox-0.2.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.selectbox.css" />

		<!-- date -->
		<link rel="stylesheet" href="css/flight-calendar.css" type="text/css" media="screen" />

		<!-- uniform -->
		<script type="text/javascript" src="js/jquery.uniform.js"></script>
		<link rel="stylesheet" type="text/css" href="css/uni_form.css" />


		<title></title>

		<!-- my javascript -->
		<script type="text/javascript" src="js/custom.js"></script>

	</head>

	<body>
	<!-- start body container -->
	<div id="th-header">
		<div class="wrapper">
			<div id="header_top">
					<div id="logo"></div>
					<div id="header_nav">
						<div id="link_header">
							<a href="#" style="float:right"><img src="images/assets/think_big.jpg" /></a>
						</div>
						<div id="header_menu">
							<div class="f_right">
								<input type="text" name="q" class="search_box" placeholder="Search"/>
							</div>
							<div class="f_right">
								<img src="images/assets/flag/id.jpg" class="f_left small_flag"/>
								<select name="country_id" tabindex="1" class="f_left" id="for_country">
									<option value="" data-imagesrc="images/assets/flag/id.jpg">Select country</option>
									<option value="" data-imagesrc="images/assets/flag/id.jpg">Indonesia</option>
								</select>
							</div>
							<div class="f_right">
								<img src="images/assets/flag/en.jpg" class="f_left small_flag"/>
								<select name="language_id" tabindex="2" class="f_left" id="for_lang">
									<option value="" data-imagesrc="images/assets/flag/en.jpg">Select Language</option>
									<option value="" data-imagesrc="images/assets/flag/id.jpg">Indonesia</option>
									<option value="" data-imagesrc="images/assets/flag/en.jpg">English</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div id="top_menu">
				<ul>
					<li>
						<a href="index.php" class="home">#</a>
					</li>
					<li>
						<a href="hotel.php" class="our_hotels">Our Hotels</a>
					</li>
					<li>
						<a href="promotion.php" class="promotions">Promotions</a>
					</li>
					<li>
						<a href="corporate.php" class="corporate">Business</a>
					</li>
					<li>
						<a href="about_us.php" class="about_us">About Us</a>
					</li>
					<li>
						<a href="help_info.php" class="help_info">Help &amp; Info</a>
					</li>
				</ul>
			</div>
		</div>

	</div>
