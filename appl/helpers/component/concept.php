<?php
	$get_content = $this->pages_model->get_content('44');
	$data_get_content = $get_content->row();
	
	//update hits
	$set_content_hits = $this->pages_model->set_content_hits('44');
?>
<section id="concept">
<div class="col3 first" style="margin:20px 0 20px;">

	<!--h1><?php echo lang('about_concept');?></h1-->
	<h1><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></h1>
	<div class="gutter"></div>

	<blockquote style="width:90%;"><h3 style="color:#ff0000;">5 Reasons to Choose Tune Hotels</h3><p>
Sometimes, all you really need from a hotel is a hot shower and a good night's rest. That's why, our limited service hotels offer beds at very affordable prices by getting rid of costly full service extras (pools, spas, saunas, room service or the like) that you just don't need and shouldn't have to pay for.</p></blockquote>

<div class="video">
	<?php
		if($data_get_content->text_content){
			$head_text = $data_get_content->text_content;
		}
		else{
			$head_text = $data_get_content->def_content;
		}
		if($head_text != ""){
			echo '<div class="concept_video">' . $head_text . '</div>';
		}
	?>
</div>
<div class="row" style="margin-top:20px;">
	<table id="5reasons">
		<?php
			$get_content_list = $this->pages_model->get_content_list('10');
			if($get_content_list->num_rows() > 0){
				foreach($get_content_list->result() as $data_get_content_list){
		?>
					<tr>
					<td>

						<div class="concept_img f_left">
							<img src="<?php echo (($data_get_content_list->text_image) ? $data_get_content_list->text_image : $data_get_content_list->def_image);?>" />
						</div></td>
						<td>
						<div class="contept_text f_left">
							<h3><?php echo (($data_get_content_list->text_title) ? $data_get_content_list->text_title : $data_get_content_list->def_title);?></h3>
							<p><?php echo (($data_get_content_list->text_content) ? $data_get_content_list->text_content : $data_get_content_list->def_content);?></p>
						</div>
						<div class="clear"></div></td>
					</tr>
		<?php
				}
			}
		?>
	</table>
	</div>
</div>
</section>