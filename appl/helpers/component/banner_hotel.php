<div class="whats_hot_hotel">
	<h1><?php echo lang('whats_whats_hot');?></h1>
	
	<?php
		$get_page_promo = $this->pages_model->get_page_banner_pos($page_id, $position2);
		if($get_page_promo->num_rows() > 0){
			foreach($get_page_promo->result() as $data_get_page_promo){
	?>
				<div class="whats_hot_hotel_content">
					<img src="<?php echo (($data_get_page_promo->text_image) ? $data_get_page_promo->text_image : $data_get_page_promo->def_image); ?>"/>
					<div class="whats_hot_hotel_text">
						<h3><?php echo (($data_get_page_promo->text_title) ? $data_get_page_promo->text_title : $data_get_page_promo->def_title);?></h3>
						<span><?php echo (($data_get_page_promo->text_teaser) ? $data_get_page_promo->text_teaser : $data_get_page_promo->def_teaser);?></span>
						<br/><br/>
						<input type="button" class="btn_see_detail" value="<?php echo lang('book_now');?>" alt="<?php echo $data_get_page_promo->promo_id;?>" />
					</div>
				</div>
	<?php
			}
		}
	?>
</div>