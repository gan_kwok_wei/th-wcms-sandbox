<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script type="text/javascript">
	$(document).ready(function() {
		$('#newsletter_result').hide();
		
		$('#btn_newsletter').click(function() {
			
			$("#form_frame").contents().find("#fields_email").val($('input[name="e_news_email"]').val());
		});

		$( "#news_birth" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "1920:2003",
			dateFormat: "yy-mm-dd"
		});

//		$("#newsletter_form").ajaxForm({url: '<?php echo str_replace($this->config->item('url_suffix'), "", site_url("ajax")); ?>', type: 'post'})

		$('#btn_submit').click( function() {
//			alert('cek');
			$('.input_error').addClass('input_light');
			$('.input_error').removeClass('input_error');
			$('.error_span').hide();
			
			var news_email = $('input[name="news_email"]').val();
			var news_fname = $('input[name="news_fname"]').val();
			var news_lname = $('input[name="news_lname"]').val();
			var news_gender = $('input[name="news_gender"]:checked').val();
			var news_country_id = $('select[name="news_country_id"]').val();
			var news_birth = $('input[name="news_birth"]').val();
			var news_phone = $('input[name="news_phone"]').val();
			
			var next = '1';

			if($.trim(news_email) == ""){
				$('input[name="news_email"]').addClass('input_error');
				$('input[name="news_email"]').removeClass('input_light');
				$('.error_email').show();
				var next = '0';
			}
			if($.trim(news_fname) == ""){
				$('input[name="news_fname"]').addClass('input_error');
				$('input[name="news_fname"]').removeClass('input_light');
				$('.error_name').show();
				var next = '0';
			}
			if($.trim(news_gender) == ""){
				$('.error_gender').show();
				var next = '0';
			}
			if($.trim(news_country_id) == ""){
				$('.error_hotel_news').show();
				var next = '0';
			}
			if($.trim(news_birth) == ""){
				$('.error_hotel_dob').show();
				var next = '0';
			}
			
			if(next == '1') {

				$('#btn_submit').attr('disabled', '');
				$('.loading_img').show();
				
				$.ajax({
					type: "POST",
					url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("ajax")); ?>",
					data: "act=insert_subscribe&news_email=" + news_email + 
							"&news_fname=" + news_fname + 
							"&news_lname=" + news_lname + 
							"&news_gender=" + news_gender + 
							"&news_country_id=" + news_country_id + 
							"&news_birth=" + news_birth + 
							"&news_phone=" + news_phone,
					success : function(data) {
						$('.loading_img').hide();
						//alert(data);
						if(data == '1'){
							$('#newsletter_form')[0].reset();
							$('#newsletter_form').hide();
							$('#newsletter_result').html('<?php echo lang('e_news_success'); ?>');
							$('#newsletter_result').show();
						//	alert('Successfull sign up e-newsletter');
							
						} else {
							$('#newsletter_form').hide();
							$('#newsletter_result').html('<?php echo lang('e_news_failed'); ?>');
							$('#newsletter_result').show();
						//	alert('Failed sign up' + msg);
							
						}
						
					}
				});
				
			}
			
			return false;
			
		});
		
	});

	function close_box() {
		$( "#news_birth" ).datepicker( "hide" );
		$('.backdrop, .box').animate({'opacity':'0'}, 300, 'linear', function(){
			$('.backdrop, .box').css('display', 'none');
		});
	}
	
	function init_popup() {
		$('#newsletter_result').hide();
		$('#newsletter_form').show();
		$('#newsletter_form')[0].reset();
	}
	
</script>

<div id="popup_newsletter" class="box modal hide fade in" style="width:450px;">
	<div class="modal-header">
		<span><?php echo lang('e_news_signup');?></span>
		<a class="close" data-dismiss="modal" style="margin-top: 0px;"></a>
	</div>
	<div class="modal-body" style="overflow: hidden;">
		<iframe id="form_frame" src="<?php echo base_url('help_info/frame_sign_up'); ?>" width="435px" height="360px" scrolling="no" style="overflow: hidden;"></iframe>
	</div>
</div>