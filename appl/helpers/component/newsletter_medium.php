
<?php $this->load->view('component/popup_newsletter'); ?>
<div class="newsletter_small" >
	<img class="f_left" src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/mail.png"/>
	<h1 class="f_left"><?php echo lang('e_news_enewsletter');?></h1>
	<div class="clear"></div>
	<span>
		<?php echo lang('e_news_text');?>
	</span>
	<input type="text" placeholder="<?php echo lang('e_news_your_email');?>" name="e_news_email" class="my_input_style" style="width:180px;margin-top:10px;"/>
	<a data-toggle="modal" href="#popup_newsletter" id="btn_newsletter" class="my_red_grad lightbox" style="width:180px;margin-top:10px;"><?php echo lang('e_news_subscribe');?></a>
</div>
