<section class="banner-inner bgc-black">
		<div class="container">
				<div class="row">
						<div class="span12 text-center">

								<h1 class="banner-title">Experience life at <b>BootBiz</b></h1>
								<p class="banner-description">Proin ac porta orci. Quisque augue justo, laoreet id bibendum at, iaculis eget est. Ut sollicitudin imperdiet.Proin ac porta orci. Quisque augue justo, laoreet id bibendum at, iaculis eget est. Ut sollicitudin imperdiet.</p>
						</div>
						<!--/span6-->
				</div>
				<!--/row-->
		</div>
		<!--/container -->
</section>

<script type="text/javascript" src="<?php echo ASSET_PATH ?>/assets/js/easypaginate.js"></script>
<script>
	jQuery(function($){
		$('ul#will_paginate').easyPaginate({
			step:4
		});
	});
</script>
<?php
		$get_content = $this->pages_model->get_content('41');
		$data_get_content = $get_content->row();

		//update hits
		$set_content_hits = $this->pages_model->set_content_hits('41');
	?>
<section id="sticky-nav">
<div class="sticky-nav-wrapper">
<div class="page-title">
			<h3>About</h3>
			</div>

			<div class="nav-sections">
          <ul id="page-nav" class="clearfix">
						<li class="first"><a href="#management" class="management"><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></a></li>
            <li><a href="#shareholder">Shareholders</a></li>
            <li><a href="#press-releases">Press Releases</a></li>
						<li><a href="#videos">Videos</a></li>
            <!--<li><a href="#movie-descrip">How it works</a></li> -->
          </ul>
          <!-- /#page-nav -->
       </div>
</div>
</section>


<div class="container clearfix">
		<div class="six clearfix">
				<div id="management">
						<?php
		if($data_get_content->text_content){
			$head_text = $data_get_content->text_content;
		}
		else{
			$head_text = $data_get_content->def_content;
		}
		if($head_text != ""){
			echo '<p>' . $head_text . '</p>';
		}
	?>
						<ul id="will_paginate">
								<?php
				$get_content_list = $this->pages_model->get_content_list('7');
				if($get_content_list->num_rows() > 0){
					foreach($get_content_list->result() as $data_get_content_list){
			?>
								<li> <img  class="pull-left" src="<?php echo (($data_get_content_list->text_icon) ? $data_get_content_list->text_icon : $data_get_content_list->def_icon);?>" >
										<div class="">
												<h5 style="color:#369">
												<?php echo (($data_get_content_list->text_title) ? $data_get_content_list->text_title : $data_get_content_list->def_title);?>
												</h5>
												<p><?php echo (($data_get_content_list->text_teaser) ? $data_get_content_list->text_teaser : $data_get_content_list->def_teaser);?></p>
										</div>
								</li>
								<?php
					}
				}
			?>
						</ul>

				</div>

		</div>
</div>
<div class="pagination" id="in_pagination"></div>



<section class="marketing">
        <div class="container">
        <div class="row">
        	<div class="span12">
            <div class="center">
            	<div class="lead-content">
                    <h2>We are <b>hiring!</b></h2>
                    <p>Proin ac porta orci. Quisque augue justo, laoreet id bibendum at, iaculis eget est. Ut sollicitudin.</p>
                </div><!--/lead-content-->
                <div class="lead-action">
                	<button class="btn btn-primary btn-large btn-oval">Check Openings</button>
                </div><!--/lead-action-->
            </div><!--/hero-unit option1-->
            </div><!--/span12-->
        </div><!--/row-->
        </div><!--/container -->
    </section>