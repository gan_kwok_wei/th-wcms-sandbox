	<div id="content-container">
		<div id="home_banner">
			<!-- START BIG BANNER -->
			<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/big_slider', $this->output_data);
			?>
			<!-- END BIG BANNER -->

			<div id="container-book">
				<!-- START BOOKING VIEW -->
				<?php
					$this->load->view('component/booking_search');
				?>
				<!-- END BOOKING VIEW -->
			</div>
		</div>
		<div class="clear"></div>

		<!-- START TOP DESTIONATION -->
		<div id="container_home_content_left" class="f_left">
			<?php
				$this->load->view('component/top_destination');
			?>
		</div>
		<!-- END TOP DESTIONATION -->

		<!-- START WHATS HOT & PROMO -->
		<div id="container_home_content_center" class="f_left">
			<!-- start whats hot -->
			<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/whats_hot', $this->output_data);
			?>
			<!-- end whats hot -->

			<!-- start promo -->
			<div id="container_promo">
				<?php
					$this->output_data['page_id'] = 0;
					$this->output_data['position'] = 'mid_2';
					$this->load->view('component/image_promo1', $this->output_data);
				?>
			</div>
			<!-- end promo -->
		</div>
		<!-- END WHATS HOT & PROMO -->

		<!-- START SOCIAL & NEWSLETTER -->
		<div id="container_home_content_right" class="f_left">
			<!-- start social -->
			<div class="social_container">
				<?php
					$this->load->view('component/social_follow');
				?>
			</div>
			<!--end social-->

			<!-- start newsletter -->
			<div class="newsletter_container">
				<?php
					$this->load->view('component/form_newsletter');
				?>
			</div>
			<!--end newsletter-->

		</div>
		<!-- END SOCIAL & NEWSLETTER -->
		<div class="clear"></div>

		<!-- START FOOT HOME -->
		<div class="container_foot_home">
			<?php
				$this->load->view('component/foot_media_1');
			?>
		</div>
		<div class="clear"></div>
		<!-- END FOOT HOME -->

	</div>
</div>
<!-- end body container -->