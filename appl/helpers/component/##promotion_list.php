


<div class="six clearfix">
<div class="promo-wrap clearfix">
					<?php
						if($country_id != "" && $city_id != ""){
							$sql_get_all_promo = $this->pages_model->get_promo_by_city($city_id);
							//echo $this->db->last_query();
						}
						elseif($country_id != ""){
							$sql_get_all_promo = $this->pages_model->get_condition_promo($country_id);
						}
						else{
							$sql_get_all_promo = $this->pages_model->get_all_promo();
						}
						if($sql_get_all_promo->num_rows() > 0){
							foreach($sql_get_all_promo->result() as $data_get_all_promo){
					?>

					<div class="promoCol border-gray clearfix">
						<img src="<?php echo (($data_get_all_promo->text_icon_1) ? $data_get_all_promo->text_icon_1 : $data_get_all_promo->def_icon_1); ?>" />

						<div class="promo-inner">
							<h4 class="thick"><?php echo (($data_get_all_promo->text_title) ? $data_get_all_promo->text_title : $data_get_all_promo->def_title);?></h4>
							<p><?php echo substr((($data_get_all_promo->text_teaser) ? $data_get_all_promo->text_teaser : $data_get_all_promo->def_teaser), 0, 200);?></p>
							<br/>
							<input class="btn_go lightbox" type="button" alt="<?php echo $data_get_all_promo->promo_id;?>" value="<?php echo lang('lets_go');?>">
						</div>
					</div>
						<?php
								}
							}
							else{
								echo '<p style="padding:20px 0px;">' . lang('promo_no_found') . '</p>';
							}
						?>

</div>

<!-- paginate js-->
<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/easypaginate.js"></script>
<script>
	jQuery(function($){
		$('ul#will_paginate').easyPaginate({
			step:8
		});
		var country = $('select[name="country"]').val();

		$('select[name="country"]').change(function(){
			var nilai = $(this).val();
			var country = $('select[name="country"]').val();
			var country_name = $('select[name="country"] option:selected').data('foo');
			$('select[name="city"]').empty();

			$('select[name="city"]')
			  .prev('span')
			  .remove()
			  .end()
			.before('<span style="width: 116px; -moz-user-select: none;">Select City</span>');

			if(nilai != ""){
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>ajax",
					data: {act: "get_city_by_country", country_id: nilai}
				}).done(function( msg ) {
					$('select[name="city"]').html(msg);
				});

				window.location='<?php echo base_url('promotion/promo_hotel');?>' + country + '/' + country_name + '';

			} else {
			window.location='<?php echo base_url('promotion/promo_hotel/');?>';
			}
		});

		/* Filenya Ajax Tambahan Di bawah sinih.. */
		if(country !=""){
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url();?>ajax",
			data: {act: "get_city_by_country", country_id: country}
			}).done(function( msg ) {
				$('select[name="city"]').html(msg);
			});
		}
		/* Sampai sinih */

		$('select[name="city"]').change(function(){
			//var nilaicity = $(this).val();
			var country = $('select[name="country"]').val();
			var country_name = $('select[name="country"] option:selected').data('foo');
			var city = $('select[name="city"]').val();
			var city_name = $('select[name="city"] option:selected').data('foo');
			if(city != "" && country !=""){
				window.location='<?php echo base_url('promotion/promo_hotel');?>' + country + '/' + country_name + '/' + city + '/' + city_name + '';
			}else{
			window.location='<?php echo base_url('promotion/promo_hotel');?>' + country + '/' + country_name + '';
			//alert('Failed send City');
			}
		});
	});
</script>

<!-- paginate js-->


