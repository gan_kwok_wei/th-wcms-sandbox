	<div id="content-container">
		<div id="home_banner">
			<!-- START BIG BANNER -->
			<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/big_slider', $this->output_data);
			?>
			<!-- END BIG BANNER -->

			<div id="container-book">
				<!-- START BOOKING VIEW -->
				<?php
					$this->load->view('component/booking_search');
				?>
				<!-- END BOOKING VIEW -->
			</div>
		</div>
		<div class="clear"></div>

		<div id="promotion_container">
			<script>
				$(document).ready(function(){
					$('.lightbox').click(function(){
						var alt = $(this).attr('alt');
						var screenTop = $(document).scrollTop();

						$('.box_booking_search').hide();

						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>ajax",
							data: { act: "get_title_promo", promo_id: alt }
						}).done(function( msg ) {
							$('.box_content_text_title h2').html(msg);
						});

						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>ajax",
							data: { act: "get_content_promo", promo_id: alt }
						}).done(function( msg ) {
							$('.this_content_promo').html(msg);
						});

						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>ajax",
							data: { act: "get_image_promo", promo_id: alt }
						}).done(function( msg ) {
							$('.this_image_promo').html(msg);
						});

						var doc_tinggi = $(document).height();
						var tinggi = $(window).height();
						var lebar = $(window).width();
						var tinggi_box = $('.box').height();
						var lebar_box = $('.box').width();
						var top = ((tinggi - tinggi_box)/2)+screenTop;
						var left = (lebar - lebar_box)/2;

						$('.backdrop').css({'height' : doc_tinggi + 'px'});
						$('.backdrop, .box').animate({'opacity':'.50'}, 300, 'linear');
						$('.box').animate({'opacity':'1.00'}, 300, 'linear');
						$('.backdrop, .box').css({'display' : 'block'});
						$('.box').css({'top':top + 'px', 'left':left + 'px'})
					});

					$('.close').click(function(){
						close_box();
					});

					$('.backdrop').click(function(){
						close_box();
					});

					$('.box_booking_search').hide();

					$('.btn_book_box').click(function(){
						$('.box_promo_image').hide();
						$('.box_booking_search').show();
					});

					$('.box_booking_close').click(function(){
						$('.box_promo_image').show();
						$('.box_booking_search').hide();
					});

				});

				function close_box()
				{
					$('.backdrop, .box').animate({'opacity':'0'}, 300, 'linear', function(){
						$('.backdrop, .box').css('display', 'none');
					});
				}
			</script>

			<div class="promotion">
				<div class="promotion_title">
					<h1 class="promo_title f_left"><?php echo lang('promo_promotion');?></h1>
					<div class="share f_right">
						<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style ">
						<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
						<a class="addthis_counter addthis_pill_style"></a>
						</div>
						<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
						<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4faa1a491ffb4f02"></script>
						<!-- AddThis Button END -->
					</div>
					<div class="clear"></div>
				</div>
				<div class="promotion_content_container">
					<?php
						$this->load->view('component/promotion_list');
					?>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- end body container -->

<div class="backdrop"></div>
<div class="box">
	<div class="box_title">
		<span><?php echo lang('promo_promotion');?></span>
		<div class="close"></div>
	</div>
	<div class="box_content">
		<div class="box_left f_left">
			<div class="this_image_promo">
			</div>
			<div class="box_booking_search">
				<a href="javascript:void(0);" class="box_booking_close">X</a>
				<?php
					$this->load->view('component/booking_search');
				?>
			</div>
		</div>
		<div class="box_content_text f_left">
			<?php
				$this->load->view('component/promotion_popup');
			?>
		</div>
		<div class="clear"></div>
	</div>
</div>