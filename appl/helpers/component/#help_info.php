	<div id="content-container">
		<div id="corporate_container_left" class="f_left">
			<div id="corporate_container-book">
				<!-- START BOOKING VIEW -->
				<?php
					$this->load->view('component/booking_search.php');
				?>
				<!-- END BOOKING VIEW -->
			</div>
			
			<!--div id="contact_us_medium_container">
				<?php
					$this->load->view('component/contact_us_medium.php');
				?>
			</div-->
			
			<!-- start promo -->
			<div id="container_promo_medium">
				<?php
					$this->output_data['page_id'] = 6;
					$this->output_data['position1'] = 'left_1';
					$this->output_data['position2'] = 'left_2';
					$this->load->view('component/image_promo_medium.php', $this->output_data);
				?>
			</div>
			<!-- end promo -->
			<div class="container_newsletter_medium">
				<!-- START NEWSLETTER MEDIUM -->
				<?php
					$this->load->view('component/newsletter_medium.php');
				?>
				<!-- END NEWSLETTER MEDIUM -->
			</div>
		</div>
		
		<div id="corporate_container_right" class="f_left">
			<div class="top_page">
				<div class="bread_crumb f_left">
					<?php echo '<a href="' . base_url() . '">' . lang('home') . '</a> > <a href="' . base_url('help_info') . '">' . lang('help_info') . '</a> > ' . lang('help_' . $sub_about);?>
				</div>
				<div class="share f_right">
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
					<a class="addthis_counter addthis_pill_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
					<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4faa1a491ffb4f02"></script>
					<!-- AddThis Button END -->
				</div>
				<div class="clear"></div>
				
				<div id="about_content_container">
					<div class="menu_red">
						<div class="menu_red_left f_left"></div>
						<div class="menu_red_center f_left">
							<div class="menu_red_content4 f_left">
								<h1><?php echo lang('help_info');?></h1>
							</div>
							<div class="menu_red_content3 f_left">
								<ul>
									<li>
										<a href="<?php echo base_url('help_info/contact_us');?>" class="<?php echo ($sub_about == "contact_us" ? "selected" : "");?>"><?php echo lang('help_contact_us');?></a>
									</li>
									<li>
										<a href="<?php echo base_url('help_info/booking_cancelation');?>" class="<?php echo ($sub_about == "booking_cancelation" ? "selected" : "");?>"><?php echo lang('help_booking_cancelation');?></a>
									</li>
									<li>
										<a href="<?php echo base_url('help_info/terms_condition');?>" class="<?php echo ($sub_about == "terms_condition" ? "selected" : "");?>"><?php echo lang('help_terms_condition');?></a>
									</li>
									<li>
										<a href="<?php echo base_url('help_info/fee_hotel_schedule');?>" class="<?php echo ($sub_about == "fee_hotel_schedule" ? "selected" : "");?>"><?php echo lang('help_fee_hotel_schedule');?></a>
									</li>
									<li>
										<a href="<?php echo base_url('help_info/faq');?>" class="<?php echo ($sub_about == "faq" ? "selected" : "");?>"><?php echo lang('help_faq');?></a>
									</li>
								</ul>
							</div>
							<div class="clear"></div>
						</div>
						<div class="menu_red_right f_left"></div>
						<div class="clear"></div>
					</div>
					
					<div id="about_content_detail_container">
						<?php
							$this->load->view('component/' . $sub_about);
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
	</div>
</div>
<!-- end body container -->