			<section id="billboard" class="slider-wrapper theme-default">
				<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/big_slider', $this->output_data);
				?>
				<!-- END BIG BANNER -->
			</section>




<section id="container" class="container six clearfix">
<div class="col4 first">
	<?php
		$get_content = $this->pages_model->get_content('41');
		$data_get_content = $get_content->row();

		//update hits
		$set_content_hits = $this->pages_model->set_content_hits('41');
	?>
</div>


<!--tab respond container-->

<div style="margin-top:20px;">

	<?php
		if($data_get_content->text_content){
			$head_text = $data_get_content->text_content;
		}
		else{
			$head_text = $data_get_content->def_content;
		}
		if($head_text != ""){
			echo '<p>' . $head_text . '</p>';
		}
	?>
			<?php
			$this->load->view('component/' . $sub_about);
			?>
</div>
			<!--tab respond container-->
</div>

		<div class="col2" style="background-color: rgba(241, 244, 244, 0.36); color: rgb(35, 35, 35); border-color: rgb(35, 35, 35);">
	<div class="inner">
			<!--tabnav -->

			<ul>
				<li class="selected">
					<a href="<?php echo base_url('about_us/management_team');?>" class="<?php echo ($sub_about == "management_team" ? "selected" : "");?>"><?php echo lang('about_management_team');?></a>
				</li>
				<li>
					<a href="<?php echo base_url('about_us/shareholders');?>" class="<?php echo ($sub_about == "shareholders" ? "selected" : "");?>"><?php echo lang('about_shareholders');?></a>
				</li>
				<li>
					<a href="<?php echo base_url('about_us/career');?>" class="<?php echo ($sub_about == "career" ? "selected" : "");?>"><?php echo lang('about_career');?></a>
				</li>

			</ul>
</div>
			<!--end tabnav-->

			<div id="contact_us_medium_container">
				<?php
					$this->load->view('component/contact_us_medium.php');
				?>
			</div>

			<!-- start promo -->
			<div id="container_promo_medium">
				<?php
					$this->output_data['page_id'] = 5;
					$this->output_data['position1'] = 'left_1';
					$this->output_data['position2'] = 'left_2';
					$this->load->view('component/image_promo_medium.php', $this->output_data);
				?>
			</div>

		</div>
</section>


<!-- end body container -->