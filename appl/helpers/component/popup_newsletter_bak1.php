<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script type="text/javascript">
	$(document).ready(function() {
		$('#newsletter_result').hide();
		
		$('#btn_newsletter').click(function() {
			
			var n_email = $('input[name="e_news_email"]').val();
			$('input[name="news_email"]').val(n_email);

		});

		$( "#news_birth" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "1920:2003",
			dateFormat: "yy-mm-dd"
		});

//		$("#newsletter_form").ajaxForm({url: '<?php echo str_replace($this->config->item('url_suffix'), "", site_url("ajax")); ?>', type: 'post'})

		$('#btn_submit').click( function() {
//			alert('cek');
			$('.input_error').addClass('input_light');
			$('.input_error').removeClass('input_error');
			$('.error_span').hide();
			
			var news_email = $('input[name="news_email"]').val();
			var news_fname = $('input[name="news_fname"]').val();
			var news_lname = $('input[name="news_lname"]').val();
			var news_gender = $('input[name="news_gender"]:checked').val();
			var news_country_id = $('select[name="news_country_id"]').val();
			var news_birth = $('input[name="news_birth"]').val();
			var news_phone = $('input[name="news_phone"]').val();
			
			var next = '1';

			if($.trim(news_email) == ""){
				$('input[name="news_email"]').addClass('input_error');
				$('input[name="news_email"]').removeClass('input_light');
				$('.error_email').show();
				var next = '0';
			}
			if($.trim(news_fname) == ""){
				$('input[name="news_fname"]').addClass('input_error');
				$('input[name="news_fname"]').removeClass('input_light');
				$('.error_name').show();
				var next = '0';
			}
			if($.trim(news_gender) == ""){
				$('.error_gender').show();
				var next = '0';
			}
			if($.trim(news_country_id) == ""){
				$('.error_hotel_news').show();
				var next = '0';
			}
			if($.trim(news_birth) == ""){
				$('.error_hotel_dob').show();
				var next = '0';
			}
			
			if(next == '1') {

				$('#btn_submit').attr('disabled', '');
				$('.loading_img').show();
				
				$.ajax({
					type: "POST",
					url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("ajax")); ?>",
					data: "act=insert_subscribe&news_email=" + news_email + 
							"&news_fname=" + news_fname + 
							"&news_lname=" + news_lname + 
							"&news_gender=" + news_gender + 
							"&news_country_id=" + news_country_id + 
							"&news_birth=" + news_birth + 
							"&news_phone=" + news_phone,
					success : function(data) {
						$('.loading_img').hide();
						//alert(data);
						if(data == '1'){
							$('#newsletter_form')[0].reset();
							$('#newsletter_form').hide();
							$('#newsletter_result').html('<?php echo lang('e_news_success'); ?>');
							$('#newsletter_result').show();
						//	alert('Successfull sign up e-newsletter');
							
						} else {
							$('#newsletter_form').hide();
							$('#newsletter_result').html('<?php echo lang('e_news_failed'); ?>');
							$('#newsletter_result').show();
						//	alert('Failed sign up' + msg);
							
						}
						
					}
				});
				
			}
			
			return false;
			
		});
		
	});

	function close_box() {
		$( "#news_birth" ).datepicker( "hide" );
		$('.backdrop, .box').animate({'opacity':'0'}, 300, 'linear', function(){
			$('.backdrop, .box').css('display', 'none');
		});
	}
	
	function init_popup() {
		$('#newsletter_result').hide();
		$('#newsletter_form').show();
		$('#newsletter_form')[0].reset();
	}
	
</script>

<div id="popup_newsletter" class="box modal hide fade in" style="width:450px;">
	<div class="modal-header">
		<span><?php echo lang('e_news_signup');?></span>
		<a class="close" data-dismiss="modal" style="margin-top: 0px;"></a>
	</div>
	<div class="modal-body">
		<div id="newsletter_result" style="display: none; text-align: center; font-size: 16px; min-height: 120px; padding-top: 80px;"></div>
		<!-- form name="inquiry_form" action="" method="POST" onsubmit="return send_newsletter_form();" id="newsletter_form"-->
		<form name="inquiry_form" id="newsletter_form">
			<table style="font-size:12px;">
				<tr>
					<td style="width:140px;"><?php echo lang('e_news_email');?>: <span class="red">*</span></td>
					<td>
						<input type="text" name="news_email" class="input_light" style="width:280px;" />
							<span class="span_input_error error_span error_email" style="display:none;">
								<?php echo lang('false_field_email'); ?>
							</span>
					</td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_fname');?>: <span class="red">*</span></td>
					<td>
						<input type="text" name="news_fname" class="input_light" style="width:280px;" />
						<span class="span_input_error error_span error_name" style="display:none;">
							<?php echo lang('false_field_name'); ?>
						</span>
					</td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_lname');?>:</td>
					<td>
						<input type="text" name="news_lname" class="input_light" style="width:280px;" />
					</td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('e_news_gender');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;" nowrap="nowrap">
						<input type="radio" id="news_gender_m" name="news_gender" value="m" checked="checked" /><label for="news_gender_m" style="display: inline-table;"><?php echo lang('e_news_gender_m');?></label>&nbsp;&nbsp;&nbsp;
						<input type="radio" id="news_gender_f" name="news_gender" value="f" /> <label for="news_gender_f" style="display: inline-table;"><?php echo lang('e_news_gender_f');?></label>
						<span class="span_input_error error_span error_gender" style="display:none;">
							<?php echo lang('false_field'); ?>
						</span>
					</td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('corp_country');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;">
						<select class="myselect" name="news_country_id" style="width:150px;">
							<option value=""><?php echo lang('corp_sel_country');?></option>
							<?php
								$query_country = $this->pages_model->get_country_all();
								foreach($query_country->result() as $data_country){
									echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
								}
							?>
						</select>
						<span class="span_input_error error_span error_hotel_news" style="display:none;">
							<?php echo lang('false_field'); ?>
						</span>
					</td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_birth');?>: <span class="red">*</span></td>
					<td>
						<input type="text" id="news_birth" name="news_birth" class="date_clean" style="" />
						<span class="span_input_error error_span error_hotel_dob" style="display:none;">
							<?php echo lang('false_field_date'); ?>
						</span>
					</td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_phone');?>:</td>
					<td>
						<input type="text" name="news_phone" class="input_light" style="width:280px;" />
					</td>
				</tr>
				<tr>
					<td>
						<br/><br/>
						<input id="btn_submit" class="btn_see_detail btn_book_box f_left" type="submit" value="<?php echo lang('sign_up');?>" >
						<span style="display:none;" class="loading_img f_left">
							<img  src="/tune_media/images/img/loader.gif" /> Sending Data...
						</span>
						<div class="clean"></div>
					</td>
				</tr>
			</table>
		</form>
		<div id="dialogsendinquiry" title="Message" style="z-index:999;">
		<p><?php echo lang('email_gak_valid');?></p>
	</div>
	</div>
</div>