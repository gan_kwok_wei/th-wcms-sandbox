<div id="container" class="clearfix">

	<div class="six clearfix" style="display:block; margin-bottom:10px;">
		<h1><?php echo lang('about_us');?></h1>	
		<div id="container_promo_medium">
				<?php
					$this->output_data['page_id'] = 5;
					$this->output_data['position1'] = 'left_1';
					$this->output_data['position2'] = 'left_2';
					$this->load->view('component/image_promo_medium.php', $this->output_data);
				?>
		</div>
	</div>
	<div class="clear"></div>
	<div class-"six clearfix" style="display:block; margin-top:10px">
		<!--breadcrumbs-->
		<div class="breadcrumbs clearfix" style="display:block;"><?php echo '<a href="' . base_url() . '">' . lang('home') . '</a> > <a href="' . base_url('about_us') . '">' . lang('about_us') . '</a> > ' . lang('about_' . $sub_about);?>
		</div>
		<!--page title header-->
		<h1><?php echo lang('about_us');?></h1>
		<!--about us tabs-->
		<ul>
			<li>
			<a href="<?php echo base_url('about_us/management_team');?>" class="<?php echo ($sub_about == "management_team" ? "selected" : "");?>"><?php echo lang('about_management_team');?></a>
			</li>
			<li>
			<a href="<?php echo base_url('about_us/shareholders');?>" class="<?php echo ($sub_about == "shareholders" ? "selected" : "");?>"><?php echo lang('about_shareholders');?></a>
			</li>						
			<li>
				<a href="<?php echo base_url('about_us/career');?>" class="<?php echo ($sub_about == "career" ? "selected" : "");?>"><?php echo lang('about_career');?></a>
			</li>
			<li>
				<a href="<?php echo base_url('about_us/concept');?>" class="<?php echo ($sub_about == "concept" ? "selected" : "");?>"><?php echo lang('about_concept');?></a>
			</li>
			<li>
				<a href="<?php echo base_url('about_us/media');?>" class="<?php echo ($sub_about == "media" ? "selected" : "");?>"><?php echo lang('about_media');?></a>
			</li>
		</ul>
	</div>
	<div class="clear"></div>
	<div class="six clearfix">
		<!--load sub content about us-->
		<?php
		$this->load->view('component/' . $sub_about);
		?>
	</div>

</div>

<!-- end body container -->