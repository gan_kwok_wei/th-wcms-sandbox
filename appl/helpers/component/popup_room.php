<script type="text/javascript">
<!--
	$(document).ready(function(){
		$('.detail_room').click(function(){
			var alt = $(this).attr('alt');
			var screenTop = $(document).scrollTop();

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('ajax/room_detail');?>",
				data: { room_id: alt }
			}).done(function( data ) {
				if(data.result > 0) {
					$('.this_image_room img').attr('src', data.image);
					$('.box_content_text_title_room h2').html(data.title);
					$('.this_content_room').html(data.feature_text);

					var doc_tinggi = $(document).height();
					var tinggi = $(window).height();
					var lebar = $(window).width();
					var tinggi_box = $('.box_room').height();
					var lebar_box = $('.box_room').width();
					var top = ((tinggi - tinggi_box)/2) + screenTop;
					var left = (lebar - lebar_box)/2;

					$('.backdrop').css({'height' : doc_tinggi + 'px'});
					$('.backdrop, .box_room').animate({'opacity':'.50'}, 300, 'linear');
					$('.box_room').animate({'opacity':'1.00'}, 300, 'linear');
					$('.backdrop, .box_room').css({'display' : 'block'});
					//$('.box_room').css({'top':top + 'px', 'left':left + 'px'});
					$('.box_room').css({'top': top + 'px', 'left': left + 'px', 'z-index':'99999'});
//					alert(msg.feature_text);
				} else {
					alert('Request failed!');
				}
//				$('.box_content_text_title_room h2').html(msg);
			});

		});

		$('.close_room').click(function(){
			close_box_room();
		});
/*
		$('.backdrop').click(function(){
			close_box_room();
		});
*/
	});

	function close_box_room() {
		$('.backdrop, .box_room').animate({'opacity':'0'}, 300, 'linear', function(){
			$('.backdrop, .box_room').css('display', 'none');
		});
	}
//-->
</script>

<div class="box_room">
	<div class="box_title_room">
		<span><?php echo $hotel_title; ?></span>
		<div class="close_room"></div>
	</div>
	<div class="box_content_room">
		<div class="box_left_room f_left">
			<div class="this_image_room">
				<img src="" width="265px"/>
			</div>
		</div>
		<div class="box_content_text_room f_left">
			<div class="box_content_text_title_room">
				<h2></h2>
			</div>
			<div class="box_content_texts_room">
				<div class="this_content_room"></div>
				<br/>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
