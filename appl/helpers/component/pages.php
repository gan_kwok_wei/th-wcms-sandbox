<?php
			$get_content = $this->pages_model->get_content(str_replace('content_id_', '', $menu_id));
//			echo $this->db->last_query();
			if($get_content->num_rows() == 0) {
				redirect('lost');
				exit;
			}
//			echo $this->db->last_query();
			$data_get_content = $get_content->row();
?>	
	<div id="content-container">
		<div id="corporate_container_left" class="f_left">
			<div id="corporate_container-book">
				<!-- START BOOKING VIEW -->
				<?php
					$this->load->view('component/booking_search.php');
				?>
				<!-- END BOOKING VIEW -->
			</div>
			
			<div id="contact_us_medium_container">
				<?php
					$this->load->view('component/contact_us_medium.php');
				?>
			</div>
			
			<!-- start promo -->
			<div id="container_promo_medium">
				<?php
					$this->output_data['page_id'] = 7;
					$this->output_data['position1'] = 'left_1';
					$this->output_data['position2'] = 'left_2';
					$this->load->view('component/image_promo_medium.php', $this->output_data);
				?>
			</div>
			<!-- end promo -->
			<div class="container_newsletter_medium">
				<!-- START NEWSLETTER MEDIUM -->
				<?php
					$this->load->view('component/newsletter_medium.php');
				?>
				<!-- END NEWSLETTER MEDIUM -->
			</div>
		</div>
		
		
		<div id="corporate_container_right" class="f_left">
			<div class="top_page">
				<div class="bread_crumb f_left">
					<?php
						$this_page = $this->uri->segment(3);
						$sql = "SELECT menu_id, menu_name, title FROM menu WHERE menu_name = '" . $this_page . "'";
						$query = $this->db->query($sql);
						$data_menu = $query->row();
						
						echo '<a href="' . base_url() . '">' . lang('home') . '</a> > ';
						echo '<a href="' . base_url($data_menu->menu_name) . '">' . $data_menu->title . '</a> > ';
						/* $get_menu = $this->pages_model->get_menu($menu_id);
						$data_get_menu = $get_menu->row();
						
						$get_menu1 = $this->pages_model->get_menu($data_get_menu->parent_id);
						$data_get_menu1 = $get_menu1->row();
						
						if($data_get_menu1->parent_id == '0'){
							echo $data_get_menu1->title . ' > ';
						}
						else{
							$get_menu2 = $this->pages_model->get_menu($data_get_menu1->parent_id);
							$data_get_menu2 = $get_menu2->row();
							
							echo $data_get_menu2->title . ' > ';
							
							echo $data_get_menu1->title . ' > ';
						} */
						
						echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);
					?>
				</div>
				<div class="share f_right">
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
					<a class="addthis_counter addthis_pill_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
					<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4faa1a491ffb4f02"></script>
					<!-- AddThis Button END -->
				</div>
				<div class="clear"></div>
				
				<div id="corporate_content_container">
					<div class="page_content">
						<?php
							
							echo '<h1>' . (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title) . '</h1>';
							echo (($data_get_content->text_content) ? $data_get_content->text_content : $data_get_content->def_content);
						?>
						
						<!--div class="inquiry_form_content">
							
						</div-->
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
	</div>
</div>
<!-- end body container -->