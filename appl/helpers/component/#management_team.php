<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/easypaginate.js"></script>
<script>
	jQuery(function($){
		$('ul#will_paginate').easyPaginate({
			step:4
		});
	});
</script>

<div class="management_team">
	<?php
		$get_content = $this->pages_model->get_content('41');
		$data_get_content = $get_content->row();
		
		//update hits
		$set_content_hits = $this->pages_model->set_content_hits('41');
	?>
	<h1><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></h1>
	
	<?php
		if($data_get_content->text_content){
			$head_text = $data_get_content->text_content;
		}
		else{
			$head_text = $data_get_content->def_content;
		}
		if($head_text != ""){
			echo '<p style="padding:10px; font-size:12px;">' . $head_text . '</p>';
		}
	?>
	
	<ul id="will_paginate">
		<?php
			$get_content_list = $this->pages_model->get_content_list('7');
			if($get_content_list->num_rows() > 0){
				foreach($get_content_list->result() as $data_get_content_list){
		?>
					<li>
						<h2><?php echo (($data_get_content_list->text_title) ? $data_get_content_list->text_title : $data_get_content_list->def_title);?></h2>
						<div class="mt_image f_left">
							<img src="<?php echo (($data_get_content_list->text_icon) ? $data_get_content_list->text_icon : $data_get_content_list->def_icon);?>" />
						</div>
						<div class="mt_text f_left">
							<p><?php echo (($data_get_content_list->text_teaser) ? $data_get_content_list->text_teaser : $data_get_content_list->def_teaser);?></p>
						</div>
						<div class="clear"></div>
					</li>
		<?php
				}
			}
		?>
	</ul>
	<div class="pagination" id="in_pagination">
		
	</div>
</div>