<?php

	$get_page_banner_page = $this->pages_model->get_page_banner_pos($page_id, $position);

	$data_get_page_banner_page = $get_page_banner_page->row();

?>

<div class="image_promo clearfix">

	<?php

		if($get_page_banner_page->num_rows() > 0){

			if($data_get_page_banner_page->text_link){

				$link = $data_get_page_banner_page->text_link;

			}

			else{

				$link = $data_get_page_banner_page->def_link;

			}



			if($link == ""){

				echo '<img src="' . (($data_get_page_banner_page->text_image) ? $data_get_page_banner_page->text_image : $data_get_page_banner_page->def_image) . '" />';

			}

			else{

				echo '<a href="' . $link . '"><img src="' . (($data_get_page_banner_page->text_image) ? $data_get_page_banner_page->text_image : $data_get_page_banner_page->def_image) . '" /></a>';

			}

		}

	?>

</div>