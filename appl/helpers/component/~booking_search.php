<div class="booking">
	<h1 id="hello"><?php echo lang('book_hello');?></h1>
	<form>
		<ul>
			<li class="booking_ul">
				<select id="" class="black_light">
					<option value=""><?php echo lang('book_city');?></option>
					<?php
						$query_country = $this->pages_model->get_country();
						foreach($query_country->result() as $data_country){
							echo '<optgroup label="' . $data_country->name . '">';
							$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
							if($query_hotel_by_country->num_rows() > 0){
								foreach($query_hotel_by_country->result() as $data_hotel_by_country){
									echo "<option value=\"" . $data_hotel_by_country->hotel_id . "\">" . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . "</option>";
								}
							}
							echo '</optgroup>';
						}
					?>
				</select>
			</li>
			<li class="booking_ul"  style="margin-bottom:15px;">
				<input type="date" name="arrival" placeholder="<?php echo lang('book_checkin');?>" class="no_border date d_first d_search" min="<?php echo date("Y-m-d", mktime(0, 0, 0, date('m'), date('d')+1, date('Y')));?>"  />
				<input type="date" name="departure" data-value="1" placeholder="<?php echo lang('book_ckeckout');?>" class="rightout no_border date d_last d_search" />
			</li>
			<li class="booking_ul">
				<div class="f_left">
					<label><?php echo lang('book_rooms');?></label>
					<select class="small myselect" id="btnAdd">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="f_left" style="">
					<label><?php echo lang('book_adults');?></label>
					<select class="small satu myselect">
						<option>1</option>
						<option>2</option>
						<option>3</option>
					</select>
				</div>
			</li>
			<li class="booking_ul" style="">
				<input type="text" placeholder="<?php echo lang('book_promotion_code');?>" class="my_input_style" style="width:257px;margin-top:20px;"/>
			</li>
			<li class="booking_ul">
				<input type="button" class="btn_search" onclick="" value="<?php echo lang('book_search');?>" style="margin-top:5px;"/>
			</li>
			<li class="booking_ul">
				<a href="#">&gt; <?php echo lang('booking_managed_confirm');?></a>	
			</li>
			</ul>
	</form>
</div>