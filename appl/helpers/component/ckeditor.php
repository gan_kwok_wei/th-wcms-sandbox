<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Ckeditor</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<script src="<?php echo base_url();?>assets/js/ckeditor/ckeditor.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/ckfinder/ckfinder.js"></script>

</head>
<body>
<?php 
	// fungsi untuk membuat text editor dengan nama my_editor
	//configure base path of ckeditor folder 
	$this->ckeditor->basePath = base_url() . 'assets/js/ckeditor/';
	$this->ckeditor->config['toolbar'] = 'Full';
	$this->ckeditor->config['language'] = 'en';
	//configure ckfinder with ckeditor config 
	$this->ckfinder->SetupCKEditor($this->ckeditor, 'assets/js/ckfinder');
	echo $this->ckeditor->editor("my_editor");

?>
</body>
</html>
