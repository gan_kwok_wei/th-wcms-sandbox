<script type="text/javascript">
<!--

$(window).load(function() {
	//BIG SLIDER BANNER
	$('#slider_home').nivoSlider({
		directionNav: false,
		controlNav: true,
		controlNavThumbs:true,
		effect : 'fade'
	});

	//WHATS HOT BANNER
	$('#slider_hot').nivoSlider({
		directionNav: false,
		controlNav: true,
		effect : 'fade'
	});

	//SELECT UNIFORM
	$(".myselect").uniform();
});

//-->
</script>

<section id="container">
<div class="promo-banner"><!-- START BIG BANNER -->
			<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/big_slider', $this->output_data);
			?>
			<!-- END BIG BANNER -->
</div>

<div class="clear"></div>
<div class="container clearfix">
			<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/promotion_list', $this->output_data);
			?>
	
</div>
<div class="gutter"></div>
</section>

