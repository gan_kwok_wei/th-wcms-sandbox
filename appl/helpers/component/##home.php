

<div id="container" class="clearfix">
		<!--Start / home fist row -->
<div class="six clearfix" style="display:block;">
	<div id="booker">
		<!-- START BOOKING VIEW -->
		<?php
		$this->load->view('component/booking_search');
		?>
		<!-- END BOOKING VIEW -->
	</div>
	<div class="col6" style="display:block; margin-bottom:10px;">

		<!-- START BIG BANNER -->
		<?php
		$this->output_data['page_id'] = 0;
		$this->load->view('component/big_slider', $this->output_data);
		?>
		<!-- END BIG BANNER -->

	</div>
</div>

<!--End / home first row -->

		<div id="subpromo-wrap" class="six clearfix">

						<div class="col2 first">
						<!--top destinations -->
						<?php
							$this->load->view('component/top_destination');
						?>
						<!--end /top destinations -->
						</div>

						<div class="col2">
							<!--whats hot-->
							<div style="float: left; margin-bottom:10px;">
								<?php
								$this->output_data['page_id'] = 0;
								$this->load->view('component/whats_hot', $this->output_data);
							?>
						</div>
							<!--subpromo -->

							<?php
								$this->output_data['page_id'] = 0;
								$this->output_data['position'] = 'mid_2';
								$this->load->view('component/image_promo1', $this->output_data);
							?>
						</div>

						<div class="col2">
							<!-- start social -->

							<?php
								$this->load->view('component/social_follow');
							?>
							<br class="clear">
							<?php
							$this->load->view('component/form_newsletter');
							?>

							<!--end social-->
						</div>
		</div>

		<div class="clear"></div>

		<!-- START FOOT HOME -->
		<div id="th-footer">
			<?php
				$this->load->view('component/foot_media_1');
			?>
		</div>

		<!-- END FOOT HOME -->


</div>