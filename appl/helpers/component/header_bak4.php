<?php 
	define('ASSET_PATH', base_url().'assets/');
	if(!isset($style_extras)) { $style_extras = array(); }
	if(!isset($js_extras)) { $js_extras = array(); }
	
	$ge_page = $this->config->item('ge_page');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7]>	<html class="no-js ie6 oldie" lang="en" itemscope itemtype="http://schema.org/Article">	<![endif]-->
<!--[if IE 7]>		<html class="no-js ie7 oldie" lang="en" itemscope itemtype="http://schema.org/Article">	<![endif]-->
<!--[if IE 8]>		<html class="no-js ie8 oldie" lang="en" itemscope itemtype="http://schema.org/Article">	<![endif]-->
<!--[if IE 9]>		<html class="no-js ie9" lang="en" itemscope itemtype="http://schema.org/Article">		<![endif]-->
<!--[if (gt IE 9)|!(IE)]<!-->
<html style="" class=" js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" itemscope="" itemtype="http://schema.org/Article" lang="en">
<!--<![endif]-->

	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="TuneHotels.com, Tune Hotels, Budget Hotels, Boutique Hotels<?php echo (isset($keywords)) ? (', ' . $keywords) : '' ; ?>" />
		<meta name="description" content="Tune Hotel<?php echo (isset($description)) ? (' - ' . $description) : '' ; ?>" />
		
		<?php foreach ($style_extras as $style_extra) { echo '<link rel="stylesheet" type="text/css" media="screen" href="' . $style_extra . '" />'; } ?>
		
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery-1.8.1.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/modernizr.js"></script>
		<?php foreach ($js_extras as $js_extra) { echo '<script type="text/javascript" src="' . $js_extra . '"></script>'; } ?>
		
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/custom.js"></script>
		
		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "ur-bc6e7663-6ee9-a9fc-9231-1c3420c75ce3"});</script>
		
		<script type="text/javascript">
		
			$(document).ready(function(){
				//change language
				$('#for_lang>ul>li>a.dd-option').click(function() {
					var country = $('#for_country>div>input.dd-selected-value').val();
					var lang = $('#for_lang>div>input.dd-selected-value').val();
					if(country != ""){
						window.location='<?php echo "http://" . $_SERVER['HTTP_HOST'];?>/' + country + '/' + lang;
					} else {
						alert('Please select country first!');
					}
				});
				
			});
		</script>
		<title><?php echo ((isset($title)) ? ($title . ' | ') : '') . $this->config->item('site_title') ; ?></title>
		
	</head>
	<body>
	<!-- start body container -->
	<div id="body_container">
		<div id="header">
			<div id="header_top">
				<a href="<?php echo base_url();?>"><div id="logo"></div></a>
				<div id="header_nav">
					<div id="link_header">
						<a href="#" style="float:right"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/think_big.jpg" /></a>
					</div>
					<div id="header_menu">
						<div class="f_right">
							<form action="<?php echo base_url();?>search/" method="post">
								<input type="text" name="q" class="search_box" placeholder="<?php echo lang('search');?>" value="<?php echo ((isset($_POST['q'])) ? $_POST['q'] : ''); ?>"/>
							</form>
						</div>
						<div class="f_right">
							<!--select name="language_id" tabindex="2" class="f_left" id="for_lang">
								<option value="" data-imagesrc="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/flag/en.png"><?php echo lang('select_language');?></option>
								<?php
									$this_lang = $this->uri->segment(2);
									
									$tune_lang = $this->config->item('ge_language');
									foreach($tune_lang as $tune_langs){
										$tune_lang_key = array_keys($tune_lang, $tune_langs);
										echo '<option value="' . $tune_lang_key[0] . '" data-imagesrc="' . ASSET_PATH . 'themes/publish/' . $this->system_model->get_publish_theme() . '/images/assets/flag/' . $tune_lang_key[0] . '.png" ' . ($this_lang == $tune_lang_key[0] ? 'selected' : '') . ' >' . $tune_langs . '</option>';
									}
								?>
							</select-->
						</div>
						<div class="f_right">
							<!--select name="country_id" tabindex="1" class="f_left" id="for_country">
								<option value="" data-imagesrc="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/flag/en.png"><?php echo lang('select_country');?></option>
								<?php
									$this_country = $this->uri->segment(1);
									
									$tune_country = $this->config->item('ge_country');
									foreach($tune_country as $tune_countrys){
										$tune_countrys_key = array_keys($tune_country, $tune_countrys);
										echo '<option value="' . $tune_countrys_key[0] . '" data-imagesrc="' . ASSET_PATH . 'themes/publish/' . $this->system_model->get_publish_theme() . '/images/assets/flag/' . $tune_countrys_key[0] . '.png" ' . ($this_country == $tune_countrys_key[0] ? 'selected' : '') . ' >' . $tune_countrys . '</option>';
									}
								?>
							</select-->
							
							<div id="this_country_lang">
								<?php
									$full_session_country = $this->db->get_where('country', array('letter_code' => strtoupper($this->session->get_dec('country'))));
									$data_full_session_country = $full_session_country->row();
									
									$tune_lang = $this->config->item('ge_language');
									echo $data_full_session_country->name . ' ' . ucwords($tune_lang[$this->session->get_dec('lang')]);
								?>
							</div>
							
							<div class="backdrop1"></div>
							<div class="box1">
								<div class="box_title1">
									<span>Please Choose Your Country And Language</span>
									<div class="close1"></div>
								</div>
								<div class="box_content1">
									<div class="boc_cont_cont1">
										<ul id="list_language">
											<?php
												$get_country_list = $this->system_model->get_country_list();
												foreach($get_country_list->result() as $data_get_country_list){
											?>
													<li>
														<span><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/flag/<?php echo strtolower($data_get_country_list->letter_code);?>.png" style="width:16px; height: 11px;" width="16" height="11" />
														&nbsp;<?php echo $data_get_country_list->name;?></span>
														<ul>
															<?php
																$get_country_language = $this->system_model->get_country_language($data_get_country_list->country_id);
																foreach($get_country_language->result() as $data_get_country_language){
																	echo '<li><a href="http://' . $_SERVER['HTTP_HOST'] . '/' . strtolower($data_get_country_list->letter_code) . '/' . $data_get_country_language->key . '">' . $data_get_country_language->val . '</a></li>';
																}
															?>
														</ul>
													</li>
											<?php
												}
											?>
										</ul>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<div id="top_menu">
				<?php
					$this_page = (in_array($this->uri->segment(1), $this->system_model->get_registered_country(TRUE))) ?  $this->uri->segment(3) : $this->uri->segment(1);
					if(!$this_page){
						$this_page = "home";
					}
				?>
				<ul id="ul_top_menu">
					<?php
						$get_menu_list = $this->pages_model->get_menu_list();
						foreach($get_menu_list->result() as $data_get_menu_list){
							echo ' 
								<li>
									<a href="' . base_url($data_get_menu_list->menu_name) . '" class="' . $data_get_menu_list->menu_name . ' a_first ' . ($this_page == $data_get_menu_list->menu_name ? $data_get_menu_list->menu_name . "_hover" : "") . '">' . (($data_get_menu_list->text_title) ? $data_get_menu_list->text_title : $data_get_menu_list->def_title) . '</a>';
									
									//if hotel
									if($data_get_menu_list->menu_name == "hotel"){
										echo '<ul class="child">';
											$query_country = $this->pages_model->get_country();
											foreach($query_country->result() as $data_country){
												echo '<li><a href="' . base_url('hotel/hotels/' . $data_country->country_id . '/' . underscore(strtolower($data_country->name))) . '">' . (($data_country->text_title) ? $data_country->text_title : $data_country->def_title) . '</a>';
													$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
													if($query_hotel_by_country->num_rows() > 0){
														echo "<ul>";
														foreach($query_hotel_by_country->result() as $data_hotel_by_country){
															echo "<li>
																<a href=\"" . base_url('hotel/hotel_detail/') . $data_hotel_by_country->hotel_id . "/" . underscore(strtolower($data_hotel_by_country->hotel_name)) . "\">" . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . "</a>
															</li>";
														}
														echo "</ul>";
													}
												echo "</li>";
											}
										echo '</ul>';
									} else {
										$get_menu_list1 = $this->pages_model->get_menu_list($data_get_menu_list->menu_id);
										if($get_menu_list1->num_rows() > 0){
											echo '<ul class="child">';
											foreach($get_menu_list1->result() as $data_get_menu_list1){
												echo '<li>
													<!--a href="' . base_url($data_get_menu_list->menu_name . '/page/' . $data_get_menu_list1->menu_id . '/' . $data_get_menu_list1->menu_name) . '">' . $data_get_menu_list1->title . '</a-->
													<a href="' . base_url($data_get_menu_list1->link) . '">' . (($data_get_menu_list1->text_title) ? $data_get_menu_list1->text_title : $data_get_menu_list1->def_title) . '</a>';
													
													
													$get_menu_list2 = $this->pages_model->get_menu_list($data_get_menu_list1->menu_id);
													if($get_menu_list2->num_rows() > 0){
														echo '<ul>';
														foreach($get_menu_list2->result() as $data_get_menu_list2){
															echo '<li>
																<a href="' . base_url($data_get_menu_list->menu_name . '/page/' . $data_get_menu_list2->link) . '">' . (($data_get_menu_list2->text_title) ? $data_get_menu_list2->text_title : $data_get_menu_list2->def_title) . '</a>';
															echo '</li>';
														}
														echo '</ul>';
													}
													
												echo '</li>';
											}
											echo '</ul>';
										}
									}
							
							echo '
								</li>
							';
						}
					?>
					<!--li>
						<a href="<?php echo base_url('home');?>" class="home a_first <?php echo ($this_page == "home" ? "home_hover" : "");?>"><?php echo lang('home');?></a>
					</li>
					<li>
						<a href="<?php echo base_url('hotel');?>" class="hotel a_first <?php echo ($this_page == "hotel" ? "hotel_hover" : "");?>"><?php echo lang('our_hotel');?></a>
						<ul class="child">
							<?php
								$query_country = $this->pages_model->get_country();
								foreach($query_country->result() as $data_country){
									echo "<li><a href=\"" . base_url('hotel/hotels/') . $data_country->country_id . "/" . underscore(strtolower($data_country->name)) . "\">" . $data_country->name . "</a>";
										$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
										if($query_hotel_by_country->num_rows() > 0){
											echo "<ul>";
											foreach($query_hotel_by_country->result() as $data_hotel_by_country){
												echo "<li>
													<a href=\"" . base_url('hotel/hotel_detail/') . $data_hotel_by_country->hotel_id . "/" . underscore(strtolower($data_hotel_by_country->hotel_name)) . "\">" . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . "</a>
												</li>";
											}
											echo "</ul>";
										}
									echo "</li>";
								}
							?>
						</ul>
					</li>
					<li>
						<a href="<?php echo base_url('promotion');?>" class="promotion a_first <?php echo ($this_page == "promotion" ? "promotion_hover" : "");?>"><?php echo lang('promotions');?></a>
					</li>
					<li>
						<a href="<?php echo base_url('corporate');?>" class="corporate a_first <?php echo ($this_page == "corporate" ? "corporate_hover" : "");?>"><?php echo lang('corporate');?></a>
					</li>
					<li>
						<a href="<?php echo base_url('about_us');?>" class="about_us a_first <?php echo ($this_page == "about_us" ? "about_us_hover" : "");?>"><?php echo lang('about_us');?></a>
					</li>
					<li>
						<a href="<?php echo base_url('help_info');?>" class="help_info a_first <?php echo ($this_page == "help_info" ? "help_info_hover" : "");?>"><?php echo lang('help_info');?></a>
					</li-->
				</ul>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>