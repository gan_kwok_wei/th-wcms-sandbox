<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/easypaginate.js"></script>
<script>
	jQuery(function($){
		$('ul#will_paginate').easyPaginate({
			step:6
		});
	});
</script>

<div class="list_hotel">
	<h1><?php echo lang('list_hotel_all_hotel');?></h1>
	<div class="list_hotels">
		<ul id="will_paginate">
			<?php
				$query_hotel_by_country = $this->pages_model->get_hotel_by_country($country_id);
				if($query_hotel_by_country->num_rows() > 1){
					foreach($query_hotel_by_country->result() as $data_hotel_by_country){
			?>
						<li>
							<div class="lists_hotels">
								<h3><?php echo lang('list_hotel_tune_hotel');?><br/><?php echo (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title);?></h3>
								<img src="<?php echo (($data_hotel_by_country->text_icon) ? $data_hotel_by_country->text_icon : $data_hotel_by_country->def_icon);?>" width="211" height="116" style="margin-bottom:10px;">
								<input type="button" class="btn_see_detail" value="<?php echo lang('see_detail');?>" onclick="window.location='<?php echo base_url();?>hotel/hotel_detail/<?php echo $data_hotel_by_country->hotel_id;?>/<?php echo underscore(strtolower($data_hotel_by_country->hotel_name));?>'" />
							</div>
						</li>
			<?php
					}
				}
			?>
		</ul>
	</div>
	<div class="clear"></div>
	<div id="in_pagination">
		
	</div>
	
</div>