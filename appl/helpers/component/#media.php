<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/easypaginate.js"></script>
<script>
	jQuery(function($){
		$('ul#will_paginate').easyPaginate({
			step:6
		});
	});
</script>

<?php
	$get_content = $this->pages_model->get_content('45');
	$data_get_content = $get_content->row();
	
	//update hits
	$set_content_hits = $this->pages_model->set_content_hits('45');
?>
	
<div class="media">
	<!--h1><?php echo lang('about_media');?></h1-->
	<h1><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></h1>
	<?php
		if($data_get_content->text_content){
			$head_text = $data_get_content->text_content;
		}
		else{
			$head_text = $data_get_content->def_content;
		}
		if($head_text != ""){
			echo '<p style="padding:10px; font-size:12px;border-bottom: 1px solid #E1E1E1;">' . $head_text . '</p>';
		}
	?>
	
	<div class="media_left f_left">
		<h2><?php echo lang('about_medi_type');?></h2>
		<ul>
			<?php
				if($parent_id == ""){
					$get_content_category = $this->pages_model->get_content_category(11);
					$data_get_content_category = $get_content_category->row();
					$parent_id = $data_get_content_category->content_category_id;
				}
				$get_content_category = $this->pages_model->get_content_category(11);
				foreach($get_content_category->result() as $data_get_content_category){
					echo '<li><a href="' . base_url() . 'about_us/media/' . $data_get_content_category->content_category_id . '/' . $data_get_content_category->content_category_name . '" class="' . ($data_get_content_category->content_category_id == $parent_id ? 'selected' : '') . '">' . $data_get_content_category->title . '</a></li>';
				}
			?>
		</ul>
	</div>
	<div class="media_right f_left">
		<ul id="will_paginate">
			<?php
				$get_content_list = $this->pages_model->get_content_list($parent_id);
				if($get_content_list->num_rows() > 0){
					foreach($get_content_list->result() as $data_get_content_list){
			?>
						<li>
							<?php
								if($data_get_content_list->text_link){
									$link = $data_get_content_list->text_link;
								}
								else{
									$link = $data_get_content_list->def_link;
								}
								if($link != ''){
									echo '<a href="' . $link . '" target="_blank"><img class="media_vid_img f_left" src="'. (($data_get_content_list->text_icon) ? $data_get_content_list->text_icon : $data_get_content_list->def_icon) . '" /></a>';
								}
								else{
									echo '<img class="media_vid_img f_left" src="'. (($data_get_content_list->text_icon) ? $data_get_content_list->text_icon : $data_get_content_list->def_icon) . '" />';
								}
							?>
							<!--img class="media_vid_img f_left" src="<?php echo (($data_get_content_list->text_icon) ? $data_get_content_list->text_icon : $data_get_content_list->def_icon);?>" /-->
							<div class="title_media_vid f_left">
								<?php echo (($data_get_content_list->text_title) ? $data_get_content_list->text_title : $data_get_content_list->def_title);?>
							</div>
							<p><?php echo substr((($data_get_content_list->text_teaser) ? $data_get_content_list->text_teaser : $data_get_content_list->def_teaser), 0, 100);?></p>
							<div class="clear"></div>
						</li>
			<?php
					}
				}
			?>
		</ul>
		<div class="clear"></div>
		<div class="pagination_media" id="in_pagination">
			
		</div>
	</div>
	<div class="clear"></div>
</div>