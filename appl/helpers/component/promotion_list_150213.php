<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/easypaginate.js"></script>
<script>
	jQuery(function($){
		$('ul#will_paginate').easyPaginate({
			step:8
		});
		var country = $('select[name="country"]').val();
		
		$('select[name="country"]').change(function(){
			var nilai = $(this).val();
			var country = $('select[name="country"]').val();
			var country_name = $('select[name="country"] option:selected').data('foo');
			$('select[name="city"]').empty();
			
			$('select[name="city"]')
			  .prev('span')
			  .remove()
			  .end()
			.before('<span style="width: 116px; -moz-user-select: none;">Select City</span>');
			
			if(nilai != ""){
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>ajax",
					data: { act: "get_city_by_country", country_id: nilai }
				}).done(function( msg ) {
					$('select[name="city"]').html(msg);
				});
				
				window.location='<?php echo base_url('promotion/promo_hotel');?>' + country + '/' + country_name + '';
				
			} else {
			window.location='<?php echo base_url('promotion/promo_hotel/');?>';
			}
		});
		
		/* Filenya Ajax Tambahan Di bawah sinih.. */
		if(country !=""){
		 $.ajax({
			type: "POST",
			url: "<?php echo base_url();?>ajax",
			data: { act: "get_city_by_country", country_id: country }
			}).done(function( msg ) {
				$('select[name="city"]').html(msg);
			}); 
		}
		/* Sampai sinih */
		
		$('select[name="city"]').change(function(){
			//var nilaicity = $(this).val();
			var country = $('select[name="country"]').val();
			var country_name = $('select[name="country"] option:selected').data('foo');
			var city = $('select[name="city"]').val();
			var city_name = $('select[name="city"] option:selected').data('foo');
			if(city != "" && country !=""){
				window.location='<?php echo base_url('promotion/promo_hotel');?>' + country + '/' + country_name + '/' + city + '/' + city_name + '';
			}else{
			window.location='<?php echo base_url('promotion/promo_hotel');?>' + country + '/' + country_name + '';
			//alert('Failed send City');
			}
		});
	});
</script>

<div class="promotion_content">
	<h3><?php echo lang('promo_sel_cc');?></h3>
	<select name="country" class="myselect" style="width:150px;">
		<option value=""><?php echo lang('promo_sel_country');?></option>
		<option value=""><?php echo lang('promo_all_country');?></option>
		<?php
			$query_country = $this->pages_model->get_country();
			foreach($query_country->result() as $data_country){
				echo '<option value="' . $data_country->country_id . '" data-foo="' . underscore(strtolower($data_country->name)) . '" ' . ($data_country->country_id == $country_id ? 'selected' : '') . ' >' . $data_country->name . '</option>';
			}
		?>
	</select>
	
	<select name="city" class="myselect" style="width:150px;">
		<option value=""><?php echo lang('promo_sel_city');?></option>
		<?php
			if($country_id != "" && $city_id != ""){
				$query_hotel_by_country = $this->pages_model->get_hotel_by_country($country_id);
				if($query_hotel_by_country->num_rows() > 0){
					foreach($query_hotel_by_country->result() as $data_hotel_by_country){
						
						echo '<option value="' . $data_hotel_by_country->hotel_id . '" data-foo="' . $data_hotel_by_country->hotel_name . '" ' . ($data_hotel_by_country->hotel_id == $city_id ? 'selected' : '') . ' >' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
					}
				}
			}
		?>
	</select>
	<div class="clean"></div>
</div>
<div class="promotion_content2">
	<div class="list_promotion">
		<ul id="will_paginate">
			<?php
				if($country_id != "" && $city_id != ""){
					$sql_get_all_promo = $this->pages_model->get_promo_by_city($city_id);
				}
				elseif($country_id != ""){
					$sql_get_all_promo = $this->pages_model->get_condition_promo($country_id);
				}
				else{
					$sql_get_all_promo = $this->pages_model->get_all_promo();
				}
				if($sql_get_all_promo->num_rows() > 0){
					foreach($sql_get_all_promo->result() as $data_get_all_promo){
			?>
						<li>
							<div class="list_promotions f_left">
								<img src="<?php echo (($data_get_all_promo->text_icon_1) ? $data_get_all_promo->text_icon_1 : $data_get_all_promo->def_icon_1); ?>" />
								<div class="list_promotions_text">
									<h4><?php echo (($data_get_all_promo->text_title) ? $data_get_all_promo->text_title : $data_get_all_promo->def_title);?></h4>
									<p><?php echo substr((($data_get_all_promo->text_teaser) ? $data_get_all_promo->text_teaser : $data_get_all_promo->def_teaser), 0, 200);?></p>
									<br/>
									<input class="btn_go lightbox" type="button" alt="<?php echo $data_get_all_promo->promo_id;?>" value="<?php echo lang('lets_go');?>">
								</div>
							</div>
						</li>
			<?php
					}
				}
				else{
					echo '<p style="padding:20px 0px;">' . lang('promo_no_found') . '</p>';
				}
			?>
			
		</ul>
		<div class="clean"></div>
	</div>
	
	<div class="promo_paging" id="in_pagination">
		
	</div>
	
	
</div>