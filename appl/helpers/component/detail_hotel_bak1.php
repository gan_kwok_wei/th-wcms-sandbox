<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.idTabs.min.js"></script>

<!--script type="text/javascript" src="js/jquery.ad-gallery.js"></script-->
<?php
	$query_hotel_detail = $this->pages_model->get_hotel($hotel_id);
	$data_hotel_detail = $query_hotel_detail->row();
	
	if($tab_hotel == ""){
		$tab_hotel = 'hotel_information';
	}
			
?>

<link rel="stylesheet" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/nivo/themes/dark/dark.css" type="text/css" media="screen" />

<script type="text/javascript"> 
	$(function(){
		//$("#ul_tab_detail_hotel").idTabs();
	});
 
</script>
<div class="map_hotel_top">
	<div class="title_hotel f_left">
		<h2><?php echo lang('map_hotel_tune_hotel');?></h2>
		<h1><?php echo (($data_hotel_detail->text_title) ? $data_hotel_detail->text_title : $data_hotel_detail->def_title);?></h1>
	</div>
	<div class="share_hotel f_right">
		<!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style ">
		<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
		<a class="addthis_counter addthis_pill_style"></a>
		</div>
		<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
		<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4faa1a491ffb4f02"></script>
		<!-- AddThis Button END -->
	</div>
</div>
<div class="clear"></div>
<div class="detail_hotel_tab">
	<ul id="ul_tab_detail_hotel" class="ul_tab_detail_hotels">
		<li>
			<a href="<?php echo base_url('our-hotel/' . $hotel_name . '/hotel_information');?>" class="<?php echo ($tab_hotel == 'hotel_information' ? 'selected' : '');?>"><?php echo lang('det_hotel_hotel_information');?></a>
		</li>
		<li>
			<a href="<?php echo base_url('our-hotel/' . $hotel_name . '/hotel_gallery');?>" class="<?php echo ($tab_hotel == 'hotel_gallery' ? 'selected' : '');?>"><?php echo lang('det_hotel_gallery');?></a>
		</li>
		<li>
			<a href="<?php echo base_url('our-hotel/' . $hotel_name . '/nearby_attraction');?>" class="<?php echo ($tab_hotel == 'nearby_attraction' ? 'selected' : '');?>"><?php echo lang('det_hotel_nearby_attraction');?></a>
		</li>
		<li>
			<a href="<?php echo base_url('our-hotel/' . $hotel_name . '/how_to_get_here');?>" class="<?php echo ($tab_hotel == 'how_to_get_here' ? 'selected' : '');?>"><?php echo lang('det_hotel_how_to_here');?></a>
		</li>
		<li>
			<a href="<?php echo base_url('our-hotel/' . $hotel_name . '/customer_review');?>" class="<?php echo ($tab_hotel == 'customer_review' ? 'selected' : '');?>"><?php echo lang('det_hotel_customer_review');?></a>
		</li>
	</ul>
	
	<div id="tab_hotel_information" style="display:block">
		<?php
			$this->output_data2['hotel_id'] = $hotel_id;
			$this->output_data2['city_id'] = $data_hotel_detail->city_id;
			
			$this->load->view('component/' . $tab_hotel, $this->output_data2);
		?>
		<div class="clear"></div>
	</div>

</div>