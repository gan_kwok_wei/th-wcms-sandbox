<page id="business">
<section class="jumbotron masthead">
		<div class="container">
			
						
								<h1 class="banner-title">Business Travel </h1>
								<p class="banner-description">Great rates, Many advantages. No hassles </p>
						
		</div>
		<!--/container --> 
</section>

<div class="container clearfix" style="padding-top:50px;">	


<div class="container six">


<section class="col4 first left">
	<?php
					$this->output_data['page_id'] = 4;
					$this->output_data['position1'] = 'left_1';
					$this->output_data['position2'] = 'left_2';
					
				?>

				<?php
						$this->load->view('component/corporate_content.php');
				?>
</section>
<section class="col2">

	<div class="inner inner">
		<h5>For enquiries in your preferred language:</h5>

			<ul style="margin-top:16px;">
			<li class="med">English and Bahasa Malaysia</li>
			<li>Tel: +6(03)-7962 5888</li>
			<li>Mon-Fri 9:00am - 9:00pm</li>
			<li>Email: enquiry@tunehotels.com</li>
			<li>Tweet Us: https://twitter.com/#!/thconcierge</li>
			</ul>

			<ul style="margin-top:16px;">
			<li class="med">Bahasa Indonesia</li>
			<li>Tel: +622129392111</li>
			<li>Mon-Fri 9:00am - 9:00pm</li>
			<li>(excluding Public Holidays)</li>
			<li>Email: enquiry.id@tunehotels.com</li>
			</ul>

			<ul style="margin-top:16px;">
			<li class="med">Thai Speaking</li>
			<li>Tel: +66-2-6135888 / 02-6135888</li>
			<li>Mon-Fri 10:00am - 7:00pm</li>
			<li>(excluding Public Holidays)</li>
			<li>Email: enquiry.thai@tunehotels.com</li>
			</ul>

	</div>
</section>
</div>
</div>
</page>
