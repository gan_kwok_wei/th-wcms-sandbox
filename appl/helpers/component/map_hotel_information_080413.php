<?php
	$query_hotel_detail = $this->pages_model->get_hotel($hotel_id);
	$data_hotel_detail = $query_hotel_detail->row();

	$hotel_title = ($data_hotel_detail->text_title) ? $data_hotel_detail->text_title : $data_hotel_detail->def_title;
?>

<script type="text/javascript">
<!--
	$(document).ready(function(){
		$('.detail_room').click(function(){
			var alt = $(this).attr('alt');
			var screenTop = $(document).scrollTop();

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('ajax/room_detail');?>",
				data: { room_id: alt }
			}).done(function( data ) {
				if(data.result > 0) {
					$('.this_image_room img').attr('src', data.image);
					$('.box_content_text_title_room h2').html(data.title);
					$('.this_content_room').html(data.feature_text);

					var doc_tinggi = $(document).height();
					var tinggi = $(window).height();
					var lebar = $(window).width();
					var tinggi_box = $('.box_room').height();
					var lebar_box = $('.box_room').width();
					var top = ((tinggi - tinggi_box)/2) + screenTop;
					var left = (lebar - lebar_box)/2;

					$('.backdrop_room').css({'height' : doc_tinggi + 'px'});
					$('.backdrop_room, .box_room').animate({'opacity':'.50'}, 300, 'linear');
					$('.box_room').animate({'opacity':'1.00'}, 300, 'linear');
					$('.backdrop_room, .box_room').css({'display' : 'block'});
					//$('.box_room').css({'top':top + 'px', 'left':left + 'px'});
					$('.box_room').css({'top':0 + 'px', 'left':0 + 'px'});
//					alert(msg.feature_text);
				} else {
					alert('Request failed!');
				}
//				$('.box_content_text_title_room h2').html(msg);
			});

		});

		$('.close_room').click(function(){
			close_box_room();
		});

		$('.backdrop_room').click(function(){
			close_box_room();
		});

	});

	function close_box_room()
	{
		$('.backdrop_room, .box_room').animate({'opacity':'0'}, 300, 'linear', function(){
			$('.backdrop_room, .box_room').css('display', 'none');
		});
	}
//-->
</script>

<div class="row box-shade round white">

<div class="row" style="margin-left:10px; width:300px; float:left;">
		<h4><?php echo lang('h_info_address');?> :</h4>
		<p ><?php echo (($data_hotel_detail->text_address) ? $data_hotel_detail->text_address : $data_hotel_detail->def_address);?></p>
		<p>
			<?php
				echo (($data_hotel_detail->phone1) ? 'Telp : ' . $data_hotel_detail->phone1 : '');
				echo (($data_hotel_detail->phone2) ? ' / ' . $data_hotel_detail->phone2 : '');
				echo (($data_hotel_detail->fax) ? '<br/>Fax : ' . $data_hotel_detail->fax : '');
				echo (($data_hotel_detail->facebook_id) ? '<br/>Fb : ' . $data_hotel_detail->facebook_id : '');
				echo (($data_hotel_detail->twitter_id) ? '<br/>Twitter : ' . $data_hotel_detail->twitter_id : '');
				echo (($data_hotel_detail->www) ? '<br/>Website : ' . $data_hotel_detail->www : '');
				echo (($data_hotel_detail->email) ? '<br/>Email : ' . $data_hotel_detail->email : '');
			?>
		</p>
		</div>

		<div class="row" style="width:300px; float:left;">
			<h4><?php echo lang('h_info_feature');?> :</h4>
			<div class="will_red_bullet">
	<?php
		$get_hotel_room = $this->pages_model->get_hotel_room($hotel_id);
		if($get_hotel_room->num_rows() > 0){
			echo '<ul>';
			foreach($get_hotel_room->result() as $data_get_hotel_room){
				echo '<li class="detail_room" alt="' . $data_get_hotel_room->room_id . '" ><u>' . $data_get_hotel_room->quantity . ' ' . $data_get_hotel_room->title . '</u></li>';
			}
			echo '</ul>';
		}

		echo '<p>' . (($data_hotel_detail->text_feature_text) ? $data_hotel_detail->text_feature_text : $data_hotel_detail->def_feature_text) . '</p>';

	?>
			</div>		
	</div>
	
	<div class="row red">
	<div class="gutter"></div>
	</div>

	<div class="row" style="display:block; width: 700px; margin:10px;">
		<?php
			$query_hotel_detail = $this->pages_model->get_hotel($hotel_id);
			$data_hotel_detail = $query_hotel_detail->row();
			
			echo '<div style="list-style-type: none; line-height:18px;">' . (($data_hotel_detail->text_note) ? $data_hotel_detail->text_note : $data_hotel_detail->def_note) . '</div>';
		?>

	</div>
	
	<div class="row" id="hotel-map">
		<!-- iframe width="445" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.id/maps?q=<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>&amp;num=1&amp;ie=UTF8&amp;t=m&amp;z=14&amp;ll=<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>&amp;output=embed"></iframe><br /-->

<!-- map key -->
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=AIzaSyBYS9BTLOAkQm9rEELQSVMCAxH728uICSE" type="text/javascript"></script>

<!-- function kode latitude longitude  n detail alamat di google -->
<script type="text/javascript">


//map
var map;
var icon0;
var newpoints = new Array();

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function'){
		window.onload = func
	} else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}
addLoadEvent(loadMap);
addLoadEvent(addPoints);
function loadMap() {
	map = new GMap2(document.getElementById("gmap"));
	map.addControl(new GLargeMapControl());
	map.addControl(new GMapTypeControl());
	map.setCenter(new GLatLng(<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>), 16);
	map.setMapType(G_NORMAL_MAP);

	icon0 = new GIcon();
	icon0.image = "http://www.google.com/mapfiles/marker.png";
	icon0.shadow = "http://www.google.com/mapfiles/shadow50.png";
	icon0.iconSize = new GSize(20, 34);
	icon0.shadowSize = new GSize(37, 34);
	icon0.iconAnchor = new GPoint(9, 34);
	icon0.infoWindowAnchor = new GPoint(9, 2);
	icon0.infoShadowAnchor = new GPoint(18, 25);
}

function createMarker(point, icon, popuphtml) {
	var popuphtml = "<div id=\"popup\">" + popuphtml + "<\/div>";
	var marker = new GMarker(point, icon);
	GEvent.addListener(marker, "click", function() {
		marker.openInfoWindowHtml(popuphtml);
	});
	return marker;
}



					function addPoints() {

						newpoints[0] = new Array(<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>, icon0, 'left', '<?php echo "<h4> $hotel_title :</h4>" . str_replace("\n", "", str_replace("\r\n", "", $data_hotel_detail->text_address)); ?>');

						for(var i = 0; i < newpoints.length; i++) {
							var point = new GPoint(newpoints[i][1],newpoints[i][0]);
							var popuphtml = newpoints[i][4] ;
							var marker = createMarker(point, newpoints[i][2], popuphtml);
							map.addOverlay(marker);
						}
					}

</script>

		<div class="row">
			<div id="gmap" style="margin:10px; width:700px; height:320px" align="center"></div>
			<div class="clear"></div>
		</div>
	</div>

	<?php
		$get_content = $this->pages_model->get_content('40');
		$data_get_content = $get_content->row();
	?>
</div>

<div class="backdrop_room"></div>
<div class="box_room">
	<div class="box_title_room">
		<span><?php echo (($data_hotel_detail->text_title) ? $data_hotel_detail->text_title : $data_hotel_detail->def_title);?></span>
		<div class="close_room"></div>
	</div>
	<div class="box_content_room">
		<div class="box_left_room f_left">
			<div class="this_image_room">
				<img src="" width="265px"/>
			</div>
		</div>
		<div class="box_content_text_room f_left">
			<div class="box_content_text_title_room">
				<h2></h2>
			</div>
			<div class="box_content_texts_room">
				<div class="this_content_room"></div>
				<br/>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

	


