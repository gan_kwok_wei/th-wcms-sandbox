<script type="text/javascript">
	$(document).ready(function(){
		$('.detail_promo').click(function(){
			var alt = $(this).attr('alt');
			var screenTop = $(document).scrollTop();
	
			$('.box_booking_search_promo').hide();
	
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>ajax",
				data: { act: "get_title_promo", promo_id: alt }
			}).done(function( msg ) {
				$('.box_content_text_title_promo h2').html(msg);
			});
	
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>ajax",
				data: { act: "get_content_promo", promo_id: alt }
			}).done(function( msg ) {
				$('.this_content_promo').html(msg);
			});
	
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>ajax",
				data: { act: "get_image_promo", promo_id: alt }
			}).done(function( msg ) {
				$('.this_image_promo').html(msg);
			});
	
			var doc_tinggi = $(document).height();
			var tinggi = $(window).height();
			var lebar = $(window).width();
			var tinggi_box = $('.box_promo').height();
			var lebar_box = $('.box_promo').width();
			var top = ((tinggi - tinggi_box)/2)+screenTop;
			var left = (lebar - lebar_box)/2;
	
			$('.backdrop').css({'height' : doc_tinggi + 'px'});
			$('.backdrop, .box_promo').animate({'opacity':'.50'}, 300, 'linear');
			$('.box_promo').animate({'opacity':'1.00'}, 300, 'linear');
			$('.backdrop, .box_promo').css({'display' : 'block'});
			$('.box_promo').css({'top':top + 'px', 'left':left + 'px', 'z-index':'99999'})
		});
	
		$('.close_promo').click(function(){
			close_box_promo();
		});
/*	
		$('.backdrop_promo').click(function(){
			close_box_promo();
		});
*/	
		$('.box_booking_search_promo').hide();
	
		$('.btn_book_box').click(function(){
			$('.box_promo_image').hide();
			$('.box_booking_search_promo').show();
		});
	
		$('.box_booking_close_promo').click(function(){
			$('.box_promo_image').show();
			$('.box_booking_search_promo').hide();
		});
	
	});
	
	function close_box_promo() {
		$('.backdrop, .box_promo').animate({'opacity':'0'}, 300, 'linear', function(){
			$('.backdrop, .box_promo').css('display', 'none');
		});
	}
</script>


<div class="box_promo">
	<div class="box_title_promo">
		<span><?php echo lang('promo_promotion');?></span>
		<div class="close_promo"></div>
	</div>
	<div class="box_content_promo">
		<div class="box_left_promo f_left">
			<div class="this_image_promo">
			</div>
			<div class="box_booking_search_promo">
				<a href="javascript:void(0);" class="box_booking_close">X</a>
				<?php //$this->load->view('component/booking_search.php'); ?>
			</div>
		</div>
		<div class="box_content_text_promo f_left">
			<div class="box_content_text_title">
				<h3><?php echo lang('promo_tune_promo');?></h3>
				<h2></h2>
			</div>
			<div class="box_content_texts_promo">
				<div class="this_content_promo">

				</div>
				<br/>
				<input class="btn_see_detail btn_book_box" type="button" value="<?php echo lang('book_now');?>">
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>