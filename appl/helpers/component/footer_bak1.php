	<!-- START FOOTER CONTAINER -->
	<div id="footer_container">
		<div class="footer_content">
			<div class="footer_content_left1 f_left">
				<h1><a href="<?php echo base_url('hotel/');?>" style="color: #E91126;"><?php echo lang('footer_our_hotel');?></a></h1>
				<table>
					<tr>
						<td>
						<?php
							$query_country = $this->pages_model->get_country();
							
							$x = 1;
							foreach($query_country->result() as $data_country){
								if($x == 4){
								echo '</td><td>';
								}
								echo '<div class="foot_list_hotel ">
										<h2>' . $data_country->name . '</h2>';
									$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
									if($query_hotel_by_country->num_rows() > 0){
										
										$i = 1;
										foreach($query_hotel_by_country->result() as $data_hotel_by_country){
											echo ($i != 1 ? ' / ' : '');
											echo '<a href="' . base_url('hotel/hotel_detail/') . $data_hotel_by_country->hotel_id . "/" . underscore(strtolower($data_hotel_by_country->hotel_name)) . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</a>';
											
											/* echo "<li>
												<a href=\"" . base_url('hotel/hotel_detail/') . $data_hotel_by_country->hotel_id . "/" . underscore(strtolower($data_hotel_by_country->hotel_name)) . "\">" . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . "</a>
											</li>"; */
											$i++;
										}
										
									}
								echo '</div>';
							$x ++;
							}
						?>
						</td>
					</tr>
				</table>
				
				<!--div class="foot_list_hotel f_left">
					<h2>Malaysia</h2>
					<span>
						<a href="#">Downtown Penang</a> / 
						<a href="#">Kota Damansara</a> / 
						<a href="#">Downtown, KL</a> / 
						<a href="#">KLIA-LCCT Airport</a> / 
						<a href="#">Danga Bay, Johor</a> / 
						<a href="#">Kota Bharu City Centre</a> / 
						<a href="#">Bintulu</a> / 
						<a href="#">Kulm, Kedah</a> / 
						<a href="#">Waterfront Kuching</a> / 
						<a href="#">Borneo, Kota Kinabalu</a> / 
						<a href="#">Ipoh</a>
					</span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>Phillipines</h2>
					<span><a href="#">Ermita, Manila</a> / <a href="#">Angeles City</a> / <a href="#">Cebu</a></span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>United Kingdom</h2>
					<span><a href="#">Westminster, London</a> / <a href="#">Liverpool Street, London</a></span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>Indonesia</h2>
					<span><a href="#">Double Six, Legian, Bali</a> / <a href="#">Kuta, Bali</a></span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>Thailand</h2>
					<span><a href="#">Hat Yai</a> / <a href="#">Pattaya</a></span>
				</div-->
			</div>
			<div class="footer_content_right1 f_left">
				<div class="foot_list_hotel f_left">
					<div class="foot_list_hotel_isi">
						<h1><a href="<?php echo base_url('about_us/');?>" style="color: #E91126;"><?php echo lang('footer_about_us');?></a></h1>
						<span>
							<a href="<?php echo base_url('about_us/management_team');?>"><?php echo lang('about_management_team');?></a> / 
							<a href="<?php echo base_url('about_us/shareholders');?>"><?php echo lang('about_shareholders');?></a> / 
							<a href="<?php echo base_url('about_us/career');?>"><?php echo lang('about_career');?></a> / 
							<a href="<?php echo base_url('about_us/concept');?>"><?php echo lang('about_concept');?></a> / 
							<a href="<?php echo base_url('about_us/media');?>"><?php echo lang('about_media');?></a> 
						</span>
					</div>
					<div class="foot_list_hotel_isi">
						<h1><a href="<?php echo base_url('help_info/');?>" style="color: #E91126;"><?php echo lang('footer_help_info');?></a></h1>
						<span>
							<a href="<?php echo base_url('help_info/contact_us');?>"><?php echo lang('help_contact_us');?></a> / 
							<a href="<?php echo base_url('help_info/booking_cancelation');?>"><?php echo lang('help_booking_cancelation');?></a> / 
							<a href="<?php echo base_url('help_info/terms_condition');?>"><?php echo lang('help_terms_condition');?></a> / 
							<a href="<?php echo base_url('help_info/fee_hotel_schedule');?>"><?php echo lang('help_fee_hotel_schedule');?></a> / 
							<a href="<?php echo base_url('help_info/faq');?>"><?php echo lang('help_faq');?></a>
						</span>
					</div>
					<div class="foot_list_hotel_isi">
						<h1><a href="<?php echo base_url('promotion/');?>" style="color: #E91126;"><?php echo lang('footer_promotion');?></a></h1>
						<span></span>
					</div>
				</div>
				<div class="foot_list_hotel f_left">
					<div class="foot_list_hotel_isi">
						<h1><a href="<?php echo base_url('corporate/');?>" style="color: #E91126;"><?php echo lang('footer_corporate');?></a></h1>
						<span></span>
					</div>
					<div class="foot_list_hotel_isi" style="width:250px;">
						<h1><?php echo lang('footer_follow_us');?></h1>
						<div class="follow_media f_left">
							<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/fb_icon.png" class="f_left"/>
							<h3>Facebook</h3>
							<h4>Like</h4>
						</div>
						
						<div class="follow_media f_left">
							<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/pin_icon.png" class="f_left"/>
							<h3>Pinterest</h3>
							<h4>Pinned</h4>
						</div>
						
						<div class="follow_media f_left">
							<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/tw_icon.png" class="f_left"/>
							<h3>Twitter</h3>
							<h4>Follow</h4>
						</div>
						<div class="follow_media f_left">
							<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/yt_icon.png" class="f_left"/>
							<h3>Youtube</h3>
							<h4>Subscribe</h4>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<hr class="hr_one"/>
		<div class="footer_content">
			<div class="footer_content_left2 f_left">
				<h5><?php echo lang('footer_secure_by');?></h5>
				<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/secure_by.png" />
			</div>
			<div class="footer_content_right2 f_left">
				<h5><?php echo lang('footer_our_partner');?></h5>
				<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/our_partner.png" />
			</div>
			<div class="clear"></div>
		</div>
		
		<hr class="hr_one"/>
		<div class="footer_content">
			<div class="footer_text">
				<?php echo lang('footer_text');?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<!-- END FOOTER CONTAINER -->
	
</body>
</html>