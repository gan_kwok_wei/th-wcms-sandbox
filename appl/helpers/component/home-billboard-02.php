    <link rel="stylesheet" href="<?php echo ASSET_PATH ?>extend/fws/css/fwslider.css" media="all">
    
    <!-- Fonts 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->
    <script src="<?php echo ASSET_PATH ?>extend/fws/js/jquery.min.js"></script>
    <script src="<?php echo ASSET_PATH ?>extend/fws/js/jquery-ui.min.js"></script>

    <script src="<?php echo ASSET_PATH ?>extend/fws/js/fwslider.js"></script>
    <!-- Java Scripts -->

    <!-- ****************** STEP 2: INCLUDE THE HTML PART ****************** -->
    
    <div id="fwslider">
        <div class="slider_container">

            <!-- ****************** STEP 3: ADD SLIDES ****************** -->
            
            <!-- Duplicate to create more slides -->
            <div class="slide"> 
                
                <!-- Slide image -->
                    <img src="<?php echo ASSET_PATH ?>extend/fws/img/slide.jpg">
                <!-- /Slide image -->
                
                <!-- Texts container -->
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <!-- Text title -->
                        <h4 class="title">REVEAL THE MAGIC OF LEGOLAND, MALAYSIA</h4>
                        <!-- /Text title -->
                        
                        <!-- Text description -->
                        <p class="description">Enter a chance to win a stay in Tune Hotel Danga Bay, Johor</p>
                        <!-- /Text description -->
                        
                        <!-- Learn more button -->
                        <a class="readmore" href="http://www.tunehotels.com">LET'S GO!</a>
                        <!-- /Learn more button -->
                    </div>
                </div>
                 <!-- /Texts container -->
                 
            </div>
            <!-- /Duplicate to create more slides -->
            
            <!-- ****************** /STEP 3: ADD SLIDES ****************** -->

            <div class="slide">
                <img src="<?php echo ASSET_PATH ?>extend/fws/img/slide2.jpg">
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h4 class="title">Get away this weekend to Edinburgh 
</h4>
                        <p class="description">Save up to GBP 20 and earn 15,000 extra BIG points</p>
                        <a class="readmore" href="http://www.tunehotels.com">LET'S GO!</a>
                    </div>
                </div>
            </div><!--/slide -->

            <div class="slide">
                <img src="<?php echo ASSET_PATH ?>extend/fws/img/slide3.jpg">
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h4 class="title">Escape to the blissful beaches of Bali</h4>
                        <p class="description">Save up to Rp 2000 per night booking...</p>
                        <a class="readmore" href="/home">LETS GO!</a>
                    </div>
                </div>
            </div><!--/slide -->

 

        </div>

        <div class="timers"></div>

        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>

    </div> <!--/slider -->
    <!-- SLIDER END HERE -->
    
    <!-- ****************** /STEP 2: INCLUDE THE HTML PART ****************** -->