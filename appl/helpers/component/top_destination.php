<div class="top_destination box-shade round">
	<h1><?php echo lang('top_top_destination');?></h1>
	<ul>
	<?php
		$get_hotel_top_destination = $this->pages_model->get_hotel_top_destination();
		if($get_hotel_top_destination->num_rows() > 0){
			foreach($get_hotel_top_destination->result() as $data_get_hotel_top_destination){
				$explode_feature = $data_get_hotel_top_destination->price;
				
				$price_len = strlen($explode_feature);
				if($price_len > 3){
					$cek_titik = substr($explode_feature, -3, 1);
					if($cek_titik == '.' || $cek_titik == ','){
						$price_small = substr($explode_feature, -4);
						$price_large = substr($explode_feature, 0, -4);
					}
					else{
						$price_small = "." . substr($explode_feature, -3);
						$price_large = substr($explode_feature, 0, -3);
					}
				}
				else{
					$price_small = '';
					$price_large = $explode_feature;
				}
				
				if($data_get_hotel_top_destination->package == "per_night"){
					$per_night = lang('top_per_Night');
				}
	?>
				<li><a href="<?php echo base_url('our-hotel/' . $data_get_hotel_top_destination->hotel_name); ?>">
					<div class="top_destination_from f_left">
						<?php echo lang('top_from');?><br/><?php echo $data_get_hotel_top_destination->currency_code;?>
					</div>
					<div class="top_destination_price f_left">
						<span class="top_destination_price_large"><?php echo $price_large;?></span>
						<span class="top_destination_price_small"><?php echo $price_small;?></span>
						<span class="top_destination_price_pernight"><?php echo $per_night;?></span>
					</div>
					<div class="top_destination_right f_right">
						
						<div class="isi">
							<?php echo (($data_get_hotel_top_destination->text_title) ? $data_get_hotel_top_destination->text_title : $data_get_hotel_top_destination->def_title);?>
						</div>
						
					</div>
				</a></li>
	<?php
			}
		}
	?>
	</ul>
	
	<!-- input type="button" class="btn_go f_right" value="Let's Go" /-->
	<div class="clean"></div>
</div>