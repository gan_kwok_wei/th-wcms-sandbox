<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/circular.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/jquery.jscrollpane.css" media="all" />

<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.easing.1.3.js"></script>
<!-- the jScrollPane script -->
<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.contentcarousel.js"></script>
<script type="text/javascript">
$(function(){
	$('#ca-container').contentcarousel();
});
	
</script>

<div class="place_of_interest">
	<h1><?php echo lang('nearby_place_interes');?></h1>

	<div id="ca-container" class="ca-container">
		<div class="ca-wrapper" style="font-size:11px; line-height:13px; height:360px; overflow:hidden;">
			<?php
				$query_get_landmark_list = $this->pages_model->get_landmark_list($city_id);
				if($query_get_landmark_list->num_rows() > 0){
					$l1 = 1;
					foreach($query_get_landmark_list->result() as $data_get_landmark_list){
			?>
						<div class="ca-item ca-item-<?php echo $l1;?>">
							<img src="<?php echo (($data_get_landmark_list->text_icon) ? $data_get_landmark_list->text_icon : $data_get_landmark_list->def_icon) ?>"/>
							<h2><?php echo (($data_get_landmark_list->text_title) ? $data_get_landmark_list->text_title : $data_get_landmark_list->def_title) ?></h2>
							<div class="ca-item-text">
								<p><?php echo substr(strip_tags((($data_get_landmark_list->text_teaser) ? $data_get_landmark_list->text_teaser : $data_get_landmark_list->def_teaser)), 0, 400) ?></p>
								
								
							</div>
						</div>
			<?php
						$l1++;
					}
				}
			?>
			
		</div>
	</div>

</div>