<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


	function show_message() {
		$ci =& get_instance();
		if($error = validation_errors()){
			echo '<div class="error_msg">'
				.$error
				.'</div>';
		}
	 	
		if($ci->session->get('ge_error_msg')){
//		if($ci->config->item('ge_error_msg')){
			echo '<div class="error_msg">'
				.$ci->session->get_dec('ge_error_msg')
//				.$ci->config->item('ge_error_msg')
				.'</div>';
			$ci->session->clear('ge_error_msg');
		}
	 
		if($ci->session->get('ge_warning_msg')){
//		if($ci->config->item('ge_warning_msg')){
			echo '<div class="warning_msg">'
				.$ci->session->get_dec('ge_warning_msg')
//				.$ci->config->item('ge_warning_msg')
				.'</div>';
			$ci->session->clear('ge_warning_msg');
		}
		 
		if($ci->session->get('ge_success_msg')){
//		if($ci->config->item('ge_success_msg')){
			echo '<div class="success_msg">'
				.$ci->session->get_dec('ge_success_msg')
//				.$ci->config->item('ge_success_msg')
				.'</div>';
			$ci->session->clear('ge_success_msg');
		}
		
		
	}
	
	/**
	 * @param $msg
	 * @return void
	 */
	function set_error_message($msg){
		$ci =& get_instance();
//		$ci->config->set_item('ge_error_msg', $msg);
		$ci->session->save_enc("ge_error_msg", $msg);
			
	}
	
	/**
	 * @param $msg
	 * @return void
	 */
	function set_warning_message($msg){
		$ci =& get_instance();
//		$ci->config->set_item('ge_warning_msg', $msg);
		$ci->session->save_enc("ge_warning_msg", $msg);
	}
	
	/**
	 * @param $msg
	 * @return void
	 */
	function set_success_message($msg){
		$ci =& get_instance();
//		$ci->config->set_item('ge_success_msg', $msg);
		$ci->session->save_enc("ge_success_msg", $msg);
	}
	
	/**
	 * @return string
	 */
	function get_error_message(){
		$ci =& get_instance();
		return $ci->session->get_dec('ge_error_msg');
//		return $ci->config->item('ge_error_msg');
	}
	
	/**
	 * @return string
	 */
	function get_warning_message(){
		$ci =& get_instance();
		return $ci->session->get_dec('ge_warning_msg');
//		return $ci->config->item('ge_warning_msg');
	}
	
	/**
	 * @return string
	 */
	function get_success_message() {
		$ci =& get_instance();
		return $ci->session->get_dec('ge_success_msg');
//		return $ci->config->item('ge_success_msg');
	}
	
?>