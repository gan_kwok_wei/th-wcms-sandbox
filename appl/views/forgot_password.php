<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*
|---------------------------------------------------------------
| DEFINE Assets path
|---------------------------------------------------------------
*/

define('ASSET_PATH', base_url().'assets/');

?>
<head>
	<title><?php echo $this->config->item('site_name'); ?></title>
	<meta name="author"      content="Budi Laxono">
	<meta name="copyright"   content="&copy; 2012 Green Lab Group" >
	<meta name="keywords"    content="Content Keywords">
	<meta name="description" content="Content Description">
	<meta name="template"    content="<?php echo $module . '.' . $class . '.' . $function;  ?>" />
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<link rel="shortcut icon" href="greenlab.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="<?=ASSET_PATH?>themes/default/css/style.css" />

	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery-1.7.1.min.js"></script>
	<script language="javascript" type="text/javascript">
			
		$(document).ready(function(){
			
			$('#email').focus();
			
		});
		
	</script>
</head>
	
<body>
	
	<!-- C. MAIN SECTION -->
	<div id='login_logo'>
		<a href="http://greenlabgroup.com/" title="powered by greenlab multimedia engine" target="_blank">
			<img src="<?=ASSET_PATH?>media/images/logo.jpg" alt="" />
		</a>
	</div>
	<div id='login_main'>
		<h1><?php echo strtoupper(lang('label_forget_password'))?></h1>                 

		<div class="loginform" id='frLogin'>
			<?php echo form_open('login/forgot_password')?>
			<table border="0" width="100%">
				<tr>
					<td width="30%" nowrap="nowrap" align="right"><?php echo lang('label_email', 'name')?> : </td>
					<td><?php echo form_input(array('id' => 'email', 'name' => 'email', 'size' => '50'))?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<?php echo form_submit('login', lang('label_reset_password'))?>
						<input type="button" id="btnBack" value="<?php echo lang('label_login'); ?>" onclick="location.assign('<?php echo base_url(); ?>')" />
					</td>
				</tr>
			</table>
			<div class="alert">
				<?php echo validation_errors()?>
				<?php echo $msg?>
			</div>
			<?php echo form_close();?>
		</div>

	</div>
</body>
</html>
			