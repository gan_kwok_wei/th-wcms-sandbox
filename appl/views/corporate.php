	<section class="masthead">
		<div class="container">
			

			<h1 class="banner-title">Business Travel </h1>
			<p class="banner-description">Great rates, Many advantages. No hassles </p>

		</div>
		<!--/container --> 
	</section>

	<div class="container clearfix" style="margin: -50px auto 50px; z-index:100;">	

		<div class="container four clearfix">


			<section class="col3 first box-shade white left" style="padding-top:16px;">
				<?php
				$this->output_data['page_id'] = 4;
				$this->output_data['position1'] = 'left_1';
				$this->output_data['position2'] = 'left_2';

				?>

				<?php
				$this->load->view('component/corporate_content.php');
				?>
			</section>
			<section class="col1 box-shade white" style="margin-top:75px;">

				<div class="business-col1 inner">
					
					<ul style="margin-top:32px;">
						<h5 style="font-family:arial; color:#ff0000; line-height:22px; padding-bottom:20px;">
						For enquiries in your preferred language:</h5>

						<li class="med"><strong>English and Bahasa Malaysia</strong></li>
						<li>Tel: +6(03)-7962 5888</li>
						<li>Mon-Fri 9:00am - 9:00pm</li>
						<li>Email: enquiry@tunehotels.com</li>
						<li>Tweet Us: <a href="https://twitter.com/#!/thconcierge">twitter</a></li>
					</ul>

					<ul style="margin-top:32px;">
						<li class="med"><strong>Bahasa Indonesia</strong></li>
						<li>Tel: +622129392111</li>
						<li>Mon-Fri 9:00am - 9:00pm</li>
						<li>(excluding Public Holidays)</li>
						<li>Email: <a href="mailto:info@tunehotels.com?Subject=Enquiry">
						enquiry.id@tunehotels.com</a></li>
					</ul>

					<ul style="margin:32px 0;">
						<li class="med"><strong>Thai</strong></li>
						<li>Tel: +66-2-6135888 / 02-6135888</li>
						<li>Mon-Fri 10:00am - 7:00pm</li>
						<li>(excluding Public Holidays)</li>
						<li>Email: <a href="mailto:enquiry.thai@tunehotels.com?Subject=Enquiry Thai">
						thai@tunehotels.com</a></li>
					</ul>

				</div>
			</section>
		</div>
	</div>
</div>
