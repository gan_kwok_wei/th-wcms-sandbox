<!--start / what we provide -->
<div class="what_we_provide box-shade">
<h2 style="margin:20px 0"><?php echo lang('h_info_what_we_provide');?></h2>
<table>
	<?php
	$f1 = 0;
	$query_get_hotel_feature = $this->pages_model->get_hotel_feature($hotel_id, 'we_provide');
	//echo '<!-- ' . $this->db->last_query() . ' -->';
	echo "<tr>";
	foreach($query_get_hotel_feature->result() as $data_get_hotel_feature) {
		if(!($f1 % 3)){
			echo "</tr><tr>";
		}
		$tooltip = (($data_get_hotel_feature->text_title) ? $data_get_hotel_feature->text_title : $data_get_hotel_feature->def_title) . '<br/>' . (($data_get_hotel_feature->text_content) ? $data_get_hotel_feature->text_content : $data_get_hotel_feature->def_content);
		?>
		<!--what we provide icon -->
		<td>
			<div class="will_t" title="<?php echo strip_tags($tooltip, '<p><strong><b><br>');?>" style="height:28px;">
				<div id="" style="background-image:url('<?php echo (($data_get_hotel_feature->text_icon) ? $data_get_hotel_feature->text_icon : $data_get_hotel_feature->def_icon);?>')" class="<?php echo ($data_get_hotel_feature->available == "1" ? "" : "no_");?>detail_addon_icon f_left ">
				</div>
				<div class="detail_addon_icon_name f_left">
					<?php
					$addon_text = ($data_get_hotel_feature->text_title) ? $data_get_hotel_feature->text_title : $data_get_hotel_feature->def_title;
					$add_len = strlen($addon_text);
					if($add_len > 18){
						echo substr($addon_text, 0, 15) . ' ...';
					} else {
						echo $addon_text;
					}
					?>
				</div>
				<div id="<?php echo ($data_get_hotel_feature->available == "1" ? "" : "no_");?>available" class="detail_addon_icon_avail f_right"></div>
			</div>
		</td>
		<!--what we provide icon -->
		<?php
		$f1++;
	}
	echo "</tr>";
	?>
</table>
<!--end / what we provide -->
</div>