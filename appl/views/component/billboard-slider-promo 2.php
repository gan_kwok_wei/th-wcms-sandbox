<div id="fwslider">
<div class="slider_container">		
	
	
	<?php
			
			if(isset($hotel_id)) {
				$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top", $hotel_id);
			} else {
				$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top");
			}

			if($get_page_banner->num_rows() > 0){
				foreach($get_page_banner->result() as $data_get_page_banner){
					if($data_get_page_banner->text_link){
						$link_2 = $data_get_page_banner->text_link;
					}
					else{
						$link_2 = $data_get_page_banner->def_link;
					}

					if($link_2 == ""){
						echo '<div class="slide"><img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon) . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"></div>';
					}
					else{
						echo '<a href="' . $link_2 . '"><img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon) . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"></a>';
					}
				}
			}

			//echo $this->db->last_query();
		?>
	</div>

	<div class="timers"></div>

        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>

	</div>
	
