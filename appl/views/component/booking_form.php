<!DOCTYPE HTML>
<html>
		<head>
		<meta charset="UTF-8">
		<title>Untitled Document</title>
		<style type="text/css" media="screen">
body { font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; color: #666; padding: 40px; }

h1 { margin-top: 0; }

label { font-size: 14px; font-weight: bold; text-transform: uppercase; display: block; margin-bottom: 3px; clear: both; }

.error { background-color: #FF8080; }

.hide { display: none; }

select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
display: inline-block;
height: 20px;
padding: 4px 6px;
margin-bottom: 10px;
font-size: 14px;
line-height: 20px;
color: #555555;
vertical-align: middle;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
}

select, input[type="file"] {
height: 30px;
line-height: 30px;
}

select {
width: 220px;
background-color: #ffffff;
border: 1px solid #cccccc;
}
</style>
		</head>

		<body>
		<div>
				<label>City/ Location</label>
				<select name="cboRegion" onchange="monthfromchange();" >
						<option value= "selected" >Choose One</option>
						<optgroup label="indonesia">
						<option value="JAKT">Jakarta</option>
						<option value="BALI">Kuta - Bali</option>
						<option value="BAL2">Legian - Bali</option>
						</optgroup>
						<optgroup label="malaysia">
						<option value="BITU">Bintulu</option>
						<option value="IPOH">Ipoh</option>
						<option value="JOBH">Johor Bahru</option>
						<option value="SPNG">KLIA-LCCT Airport</option>
						<option value="KOBR">Kota Bharu City Centre</option>
						<option value="SLGR">Kota Damansara</option>
						<option value="KOKI">Kota Kinabalu</option>
						<option value="KULU">Kuala Lumpur</option>
						<option value="KUCH">Kuching</option>
						<option value="KULM">Kulim</option>
						<option value="PENG">Penang</option>
						</optgroup>
						<optgroup label="philippines">
						<option value="PAMP">Angeles</option>
						<option value="CEBU">Cebu</option>
						<option value="MANL">Ermita - Manila</option>
						<option value="MAN2">Makati City</option>
						</optgroup>
						<optgroup label="thailand">
						<option value="BGKK">Bangkok</option>
						<option value="HATY">Hat Yai</option>
						<option value="PATY">Pattaya</option>
						<option value="PHUK">Phuket</option>
						</optgroup>
						<optgroup label="united kingdom">
						<option value="EDIN">Edinburgh</option>
						<option value="LNWS">London</option>
						</optgroup>
				</select>
		</div>
</body>
</html>
