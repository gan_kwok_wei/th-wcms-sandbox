<!DOCTYPE html>

<html>
<head>
<style>
.gw_dd_content_container
{
    margin-top:8px;
    float: left;
    width: 960px;
}
.gw_dd_content_hero_container
{
    float:left;
}
.gw_dd_content_title
{
    float:left;
   font-family: din-web-jetblue,Arial,Helvetica,sans-serif;
   font-weight: bold;
   font-size: 28px;
   color: #004990;
   border-bottom: 1px solid #dae8f6;
   padding-bottom: 16px;
   margin-bottom: 20px;
   width: 960px;
   padding-top:36px;
}
.gw_dd_content_stack
{
    float:left;
    width:960px;
    margin-bottom: 20px;
}
.gw_dd_content_stack>div
{
    float:left;
    width:310px;
    height: 380px;
}
.gw_dd_content_stack_middle
{
    margin-left: 15px;
    margin-right: 15px;
}
.gw_dd_content_stack_img
{
    width:310px;
    height: 180px;
    float:left;
}
.gw_dd_content_stack_offer
{
    float:left;
    width:252px;
    height: 170px;
    padding-left:28px;
    padding-right:28px;
    padding-top: 28px;
}

.gw_dd_content_stack_title
{
    float:left;
    color: #004990;
    width:100%;
}
.gw_dd_content_stack_description{
    float:left;
    font-size: 13px;
    font-family: Arial,Helvetica,sans-serif;
    color: #454545;
    margin-top: 10px;
    margin-bottom: 17px;
    width:100%;
    line-height:1.7;    
}
.gw_dd_content_stack_starrating{
    float:left;
    padding-bottom:17px;
    
    width:100%;
}
.gw_dd_content_stack_buttondiv
{
    float:left;
    margin-right:28px;
    margin-left:28px;
    border-top: 1px solid #F1F5F8;
    padding-top: 14px;
    margin-top: 6px;
    width:252px;
}

.gw_dd_content_poi
{
    float:left;
    width:960px;
}
.gw_dd_content_poi_container
{
    float:left;
    height:auto;
    width:100%;
    padding-bottom:38px;
}
.gw_dd_content_poi_numImg
{
    float:left;
    width: 62px;
    height:56px;
    font-family: din-web-jetblue,Arial,Helvetica,sans-serif;
    font-weight: bold;
    font-size: 56px;
    color:#CCCCCC;
}
.gw_dd_content_poi_title
{
    float:left;
    width: 898px;
    height: 40px;
    color: #004990;
    font-family: din-web-jetblue,Arial,Helvetica,sans-serif;
    font-weight: bold;
    font-size: 20px;
}
.gw_dd_content_poi_content
{
    float:left;
    width: 898px;
    height:auto;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 13px;
    color: #454545;
    line-height:1.7;
}

.gw_dd_content_activities
{
    float:left;
    width:960px;    
}
.gw_dd_content_activities_stack
{
    float:left; 
    width:100%;   
    padding-bottom:35px;
}
.gw_dd_content_activities_container
{
    float:left;  
    width: 430px;
    padding-right:50px;  
}
.gw_dd_content_activities_title
{
    float:left;    
    width:100%;
    font-family: din-web-jetblue,Arial,Helvetica,sans-serif;
    font-weight: bold;
    font-size:20px;
    color: #004990;
    padding-bottom:19px;
}
.gw_dd_content_activities_content
{
    float:left;
    width:100%;
    height: auto;    
    font-family: Arial,Helvetica,sans-serif;
    font-size: 13px;
    color: #454545;
}


.gw_dd_content_ymalike_title
{
    width: 960px;
    margin-top:20px;
    float:left;
    font-family: din-web-jetblue,Arial,Helvetica,sans-serif;
    font-weight: bold;
    font-size: 28px;
    color: #004990;
    padding-bottom:14px;
    margin-bottom: 20px;
    border-bottom: 1px solid #DAE8F6;
}
.gw_dd_content_ymalike_offer
{
    float:left;
    width: 232px;
    height:162px;   
    margin-right:10px;
}
.gw_dd_content_ymalike_offer_container>.gw_firstchild
{
    margin-right:11px;
}
.gw_dd_content_ymalike_offer_container>.gw_lastchild 
{   
    margin-right:0px;
    float:right;
}
.gw_dd_content_ymalike_offer .gw_dd_content_ymalike_img
{
    float:left;
    width:100%;
}
.gw_dd_content_ymalike_offer .gw_bluebar
{
    float:left;
    position:relative;
    margin-top: -35px;
    width: 100%;
    height: 35px;
    background-color:#004890;
    opacity:0.8;
    filter:alpha(opacity=80);
}
.gw_dd_content_ymalike_offer .gw_ymalike_text
{
    float:left;
    position:relative;
    margin-top:-35px;
    color: #FFF;
    font-family: din-web-jetblue,Arial,Helvetica,sans-serif;
    font-weight: bold;
    font-size: 15px;
    padding-left:20px;
    padding-top:11px;
}

.gw_dd_content_legal
{
    float:left;
    margin-top:40px;
    padding-top:30px;
    border-top: 1px solid #999999;
    color: #999999;
    font-family: Arial,Helvetica,sans-serif;
    font-weight: bold;
    font-size: 10px;
}
</style>

</head>

<body>
<div class="gw_dd_content_stack">


    <div class="gw_Offer gw_roundedCorners">
        <a href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds= local&amp;hotel_code=752&amp;intcmp=jbgadest">
            <img src="/img/vacations/hotels/hotel_752.jpg" height="180" width="310" alt="Wyndham Nassau Resort"></a>
					
					
						
		
        <div class="gw_dd_content_stack_offer" style="height: 280px;">
            <a href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds= local&amp;hotel_code=752&amp;intcmp=jbgadest"><div class="gw_dd_content_stack_title gw_smallTitle">
                Wyndham Nassau Resort</div></a>
            <div class="gw_dd_content_stack_description">
                A haven in the Bahamas where you can escape from the hectic world, the Wyndham Nassau Resort &amp; Crystal Palace Casino is a tropical paradise waiting to be discovered.</div>
            <div class="gw_dd_content_stack_starrating">
                <img src="/img/vacations/stars/3stars.png" width="121px" height="20px" alt="3 stars"></div>
        </div>
        <div class="gw_dd_content_stack_buttondiv">
            <div class="gw_launchModal gw_floatLeft">
                <div class="gw_modalBody">
                    <div class="gw_modalHeader">
                        <h2 class="gw_midTitle"> </h2>
                    </div>
                    <div class="gw_modalContent">
                        <div class="gw_smallTitle">
                            </div>
                            
					</div>
                </div>
                
            </div>
            <div class="gw_floatRight gw_orangebutton gw_roundedCorners"><a class="gw_roundedCorners" href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds= local&amp;hotel_code=752&amp;intcmp=jbgadest">LET'S GO</a></div>
        </div>
    </div>
	
	
    <div class="gw_Offer gw_roundedCorners">
        <a href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds=local&amp;hotel_code=596&amp;intcmp=jbgadest">
            <img src="/img/vacations/hotels/hotel_596.jpg" height="180" width="310" alt="Atlantis, Paradise Island Beach and Coral Towers"></a>
					
					
					
				
        <div class="gw_dd_content_stack_offer" style="height: 280px;">
            <a href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds=local&amp;hotel_code=596&amp;intcmp=jbgadest"><div class="gw_dd_content_stack_title gw_smallTitle">
                Atlantis, Paradise Island Beach and Coral Towers</div></a>
            <div class="gw_dd_content_stack_description">
                With the beauty of the Atlantis beach just steps away, the Beach Tower delivers a true Caribbean feel in a laid-back environment and is close to the beach, many marine exhibits, Lazy River Ride, Club Rush, Earth and Fire Pottery Studio, Gamer's Reef, a main pool, and the Atlantis Theatre.</div>
            <div class="gw_dd_content_stack_starrating">
                <img src="/img/vacations/stars/4stars.png" width="121px" height="20px" alt="4 stars"></div>
        </div>
        <div class="gw_dd_content_stack_buttondiv">
            <div class="gw_launchModal gw_floatLeft">
                <div class="gw_modalBody">
                    <div class="gw_modalHeader">
                        <h2 class="gw_midTitle"></h2>
                    </div>
                    <div class="gw_modalContent">
                        <div class="gw_smallTitle">
                            </div>
                            
					</div>
                </div>
                
            </div>
            <div class="gw_floatRight gw_orangebutton gw_roundedCorners"><a class="gw_roundedCorners" href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds=local&amp;hotel_code=596&amp;intcmp=jbgadest">LET'S GO</a></div>
        </div>
    </div>
	
	
    <div class="gw_Offer gw_roundedCorners gw_ex_content_stack_right">
        <a href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds=local&amp;hotel_code=548&amp;intcmp=jbgadest">
            <img src="/img/vacations/hotels/hotel_548.jpg" height="180" width="310" alt="Sheraton Nassau Beach Resort"></a>
						
					
					
				
        <div class="gw_dd_content_stack_offer" style="height: 280px;">
            <a href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds=local&amp;hotel_code=548&amp;intcmp=jbgadest"><div class="gw_dd_content_stack_title gw_smallTitle">
                Sheraton Nassau Beach Resort</div></a>
            <div class="gw_dd_content_stack_description">
                A warm Bahamian welcome awaits at Sheraton Nassau Beach Resort, Bahamas, ideally situated on a 1,000-foot stretch of Nassau's most spectacular white-sand beach.</div>
            <div class="gw_dd_content_stack_starrating">
                <img src="/img/vacations/stars/4stars.png" width="121px" height="20px" alt="4 stars"></div>
        </div>
        <div class="gw_dd_content_stack_buttondiv">
            <div class="gw_launchModal gw_floatLeft">
                <div class="gw_modalBody">
                    <div class="gw_modalHeader">
                        <h2 class="gw_midTitle"></h2>
                    </div>
                    <div class="gw_modalContent">
                        <div class="gw_smallTitle">
                            </div>
                            
					</div>
                </div>
					
            </div>
            <div class="gw_floatRight gw_orangebutton gw_roundedCorners"><a class="gw_roundedCorners" href="//vacations.jetblue.com/service/show_hotel_details.cfm?is_landing_page=true&amp;room_gds=local&amp;hotel_code=548&amp;intcmp=jbgadest">LET'S GO</a></div>
        </div>
    </div>
	
</div>
</body>
</html>