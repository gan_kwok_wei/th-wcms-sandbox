<?php
	$query_get_city = $this->pages_model->get_city($city_id);
	$data_get_city = $query_get_city->row();
?>

<div class="must_do_think">
	<h2><?php echo lang('nearby_must_do_think');?> <?php echo (($data_get_city->text_title) ? $data_get_city->text_title : $data_get_city->def_title);?></h2>
	<div class="must_do_think_content">
	<?php
		echo '<p>' . (($data_get_city->text_teaser) ? $data_get_city->text_teaser : $data_get_city->def_teaser) . '</p>';
	?>
	</div>
	<!--h2>10 Must Do things in Kota Bharu</h2>
	<div class="must_do_think_left f_left">
		<ul class="red_bullet">
			<li>Try Local lunch at Restaurant Hoover.</li>
			<li>Visit silverware factory at Jl. Sultanah Zainab 
(1.5 km from Tune Hotel Kota Bharu City Centre)</li>
			<li>Take a boat crossing Kelantan river to Kampung Laut for Sambal Daging factory and Wayang Kulit. Depart from Royal Jetty.</li>
			<li>Drive up to Rantau Panjang for The Chinese Beijing Mosque, a unique design that combines Chinese and Islamic architecture similar to the more than 1,000-year-old Niujie mosque in Beijing.</li>
			<li>Join the locals for a swim at Pantai Cahaya Bulan beach.</li>
		</ul>
	</div>
	<div class="must_do_think_right f_left">
		<ul class="red_bullet">
			<li>Visit the largest reclining Buddha in South East Asia - Wat Photiviham Buddha.</li>
			<li>Watch sunset at Royal Jetty.</li>
			<li>5 p.m. to 9 p.m. 'Dine' at the night market - Taman Selera Biluh Kubu.</li>
			<li>Pasar Besar Siti Khadijah (Centre Market) a market mostly run by women.</li>
			<li>Try local coffee at Hayaki cafe 1st floor Tune Hotel Kota Bharu City Centre.</li>
		</ul>
	</div-->
	<div class="clear"></div>
</div>