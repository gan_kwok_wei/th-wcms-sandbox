<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Ckeditor</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/ckfinder/ckfinder.js"></script>

</head>
<body>
<?php 

	$this->ckfinder->BasePath = base_url() . 'assets/js/ckfinder/';
	$this->ckfinder->Create();

// It can also be done in a single line, calling the "static"
// Create( basePath, width, height, selectFunction ) function:
// CKFinder::CreateStatic( '../../', null, null, 'ShowFileInfo' ) ;


?>
</body>
</html>
