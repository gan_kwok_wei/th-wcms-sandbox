<div id="container">

<?php
	$list_hotel = $this->pages_model->get_hotel_by_country($country_id);
//	echo $this->db->last_query();
	$first_hotel = $list_hotel->row();
//	var_dump($list_hotel);

	$lang_map = $this->session->get_dec('lang');
	if($lang_map == "en"){
		$change_lang_map = "com";
		$change_lang_map_dir = "/maps?hl=en&";
		$dir_map = "maps";
	}else if($lang_map == "id"){
		$change_lang_map = "com";
		$change_lang_map_dir = "/maps?hl=id&";
		$dir_map = "maps";
	}else if($lang_map == "th"){
		$change_lang_map = "com";
		$change_lang_map_dir = "/maps?hl=th&";
		$dir_map = "maps";
	}else if($lang_map == "ch"){
	$change_lang_map = "cn";
	$change_lang_map_dir = "/maps?hl=cn&";
	$dir_map = "ditu";
	}
 
 ?>	



<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4faa1a491ffb4f02"></script>

<div class="container clearfix">
<h4><?php echo lang('map_hotel_tune_hotel_so_far');?></h4>
	<div class="six">
		<div class="nav-sections">
			
			<ul id="page-nav" class="clearfix">
			<li style="float:left;">
				<a href="<?php echo base_url() . "hotel";?>" class="<?php echo ($country_id == NULL ? "selected" : "");?>">Overview</a>
			</li>
				<?php
					$query_country = $this->pages_model->get_country();
					foreach($query_country->result() as $data_country){
						echo "<li><a href=\"" . base_url() . "hotel/hotels/" . $data_country->country_id . "/" . underscore(strtolower($data_country->name)) . "\" class=\"" . ($data_country->country_id == $country_id ? "selected" : "") . "\">" . $data_country->name . "</a></li>";
					}
				?>
			</ul>
		</div>

		</div>
</div>

<div class="clear"></div>
<script src="http://<?php echo $dir_map . '.google.' . $change_lang_map . $change_lang_map_dir; ?>file=api&amp;v=2&amp;sensor=false&amp;key=AIzaSyBYS9BTLOAkQm9rEELQSVMCAxH728uICSE" type="text/javascript"></script>

           
<script type="text/javascript">
//map
			var map;
			var icon0;
			var newpoints = new Array();
			 
			function addLoadEvent(func) { 
				var oldonload = window.onload; 
				if (typeof window.onload != 'function'){ 
					window.onload = func
				} else { 
					window.onload = function() {
						oldonload();
						func();
					}
				}
			}
			addLoadEvent(loadMap);
			addLoadEvent(addPoints);
			function loadMap() {
				map = new GMap2(document.getElementById("gmap"));
				map.addControl(new GLargeMapControl());
				map.addControl(new GMapTypeControl());
				map.setCenter(new GLatLng(<?php echo $first_hotel->latitude;?>,<?php echo $first_hotel->longitude;?>), 4);
				map.setMapType(G_NORMAL_MAP);
			 
				icon0 = new GIcon();
				icon0.image = "<?php echo ASSET_PATH; ?>media/images/img/maps-pointer.png";
				icon0.shadow = "<?php echo ASSET_PATH; ?>media/images/img/maps-pointer-shadow.png";
				icon0.iconSize = new GSize(35, 40);
				icon0.shadowSize = new GSize(37, 34);
				icon0.iconAnchor = new GPoint(9, 34);
				icon0.infoWindowAnchor = new GPoint(9, 2);
				icon0.infoShadowAnchor = new GPoint(18, 25);
			}
			  
			function createMarker(point, icon, popuphtml) {
				var popuphtml = "<div id=\"popup\">" + popuphtml + "<\/div>";
				var marker = new GMarker(point, icon);
				GEvent.addListener(marker, "click", function() {
					marker.openInfoWindowHtml(popuphtml);
				});
				return marker;
			}

			function addPoints() {
<?php 
	$h = 0;
	foreach($list_hotel->result() as $hotel) {
?>
				newpoints[<?php echo $h; ?>] = new Array(<?php echo $hotel->latitude;?>,<?php echo $hotel->longitude;?>, icon0, 'left', '<?php echo "<h3> HOTEL :</h3>" . str_replace("\n", "", str_replace("\r\n", "", $hotel->text_address)); ?>'); 

<?php
		$h++;
	}
?>
//				newpoints[0] = new Array(<?php echo $first_hotel->latitude;?>,<?php echo $first_hotel->longitude;?>, icon0, 'left', '<?php echo "<h3> HOTEL :</h3>" . str_replace("\n", "", str_replace("\r\n", "", $first_hotel->text_address)); ?>'); 
			
				for(var i = 0; i < newpoints.length; i++) {
					var point = new GPoint(newpoints[i][1],newpoints[i][0]);
					var popuphtml = newpoints[i][4] ;
					var marker = createMarker(point, newpoints[i][2], popuphtml);
					map.addOverlay(marker);
				}
			}
</script>
<div class="row align-center">

	<div id="gmap" style="width:980px;height:480px;"></div>
<?php 
	/*switch($country_id) {
		case 1 :
			echo "<div id='gmap' style='width:450px;height:425px;' align='center'></div>";
		break;
		case 2 :
			echo "<div id='gmap' style='width:450px;height:425px;' align='center'></div>";
		break;
		case 3 :
			echo "<div id='gmap' style='width:450px;height:425px;' align='center'></div>";
		break;
		case 4 :
			echo "<div id='gmap' style='width:450px;height:425px;' align='center'></div>";
		break;
		case 5 :
			echo "<div id='gmap' style='width:450px;height:425px;' align='center'></div>";
		break;
		case 0 :
			echo "<div id='gmap' style='width:450px;height:425px;' align='center'></div>";
		break;
	}  
			 
		*/?>
	
	</div>
