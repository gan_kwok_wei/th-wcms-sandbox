<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.idTabs.min.js"></script>
<script type="text/javascript"> 
	$(function(){
		$("#ul_inquiry").idTabs(); 
		
		$('.error_span').hide();
	});
	
	function cek_inquiry_gen(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var gen_name = $('input[name="gen_name"]').val();
		var gen_email = $('input[name="gen_email"]').val();
		var gen_address = $('input[name="gen_address"]').val();
		var gen_post_code = $('input[name="gen_post_code"]').val();
		var gen_country = $('select[name="gen_country"]').val();
		var gen_hotel = $('select[name="gen_hotel"]').val();
		var gen_remarks = $('textarea[name="gen_remarks"]').val();
		var next = '1';
		
		if($.trim(gen_name) == ''){
			$('input[name="gen_name"]').addClass('input_error');
			$('input[name="gen_name"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(gen_email) == ''){
			$('input[name="gen_email"]').addClass('input_error');
			$('input[name="gen_email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(gen_hotel) == ''){
			$('.error_hotel').show();
			var next = '0';
		}
		
		if(next == '1'){
			$('#btn_submit1').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('ajax');?>",
				data: { act: "insert_inquiry", 
						hotel_inquiry_type: "general_inquiry",
						company_name : '',
						contact_person : gen_name,
						telephone : '',
						mobile : '',
						address : gen_address,
						postcode : gen_post_code,
						country_id : gen_country,
						fax : '',
						email : gen_email,
						hotel_id : gen_hotel,
						in_year : '',
						in_month : '',
						in_day : '',
						out_year : '',
						out_month : '',
						out_day : '',
						no_of_night : '',
						no_of_room : '',
						no_of_pax : '',
						room_type : '',
						remarks : gen_remarks}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#btn_submit1').removeAttr('disabled');
				if(msg == '1'){
					$('#id_inquiry_from1')[0].reset();
					alert('Successfull send inquiry');
				}
				else{
					alert('Failed send inquiry' + msg);
				}
			});
		}
		return false;
		
	}
	
	function cek_inquiry_adv(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var adv_name = $('input[name="adv_name"]').val();
		var adv_email = $('input[name="adv_email"]').val();
		var adv_address = $('input[name="adv_address"]').val();
		var adv_post_code = $('input[name="adv_post_code"]').val();
		var adv_country = $('select[name="adv_country"]').val();
		var adv_hotel = $('select[name="adv_hotel"]').val();
		var adv_remarks = $('textarea[name="adv_remarks"]').val();
		var next = '1';
		
		if($.trim(adv_name) == ''){
			$('input[name="adv_name"]').addClass('input_error');
			$('input[name="adv_name"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(adv_email) == ''){
			$('input[name="adv_email"]').addClass('input_error');
			$('input[name="adv_email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(adv_hotel) == ''){
			$('.adv_error_hotel').show();
			var next = '0';
		}
		
		if(next == '1'){
			$('#adv_btn_submit').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('ajax');?>",
				data: { act: "insert_inquiry", 
						hotel_inquiry_type: "advertise_inquiry",
						company_name : '',
						contact_person : adv_name,
						telephone : '',
						mobile : '',
						address : adv_address,
						postcode : adv_post_code,
						country_id : adv_country,
						fax : '',
						email : adv_email,
						hotel_id : adv_hotel,
						in_year : '',
						in_month : '',
						in_day : '',
						out_year : '',
						out_month : '',
						out_day : '',
						no_of_night : '',
						no_of_room : '',
						no_of_pax : '',
						room_type : '',
						remarks : adv_remarks}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#adv_btn_submit').removeAttr('disabled');
				if(msg == '1'){
					$('#id_inquiry_from_adv')[0].reset();
					alert('Successfull send inquiry');
				}
				else{
					alert('Failed send inquiry' + msg);
				}
			});
		}
		return false;
	}
	
	function cek_inquiry_fra(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var fra_name = $('input[name="fra_name"]').val();
		var fra_email = $('input[name="fra_email"]').val();
		var fra_address = $('input[name="fra_address"]').val();
		var fra_post_code = $('input[name="fra_post_code"]').val();
		var fra_country = $('select[name="fra_country"]').val();
		var fra_hotel = $('select[name="fra_hotel"]').val();
		var fra_remarks = $('textarea[name="fra_remarks"]').val();
		var next = '1';
		
		if($.trim(fra_name) == ''){
			$('input[name="fra_name"]').addClass('input_error');
			$('input[name="fra_name"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(fra_email) == ''){
			$('input[name="fra_email"]').addClass('input_error');
			$('input[name="fra_email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(fra_hotel) == ''){
			$('.fra_error_hotel').show();
			var next = '0';
		}
		
		if(next == '1'){
			$('#btn_submit_fra').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('ajax');?>",
				data: { act: "insert_inquiry", 
						hotel_inquiry_type: "franschise_inquiry",
						company_name : '',
						contact_person : fra_name,
						telephone : '',
						mobile : '',
						address : fra_address,
						postcode : fra_post_code,
						country_id : fra_country,
						fax : '',
						email : fra_email,
						hotel_id : fra_hotel,
						in_year : '',
						in_month : '',
						in_day : '',
						out_year : '',
						out_month : '',
						out_day : '',
						no_of_night : '',
						no_of_room : '',
						no_of_pax : '',
						room_type : '',
						remarks : fra_remarks}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#btn_submit_fra').removeAttr('disabled');
				if(msg == '1'){
					$('#id_inquiry_from_fra')[0].reset();
					alert('Successfull send inquiry');
				}
				else{
					alert('Failed send inquiry' + msg);
				}
			});
		}
		return false;
	}

	function cek_inquiry_fee(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var fee_name = $('input[name="fee_name"]').val();
		var fee_email = $('input[name="fee_email"]').val();
		var fee_address = $('input[name="fee_address"]').val();
		var fee_post_code = $('input[name="fee_post_code"]').val();
		var fee_country = $('select[name="fee_country"]').val();
		var fee_hotel = $('select[name="fee_hotel"]').val();
		var fee_remarks = $('textarea[name="fee_remarks"]').val();
		var next = '1';
		
		if($.trim(fee_name) == ''){
			$('input[name="fee_name"]').addClass('input_error');
			$('input[name="fee_name"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(fee_email) == ''){
			$('input[name="fee_email"]').addClass('input_error');
			$('input[name="fee_email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(fee_hotel) == ''){
			$('.fee_error_hotel').show();
			var next = '0';
		}
		
		if(next == '1'){
			$('#btn_submit_fee').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('ajax');?>",
				data: { act: "insert_inquiry", 
						hotel_inquiry_type: "feedback_inquiry",
						company_name : '',
						contact_person : fee_name,
						telephone : '',
						mobile : '',
						address : fee_address,
						postcode : fee_post_code,
						country_id : fee_country,
						fax : '',
						email : fee_email,
						hotel_id : fee_hotel,
						in_year : '',
						in_month : '',
						in_day : '',
						out_year : '',
						out_month : '',
						out_day : '',
						no_of_night : '',
						no_of_room : '',
						no_of_pax : '',
						room_type : '',
						remarks : fee_remarks}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#btn_submit_fee').removeAttr('disabled');
				if(msg == '1'){
					$('#id_inquiry_from_fee')[0].reset();
					alert('Successfull send inquiry');
				}
				else{
					alert('Failed send inquiry' + msg);
				}
			});
		}
		return false;
	}
	
	function cek_inquiry_sell(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var sell_name = $('input[name="sell_name"]').val();
		var sell_email = $('input[name="sell_email"]').val();
		var sell_address = $('input[name="sell_address"]').val();
		var sell_post_code = $('input[name="sell_post_code"]').val();
		var sell_country = $('select[name="sell_country"]').val();
		var sell_hotel = $('select[name="sell_hotel"]').val();
		var sell_remarks = $('textarea[name="sell_remarks"]').val();
		var next = '1';
		
		if($.trim(sell_name) == ''){
			$('input[name="sell_name"]').addClass('input_error');
			$('input[name="sell_name"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(sell_email) == ''){
			$('input[name="sell_email"]').addClass('input_error');
			$('input[name="sell_email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(sell_hotel) == ''){
			$('.sell_error_hotel').show();
			var next = '0';
		}
		
		if(next == '1'){
			$('#btn_submit_sell').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('ajax');?>",
				data: { act: "insert_inquiry", 
						hotel_inquiry_type: "sell_inquiry",
						company_name : '',
						contact_person : sell_name,
						telephone : '',
						mobile : '',
						address : sell_address,
						postcode : sell_post_code,
						country_id : sell_country,
						fax : '',
						email : sell_email,
						hotel_id : sell_hotel,
						in_year : '',
						in_month : '',
						in_day : '',
						out_year : '',
						out_month : '',
						out_day : '',
						no_of_night : '',
						no_of_room : '',
						no_of_pax : '',
						room_type : '',
						remarks : sell_remarks}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#btn_submit_sell').removeAttr('disabled');
				if(msg == '1'){
					$('#id_inquiry_from_sell')[0].reset();
					alert('Successfull send inquiry');
				}
				else{
					alert('Failed send inquiry' + msg);
				}
			});
		}
		return false;
	}
</script>

<?php
	$get_content = $this->pages_model->get_content('30');
	$data_get_content = $get_content->row();
	
	//update hits
	$set_content_hits = $this->pages_model->set_content_hits('30');
?>
		
<div class="contact_us_full">
	<h1><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></h1>
	<div class="contact_us_full_content will_red_bullet">
		<?php
			echo (($data_get_content->text_content) ? $data_get_content->text_content : $data_get_content->def_content);
		?>
	</div>
</div>

<div class="form_inquiry">
    <ul id="ul_inquiry">
        <li>
            <a href="#tab_general_inquiry"><?php echo lang('help_c_gen_in');?></a>
        </li>
        <li>
            <a href="#tab_advertise_with_tune"><?php echo lang('help_c_adv_tune');?></a>
        </li>
        <li>
            <a href="#tab_enquiry_franchise"><?php echo lang('help_c_enq_fra');?></a>
        </li>
        <li>
            <a href="#tab_feedback"><?php echo lang('help_c_feed');?></a>
        </li>
        <li>
            <a href="#tab_sell_rent_property"><?php echo lang('help_c_sell');?></a>
        </li>
    </ul>
    <div class="content_form_inquiry clean" id="tab_general_inquiry">
		<form name="inquiry_gen" action="#" method="POST" onsubmit="return cek_inquiry_gen()" id="id_inquiry_from1">
			<div class="f_left" style="width:300px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_name');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="gen_name" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_email');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="gen_email" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_addr');?></h5>
							<input type="text" class="input_light width270" name="gen_address" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_post');?></h5>
							<input type="text" class="input_light width270" name="gen_post_code" />
						</td>
					</tr>
				</table>
			</div>
			<div class="f_left" style="width:300px;margin-left:30px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_countr');?></h5>
							<select name="gen_country" class="myselect" style="width:250px;">
								<option value=""><?php echo lang('help_c_sel_cn');?></option>
								<?php
									$query_country = $this->pages_model->get_country_all();
									foreach($query_country->result() as $data_country){
										echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_htl');?> <span class="red">*</span></h5>
							<select name="gen_hotel" class="myselect" style="width:200px;">
								<option value=""><?php echo lang('help_c_sel_htl');?></option>
								<?php
									$query_country = $this->pages_model->get_country();
									foreach($query_country->result() as $data_country){
											$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
											if($query_hotel_by_country->num_rows() > 0){
												foreach($query_hotel_by_country->result() as $data_hotel_by_country){
													echo '<option value="' . $data_hotel_by_country->hotel_id . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
												}
											}
									}
								?>
							</select>
							<span class="span_input_error1 error_span error_hotel" style="">Please select one!</span>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_remarks');?></h5>
							<textarea class="input_light width270" name="gen_remarks" style="height:50px;"></textarea>
						</td>
					</tr>
					<tr>
						<td align="right">
							<input id="btn_submit1" class="btn_see_detail btn_book_box" type="submit" value="<?php echo lang('help_c_submit');?>">
							<span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						</td>
					</tr>
				</table>
			</div>
		</form>
        <div class="clean"></div>
    </div>
    <div class="content_form_inquiry clean" id="tab_advertise_with_tune">
        <form name="inquiry_gen" action="#" method="POST" onsubmit="return cek_inquiry_adv()" id="id_inquiry_from_adv">
			<div class="f_left" style="width:300px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_name');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="adv_name" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_email');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="adv_email" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_addr');?></h5>
							<input type="text" class="input_light width270" name="adv_address" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_post');?></h5>
							<input type="text" class="input_light width270" name="adv_post_code" />
						</td>
					</tr>
				</table>
			</div>
			<div class="f_left" style="width:300px;margin-left:30px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_countr');?></h5>
							<select name="adv_country" class="myselect" style="width:250px;">
								<option value=""><?php echo lang('help_c_sel_cn');?></option>
								<?php
									$query_country = $this->pages_model->get_country_all();
									foreach($query_country->result() as $data_country){
										echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_htl');?> <span class="red">*</span></h5>
							<select name="adv_hotel" class="myselect" style="width:200px;">
								<option value=""><?php echo lang('help_c_sel_htl');?></option>
								<?php
									$query_country = $this->pages_model->get_country();
									foreach($query_country->result() as $data_country){
											$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
											if($query_hotel_by_country->num_rows() > 0){
												foreach($query_hotel_by_country->result() as $data_hotel_by_country){
													echo '<option value="' . $data_hotel_by_country->hotel_id . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
												}
											}
									}
								?>
							</select>
							<span class="span_input_error1 error_span adv_error_hotel" style="">Please select one!</span>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_remarks');?></h5>
							<textarea class="input_light width270" name="adv_remarks" style="height:50px;"></textarea>
						</td>
					</tr>
					<tr>
						<td align="right">
							<input id="adv_btn_submit" class="btn_see_detail btn_book_box" type="submit" value="<?php echo lang('help_c_submit');?>">
							<span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						</td>
					</tr>
				</table>
			</div>
		</form>
        <div class="clean"></div>
    </div>
    <div class="content_form_inquiry clean" id="tab_enquiry_franchise">
        <form name="inquiry_gen" action="#" method="POST" onsubmit="return cek_inquiry_fra()" id="id_inquiry_from_fra">
			<div class="f_left" style="width:300px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_name');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="fra_name" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_email');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="fra_email" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_addr');?></h5>
							<input type="text" class="input_light width270" name="fra_address" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_post');?></h5>
							<input type="text" class="input_light width270" name="fra_post_code" />
						</td>
					</tr>
				</table>
			</div>
			<div class="f_left" style="width:300px;margin-left:30px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_countr');?></h5>
							<select name="fra_country" class="myselect" style="width:250px;">
								<option value=""><?php echo lang('help_c_sel_cn');?></option>
								<?php
									$query_country = $this->pages_model->get_country_all();
									foreach($query_country->result() as $data_country){
										echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_htl');?> <span class="red">*</span></h5>
							<select name="fra_hotel" class="myselect" style="width:200px;">
								<option value=""><?php echo lang('help_c_sel_htl');?></option>
								<?php
									$query_country = $this->pages_model->get_country();
									foreach($query_country->result() as $data_country){
											$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
											if($query_hotel_by_country->num_rows() > 0){
												foreach($query_hotel_by_country->result() as $data_hotel_by_country){
													echo '<option value="' . $data_hotel_by_country->hotel_id . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
												}
											}
									}
								?>
							</select>
							<span class="span_input_error1 error_span fra_error_hotel" style="">Please select one!</span>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_remarks');?></h5>
							<textarea class="input_light width270" name="fra_remarks" style="height:50px;"></textarea>
						</td>
					</tr>
					<tr>
						<td align="right">
							<input id="btn_submit_fra" class="btn_see_detail btn_book_box" type="submit" value="<?php echo lang('help_c_submit');?>">
							<span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						</td>
					</tr>
				</table>
			</div>
		</form>
        <div class="clean"></div>
    </div>
    <div class="content_form_inquiry clean" id="tab_feedback">
        <form name="inquiry_gen" action="#" method="POST" onsubmit="return cek_inquiry_fee()" id="id_inquiry_from_fee">
			<div class="f_left" style="width:300px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_name');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="fee_name" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_email');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="fee_email" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_addr');?></h5>
							<input type="text" class="input_light width270" name="fee_address" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_post');?></h5>
							<input type="text" class="input_light width270" name="fee_post_code" />
						</td>
					</tr>
				</table>
			</div>
			<div class="f_left" style="width:300px;margin-left:30px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_countr');?></h5>
							<select name="fee_country" class="myselect" style="width:250px;">
								<option value=""><?php echo lang('help_c_sel_cn');?></option>
								<?php
									$query_country = $this->pages_model->get_country_all();
									foreach($query_country->result() as $data_country){
										echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_htl');?> <span class="red">*</span></h5>
							<select name="fee_hotel" class="myselect" style="width:200px;">
								<option value=""><?php echo lang('help_c_sel_htl');?></option>
								<?php
									$query_country = $this->pages_model->get_country();
									foreach($query_country->result() as $data_country){
											$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
											if($query_hotel_by_country->num_rows() > 0){
												foreach($query_hotel_by_country->result() as $data_hotel_by_country){
													echo '<option value="' . $data_hotel_by_country->hotel_id . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
												}
											}
									}
								?>
							</select>
							<span class="span_input_error1 error_span fee_error_hotel" style="">Please select one!</span>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_remarks');?></h5>
							<textarea class="input_light width270" name="fee_remarks" style="height:50px;"></textarea>
						</td>
					</tr>
					<tr>
						<td align="right">
							<input id="btn_submit_fee" class="btn_see_detail btn_book_box" type="submit" value="<?php echo lang('help_c_submit');?>">
							<span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						</td>
					</tr>
				</table>
			</div>
		</form>
        <div class="clean"></div>
    </div>
    <div class="content_form_inquiry clean" id="tab_sell_rent_property">
        <form name="inquiry_gen" action="#" method="POST" onsubmit="return cek_inquiry_sell()" id="id_inquiry_from_sell">
			<div class="f_left" style="width:300px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_name');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="sell_name" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_email');?> <span class="red">*</span></h5>
							<input type="text" class="input_light width270" name="sell_email" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_addr');?></h5>
							<input type="text" class="input_light width270" name="sell_address" />
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_post');?></h5>
							<input type="text" class="input_light width270" name="sell_post_code" />
						</td>
					</tr>
				</table>
			</div>
			<div class="f_left" style="width:300px;margin-left:30px;">
				<table>
					<tr>
						<td>
							<h5><?php echo lang('help_c_countr');?></h5>
							<select name="sell_country" class="myselect" style="width:250px;">
								<option value=""><?php echo lang('help_c_sel_cn');?></option>
								<?php
									$query_country = $this->pages_model->get_country_all();
									foreach($query_country->result() as $data_country){
										echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_htl');?> <span class="red">*</span></h5>
							<select name="sell_hotel" class="myselect" style="width:200px;">
								<option value=""><?php echo lang('help_c_sel_htl');?></option>
								<?php
									$query_country = $this->pages_model->get_country();
									foreach($query_country->result() as $data_country){
											$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
											if($query_hotel_by_country->num_rows() > 0){
												foreach($query_hotel_by_country->result() as $data_hotel_by_country){
													echo '<option value="' . $data_hotel_by_country->hotel_id . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
												}
											}
									}
								?>
							</select>
							<span class="span_input_error1 error_span sell_error_hotel" style="">Please select one!</span>
						</td>
					</tr>
					<tr>
						<td>
							<h5><?php echo lang('help_c_remarks');?></h5>
							<textarea class="input_light width270" name="sell_remarks" style="height:50px;"></textarea>
						</td>
					</tr>
					<tr>
						<td align="right">
							<input id="btn_submit_sell" class="btn_see_detail btn_book_box" type="submit" value="<?php echo lang('help_c_submit');?>">
							<span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						</td>
					</tr>
				</table>
			</div>
		</form>
        <div class="clean"></div>
    </div>
</div>