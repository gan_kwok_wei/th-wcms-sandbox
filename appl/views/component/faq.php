<script>
    $(document).ready(function(){
        
        var allPanels = $('.faq_accordion > li > div').hide();
        
        $('.faq_accordion > li > a').click(function() {
            if( $(this).next().is(':hidden') ) { //If immediate next container is closed...
        		$('.faq_accordion > li > a').removeClass('active').next().slideUp(); //Remove all .acc_trigger classes and slide up the immediate next container
        		$(this).toggleClass('active').next().slideDown(); //Add .acc_trigger class to clicked trigger and slide down the immediate next container
        	}
            return false;
      });
    
    });
</script>
<div class="career">
	<?php
		$get_content = $this->pages_model->get_content('47');
		$data_get_content = $get_content->row();
		
		//update hits
		$set_content_hits = $this->pages_model->set_content_hits('47');
		
		if($data_get_content->text_content){
			$head_text = $data_get_content->text_content;
		}
		else{
			$head_text = $data_get_content->def_content;
		}
		
	?>
	<!--h1><?php echo lang('help_faq');?></h1-->
	<h1><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></h1>
	<div class="career_left booking_cancelation">
		<!--select class="myselect">
            <option value="">All</option>
        </select-->
		<?php
			if($head_text != ""){
			echo '<p style="padding:00px 0px 10px 0px; font-size:12px;">' . $head_text . '</p>';
		}
		?>
        <ul class="faq_accordion">
			<?php
				$get_content_list = $this->pages_model->get_content_list('20');
				if($get_content_list->num_rows() > 0){
					foreach($get_content_list->result() as $data_get_content_list){
			?>
						<li>
							<a href="">
								<table>
									<tr>
										<td width="25"><i>Q : </i></td>
										<td><?php echo (($data_get_content_list->text_content) ? $data_get_content_list->text_content : $data_get_content_list->def_content);?></td>
									</tr>
								</table>
							</a>
							<div>
								<table>
									<tr>
										<td width="25" style="font-size: 14px;"><i>A : </i></td>
										<td><?php echo (($data_get_content_list->text_content_1) ? $data_get_content_list->text_content_1 : $data_get_content_list->def_content_1);?></td>
									</tr>
								</table>
							</div>
						</li>
			<?php
					}
				}
			?>
        </ul>
    </div>
    <div class="clean"></div>
</div>