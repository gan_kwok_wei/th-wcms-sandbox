<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.idTabs.min.js"></script>
<script type="text/javascript"> 
	$(function(){
		$("#fee_hotel").idTabs(); 
	});
 
</script>

<?php
	$get_content = $this->pages_model->get_content('46');
	$data_get_content = $get_content->row();
	
	//update hits
	$set_content_hits = $this->pages_model->set_content_hits('46');
	
	if($data_get_content->text_content){
		$head_text = $data_get_content->text_content;
	}
	else{
		$head_text = $data_get_content->def_content;
	}
	if($head_text != ""){
		echo '<p style="padding:0px 0px 20px 0px; font-size:12px;">' . $head_text . '</p>';
	}
?>

<div class="form_inquiry">
    <ul id="fee_hotel">
		<?php
			$get_content_list = $this->pages_model->get_content_list('19');
			if($get_content_list->num_rows() > 0){
				foreach($get_content_list->result() as $data_get_content_list){
					echo '<li>
							<a href="#tab_fee_hotel_' . $data_get_content_list->content_id . '">' . (($data_get_content_list->text_title) ? $data_get_content_list->text_title : $data_get_content_list->def_title) . '</a>
						</li>';
				}
			}
		?>
    </ul>
	<?php
		$get_content_list = $this->pages_model->get_content_list('19');
		if($get_content_list->num_rows() > 0){
			foreach($get_content_list->result() as $data_get_content_list){
				echo '<div class="content_fee_hotel clean" id="tab_fee_hotel_' . $data_get_content_list->content_id . '">
							' . (($data_get_content_list->text_content) ? $data_get_content_list->text_content : $data_get_content_list->def_content) . '
					</div>';
			}
		}
	?>
</div>