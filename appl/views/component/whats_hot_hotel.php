<div>

<?php
	if(!isset($hotel_id)) $hotel_id = 0;
	$get_page_promo = $this->pages_model->get_page_promo($page_id, $position2, $hotel_id);
	if($get_page_promo->num_rows() > 0){
		echo '<h4>'.lang('whats_whats_hot').'</h4>';

		foreach($get_page_promo->result() as $data_get_page_promo){
//			var_dump($data_get_page_promo);
?>
				<div class="whats_hot_hotel_content">
					<img src="<?php echo (($data_get_page_promo->text_icon_1) ? $data_get_page_promo->text_icon_1 : $data_get_page_promo->def_icon_1); ?>"/>
					<div class="whats_hot_hotel_text">
						<h3><?php echo (($data_get_page_promo->text_title) ? $data_get_page_promo->text_title : $data_get_page_promo->def_title);?></h3>
						<span><?php echo (($data_get_page_promo->text_teaser) ? $data_get_page_promo->text_teaser : $data_get_page_promo->def_teaser);?></span>
						<br/><br/>
						<input type="button" class="btn_see_detail detail_promo" value="<?php echo lang('lets_go');?>" alt="<?php echo $data_get_page_promo->promo_id;?>" onclick="location.assign('<?php echo base_url('deals/' . $data_get_page_promo->promo_name); ?>');" />
					</div>
				</div>
<?php
		}
	}
?>
</div>
