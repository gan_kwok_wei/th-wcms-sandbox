<?php 
define('ASSET_PATH', base_url().'assets/');
if(!isset($style_extras)) { $style_extras = array(); }
	if(!isset($js_extras)) { $js_extras = array(); }
	
	$ge_page = $this->config->item('ge_page');

?>

		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/style.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/page_hotel.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/font.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/flight-calendar.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/uni_form.css" />
		
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
		
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.tools.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.ddslick.js"></script>
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.uniform.js"></script>
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.dateFormat-1.0.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
		
		<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
		<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
		
		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "ur-bc6e7663-6ee9-a9fc-9231-1c3420c75ce3"});</script>		
<script>
	$(document).ready(function(){
		$('error_span').hide();
		
		 $( "#news_birth" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "1920:2003",
			dateFormat: "yy-mm-dd"
		}); 
		
	});

	
	function send_newsletter_form(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var news_email = $('input[name="news_email"]').val();
		var news_fname = $('input[name="news_fname"]').val();
		var news_gender = $('input[name="news_gender"]:checked').val();
		var news_country_id = $('select[name="news_country_id"]').val();
		var news_birth = $('input[name="news_birth"]').val();
		var news_phone = $('input[name="news_phone"]').val();
		
		var next = '1';
		
		if($.trim(news_email) == ""){
			$('input[name="news_email"]').addClass('input_error');
			$('input[name="news_email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(news_fname) == ""){
			$('input[name="news_fname"]').addClass('input_error');
			$('input[name="news_fname"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(news_gender) == ""){
			$('.error_gender').show();
			var next = '0';
		}
		if($.trim(news_country_id) == ""){
			$('.error_hotel_news').show();
			var next = '0';
		}
		if($.trim(news_birth) == ""){
			$('.error_hotel_dob').show();
			var next = '0';
		}
		if(next == '1'){
			$('#btn_submit').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>ajax",
				data: { act: "insert_subscribe", 
						news_email: news_email,
						news_fname : news_fname,
						news_lname : news_lname,
						news_gender : news_gender,
						news_country_id : news_country_id,
						news_birth : news_birth,
						news_phone : news_phone}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#btn_submit').attr('disabled', '');
				if(msg == '1'){
					$('#newsletter_form')[0].reset();
					alert('Successfull sign up e-newsletter');
				}
				else{
					alert('Failed sign up' + msg);
				}
			});
		}
		return false;
	}
	
	
</script>

<!--div class="newsletter_small">
	<!--img class="f_left" src="<?php // echo ASSET_PATH; ?>themes/publish/<?php // echo $this->system_model->get_publish_theme(); ?>/images/assets/mail.png"/-->
	<!--h1 class="f_left"><?php echo lang('e_news_enewsletter');?></h1-->
	<!--div class="clean"></div>
	<span>
		<?php // echo lang('e_news_text');?>
	</span>
	<!--input type="text" placeholder="<?php // echo lang('e_news_your_email');?>" name="e_news_email" class="my_input_style" style="width:190px;margin-top:10px;"/-->
	<!--a href="javascript:void(0);" class="my_red_grad lightbox" style="width:197px;margin-top:10px;"><?php // echo lang('e_news_subscribe');?></a>
</div-->

<div class="e_news_later_sign_up" align="center">
<div style="width:420px; border:2px solid; color:#cf0000;">
	<div class="box_title" style="background-color: #cf0000; padding-top:5px; opacity:0.8;">
		<h1 style="margin-top: 0px; color:#FFFFFF; font-family: 'Conv_DINPRO-BOLD',Arial; font-size: 25px;"><span><?php echo lang('e_news_signup');?></span></h1>
	</div>

	<div class="box_content">
		<form name="inquiry_form" action="" method="POST" onsubmit="return send_newsletter_form();" id="newsletter_form">
			<table style="font-size:12px;">
				<tr>
					<td style="width:140px;"><?php echo lang('e_news_email');?>: <span class="red">*</span></td>
					<td>
						<input type="text" name="news_email" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_fname');?>: <span class="red">*</span></td>
					<td>
						<input type="text" name="news_fname" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_lname');?>:</td>
					<td>
						<input type="text" name="news_lname" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('e_news_gender');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;">
						<input type="radio" name="news_gender" value="m" /> <?php echo lang('e_news_gender_m');?>
						<input type="radio" name="news_gender" value="f" /> <?php echo lang('e_news_gender_f');?>
						<span class="span_input_error error_span error_gender" style="display:none;">Please select one!</span>
					<td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('corp_country');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;">
						<select class="myselect" name="news_country_id" style="width:150px;">
							<option value=""><?php echo lang('corp_sel_country');?></option>
							<?php
								$query_country = $this->pages_model->get_country_all();
								foreach($query_country->result() as $data_country){
									echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
								}
							?>
						</select>
						<span class="span_input_error error_span error_hotel_news" style="display:none;">Please select one!</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_birth');?>: <span class="red">*</span></td>
					<td>
						<input type="text" id="news_birth" name="news_birth" class="date_clean datepicker" style="" />
						<span class="span_input_error error_span error_hotel_dob" style="display:none;">Please valid date!</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_phone');?>:</td>
					<td>
						<input type="text" name="news_phone" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td>
						<br/><br/>
						<input id="btn_submit" class="btn_see_detail btn_book_box f_left" type="submit" value="<?php echo lang('sign_up');?>" > <span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						<div class="clear"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
	</div>
</div>