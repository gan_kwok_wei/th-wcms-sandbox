<link rel="stylesheet" href="<?php echo ASSET_PATH; ?>extend/fws/css/fwslider.css" media="all" />
<script src="<?php echo ASSET_PATH; ?>extend/fws/js/fwslider.js"></script>
<div id="fwslider">
	<div class="slider_container">
		<!-- div class="slide" style="width:100%;"--> 
<?php 
	
	if(isset($hotel_id)) {
		$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top", $hotel_id);
	} else {
		$get_page_banner = $this->pages_model->get_page_banner_pos($page_id, "top");
	}

	if($get_page_banner->num_rows() > 0) {
		foreach($get_page_banner->result() as $data_get_page_banner) {
			if($data_get_page_banner->text_link){
				$link_2 = $data_get_page_banner->text_link;
			}
			else{
				$link_2 = $data_get_page_banner->def_link;
			}

			if($link_2 == ""){
//				echo '<img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon) . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"/>';
			} else{
//				echo '<a href="' . $link_2 . '"><img src="' . (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image) . '" data-thumb="' . (($data_get_page_banner->text_icon) ? $data_get_page_banner->text_icon : $data_get_page_banner->def_icon) . '" alt="' . (($data_get_page_banner->text_title) ? $data_get_page_banner->text_title : $data_get_page_banner->def_title) . '"/></a>';
			}
?>
		<div class="slide"> 
                
			<!-- Slide image -->
				<img src="<?php echo (($data_get_page_banner->text_image) ? $data_get_page_banner->text_image : $data_get_page_banner->def_image); ?>">
			<!-- /Slide image -->
			
			<!-- Texts container
			<div class="slide_content">
				<div class="slide_content_wrap">
					<h4 class="title">REVEAL THE MAGIC OF LEGOLAND, MALAYSIA</h4>
					<p class="description">Enter a chance to win a stay in Tune Hotel Danga Bay, Johor</p>
					<a class="readmore" href="http://www.tunehotels.com">LET'S GO!</a>
				</div>
			</div>
			/Texts container -->
			 
		</div>
<?php
		}
	} 

	//echo $this->db->last_query();
?>

        <!-- /div-->
	</div>
	<div class="timers"></div>
	<div class="slidePrev"><span></span></div>
	<div class="slideNext"><span></span></div>

</div>
