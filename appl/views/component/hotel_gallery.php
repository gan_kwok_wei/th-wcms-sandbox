<div id="container_gallery">
	<!-- elastislide for slider -->
	<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.elastislide.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/elastislide.css" />

	<script>
		$(function(){
			$('#carousel').elastislide({
				imageW 		: 100,
				margin		: 10,
				border		: 0,
				easing		: 'easeInOutExpo'
			});
		});
	</script>

	<div class="hotel_gallery" style="text-align: center;">
<?php
	$query_get_hotel_gallery = $this->pages_model->get_hotel_gallery($hotel_id);
	if($query_get_hotel_gallery->num_rows() > 0){
		$data_image_first = $query_get_hotel_gallery->row();
?>
		<div class="detail_image">
			<img id="gallery_first" src="<?php echo $data_image_first->image;?>" style="width:705px;"/>
<?php 
		foreach($query_get_hotel_gallery->result() as $data_get_hotel_gallery){
			if($data_get_hotel_gallery->image != '') {
				$src = APPPATH .'../assets' . str_replace('/tune_media/', '/media/', $data_get_hotel_gallery->image);
				$widthdetail = 705;
				$heightdetail = 469;
				if(file_exists($src)) {
					$finfodetail = getimagesize($src);
					$finfodetail[3];
					$exs = explode(" ",$finfodetail[3]);
					$widthdetail = str_replace('width=', '', str_replace('"', '', $exs[0]));
					$heightdetail = str_replace('height=', '', str_replace('"', '', $exs[1]));
				}
			
?>		
			<img id="gallery_<?php echo $data_get_hotel_gallery->gallery_id;?>" src="<?php echo $data_get_hotel_gallery->image;?>" style="<?php echo ($heightdetail > $widthdetail) ? "height:469px;" : "width:705px;"; ?>; display: none;"/>
<?php 
			}
		}
?>
		</div>
		
		<div style="width:700px;">
			<div id="carousel" class="es-carousel-wrapper">
				<div class="hotel_gallery_list_container es-carousel">
					<ul class="hotel_gallery_ul" style="height: 67px; overflow-y: hidden;">
<?php
		foreach($query_get_hotel_gallery->result() as $data_get_hotel_gallery){
			
			if($data_get_hotel_gallery->image != '') {
				echo '<li style="background:#fff;">
					<a href="javascript:void(0);" rel="gallery_' . $data_get_hotel_gallery->gallery_id . '" rev="1"><img style="position:relative; margin:0 auto;" src="' . '/tune_media/_thumbs' . $data_get_hotel_gallery->image . '" alt="image01" /></a>
				</li>';
			}
		}
?>
					</ul>
				</div>
			</div>
		</div>
		
		<?php
			}
		?>
		<div class="clean"></div>
	</div>
</div>