
<!--start - language / countries selector-->
		<div id="this_country_lang" class="pull-right" style="margin-top:40px;"	>
				<?php
									$full_session_country = $this->db->get_where('country', array('letter_code' => strtoupper($this->session->get_dec('country'))));
									$data_full_session_country = $full_session_country->row();
									
									$tune_lang = $this->config->item('ge_language');
									echo $data_full_session_country->name . ' ' . ucwords($tune_lang[$this->session->get_dec('lang')]);
								?>
		</div>
		<div class="backdrop1"></div>
		<div class="box1">
				<div class="box_title1"><span>Please Choose Your Country And Language</span>
						<div class="close1"></div>
				</div>
				<div class="box_content1">
						<div class="boc_cont_cont1">
								<ul id="list_language">
										<?php
												$get_country_list = $this->system_model->get_country_list();
												foreach($get_country_list->result() as $data_get_country_list){
											?>
										<li> <span><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/flag/<?php echo strtolower($data_get_country_list->letter_code);?>.png" style="width:16px; height: 11px;" width="16" height="11" /> &nbsp;<?php echo $data_get_country_list->name;?></span>
												<ul>
														<?php
																$get_country_language = $this->system_model->get_country_language($data_get_country_list->country_id);
																foreach($get_country_language->result() as $data_get_country_language){
																	echo '<li><a href="http://' . $_SERVER['HTTP_HOST'] . '/' . strtolower($data_get_country_list->letter_code) . '/' . $data_get_country_language->key . '">' . $data_get_country_language->val . '</a></li>';
																}
															?>
												</ul>
										</li>
										<?php
												}
											?>
								</ul>
								<div class="clear"></div>
						</div>
				</div>
		</div>
<!--start - language / countries selector-->
