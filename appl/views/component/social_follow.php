<div class="social_container bgc-lightGrey border-color-grey round">
	<div class="inner clearfix">


		<h4 style="font-size: 18px; color: #4F5966; font-family: Arial; margin:0 0 20px 0px;"><?php echo lang('follow_follow_us');?></h4>

		<ul class="social">
			<li><a href="http://www.facebook.com/tunehotels" target="_blank"><img src="<?php echo ASSET_PATH ?>img/ui/ico-facebook.gif"></a>
			</li>

			<li><a href="https://twitter.com/tunehotels" target="_blank"><img src="<?php echo ASSET_PATH ?>img/ui/ico-twitter.gif"></a></li>

			<li><a href="https://plus.google.com/111015249573087847900/posts" target="_blank"><img src="<?php echo ASSET_PATH ?>img/ui/ico-gplus.gif"></a></li>

			<li><a href="http://www.youtube.com/user/tunehotel" target="_blank"><img src="<?php echo ASSET_PATH ?>img/ui/icon-youtube.png"></a></li>

			<li><a href="http://e.weibo.com/tunehotels" target="_blank"><img src="<?php echo ASSET_PATH ?>img/ui/ico-weibo.gif"></a></li>
			<li><a href="http://pinterest.com/tunehotels/" target="_blank"><img src="<?php echo ASSET_PATH ?>img/ui/ico-pinterest.gif"></a></li>
		</ul>

	</div>
</div>