 <?php
	$set_hotel_hits = $this->pages_model->set_hotel_hits($hotel_id);
?>

<section>
	<div class="container clearfix">
		<section id="booker">
			 <?php
		$this->load->view('component/booking_search');
		?>
		</section>

	</div>

		<div >
			<section id="billboard" class="slider-wrapper theme-default">
				<?php
				$this->output_data['page_id'] = 0;
				$this->load->view('component/big_slider', $this->output_data);
			?>
			</section>
		</div>

</section>




<div class="gutter"></div>

<div id="container" class="clearfix">
	<div class="four clearfix">
		<div class="col3 first">
			<!-- START DETAIL HOTEL -->
				<?php
					$this->load->view('component/detail_hotel');
				?>
			<!-- END DETAIL HOTEL -->
		</div>

		<div class="col1">
		<div class="whats_hot_hotel_container">
				<!-- START WHATS HOT HOTEL -->
				<?php
					$this->output_data['page_id'] = 2;
					$this->output_data['position2'] = 'right_2';
					$this->load->view('component/whats_hot_hotel', $this->output_data);
				?>
				<!-- END WHATS HOT HOTEL -->
			</div>
		</div>

		</div>
	</div>
</div>


