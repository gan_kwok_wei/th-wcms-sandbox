<div class="what_we_provide box-shade">
	<h2><?php echo lang('h_info_addon');?></h2>
	<table>
		<?php
			$f1 = 0;
			$query_get_hotel_feature = $this->pages_model->get_hotel_feature($hotel_id, 'add-on');
			echo "<tr>";
			foreach($query_get_hotel_feature->result() as $data_get_hotel_feature){
				if(!($f1 % 3)){
					echo "</tr><tr>";
				}
				
				$tooltip = (($data_get_hotel_feature->text_title) ? $data_get_hotel_feature->text_title : $data_get_hotel_feature->def_title) . '<br/>' . (($data_get_hotel_feature->text_content) ? $data_get_hotel_feature->text_content : $data_get_hotel_feature->def_content);
		?>
				<td>
					<div class="will_t" title="<?php echo $tooltip;?>" style="height:28px;">
						<div id="" style="background-image:url('<?php echo (($data_get_hotel_feature->text_icon) ? $data_get_hotel_feature->text_icon : $data_get_hotel_feature->def_icon);?>')" class="<?php echo ($data_get_hotel_feature->available == "1" ? "" : "no_");?>detail_addon_icon f_left " ></div>
						<div class="detail_addon_icon_name f_left"><?php echo ($data_get_hotel_feature->text_title) ? $data_get_hotel_feature->text_title : $data_get_hotel_feature->def_title; ?></div>
						<div id="<?php echo ($data_get_hotel_feature->available == "1" ? "" : "no_");?>available" class="detail_addon_icon_avail f_right"></div>
					</div>
				</td>
		<?php
				$f1++;
			}
			echo "</tr>";
		?>
	</table>
</div>