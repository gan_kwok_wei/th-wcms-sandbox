<script type="text/javascript">
	$(document).ready(function(){
		$('error_span').hide();
		
		$('.lightbox').click(function(){
			var alt = $(this).attr('alt');
			var screenTop = $(document).scrollTop();
			
			var doc_tinggi = $(document).height();
			var tinggi = $(window).height();
			var lebar = $(window).width();
			var tinggi_box = $('.box').height();
			var lebar_box = $('.box').width();
			var top = ((tinggi/2)-(tinggi_box/2))+screenTop;
			var left = (lebar/2)-(lebar_box/2);
			
			$('.backdrop').css({'height' : doc_tinggi + 'px'});
			$('.backdrop, .box').animate({'opacity':'.50'}, 300, 'linear');
			$('.box').animate({'opacity':'1.00'}, 300, 'linear');
			$('.backdrop, .box').css({'display' : 'block'});
			$('.box').css({'top':top + 'px', 'left':left + 'px'})
			
			var n_email = $('input[name="e_news_email"]').val();
			$('input[name="news_email"]').val(n_email);
		});

		$('.close').click(function(){
			close_box();
		});

		$('.backdrop').click(function(){
			close_box();
		});

		$( "#news_birth" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "1920:2003",
			dateFormat: "yy-mm-dd"
		});
		
	});

	function close_box()
	{
		$( "#news_birth" ).datepicker( "hide" );
		$('.backdrop, .box').animate({'opacity':'0'}, 300, 'linear', function(){
			$('.backdrop, .box').css('display', 'none');
		});
	}
	
	function send_newsletter_form(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var news_email = $('input[name="news_email"]').val();
		var news_fname = $('input[name="news_fname"]').val();
		var news_gender = $('input[name="news_gender"]:checked').val();
		var news_country_id = $('select[name="news_country_id"]').val();
		var news_birth = $('input[name="news_birth"]').val();
		var news_phone = $('input[name="news_phone"]').val();
		
		var next = '1';
		
		if($.trim(news_email) == ""){
			$('input[name="news_email"]').addClass('input_error');
			$('input[name="news_email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(news_fname) == ""){
			$('input[name="news_fname"]').addClass('input_error');
			$('input[name="news_fname"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(news_gender) == ""){
			$('.error_gender').show();
			var next = '0';
		}
		if($.trim(news_country_id) == ""){
			$('.error_hotel_news').show();
			var next = '0';
		}
		if($.trim(news_birth) == ""){
			$('.error_hotel_dob').show();
			var next = '0';
		}
		
		if(next == '1'){
			$('#btn_submit').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>ajax",
				data: { act: "insert_subscribe", 
						news_email: news_email,
						news_fname : news_fname,
						news_lname : news_lname,
						news_gender : news_gender,
						news_country_id : news_country_id,
						news_birth : news_birth,
						news_phone : news_phone}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#btn_submit').attr('disabled', '');
				if(msg == '1'){
					$('#newsletter_form')[0].reset();
					alert('Successfull sign up e-newsletter');
				}
				else{
					alert('Failed sign up' + msg);
				}
			});
		}
		return false;
	}
</script>

<div class="mod_newsletter">
	<img class="f_left" src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/mail.png"/>
	<h1 class="f_left"><?php echo lang('e_news_enewsletter');?></h1>
	<div class="clean"></div>
	<span>
		<?php echo lang('e_news_text');?>
	</span>
	<input type="text" placeholder="<?php echo lang('e_news_your_email');?>" name="e_news_email" class="my_input_style" style="width:260px;margin-top:20px;"/>
	<a href="javascript:void(0);" class="my_red_grad lightbox" style="width:268px;margin-top:20px;"><?php echo lang('e_news_subscribe');?></a>
</div>

<div class="backdrop"></div>
<div class="box" style="width:450px;">
	<div class="box_title">
		<span><?php echo lang('e_news_signup');?></span>
		<div class="close"></div>
	</div>
	<div class="box_content">
		<form name="inquiry_form" action="" method="POST" onsubmit="return send_newsletter_form();" id="newsletter_form">
			<table style="font-size:12px;">
				<tr>
					<td style="width:140px;"><?php echo lang('e_news_email');?>: <span class="red">*</span></td>
					<td>
						<input type="text" name="news_email" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_fname');?>: <span class="red">*</span></td>
					<td>
						<input type="text" name="news_fname" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_lname');?>:</td>
					<td>
						<input type="text" name="news_lname" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('e_news_gender');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;">
						<input type="radio" name="news_gender" value="m" /> <?php echo lang('e_news_gender_m');?>
						<input type="radio" name="news_gender" value="f" /> <?php echo lang('e_news_gender_f');?>
						<span class="span_input_error error_span error_gender" style="display:none;">Please select one!</span>
					<td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('corp_country');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;">
						<select class="myselect" name="news_country_id" style="width:150px;">
							<option value=""><?php echo lang('corp_sel_country');?></option>
							<?php
								$query_country = $this->pages_model->get_country_all();
								foreach($query_country->result() as $data_country){
									echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
								}
							?>
						</select>
						<span class="span_input_error error_span error_hotel_news" style="display:none;">Please select one!</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_birth');?>: <span class="red">*</span></td>
					<td>
						<input type="text" id="news_birth" name="news_birth" class="date_clean" style="" />
						<span class="span_input_error error_span error_hotel_dob" style="display:none;">Please valid date!</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_phone');?>:</td>
					<td>
						<input type="text" name="news_phone" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td>
						<br/><br/>
						<input id="btn_submit" class="btn_see_detail btn_book_box f_left" type="submit" value="<?php echo lang('sign_up');?>" > <span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						<div class="clean"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>