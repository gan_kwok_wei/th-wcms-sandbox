<?php
	$get_content = $this->pages_model->get_content('31');
	$data_get_content = $get_content->row();
	
	//update hits
	$set_content_hits = $this->pages_model->set_content_hits('31');
?>
<div class="career">
	<h1><?php echo (($data_get_content->text_title) ? $data_get_content->text_title : $data_get_content->def_title);?></h1>
	<div class="career_left booking_cancelation will_red_bullet">
		<?php
			echo (($data_get_content->text_content) ? $data_get_content->text_content : $data_get_content->def_content);
		?>
    </div>
    <div class="clean"></div>
</div>