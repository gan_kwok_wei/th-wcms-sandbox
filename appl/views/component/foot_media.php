<div class="foot_home">
	<ul class="ul_foot_home">
		<li>
			<div class="content_foot_home foot_map">
				<h1><?php echo lang('foot_home_find_us');?></h1>
				<h2><a href="#"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/quick_link.png"/></a></h2>
			</div>
		</li>
		<li>
			<div class="content_foot_home foot_video">
				<h1><?php echo lang('foot_home_see_video');?></h1>
				<h2><a href="<?php echo base_url() . "about_us/media";?>"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/quick_link.png"/></a></h2>
			</div>
		</li>
		<li>
			<div class="content_foot_home foot_corporate">
				<h1><?php echo lang('foot_home_corporate');?></h1>
				<h2><a href="<?php echo base_url() . "corporate";?>"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/quick_link.png"/></a></h2>
			</div>
		</li>
		<li>
			<div class="content_foot_home foot_guest">
				<h1><?php echo lang('foot_home_guest_review');?></h1>
				<h2><a href="#"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/quick_link.png"/></a></h2>
			</div>
		</li>
	</ul>
</div>