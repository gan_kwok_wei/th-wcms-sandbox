<?php
	if($content_id == ""){
		$get_content_list = $this->pages_model->get_content_list('9');
		$data_get_content_list = $get_content_list->row();
		$content_id = $data_get_content_list->content_id;
	}
	$get_content = $this->pages_model->get_content($content_id);
	$data_get_content = $get_content->row();
	
	$get_content_head = $this->pages_model->get_content('43');
	$data_get_content_head = $get_content_head->row();
	
	//update hits
	$set_content_hits = $this->pages_model->set_content_hits('43');
?>
<div class="career">
	<!--h1><?php echo lang('about_career_with_tune');?></h1-->
	<h1><?php echo (($data_get_content_head->text_title) ? $data_get_content_head->text_title : $data_get_content_head->def_title);?></h1>
	<?php
		if($data_get_content_head->text_content){
			$head_text = $data_get_content_head->text_content;
		}
		else{
			$head_text = $data_get_content_head->def_content;
		}
		if($head_text != ""){
			echo '<p style="padding:10px; font-size:12px;border-bottom: 1px solid #E1E1E1;">' . $head_text . '</p>';
		}
	?>
	
	<div class="career_left">
		<?php echo (($data_get_content->text_teaser) ? $data_get_content->text_teaser : $data_get_content->text_teaser);?>
	</div>
	<div class="career_right">
		<h2><?php echo lang('about_vacancy');?></h2>
		<ul>
			<?php
				$get_content_list = $this->pages_model->get_content_list('9');
				if($get_content_list->num_rows() > 0){
					foreach($get_content_list->result() as $data_get_content_list){
						echo '<li><a href="' . base_url() . 'about_us/career/' . $data_get_content_list->content_id . '/' . $data_get_content_list->content_name . '" class="' . ($content_id == $data_get_content_list->content_id ? 'selected' : '') . '">' . (($data_get_content_list->text_title) ? $data_get_content_list->text_title : $data_get_content_list->def_title) . '</a></li>';
					}
				}
			?>
		</ul>
	</div>
	<div class="clear"></div>
</div>