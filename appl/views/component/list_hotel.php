<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/easypaginate.js"></script>
<script>
	jQuery(function($){
		$('ul#will_paginate').easyPaginate({
			step:27
		});
	});
</script>


		<ul id="will_paginate">
			<?php
				$query_hotel_by_country = $this->pages_model->get_hotel_by_country($country_id);
				if($query_hotel_by_country->num_rows() > 0){
					foreach($query_hotel_by_country->result() as $data_hotel_by_country){
			?>
						<li class="col1 clearfix">
							<div class="lists_hotels box-shade clearfix">
								
								<img src="<?php echo (($data_hotel_by_country->text_icon) ? $data_hotel_by_country->text_icon : $data_hotel_by_country->def_icon);?>" width="211" height="116" style="margin-bottom:10px;">
								<div style="height: 60px; display:block;">
								<h3 style="margin:10px auto;"><?php echo lang('list_hotel_tune_hotel');?>&nbsp;<?php echo (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title);?></h3>
								</div>

								<div>
								<input class="btn" type="button" style="color:#000000; width:80%; height:32px;" value="<?php echo lang('see_detail');?>" onclick="window.location='<?php echo base_url();?>hotel/hotel_detail/<?php echo $data_hotel_by_country->hotel_id;?>/<?php echo underscore(strtolower($data_hotel_by_country->hotel_name));?>'"></div>

								</div>
								
						</li>
			<?php
					}
				}
			?>
		</ul>

	<div class="gutter"></div>
	<div id="in_pagination"></div>
	<div class="gutter"></div>
