	<!-- START FOOTER CONTAINER -->
	<div id="th-footer">
	
		<div class="footer_content">
			<div class="footer_content_left1 f_left">
				<h4><?php echo lang('footer_our_hotel');?></h4>
				<div class="foot_list_hotel border-gray">
					<h2>Malaysia</h2>
					<span><a href="#">Downtown Penang</a> / <a href="#">Kota Damansara</a> / <a href="#">Downtown, KL</a> / <a href="#">KLIA-LCCT Airport</a> / <a href="#">Danga Bay, Johor</a> / <a href="#">Kota Bharu City Centre</a> / <a href="#">Bintulu</a> / <a href="#">Kulm, Kedah</a> / <a href="#">Waterfront Kuching</a> / <a href="#">1Borneo, Kota Kinabalu</a> / <a href="#">Ipoh</a></span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>Phillipines</h2>
					<span><a href="#">Ermita, Manila</a> / <a href="#">Angeles City</a> / <a href="#">Cebu</a></span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>United Kingdom</h2>
					<span><a href="#">Westminster, London</a> / <a href="#">Liverpool Street, London</a></span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>Indonesia</h2>
					<span><a href="#">Double Six, Legian, Bali</a> / <a href="#">Kuta, Bali</a></span>
				</div>
				<div class="foot_list_hotel f_left">
					<h2>Thailand</h2>
					<span><a href="#">Hat Yai</a> / <a href="#">Pattaya</a></span>
				</div>
			</div>

			<div class="clear"></div>
		</div>
		<hr class="hr_one"/>
		<div class="footer_content">
			<div class="footer_content_left2 f_left">
				<h5><?php echo lang('footer_secure_by');?></h5>
				<img src="images/assets/secure_by.png" />
			</div>
			<div class="footer_content_right2 f_left">
				<h5><?php echo lang('footer_our_partner');?></h5>
				<!--<img src="images/assets/our_partner.png" />-->
			</div>
			<div class="clear"></div>
		</div>

		<hr class="hr_one"/>
		<div class="footer_content">
			<div class="footer_text">
				<?php echo lang('footer_text');?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<!-- END FOOTER CONTAINER -->

