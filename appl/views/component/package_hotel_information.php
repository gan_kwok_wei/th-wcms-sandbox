<div class="package_hotel round box-shade">
	<h2><?php echo lang('h_info_package');?></h2>
	<div class="package_list">
		<ul class="package_lists">
			
			<?php
				$get_hotel_package = $this->pages_model->get_hotel_package($hotel_id);
				if($get_hotel_package->num_rows() > 0){
					foreach($get_hotel_package->result() as $data_get_hotel_package){
			?>
						<li>
							<div class="hotel_package_image f_left" style="background-image: url('<?php echo (($data_get_hotel_package->text_icon) ? $data_get_hotel_package->text_icon: $data_get_hotel_package->def_icon);?>');"></div>
							<div class="desc_package f_left">
								<h4><?php echo (($data_get_hotel_package->text_title) ? $data_get_hotel_package->text_title: $data_get_hotel_package->text_title);?></h4>
								
								<?php
									$get_package_feature = $this->pages_model->get_package_feature($data_get_hotel_package->package_id);
									if($get_package_feature->num_rows() > 0){
										foreach($get_package_feature->result() as $data_get_package_feature){
											$tooltip3 = (($data_get_package_feature->text_title) ? $data_get_package_feature->text_title : $data_get_package_feature->def_title) . '<br/>' . (($data_get_package_feature->text_content) ? $data_get_package_feature->text_content : $data_get_package_feature->def_content);
								?>
											<div class="list_desc_package f_left">
												<div id="" style="background-image:url('<?php echo (($data_get_package_feature->text_icon) ? $data_get_package_feature->text_icon : $data_get_package_feature->def_icon);?>')" class="detail_addon_icon f_left will_t" title="<?php echo $tooltip3;?>"></div>
												<div class="detail_addon_icon_name f_left"><?php echo ($data_get_package_feature->text_title) ? $data_get_package_feature->text_title : $data_get_package_feature->def_title; ?></div>
												<div id="available" class="detail_addon_icon_avail f_right"></div>
											</div>
								<?php
										}
									}
								?>	
							</div>
							<div class="clear"></div>
						</li>
			<?php
					}
				}
				else{
					echo '<li>No Package</li>';
				}
			?>
		</ul>
	</div>
</div>