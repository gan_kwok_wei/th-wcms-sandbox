<div id="nearby_container">
	<div id="must_do_think_container">
		<?php
			$this->output_data2['city_id'] = $city_id;
			$this->load->view('component/must_do_think', $this->output_data2);
		?>
	</div>
	
	<!-- circular -->
	<div id="place_of_interest_container">
		<?php
			$this->output_data2['city_id'] = $city_id;
			$this->load->view('component/place_of_interest', $this->output_data2);
		?>
	</div>
	
	
	<div id="appetite_container">
		<?php
			$this->output_data2['city_id'] = $city_id;
			$this->load->view('component/appetite', $this->output_data2);
		?>
	</div>
</div>