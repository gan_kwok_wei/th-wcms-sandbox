<div class="map" id="hotel-map">
		<!-- iframe width="445" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.id/maps?q=<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>&amp;num=1&amp;ie=UTF8&amp;t=m&amp;z=14&amp;ll=<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>&amp;output=embed"></iframe><br /-->

<!-- map key -->
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=AIzaSyBYS9BTLOAkQm9rEELQSVMCAxH728uICSE" type="text/javascript"></script>

<!-- function kode latitude longitude  n detail alamat di google -->
<script type="text/javascript">


//map
var map;
var icon0;
var newpoints = new Array();

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function'){
		window.onload = func
	} else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}
addLoadEvent(loadMap);
addLoadEvent(addPoints);
function loadMap() {
	map = new GMap2(document.getElementById("gmap"));
	map.addControl(new GLargeMapControl());
	map.addControl(new GMapTypeControl());
	map.setCenter(new GLatLng(<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>), 16);
	map.setMapType(G_NORMAL_MAP);

	icon0 = new GIcon();
	icon0.image = "http://www.google.com/mapfiles/marker.png";
	icon0.shadow = "http://www.google.com/mapfiles/shadow50.png";
	icon0.iconSize = new GSize(20, 34);
	icon0.shadowSize = new GSize(37, 34);
	icon0.iconAnchor = new GPoint(9, 34);
	icon0.infoWindowAnchor = new GPoint(9, 2);
	icon0.infoShadowAnchor = new GPoint(18, 25);
}

function createMarker(point, icon, popuphtml) {
	var popuphtml = "<div id=\"popup\">" + popuphtml + "<\/div>";
	var marker = new GMarker(point, icon);
	GEvent.addListener(marker, "click", function() {
		marker.openInfoWindowHtml(popuphtml);
	});
	return marker;
}



					function addPoints() {

						newpoints[0] = new Array(<?php echo $data_hotel_detail->latitude;?>,<?php echo $data_hotel_detail->longitude;?>, icon0, 'left', '<?php echo "<h4> $hotel_title :</h4>" . str_replace("\n", "", str_replace("\r\n", "", $data_hotel_detail->text_address)); ?>');

						for(var i = 0; i < newpoints.length; i++) {
							var point = new GPoint(newpoints[i][1],newpoints[i][0]);
							var popuphtml = newpoints[i][4] ;
							var marker = createMarker(point, newpoints[i][2], popuphtml);
							map.addOverlay(marker);
						}
					}

</script>

		<div class="left" style="padding-right:10px;">
			<div id="gmap" style="width:450px;height:425px;" align="center"></div>
			<div class="clear"></div>
		</div>
	</div>