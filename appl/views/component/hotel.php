<section>

			<?php
				$this->load->view('component/map_hotel');
			?>
			
</section>


<section  id="hotel-info">
<div class="container" class="container">
	<div class="four clearfix">
		<!--start / tab -->
		<div class="col3 first red">
		<!-- START LIST HOTEL -->
			<?php
				$this->load->view('component/list_hotel');
			?>
		<!-- END LIST HOTEL -->
		</div>
		
		<!-- start/ side promo -->
		<div class="col1 black">
			<div class="whats_hot_hotel_container">
				<!-- START WHATS HOT HOTEL -->
				<?php
					$this->output_data['page_id'] = 2;
					$this->output_data['position2'] = 'right_2';
					$this->load->view('component/whats_hot_hotel', $this->output_data);
				?>
				<!-- END WHATS HOT HOTEL -->
			</div>
		</div>

	</div>
</div>
</section>
