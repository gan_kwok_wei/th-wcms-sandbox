<div class="how_to_get_here">
	<?php
		$query_hotel_detail = $this->pages_model->get_hotel($hotel_id);
		$data_hotel_detail = $query_hotel_detail->row();
		
		echo (($data_hotel_detail->text_note) ? $data_hotel_detail->text_note : $data_hotel_detail->def_note );
	?>
	<!--div class="title_how_to_get_here">
		<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/bus_get_there.png" class="f_left" />
		<div class="title_how_to_get_here_text f_left">
			<h3>Bus schedule from</h3>
			<h2>Kota Bharu by City Liner:</h2>
			<span>Single bus fare to Kuala Besut is RM6 and will take approximately 1 hour. </span>
		</div>
		<div class="clear"></div>
	</div>
	<div class="content_how_to_get_here">
		<table>
			<tr>
				<td colspan="2"><h5>Kota Bharu - K.Besut - Jerteh</h5></td>
				<td colspan="2"><h5>Jerteh - K.Besut - Kota Bharu</h5></td>
			</tr>
			<tr>
				<td><h5>Kota Bharu</h5></td>
				<td><h5>K.Besut</h5></td>
				<td><h5>Jerteh</h5></td>
				<td><h5>K.Besut</h5></td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td>6.15</td>
				<td>7.30</td>
				<td>6.15</td>
				<td>7.30</td>
			</tr>
			<tr>
				<td colspan="2">TO PERHENTIAN ISLAND</td>
				<td colspan="2">FROM PERHENTIAN ISLAND</td>
			</tr>
		</table>
	</div-->
</div>

<div class="hotel_disclaimer">
	<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/disclaimer.png" class="f_left" />
	<h4 class="f_left">Disclaimer</h4>
	<div class="clear"></div>
	<p>Prices indicated above are strictly for reference only. These prices may vary. Tune Hotels shall not be liable for any loss, damage or injury whatsoever, howsoever suffered by any participant or any loss or damage to any property of any participant arising from or in connection with his / her usage of the any transportations mentioned above.</p>
</div>