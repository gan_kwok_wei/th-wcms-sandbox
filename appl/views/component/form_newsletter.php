
<?php $this->load->view('component/popup_newsletter'); ?>
<div class="mod_newsletter">
	<DIV class="inner">
	<img class="block" src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/mail.png"/>
	<h4 class="left" style="padding-top:10px;"><?php echo lang('e_news_enewsletter');?></h4>
		<p style="margin-bottom:20px;">
			<?php echo lang('e_news_text');?>
		</p>
		<input type="text" placeholder="<?php echo lang('e_news_your_email');?>" name="e_news_email" class="my_input_style" style="width:240px;"/>
		<div style="height:10px;margin:10px 0;"></div>
		<a data-toggle="modal" href="#popup_newsletter" id="btn_newsletter" class="btn-red round"><?php echo lang('e_news_subscribe');?></a>
		<div style="height:10px;margin:10px 0;"></div>
	</DIV>
</div>
