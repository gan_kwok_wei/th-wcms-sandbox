<script>
	$(document).ready(function(){
		$('error_span').hide();
	});
	function send_inquiry(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
	
		var company_name = $('input[name="company_name"]').val();
		var contact_person = $('input[name="contact_person"]').val();
		var telephone = $('input[name="telephone"]').val();
		var mobile = $('input[name="mobile"]').val();
		var address = $('textarea[name="address"]').val();
		var postcode = $('input[name="postcode"]').val();
		var country_id = $('select[name="country_id"]').val();
		var fax = $('input[name="fax"]').val();
		var email = $('input[name="email"]').val();
		var hotel_id = $('select[name="hotel_id"]').val();
		var in_year = $('select[name="in_year"]').val();
		var in_month = $('select[name="in_month"]').val();
		var in_day = $('select[name="in_day"]').val();
		var out_year = $('select[name="out_year"]').val();
		var out_month = $('select[name="out_month"]').val();
		var out_day = $('select[name="out_day"]').val();
		var no_of_night = $('input[name="no_of_night"]').val();
		var no_of_room = $('input[name="no_of_room"]').val();
		var no_of_pax = $('input[name="no_of_pax"]').val();
		var room_type = $('select[name="room_type"]').val();
		var remarks = $('textarea[name="remarks"]').val();
		
		var next = '1';
		if($.trim(contact_person) == ''){
			$('input[name="contact_person"]').addClass('input_error');
			$('input[name="contact_person"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(mobile) == ''){
			$('input[name="mobile"]').addClass('input_error');
			$('input[name="mobile"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(email) == ''){
			$('input[name="email"]').addClass('input_error');
			$('input[name="email"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(hotel_id) == ''){
			$('.error_hotel').show();
			var next = '0';
		}
		if($.trim(in_year) == ''){
			$('.error_checkin').show();
			var next = '0';
		}
		if($.trim(in_month) == ''){
			$('.error_checkin').show();
			var next = '0';
		}
		if($.trim(in_day) == ''){
			$('.error_checkin').show();
			var next = '0';
		}
		if($.trim(out_year) == ''){
			$('.error_checkout').show();
			var next = '0';
		}
		if($.trim(out_month) == ''){
			$('.error_checkout').show();
			var next = '0';
		}
		if($.trim(out_day) == ''){
			$('.error_checkout').show();
			var next = '0';
		}
		if($.trim(no_of_night) == ''){
			$('input[name="no_of_night"]').addClass('input_error');
			$('input[name="no_of_night"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(no_of_room) == ''){
			$('input[name="no_of_room"]').addClass('input_error');
			$('input[name="no_of_room"]').removeClass('input_light');
			var next = '0';
		}
		if($.trim(room_type) == ''){
			$('.error_room_type').show();
			var next = '0';
		}
		
		if(next == '1'){
			$('#btn_submit').attr('disabled', 'disabled');
			$('.loading_img').show();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>ajax",
				data: { act: "insert_inquiry", 
						hotel_inquiry_type: "corporate",
						company_name : company_name,
						contact_person : contact_person,
						telephone : telephone,
						mobile : mobile,
						address : address,
						postcode : postcode,
						country_id : country_id,
						fax : fax,
						email : email,
						hotel_id : hotel_id,
						in_year : in_year,
						in_month : in_month,
						in_day : in_day,
						out_year : out_year,
						out_month : out_month,
						out_day : out_day,
						no_of_night : no_of_night,
						no_of_room : no_of_room,
						no_of_pax : no_of_pax,
						room_type : room_type,
						remarks : remarks}
			}).done(function( msg ) {
				$('.loading_img').hide();
				$('#btn_submit').attr('disabled', '');
				if(msg == '1'){
					$('#id_inquiry_form')[0].reset();
					alert('Successfull send inquiry');
				}
				else{
					alert('Failed send inquiry');
				}
			});
		}
		else{
			//alert('ada salah');
		}
		
		return false;
	}
</script>

<div class="corporate_content">
	<?php
		$get_content_39 = $this->pages_model->get_content('39');
		$data_get_content_39 = $get_content_39->row();
		$set_content_hits = $this->pages_model->set_content_hits('39');
		
		echo (($data_get_content_39->text_content) ? $data_get_content_39->text_content : $data_get_content_39->def_content);
	?>
	<div id="inquiry_container">
		<h1><?php echo lang('corp_inquiry_form');?></h1>
		<div class="inquiry_form_content">
			<form name="inquiry_form" action="" method="POST" onsubmit="return send_inquiry();" id="id_inquiry_form">
				<table style="width:625px">
					<tr>
						<td style="width:140px;"><?php echo lang('corp_company_name');?>:</td>
						<td>
							<input type="text" name="company_name" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_contact_person');?>: <span class="red">*</span></td>
						<td>
							<input type="text" name="contact_person" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_telp');?>:</td>
						<td>
							<input type="text" name="telephone" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_mobile');?>: <span class="red">*</span></td>
						<td>
							<input type="text" name="mobile" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td style="vertical-align:top;"><label style="display:inline-block; margin-top:10px;"><?php echo lang('corp_addr');?>:</label></td>
						<td>
							<textarea name="address" class="input_light full_width" style="height:50px;"></textarea>
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_post_code');?>:</td>
						<td>
							<input type="text" name="postcode" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_country');?>:</td>
						<td>
							<select class="myselect" name="country_id">
								<option value=""><?php echo lang('corp_sel_country');?></option>
								<?php
									$query_country = $this->pages_model->get_country_all();
									foreach($query_country->result() as $data_country){
										echo '<option value="' . $data_country->country_id . '">' . $data_country->name . '</option>';
									}
								?>
							</select>
							
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_fax');?>:</td>
						<td>
							<input type="text" name="fax" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_email');?>: <span class="red">*</span></td>
						<td>
							<input type="text" name="email" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_hotel');?>: <span class="red">*</span></td>
						<td>
							<select class="myselect" name="hotel_id">
								<option value=""><?php echo lang('corp_sel_hotel');?></option>
								<?php
									$query_country = $this->pages_model->get_country();
									foreach($query_country->result() as $data_country){
											$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
											if($query_hotel_by_country->num_rows() > 0){
												foreach($query_hotel_by_country->result() as $data_hotel_by_country){
													echo '<option value="' . $data_hotel_by_country->hotel_id . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</option>';
												}
											}
									}
								?>
							</select>
							<span class="span_input_error error_span error_hotel" style="display:none;">Please select one!</span>
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_checkin');?>: <span class="red">*</span></td>
						<td>
							<div style="margin-right:-50px;">
								<select class="myselect" name="in_year">
									<option value=""><?php echo lang('corp_year');?></option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
								</select>
								
								<select class="myselect" name="in_month">
									<option value=""><?php echo lang('corp_month');?></option>
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
								
								<select class="myselect" name="in_day">
									<option value=""><?php echo lang('corp_day');?></option>
									<?php 
										for($i=1;$i<32;$i++){
											echo "<option value=\"" . $i . "\">" . $i . "</option>";
										}
									?>
								</select>
								<span class="span_input_error error_span error_checkin" style="display:none;">Please select valid date!</span>
							</div>
							
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_checkout');?>: <span class="red">*</span></td>
						<td>
							<div style="margin-right:-50px;">
								<select class="myselect" name="out_year">
									<option value=""><?php echo lang('corp_year');?></option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
								</select>
								
								<select class="myselect" name="out_month">
									<option value=""><?php echo lang('corp_month');?></option>
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
								
								<select class="myselect" name="out_day">
									<option value=""><?php echo lang('corp_day');?></option>
									<?php 
										for($i=1;$i<32;$i++){
											echo "<option value=\"" . $i . "\">" . $i . "</option>";
										}
									?>
								</select>
								
								<span class="span_input_error error_span error_checkout" style="display:none;">Please select valid date!</span>
							</div>
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_n_night');?>: <span class="red">*</span></td>
						<td>
							<input type="text" name="no_of_night" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_no_room');?>: <span class="red">*</span></td>
						<td>
							<input type="text" name="no_of_room" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_no_pax');?>:</td>
						<td>
							<input type="text" name="no_of_pax" class="input_light full_width" />
						<td>
					</tr>
					<tr>
						<td><?php echo lang('corp_room_type');?>: <span class="red">*</span></td>
						<td>
							<select class="myselect" name="room_type" style="width:200px;">
								<option value=""><?php echo lang('corp_sel_room');?></option>
							</select>
							
							<span class="span_input_error error_span error_room_type" style="display:none;">Please select room type!</span>
						<td>
					</tr>
					<tr>
						<td style="vertical-align:top;"><label style="display:inline-block; margin-top:10px;"><?php echo lang('corp_remarks');?>:</td>
						<td>
							<textarea name="remarks" class="input_light full_width" style="height:50px;"></textarea>
						<td>
					</tr>
					<tr>
						<td>
							<br/><br/>
							<input id="btn_submit" class="btn_see_detail btn_book_box f_left" type="submit" value="<?php echo lang('book_now');?>" > <span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
							<div class="clean"></div>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<script>
	$('select[name="hotel_id"]').change(function(){
		var nilai = $(this).val();
		
		$('select[name="room_type"]').empty();
		
		if(nilai != ""){
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>ajax",
				data: { act: "get_room", hotel_id: nilai }
			}).done(function( msg ) {
				$('select[name="room_type"]').html(msg);
			});
		}
	});
</script>