	<div id="content-container">
		<div id="corporate_container_left" class="f_left">
			<div id="corporate_container-book">
				<!-- START BOOKING VIEW -->
				<?php
					$this->load->view('component/booking_search.php');
				?>
				<!-- END BOOKING VIEW -->
			</div>
			
			<div id="contact_us_medium_container">
				<?php
					$this->load->view('component/contact_us_medium.php');
				?>
			</div>
			
			<!-- start promo -->
			<div id="container_promo_medium">
				<?php
					$this->output_data['page_id'] = 8;
					$this->output_data['position1'] = 'left_1';
					$this->output_data['position2'] = 'left_2';
					$this->load->view('component/image_promo_medium.php', $this->output_data);
				?>
			</div>
			<!-- end promo -->
			<div class="container_newsletter_medium">
				<!-- START NEWSLETTER MEDIUM -->
				<?php
					$this->load->view('component/newsletter_medium.php');
				?>
				<!-- END NEWSLETTER MEDIUM -->
			</div>
		</div>
		
		<div id="corporate_container_right" class="f_left">
			<div class="top_page">
				<div class="bread_crumb f_left">
					<?php
						echo '<a href="' . base_url() . '">' . lang('home') . '</a> > ';
						echo lang('search');
					?>
				</div>
				<div class="share f_right">
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style ">
					<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
					<a class="addthis_counter addthis_pill_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
					<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4faa1a491ffb4f02"></script>
					<!-- AddThis Button END -->
				</div>
				<div class="clear"></div>
				
				<div id="corporate_content_container">
					<div class="page_content">
						<h1><?php echo lang('search_result');?></h1>
						<?php
							if(isset($_POST['q'])){
								$this->pages_model->set_search($_POST['q'], $_SERVER['REMOTE_ADDR']);
								$n = '0';
								
								$get_search = $this->pages_model->get_search($_POST['q']);
								if($get_search->num_rows() > 0){
									$n = '1';
									foreach($get_search->result() as $data_get_search){
										echo '<div class="search_result">
											<h5>' . $data_get_search->text_title . '</h5>
											<p>' . substr($data_get_search->text_teaser, 0, 200) . ' ...</p></p><a href="#" class="read_more">' . lang('search_readmore') . '</a>
										</div>';
									}
								}
								
								$get_search = $this->pages_model->get_search_media($_POST['q']);
								if($get_search->num_rows() > 0){
									$n = '1';
									foreach($get_search->result() as $data_get_search){
										echo '<div class="search_result">
											<h5>' . $data_get_search->text_title . '</h5>
											<p>' . substr($data_get_search->text_content, 0, 200) . ' ...</p></p><a href="#" class="read_more">' . lang('search_readmore') . '</a>
										</div>';
									}
								}
								if($n == '0'){
									echo 'No result found.';
								}
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
	</div>
</div>
<!-- end body container -->