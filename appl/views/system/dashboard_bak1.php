<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript">
<!--
	$().ready(function() {
<?php 
	if($outstanding_hotel->num_rows() > 0) {
?>
		osHotelTable = $('#os_hotel_table').dataTable({
							"bJQueryUI": true, 
							"sPaginationType": "full_numbers", 
							"oLanguage": {"sSearch": "Search all columns:"}
		});

<?php 
	}
	
	if($outstanding_promo->num_rows() > 0) {
?>
		osPromoTable = $('#os_promo_table').dataTable({
							"bJQueryUI": true, 
							"sPaginationType": "full_numbers", 
							"oLanguage": {"sSearch": "Search all columns:"}
		});

<?php 
	}
	if($outstanding_banner->num_rows() > 0) {
?>
		osBannerTable = $('#os_banner_table').dataTable({
							"bJQueryUI": true, 
							"sPaginationType": "full_numbers", 
							"oLanguage": {"sSearch": "Search all columns:"}
		});

<?php 
	}
	if($outstanding_content->num_rows() > 0) {
?>
		osContentTable = $('#os_content_table').dataTable({
							"bJQueryUI": true, 
							"sPaginationType": "full_numbers", 
							"oLanguage": {"sSearch": "Search all columns:"}
		});

<?php 
	}
?>
	
	
	});
//-->
</script>

<div id="main">
<h2 id="content_title"><?php echo get_user_data('name'); ?> Dashboard</h2>
<?php
	show_message();
?>

	<div class="sections">
		<div class="seccentwhite">
			<div class="col3">
						
				<!-- Static Page -->
				<div class="static">
					<div class="sec1">
						<div class="corporate">
							<h2 class="white headingSmall"><div id="titleCorporate">Statistic</div></h2>
<?php 
	if($hits_hotel->num_rows() > 0) {
?>
							<strong>Most viewed Hotel :</strong>
							<ul id="nav" style="margin: 0; padding: 0;">
<?php 
		foreach($hits_hotel->result() as $hits_hotel_row) {
			echo '<li>[' . $hits_hotel_row->hits . '] ' . $hits_hotel_row->hotel_name . '</li>'; 
		}
?>
							</ul>
							<br>
<?php 
	}

	if($hits_promo->num_rows() > 0) {
?>
							<strong>Most viewed Promo :</strong>
							<ul id="nav" style="margin: 0; padding: 0;">
<?php 
		foreach($hits_promo->result() as $hits_promo_row) {
			echo '<li>[' . $hits_promo_row->hits . '] ' . $hits_promo_row->promo_name . '</li>'; 
		}
?>
							</ul>
							<br>
<?php 
	}

	if($hits_content->num_rows() > 0) {
?>
							<strong>Most viewed Content :</strong>
							<ul id="nav" style="margin: 0; padding: 0;">
<?php 
		foreach($hits_content->result() as $hits_content_row) {
			echo '<li>[' . $hits_content_row->hits . '] ' . $hits_content_row->content_name . '</li>'; 
		}
?>
							</ul>
							<br>
<?php 
	}
?>
						</div>
						<div class="corporate_footer"></div>
						
					</div>
					<div class="sec2">
<?php 
	if($outstanding_hotel->num_rows() > 0) {
?>
						<div class="group_title">Outstanding Hotel</div>
						<table cellpadding="0" cellspacing="0" border="0" class="display" id="os_hotel_table">
							<thead>
								<!-- tr>
									<th width="20" class="ui-state-default"></th>
									<th class="ui-state-default"><input type="text" name="search_country" value="Search Country" class="search_init" /></th>
									<th class="ui-state-default"><input type="text" name="search_city" value="Search City" class="search_init" /></th>
									<th class="ui-state-default"><input type="text" name="search_hotel" value="Search Hotel" class="search_init" /></th>
								</tr-->
								<tr>
									<th>ID</th>
									<th>Country</th>
									<th>City</th>
									<th>Title</th>
									<th>Author</th>
								</tr>
							</thead>
							<tbody>
<?php 
		foreach($outstanding_hotel->result() as $outstanding_hotel_row) {
?>
								<tr>
									<td><?php echo $outstanding_hotel_row->city_id; ?></td>
									<td><a href="<?php echo base_url() . 'system/hotel/hotel_edit/' . $outstanding_hotel_row->hotel_id; ?>" title="<?php echo $outstanding_hotel_row->hotel_name; ?>"><?php echo $outstanding_hotel_row->country_name; ?></a></td>
									<td><a href="<?php echo base_url() . 'system/hotel/hotel_edit/' . $outstanding_hotel_row->hotel_id; ?>" title="<?php echo $outstanding_hotel_row->hotel_name; ?>"><?php echo $outstanding_hotel_row->city_name; ?></a></td>
									<td><a href="<?php echo base_url() . 'system/hotel/hotel_edit/' . $outstanding_hotel_row->hotel_id; ?>" title="<?php echo $outstanding_hotel_row->hotel_name; ?>"><?php echo $outstanding_hotel_row->hotel_name; ?></a></td>
									<td><a href="<?php echo base_url() . 'system/hotel/hotel_edit/' . $outstanding_hotel_row->hotel_id; ?>" title="<?php echo $outstanding_hotel_row->hotel_name; ?>"><?php echo $outstanding_hotel_row->first_name . ' ' . $outstanding_hotel_row->last_name; ?></a></td>
								</tr>
<?php 
		}
?>
							</tbody>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Country</th>
									<th>City</th>
									<th>Title</th>
									<th>Author</th>
								</tr>
							</tfoot>
						</table><br/>
					
<?php 
	}
	
	if($outstanding_promo->num_rows() > 0) {
?>
							<div class="group_title">Outstanding Promo</div>
							<table cellpadding="0" cellspacing="0" border="0" class="display" id="os_promo_table">
								<thead>
									<!--tr>
										<th width="20" class="ui-state-default"></th>
										<th class="ui-state-default"><input type="text" name="search_country" value="Search Country" class="search_init" /></th>
										<th class="ui-state-default"><input type="text" name="search_city" value="Search City" class="search_init" /></th>
									</tr-->
									<tr>
										<th>ID</th>
										<th>Hotel</th>
										<th>Title</th>
										<th>Author</th>
									</tr>
								</thead>
								<tbody>
<?php 
		foreach($outstanding_promo->result() as $outstanding_promo_row) {
?>
									<tr>
										<td><?php echo $outstanding_promo_row->promo_id; ?></td>
										<td><a href="<?php echo base_url() . 'system/promo/promo_edit/' . $outstanding_promo_row->promo_id; ?>" title="<?php echo $outstanding_promo_row->promo_name; ?>"><?php echo $outstanding_promo_row->hotel_name; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/promo/promo_edit/' . $outstanding_promo_row->promo_id; ?>" title="<?php echo $outstanding_promo_row->promo_name; ?>"><?php echo $outstanding_promo_row->promo_name; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/promo/promo_edit/' . $outstanding_promo_row->promo_id; ?>" title="<?php echo $outstanding_promo_row->promo_name; ?>"><?php echo $outstanding_promo_row->first_name . ' ' . $outstanding_promo_row->last_name; ?></a></td>
									</tr>
<?php 
		}
?>
								</tbody>
								<tfoot>
									<tr>
										<th>ID</th>
										<th>Hotel</th>
										<th>Title</th>
										<th>Author</th>
									</tr>
								</tfoot>
							</table><br/>
<?php 
	}

	if($outstanding_banner->num_rows() > 0) {
?>
							<div class="group_title">Outstanding Banner</div>
							<table cellpadding="0" cellspacing="0" border="0" class="display" id="os_banner_table">
								<thead>
									<!--tr>
										<th width="20" class="ui-state-default"></th>
										<th class="ui-state-default"><input type="text" name="search_category" value="Search category" class="search_init" /></th>
										<th class="ui-state-default"><input type="text" name="search_title" value="Search title" class="search_init" /></th>
										<th class="ui-state-default"><input type="text" name="search_link" value="Search link" class="search_init" /></th>
									</tr-->
									<tr>
										<th>ID</th>
										<th>Category</th>
										<th>Title</th>
										<th>Link</th>
										<th>Author</th>
									</tr>
								</thead>
								<tbody>
<?php 
		foreach($outstanding_banner->result() as $outstanding_banner_row) {
?>
									<tr>
										<td><?php echo $outstanding_banner_row->banner_id; ?></td>
										<td><a href="<?php echo base_url() . 'system/cms/banner_edit/' . $outstanding_banner_row->banner_id; ?>" title="<?php echo $outstanding_banner_row->banner_name; ?>"><?php echo $outstanding_banner_row->category_title; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/cms/banner_edit/' . $outstanding_banner_row->banner_id; ?>" title="<?php echo $outstanding_banner_row->banner_name; ?>"><?php echo $outstanding_banner_row->title; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/cms/banner_edit/' . $outstanding_banner_row->banner_id; ?>" title="<?php echo $outstanding_banner_row->banner_name; ?>"><?php echo $outstanding_banner_row->link; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/cms/banner_edit/' . $outstanding_banner_row->banner_id; ?>" title="<?php echo $outstanding_banner_row->banner_name; ?>"><?php echo $outstanding_banner_row->first_name . ' ' . $outstanding_banner_row->last_name; ?></a></td>
									</tr>
<?php 
		}
?>
								</tbody>
								<tfoot>
									<tr>
										<th>ID</th>
										<th>Category</th>
										<th>Title</th>
										<th>Link</th>
										<th>Author</th>
									</tr>
								</tfoot>
							</table><br/>
							
<?php 
	}
	
	if($outstanding_content->num_rows() > 0) {
 ?>		
							<div class="group_title">Outstanding Content</div>
						 	<table cellpadding="0" cellspacing="0" border="0" class="display" id="os_content_table">
								<thead>
									<!-- tr>
										<th width="20" class="ui-state-default"></th>
										<th class="ui-state-default"><input type="text" name="search_category" value="Search category" class="search_init" /></th>
										<th class="ui-state-default"><input type="text" name="search_title" value="Search title" class="search_init" /></th>
										<th class="ui-state-default"><input type="text" name="search_link" value="Search link" class="search_init" /></th>
									</tr-->
									<tr>
										<th>ID</th>
										<th>Category</th>
										<th>Title</th>
										<th>Link</th>
										<th>Author</th>
									</tr>
								</thead>
								<tbody>
<?php 
		foreach($outstanding_content->result() as $outstanding_content_row) {
?>
									<tr>
										<td><?php echo $outstanding_content_row->content_id; ?></td>
										<td><a href="<?php echo base_url() . 'system/cms/content_edit/' . $outstanding_content_row->content_id; ?>" title="<?php echo $outstanding_content_row->content_name; ?>"><?php echo $outstanding_content_row->category_title; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/cms/content_edit/' . $outstanding_content_row->content_id; ?>" title="<?php echo $outstanding_content_row->content_name; ?>"><?php echo $outstanding_content_row->content_name; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/cms/content_edit/' . $outstanding_content_row->content_id; ?>" title="<?php echo $outstanding_content_row->content_name; ?>"><?php echo $outstanding_content_row->link; ?></a></td>
										<td><a href="<?php echo base_url() . 'system/cms/content_edit/' . $outstanding_content_row->content_id; ?>" title="<?php echo $outstanding_content_row->content_name; ?>"><?php echo $outstanding_content_row->first_name . ' ' . $outstanding_content_row->last_name; ?></a></td>
									</tr>
<?php 
		}
?>
								</tbody>
								<tfoot>
									<tr>
										<th width="20">ID</th>
										<th>Category</th>
										<th>Title</th>
										<th>Link</th>
										<th>Author</th>
									</tr>
								</tfoot>
							</table>
<?php 
	}
?>
						</div>
				</div>
		</div>
		
	</div>
</div>
</div>