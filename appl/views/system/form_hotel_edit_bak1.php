<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
			
		$("#form_content").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$result = $this->city_model->get_city($city_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/city');
			exit;
		}
	}
	
	$category = $this->system_model->get_parent_city();
	
	show_message();
	echo form_open('', 'id="form_city"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'city_model.save_city');
	echo ($mode != 'add') ? form_hidden('city_id', $city_id) : '';
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $data->city_id; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_city_name = array(
								'name'        => 'city_name',
								'id'          => 'city_name',
								'value'       => ($mode == 'add') ? set_value('city_name') : $data->city_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_city_name); 
			?>
			</td>
			<td><?php echo lang('label_city'); ?>*</td>
			<td><?php echo form_dropdown('city_id', $category, ($mode == 'add') ? set_value('city_id') : $data->city_id); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title"><?php echo lang('label_contact'); ?></div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_phone'); ?> 1</td>
			<td>
			<?php 
				$field_phone1 = array(
								'name'        => 'phone1',
								'id'          => 'phone1',
								'value'       => ($mode == 'add') ? set_value('phone1'): $data->phone1,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone1); 
			?>
			</td>
			<td><?php echo lang('label_phone'); ?> 2</td>
			<td>
			<?php 
				$field_phone2 = array(
								'name'        => 'phone2',
								'id'          => 'phone2',
								'value'       => ($mode == 'add') ? set_value('phone2'): $data->phone2,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone2); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_fax'); ?></td>
			<td>
			<?php 
				$field_fax = array(
								'name'        => 'fax',
								'id'          => 'fax',
								'value'       => ($mode == 'add') ? set_value('fax'): $user_data->fax,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_fax); 
			?>
			</td>
			<td><?php echo lang('label_email'); ?></td>
			<td>
			<?php 
				$field_email = array(
								'name'        => 'email',
								'id'          => 'email',
								'value'       => ($mode == 'add') ? set_value('email'): $user_data->email,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_email); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_latitude'); ?></td>
			<td>
			<?php 
				$field_latitude = array(
								'name'        => 'latitude',
								'id'          => 'latitude',
								'value'       => ($mode == 'add') ? set_value('latitude'): $data->latitude,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_latitude); 
			?>
			</td>
			<td><?php echo lang('label_longitude'); ?></td>
			<td>
			<?php 
				$field_longitude = array(
								'name'        => 'longitude',
								'id'          => 'longitude',
								'value'       => ($mode == 'add') ? set_value('longitude'): $data->longitude,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_longitude); 
			?>
			</td>
		</tr>
		
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
					</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		if($mode != 'add') {
			$result = $this->system_model->get_content_text($content_text_type, $parent_id, $lang_v);
			if($result->num_rows() > 0) {
				$text = $result->row();
			} else {
				$text = new stdClass();
				$text->title = "";
				$text->icon = "";
				$text->image = "";
				$text->teaser = "";
				$text->content = "";
				$text->link = "";
				$text->country = "";
			}
		}
		echo form_hidden("language[$lang_k]", $lang_v); 
?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table width="100%">
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td>
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => ($mode == 'add') ? set_value('title'): $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_address'); ?></td>
								<td><?php echo $this->ckeditor->editor("address[$lang_k]", ($mode == 'add') ? '' : $text->address); ?></td>
							</tr>
							<tr>
								<td><?php echo lang('label_icon'); ?></td>
								<td>
								<?php 
									$field_icon = array(
													'name'        => "icon[$lang_k]",
													'id'          => "icon_$lang_k",
													'value'       => ($mode == 'add') ? set_value('icon'): $text->icon,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_icon); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
								</td>
							</tr>
							<tr>
								<td valign="top">Feature text</td>
								<td><?php echo $this->ckeditor->editor("feature_text[$lang_k]", ($mode == 'add') ? '' : $text->feature_text); ?></td>
							</tr>
							<tr>
								<td valign="top">Note</td>
								<td><?php echo $this->ckeditor->editor("note[$lang_k]", ($mode == 'add') ? '' : $text->note); ?></td>
							</tr>
							<tr>
								<td colspan="2"><div class="group_title">Hotel Feature</div></td>
							</tr>
							<tr>
								<td colspan="2">
								
								</td>
							</tr>
							<tr>
								<td colspan="2"><div class="group_title">Room</div></td>
							</tr>
							<tr>
								<td colspan="2">
									<div style="text-align:right; border-bottom: 1px solid #999999;">
										<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick=""/>
									</div>
								<table width="100%" id="room_<?php echo $lang_k; ?>" style="border: 1px dashed #cccccc;">
									<tr>
										<td>Room type</td>
										<td>quantity</td>
										<td>quantity</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
					</div>
<?php 
	}
?>
				</div>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td><?php echo lang('label_tags'); ?></td>
			<td colspan="3">
			<?php 
				$field_tags = array(
								'name'        => 'tags',
								'id'          => 'tags',
								'value'       => ($mode == 'add') ? set_value('tags'): $data->tags,
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_tags); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0: $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE); ?></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
					echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url(); ?>system/city')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
<script type="text/javascript">
<?php 
	if($mode != 'add') {
?>
	$('#btnDelete').click(function() {	
		if(confirm("<?php echo lang('conf_delete') . $content_id; ?>")){
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "content_id=<?php echo $content_id ?>&mode=delete&action=cms_model.delete_content",
				success: function(data){//alert(typeof(data));
					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/cms/content/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
		}
	});
<?php 	
	}
?>
</script>
</div>