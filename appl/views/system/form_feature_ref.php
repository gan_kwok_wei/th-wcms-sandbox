<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
			
		$("#form_content").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$result = $this->hotel_model->get_feature_ref($feature_ref_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/dashboard/menu');
			exit;
		}
	}
	
	$category = array('we_provide' => 'What We Provide', 'add-on' => 'Add-On', 'executive' => 'Executive Package', 'connect' => 'Connect Package');
	
	show_message();
	echo form_open('', 'id="form_feature_ref"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'hotel_model.save_feature_ref');
	echo ($mode != 'add') ? form_hidden('feature_ref_id', $feature_ref_id) : '';
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $data->feature_ref_id; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_feature_name = array(
								'name'        => 'feature_name',
								'id'          => 'feature_name',
								'value'       => ($mode == 'add') ? set_value('feature_name') : $data->feature_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_feature_name); 
			?>
			</td>
			<td><?php echo lang('label_category'); ?>*</td>
			<td><?php echo form_dropdown('feature_type', $category, ($mode == 'add') ? set_value('feature_type') : $data->feature_type); ?></td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
				<?php 
					foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
						echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
					}
				?>					
					</ul>
				<?php 
					foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
						if($mode != 'add') {
							$result = $this->system_model->get_feature_ref_text($feature_ref_id, $lang_v);
							if($result->num_rows() > 0) {
								$text = $result->row();
							} else {
								$text = new stdClass();
								$text->title = "";
								$text->icon = "";
								$text->content = "";
							}
						}
						echo form_hidden("language[$lang_k]", $lang_v); 
				?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table>		
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => ($mode == 'add') ? set_value('title'): $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td><?php echo lang('label_icon'); ?></td>
								<td colspan="3">
								<?php 
									if(($mode != 'add') && ($text->icon)) {
										echo '<img src="' . $text->icon . '" /> <br/>';
									}
									$field_icon = array(
													'name'        => "icon[$lang_k]",
													'id'          => "icon_$lang_k",
													'value'       => ($mode == 'add') ? set_value('icon'): $text->icon,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_icon); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_content'); ?></td>
								<td colspan="3"><?php echo $this->ckeditor->editor("content[$lang_k]", ($mode == 'add') ? '' : $text->content); ?></td>
							</tr>
						</table>
					</div>
				<?php 
					}
				?>				
				</div>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0: $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE); ?></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/hotel/feature_ref_list'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
<script type="text/javascript">
<?php 
	if($mode != 'add') {
?>
	$('#btnDelete').click(function() {	
		if(confirm("<?php echo lang('conf_delete') . $content_id; ?>")){
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "content_id=<?php echo $content_id ?>&mode=delete&action=cms_model.delete_content",
				success: function(data){//alert(typeof(data));
					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/cms/content/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
		}
	});
<?php 	
	}
?>
</script>
</div>