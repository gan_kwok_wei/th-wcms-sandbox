<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	
	show_message();
	echo form_open('', 'id="form_list"'); 
	echo form_hidden('action', 'system_model.save_list_menu');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url(); ?>system/dashboard/menu_add');"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_name" value="Search Name" class="search_init" /></th>
				<th class="ui-state-default"><input type="text" name="search_link" value="Search link" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_parent" value="Search parent" size="5" class="search_init" /></th>
				<th width="60" class="ui-state-default"></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Link</th>
				<th>Status</th>
				<th>Parent</th>
				<th>Sort</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->system_model->get_menu_list();
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->menu_id; ?></td>
				<td><a href="<?php echo base_url('system/dashboard/menu_edit/' . $row->menu_id); ?>" title="<?php echo $row->menu_name; ?>"><?php echo $row->menu_name; ?></a></td>
				<td><?php echo $row->link; ?></a></td>
				<td><?php echo $data_status[$row->status]; ?></td>
				<td><?php echo $row->parent_id; ?></a></td>
				<td><?php echo form_input('sort[' . $row->menu_id . ']', $row->sort, ' class="inline_text"') . '<span style="display:none;"> ' . $row->sort . '</span>'; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th width="20">ID</th>
				<th>Name</th>
				<th>Link</th>
				<th>Status</th>
				<th>Parent</th>
				<th>Sort</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>