<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	show_message();
	echo form_open('', 'id="form_config"'); 
	echo form_hidden('action', 'system_model.save_config');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/user/user_add'); ?>');"/>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20">ID</th>
				<th>Name</th>
				<th>Title</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Role</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$users = $this->user_model->get_user_list();
	if($users->num_rows() > 0) {
		foreach($users->result() as $row) {
?>
			<tr>
				<td><a href="<?php echo base_url('system/user/user_edit/' . $row->user_id); ?>"><?php echo $row->external_id; ?></a></td>
				<td><a href="<?php echo base_url('system/user/user_edit/' . $row->user_id); ?>"><?php echo $row->first_name . ' ' . $row->last_name; ?></a></td>
				<td><?php echo $row->job_title; ?></td>
				<td><a href="mailto:<?php echo $row->email; ?>"><?php echo $row->email; ?></a></td>
				<td><?php echo $row->phone_work; ?></td>
				<td><?php echo $row->role; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Title</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Role</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>