<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];

		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
/*			
		$("#form_hotel_gallery").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
*/		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
		
	});

	function pop_up(hyperlink, window_name) {
		if (! window.focus)
			return true;
		var href;
		if (typeof(hyperlink) == 'string')
			href = hyperlink;
		else
			href = hyperlink.href;
		window.open(
			href ,
			window_name,
			'width=800,height=700,toolbar=no, scrollbars=yes'
		);
		return false;
	}
		
	function delete_gallery(id, gallery_id) {
		$('#gallery_' + id).remove();
		if(gallery_id != 0) {
			strDel = '<input type="hidden" name="gallery_id[' + id + ']" value="' + gallery_id + '" >' +
					'<input type="hidden" name="gallery_image[' + id + ']" value="" >';
			$('#gallery_area').append(strDel);
		}
	}
	
//	galleryCount = 1;
	function addGallery() {
		strGallery = '<tr id="gallery_' + galleryCount + '" style="border: 1px dashed #cccccc;">' +
'	<td>' +
'		<input type="hidden" name="gallery_id[' + galleryCount + ']" value="0" />' +
'		Image <input type="text" size="32" name="gallery_image[' + galleryCount + ']" id="gallery-image-' + galleryCount + '">' +
'		<input type="button" value="Browse Server" onclick="BrowseServer(\'images:\/\', \'gallery-image-' + galleryCount + '\' )" />' +
'	</td>' +
'	<td valign="bottom">Title <input type="text" size="50" name="gallery_title[' + galleryCount + ']" id="gallery-title-' + galleryCount + '"></td>' +
'	<td valign="bottom"><input type="button" value="Delete" onclick="delete_gallery(\'' + galleryCount + '\', \'0\')"></td>' +
'</tr>';
		$('#gallery_area').append(strGallery);
		galleryCount++;
	}
	
</script>
<div id="main">
<?php 
	$result = $this->hotel_model->get_hotel($hotel_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
		if((get_role() > 3) && ($data->created_id != get_user_id())) {
    		set_warning_message(lang('alert_no_permission'));
			redirect('system/hotel');
			exit;
    	}
	    	
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel');
		exit;
	}
	echo '<h2 id="content_title">Edit Hotel : ' . $data->hotel_name . '</h2>';
	$category = $this->system_model->get_parent_city();
 
	show_message();
	echo form_open('', 'id="form_hotel_gallery"');
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'hotel_model.update_hotel_gallery');
	echo form_hidden('hotel_id', $hotel_id);
?>
	<div id="hotel_tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_edit/' . $hotel_id)  ?>">Hotel Detail</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_room_edit/' . $hotel_id); ?>">Features</a></li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="<?php echo base_url('system/hotel/hotel_gallery_edit/' . $hotel_id);  ?>">Gallery</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_feature_edit/' . $hotel_id);  ?>">What We Provide</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_package_edit/' . $hotel_id);  ?>">Packages</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_meta_edit/' . $hotel_id);  ?>">Meta Data Options</a></li>
		</ul>
	</div>
	
	<table width="100%">
		<tr>
			<td><?php echo lang('label_name'); ?></td>
			<td><strong><?php echo $data->hotel_name; ?></strong></td>
			<td><?php echo lang('label_city'); ?></td>
			<td><?php echo form_dropdown('city_id', $category, $data->city_id, ' disabled="disabled"'); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Hotel Gallery</div></td>
		</tr>
		<tr>
			<td colspan="4">
				<div style="text-align:right; border-bottom: 1px solid #999999;">
					<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="addGallery()"/>
				</div>
				<table width="100%" id="gallery_area">
<?php 
		$gallery_list = $this->hotel_model->get_hotel_gallery($hotel_id);
		if($gallery_list->num_rows() > 0) {
			$i = 0;
?>
					<script type="text/javascript">
						<?php echo "galleryCount = " . ($gallery_list->num_rows() + 1) . ";" ?>
					</script>
<?php 
			foreach($gallery_list->result() as $gallery_data) {
?>
					<tr id="gallery_<?php echo $i; ?>" style="border: 1px dashed #cccccc;">
						<td>
							<input type="hidden" name="gallery_id[<?php echo $i; ?>]" value="<?php echo $gallery_data->gallery_id; ?>" />
<?php 
				if($gallery_data->image) { 
					echo '<a href="' . base_url('system/cms/image_edit/gallery-image-' . $i) . '" target="_blank" onclick="return pop_up(this, \'Image Edit\')" title="Click to edit" ><img src="' . $gallery_data->image . '" height="75px"><br/>Click to edit</a><br/>'; } 
?>
							Image <input type="text" size="32" name="gallery_image[<?php echo $i; ?>]" id="gallery-image-<?php echo $i; ?>" value="<?php echo $gallery_data->image; ?>" >
							<input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'gallery-image-<?php echo $i; ?>' );" />
						</td>
						<td valign="bottom">Title <input type="text" size="50" name="gallery_title[<?php echo $i; ?>]" id="gallery-title-<?php echo $i; ?>" value="<?php echo $gallery_data->title; ?>" ></td>
						<td valign="bottom"><input type="button" value="Delete" onclick="delete_gallery('<?php echo $i; ?>', '<?php echo $gallery_data->gallery_id; ?>')"></td>
					</tr>
<?php 
				$i++;
			}
		} else {
?>
					<script type="text/javascript">
						galleryCount = 1;
					</script>
					<tr style="border: 1px dashed #cccccc;">
						<td>
							Image <input type="text" size="32" name="gallery_image[0]" id="gallery-image-0">
							<input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'gallery-image-0' );" />
						</td>
						<td valign="bottom">Title <input type="text" size="50" name="gallery_title[0]" id="gallery-title-0"></td>
						<td valign="bottom"></td>
					</tr>
<?php 
		}
?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>

		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo form_submit('btnSave', lang('label_update')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_close'); ?>" onclick="location.replace('<?php echo base_url('system/hotel'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>