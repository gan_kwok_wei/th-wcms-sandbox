<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	var newGallery;
	
	$().ready(function() {

		$( "#gallery_area" ).sortable({
			connectWith: ".list",
			receive: function(event,ui) {
				$(newGallery).removeAttr('style')
							.attr('id', 'gallery_' + galleryCount)
							.css({'background': '#ffffff', 'border': '1px dashed #cccccc'})
							.html('<table><tr>' +
									'	<td>' +
									'		<input type="hidden" id="sort_gallery_' + galleryCount + '" name="sort[' + galleryCount + ']" value="' + galleryCount + '" />' +
									'		<input type="hidden" name="gallery_id[' + galleryCount + ']" value="0" />' +
									'		Image <input type="text" size="32" name="gallery_image[' + galleryCount + ']" id="gallery-image-' + galleryCount + '">' +
									'		<input type="button" value="Browse Server" onclick="BrowseServer(\'images:\/\', \'gallery-image-' + galleryCount + '\' )" />' +
									'	</td>' +
									'	<td valign="bottom">Title <input type="text" size="50" name="gallery_title[' + galleryCount + ']" id="gallery-title-' + galleryCount + '"></td>' +
									'	<td valign="bottom"><input type="button" value="Delete" onclick="delete_gallery(\'' + galleryCount + '\', \'0\')"></td>' +
									'</tr></table></div>');
				galleryCount++;
			},
			beforeStop: function (event, ui) { 
				newGallery = ui.item;
			},
			update: function(event, ui) {
				$.each($(this).sortable('toArray'), function(k, v) {
					$('#sort_' + v).val(k);
				});
			}
		});

		$( "#new_gallery" ).draggable({
			helper: "clone",
			connectToSortable: "#gallery_area"
		});

	});

	function pop_up(hyperlink, window_name) {
		if (! window.focus)
			return true;
		var href;
		if (typeof(hyperlink) == 'string')
			href = hyperlink;
		else
			href = hyperlink.href;
		window.open(
			href ,
			window_name,
			'width=800,height=700,toolbar=no, scrollbars=yes'
		);
		return false;
	}
		
	function delete_gallery(id, gallery_id) {
		$('#gallery_' + id).remove();
		if(gallery_id != 0) {
			strDel = '<input type="hidden" name="gallery_id[' + id + ']" value="' + gallery_id + '" >' +
					'<input type="hidden" name="gallery_image[' + id + ']" value="" >';
			$('#gallery_area').append(strDel);
		}
	}

</script>
<div id="main">
<?php 
	$result = $this->hotel_model->get_hotel($hotel_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
		if((get_role() > 3) && ($data->created_id != get_user_id())) {
    		set_warning_message(lang('alert_no_permission'));
			redirect('system/hotel');
			exit;
    	}
	    	
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel');
		exit;
	}
	echo '<h2 id="content_title">Edit Hotel : ' . $data->hotel_name . '</h2>';
	$category = $this->system_model->get_parent_city();
 
	show_message();
	echo form_open('', 'id="form_hotel_gallery"');
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'hotel_model.update_hotel_gallery');
	echo form_hidden('hotel_id', $hotel_id);
?>
	<div id="hotel_tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_edit/' . $hotel_id)  ?>">Hotel Detail</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_room_edit/' . $hotel_id); ?>">Features</a></li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="<?php echo base_url('system/hotel/hotel_gallery_edit/' . $hotel_id);  ?>">Gallery</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_feature_edit/' . $hotel_id);  ?>">What We Provide</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_package_edit/' . $hotel_id);  ?>">Packages</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_meta_edit/' . $hotel_id);  ?>">Meta Data Options</a></li>
		</ul>
	</div>
	
	<table width="100%">
		<tr>
			<td>URI name</td>
			<td><strong><?php echo $data->hotel_name; ?></strong></td>
			<td><?php echo lang('label_city'); ?></td>
			<td><?php echo form_dropdown('city_id', $category, $data->city_id, ' disabled="disabled"'); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Hotel Gallery</div></td>
		</tr>
		<tr>
			<td colspan="4">
				<div id="new_gallery" style="text-align:right; border-bottom: 1px solid #999999; text-align: center; background: #08f884; height: 20px; padding: 10px; cursor: move;">Drag to add new gallery
					<!-- input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="addGallery()"/-->
				</div>
				<div id="gallery_area" style="cursor: move;">
<?php 
		$gallery_list = $this->hotel_model->get_hotel_gallery($hotel_id);
		if($gallery_list->num_rows() > 0) {
			$i = 0;
?>
					<script type="text/javascript">
						<?php echo "galleryCount = " . ($gallery_list->num_rows() + 1) . ";" ?>
					</script>
<?php 
			foreach($gallery_list->result() as $gallery_data) {
?>
						<div id="gallery_<?php echo $i; ?>" style="border: 1px dashed #cccccc; background: #ffffff;">
						<table>
						<tr>
							<td>
								<input type="hidden" id="sort_gallery_<?php echo $i; ?>" name="sort[<?php echo $i; ?>]" value="<?php echo $gallery_data->sort; ?>" />
								<input type="hidden" name="gallery_id[<?php echo $i; ?>]" value="<?php echo $gallery_data->gallery_id; ?>" />
<?php 
				if($gallery_data->image) { 
					echo '<a href="' . base_url('system/cms/image_edit/gallery-image-' . $i) . '" target="_blank" onclick="return pop_up(this, \'Image Edit\')" title="Click to edit" ><img src="' . $gallery_data->image . '" height="75px"><br/>Click to edit</a><br/>'; } 
?>
								Image <input type="text" size="32" name="gallery_image[<?php echo $i; ?>]" id="gallery-image-<?php echo $i; ?>" value="<?php echo $gallery_data->image; ?>" >
								<input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'gallery-image-<?php echo $i; ?>' );" />
							</td>
							<td valign="bottom">Title <input type="text" size="50" name="gallery_title[<?php echo $i; ?>]" id="gallery-title-<?php echo $i; ?>" value="<?php echo $gallery_data->title; ?>" ></td>
							<td valign="bottom"><input type="button" value="Delete" onclick="delete_gallery('<?php echo $i; ?>', '<?php echo $gallery_data->gallery_id; ?>')"></td>
						</tr>
					</table>
					</div>
<?php 
				$i++;
			}
		} else {
?>
					<script type="text/javascript">
						galleryCount = 1;
					</script>
					<div id="gallery_0" style="border: 1px dashed #cccccc; background: #ffffff;">
						<table>
							<tr>
								<td>
									<input type="hidden" id="sort_0" name="sort[0]" value="1" />
									<input type="hidden" name="gallery_id[0]" value="0" />
									Image <input type="text" size="32" name="gallery_image[0]" id="gallery-image-0">
									<input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'gallery-image-0' );" />
								</td>
								<td valign="bottom">Title <input type="text" size="50" name="gallery_title[0]" id="gallery-title-0"></td>
								<td valign="bottom"></td>
							</tr>
						</table>
					</div>
<?php 
		}
?>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>

		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo form_submit('btnSave', lang('label_update')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_close'); ?>" onclick="location.replace('<?php echo base_url('system/hotel'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>