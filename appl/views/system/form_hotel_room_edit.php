<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];
	
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
/*			
		$("#form_hotel_room").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
*/		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});

		$(".room_feature_text").each(function() {
				$(this).ckeditor();
			});
		
	});

	function delete_room(lang, id, room_id) {
		$('#room_' + lang + '_' + id).remove();
		if(room_id != 0) {
			strDel = '<input type="hidden" name="room_id[' + lang + '][' + id + ']" value="' + room_id + '" >' +
					'<input type="hidden" name="room_title[' + lang + '][' + id + ']" value="" >';
			$('#room_area_' + lang).append(strDel);
		}
	}
	
	function addRoom(lang) {
		eval('roomCount = roomCount_' + lang);
		
strRoom = '<table width="100%" id="room_' + lang + '_' + roomCount + '" style="border: 1px dashed #cccccc;">' +
'<input type="hidden" name="room_id[' + lang + '][' + roomCount + ']" id="room_id-' + lang + '-' + roomCount + '" value="0" >' +
'	<tr>' +
'		<td>Title</td>' +
'		<td><input type="text" size="32" name="room_title[' + lang + '][' + roomCount + ']" id="title-' + lang + '-' + roomCount + '"> </td>' +
'		<td>Quantity</td>' +
'		<td><input type="text" name="room_quantity[' + lang + '][' + roomCount + ']" id="quantity-' + lang + '-' + roomCount + '"> </td>' +
'	</tr>' +
'	<tr>' +
'		<td valign="top" nowrap="nowrap">Image</br>(288px X 373px)</td>' +
'		<td>' +
'			<input type="text" size="32" maxlength="150" id="room_image_' + lang + '_' + roomCount + '" value="" name="image[' + lang + '][' + roomCount + ']">' +
'			<input type="button" onclick="BrowseServer(\'Files:/images\', \'room_image_' + lang + '_' + roomCount + '\' );" value="Browse Server">' +
'		</td>' +
'		<td valign="top">Feature</td>' +
'		<td><textarea rows="3" cols="50" name="room_feature_text[' + lang + '][' + roomCount + ']" id="room_feature_text_' + lang + '_' + roomCount + '"></textarea></td>' +
'	</tr>' +
'	<tr>' +
'		<td align="right" colspan="4"><input type="button" value="Delete" onclick="delete_room(\'' + lang + '\', \'' + roomCount + '\', \'0\')"></td>' +
'	</tr>' +
'</table>';
		
		$('#room_area_' + lang).append(strRoom);
		$('#room_feature_text_' + lang + '_' + roomCount).ckeditor();
		eval('roomCount_' + lang + '++');
	}

</script>
<div id="main">
<?php 
	$result = $this->hotel_model->get_hotel($hotel_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
		if((get_role() > 3) && ($data->created_id != get_user_id())) {
    		set_warning_message(lang('alert_no_permission'));
			redirect('system/hotel');
			exit;
    	}
	    	
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel');
		exit;
	}
	echo '<h2 id="content_title">Edit Hotel : ' . $data->hotel_name . '</h2>';
	$category = $this->system_model->get_parent_city();

	show_message();
	echo form_open('', 'id="form_hotel_room"');
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'hotel_model.update_hotel_room');
	echo form_hidden('hotel_id', $hotel_id);
?>
	<div id="hotel_tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_edit/' . $hotel_id)  ?>">Hotel Detail</a></li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="<?php echo base_url('system/hotel/hotel_room_edit/' . $hotel_id); ?>">Features</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_gallery_edit/' . $hotel_id);  ?>">Gallery</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_feature_edit/' . $hotel_id);  ?>">What We Provide</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_package_edit/' . $hotel_id);  ?>">Packages</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_meta_edit/' . $hotel_id);  ?>">Meta Data Options</a></li>
		</ul>
	</div>
	
	<table width="100%">
		<tr>
			<td>URI name</td>
			<td><strong><?php echo $data->hotel_name; ?></strong></td>
			<td><?php echo lang('label_city'); ?></td>
			<td><?php echo form_dropdown('city_id', $category, $data->city_id, ' disabled="disabled"'); ?></td>
		</tr>
		
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
					</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		$result = $this->hotel_model->get_hotel_text($hotel_id, $lang_v);
		if($result->num_rows() > 0) {
			$text = $result->row();
		} else {
			$text = new stdClass();
			$text->hotel_text_id = "0";
			$text->feature_text = "";
		}
		echo form_hidden("hotel_text_id[$lang_k]", $text->hotel_text_id);
		echo form_hidden("language[$lang_k]", $lang_v);
?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table width="100%">
							<tr>
								<td colspan="4"><div class="group_title">Room</div></td>
							</tr>
							<tr>
								<td colspan="4" id="room_area_<?php echo $lang_k; ?>">
									<div style="text-align:right; border-bottom: 1px solid #999999;">
										<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="addRoom('<?php echo $lang_k; ?>')"/>
									</div>
<?php 
		$room_list = $this->hotel_model->get_room_list($hotel_id, $lang_v);
		if($room_list->num_rows() > 0) {
			$i = 0;
?>
									<script type="text/javascript">
										<?php echo "roomCount_" . $lang_k . "= " . ($room_list->num_rows() + 1) . ";" ?>
									</script>
<?php 
			foreach($room_list->result() as $room_data) {
?>
									<table width="100%" id="room_<?php echo $lang_k . '_' . $i; ?>" style="border: 1px dashed #cccccc;">
										<input type="hidden" name="room_id[<?php echo $lang_k . '][' . $i; ?>]" id="room_id-<?php echo $lang_k; ?>-<?php echo $i; ?>" value="<?php echo $room_data->room_id; ?>" >
										<!-- tr>
											<td>Room type</td>
											<td><input type="text" size="32" name="room_type[<?php echo $lang_k . '][' . $i; ?>]" name="room_type-<?php echo $lang_k; ?>-<?php echo $i; ?>"> </td>
											<td>Room name</td>
											<td><input type="text" size="32" name="room_name[<?php echo $lang_k . '][' . $i; ?>]" name="room_name-<?php echo $lang_k; ?>-<?php echo $i; ?>"> </td>
										</tr-->
										<tr>
											<td>Title</td>
											<td><input type="text" size="32" name="room_title[<?php echo $lang_k . '][' . $i; ?>]" id="title-<?php echo $lang_k; ?>-<?php echo $i; ?>" value="<?php echo $room_data->title; ?>" > </td>
											<td>Quantity</td>
											<td><input type="text" name="room_quantity[<?php echo $lang_k . '][' . $i; ?>]" id="quantity-<?php echo $lang_k; ?>-<?php echo $i; ?>" value="<?php echo $room_data->quantity; ?>" > </td>
										</tr>
										<tr>
											<td valign="top" nowrap="nowrap">Image</br>(288px X 373px)</td>	
											<td>
<?php  
		if($room_data->image) {
			echo '<img src="' . $room_data->image . '" width="300"/> <br/>';
		}
		$field_image = array(
						'name'        => "image[$lang_k][$i]",
						'id'          => "room_image_$lang_k" . "_" . $i ,
						'value'       => $room_data->image,
						'maxlength'   => '150',
						'size'        => '32'
					);
		echo form_input($field_image); 
?>
												<input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'room_image_<?php echo $lang_k . '_' . $i; ?>' );" />
											</td>	
											<td valign="top">Feature</td>
											<td>
												<textarea rows="3" cols="50" name="room_feature_text[<?php echo $lang_k . '][' . $i; ?>]" id="room_feature_text_<?php echo $lang_k . '_' . $i; ?>" class="room_feature_text"><?php echo $room_data->feature_text; ?></textarea>
											</td>
										</tr>
										<tr>
											<td align="right" colspan="4"><input type="button" value="Delete" onclick="delete_room(<?php echo "'$lang_k', '$i', '" . $room_data->room_id . "'"; ?>)"></td>
										</tr>
									</table>
<?php 
				$i++;
			}
		} else {
?>
									<script type="text/javascript">
										<?php echo "roomCount_" . $lang_k . "= 1;" ?>
									</script>
									<table width="100%" id="room_<?php echo $lang_k; ?>_0" style="border: 1px dashed #cccccc;">
										<input type="hidden" name="room_id[<?php echo $lang_k; ?>][0]" id="room_id-<?php echo $lang_k; ?>-0" value="0" >
										<!-- tr>
											<td>Room type</td>
											<td><input type="text" size="32" name="room_type[<?php echo $lang_k; ?>][0]" name="room_type-<?php echo $lang_k; ?>-0"> </td>
											<td>Room name</td>
											<td><input type="text" size="32" name="room_name[<?php echo $lang_k; ?>][0]" name="room_name-<?php echo $lang_k; ?>-0"> </td>
										</tr-->
										<tr>
											<td>Title</td>
											<td><input type="text" size="32" name="room_title[<?php echo $lang_k; ?>][0]" id="title-<?php echo $lang_k; ?>-0"> </td>
											<td>Quantity</td>
											<td><input type="text" name="room_quantity[<?php echo $lang_k; ?>][0]" id="quantity-<?php echo $lang_k; ?>-0"> </td>
										</tr>
										<tr>
											<td valign="top" nowrap="nowrap">Image</br>(288px X 373px)</td>	
											<td>
											<?php  
		$field_image = array(
						'name'        => "image[$lang_k][0]",
						'id'          => "room_image_$lang_k" . "_0",
						'maxlength'   => '150',
						'size'        => '32'
					);
		echo form_input($field_image); 
?>
												<input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'room_image_<?php echo $lang_k . '_0'; ?>' );" />
											</td>	
											<td valign="top">Feature</td>
											<td>
												<textarea rows="3" cols="50" name="room_feature_text[<?php echo $lang_k; ?>][0]" id="room_feature_text_<?php echo $lang_k . '_0'; ?>" class="room_feature_text"></textarea>
											</td>
										</tr>
										<tr>
											<td colspan="4"></td>
										</tr>
									</table>
<?php 
		}
?>
								</td>
							</tr>
							<tr>
								<td valign="top">Feature text</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("feature_text[$lang_k]", $text->feature_text); ?></td>
							</tr>
						</table>
					</div>
<?php 
	}
?>
				</div>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo form_submit('btnSave', lang('label_update')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_close'); ?>" onclick="location.replace('<?php echo base_url('system/hotel'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>