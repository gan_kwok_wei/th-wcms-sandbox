<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
	
		$( "#lang_tabs" ).tabs();
		
	});

</script>
<div id="main">
<?php 
	$result = $this->hotel_model->get_hotel($hotel_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
		if((get_role() > 3) && ($data->created_id != get_user_id())) {
    		set_warning_message(lang('alert_no_permission'));
			redirect('system/hotel');
			exit;
    	}
	    	
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel');
		exit;
	}
	echo '<h2 id="content_title">Edit Hotel : ' . $data->hotel_name . '</h2>';
	$category = $this->system_model->get_parent_city();
 
	show_message();
	echo form_open('', 'id="form_hotel"');
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'hotel_model.update_hotel');
	echo form_hidden('hotel_id', $hotel_id);
?>
	<div id="hotel_tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="<?php echo base_url('system/hotel/hotel_edit/' . $hotel_id)  ?>">Hotel Detail</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_room_edit/' . $hotel_id); ?>">Features</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_gallery_edit/' . $hotel_id);  ?>">Gallery</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_feature_edit/' . $hotel_id);  ?>">What We Provide</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_package_edit/' . $hotel_id);  ?>">Packages</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_meta_edit/' . $hotel_id);  ?>">Meta Data Options</a></li>
		</ul>
	</div>
	
	<table width="100%">
		<tr>
			<td>URI name*</td>
			<td>
			<?php 
				$field_hotel_name = array(
								'name'        => 'hotel_name',
								'id'          => 'hotel_name',
								'value'       => $data->hotel_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_hotel_name); 
			?>
			</td>
			<td><?php echo lang('label_city'); ?>*</td>
			<td><?php echo form_dropdown('city_id', $category, $data->city_id); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title"><?php echo lang('label_contact'); ?></div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_phone'); ?> 1</td>
			<td>
			<?php 
				$field_phone1 = array(
								'name'        => 'phone1',
								'id'          => 'phone1',
								'value'       => $data->phone1,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone1); 
			?>
			</td>
			<td><?php echo lang('label_phone'); ?> 2</td>
			<td>
			<?php 
				$field_phone2 = array(
								'name'        => 'phone2',
								'id'          => 'phone2',
								'value'       => $data->phone2,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone2); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_fax'); ?></td>
			<td>
			<?php 
				$field_fax = array(
								'name'        => 'fax',
								'id'          => 'fax',
								'value'       => $data->fax,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_fax); 
			?>
			</td>
			<td><?php echo lang('label_email'); ?></td>
			<td>
			<?php 
				$field_email = array(
								'name'        => 'email',
								'id'          => 'email',
								'value'       => $data->email,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_email); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Google Map</div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_latitude'); ?></td>
			<td>
			<?php 
				$field_latitude = array(
								'name'        => 'latitude',
								'id'          => 'latitude',
								'value'       => $data->latitude,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_latitude); 
			?>
			</td>
			<td><?php echo lang('label_longitude'); ?></td>
			<td>
			<?php 
				$field_longitude = array(
								'name'        => 'longitude',
								'id'          => 'longitude',
								'value'       => $data->longitude,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_longitude); 
			?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_icon'); ?></td>
			<td valign="bottom">
			<?php 
				if($data->icon) {
					echo '<img src="' . $data->icon . '" height="40"/> <br/>';
				}
				$field_icon = array(
								'name'        => "google_icon",
								'id'          => "google_icon",
								'value'       => $data->icon,
								'maxlength'   => '150',
								'size'        => '32'
							);
				echo form_input($field_icon); 
			?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'google_icon' );" />
			</td>
			<td valign="top"></td>
			<td valign="bottom"></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_desc'); ?></td>
			<td colspan="3"><?php echo $this->ckeditor->editor("google_note", $data->note); ?></td>
		</tr>
		
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
					</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		$result = $this->hotel_model->get_hotel_text($hotel_id, $lang_v);
		if($result->num_rows() > 0) {
			$text = $result->row();
		} else {
			$text = new stdClass();
			$text->hotel_text_id = "0";
			$text->title = "";
			$text->icon = "";
			$text->image = "";
			$text->address = "";
			$text->feature_text = "";
			$text->feature_text_1 = "";
			$text->content = "";
			$text->note = "";
		}
		echo form_hidden("language[$lang_k]", $lang_v);
		echo form_hidden("hotel_text_id[$lang_k]", $text->hotel_text_id); 
?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table width="100%">
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_address'); ?></td>
								<td colspan="3"><?php echo $this->ckeditor->editor("address[$lang_k]", $text->address); ?></td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_icon'); ?></td>
								<td valign="bottom">
								<?php 
									if($text->icon) {
										echo '<img src="' . $text->icon . '" height="75"/> <br/>';
									}
									$field_icon = array(
													'name'        => "icon[$lang_k]",
													'id'          => "icon_$lang_k",
													'value'       => $text->icon,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_icon); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
								</td>
								<td valign="top"><?php echo lang('label_image'); ?></td>
								<td valign="bottom">
								<?php 
									if($text->image) {
										echo '<img src="' . $text->image . '" height="75"/> <br/>';
									}
									$field_image = array(
													'name'        => "image[$lang_k]",
													'id'          => "image_$lang_k",
													'value'       => $text->image,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_image); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'image_<?php echo $lang_k; ?>' );" />
								</td>
							</tr>
							<!-- tr>
								<td valign="top">Feature text</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("feature_text[$lang_k]", $text->feature_text); ?></td>
							</tr-->
							<tr>
								<td valign="top">How to get Here</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("note[$lang_k]", $text->note); ?></td>
							</tr>
						</table>
					</div>
<?php 
	}
?>
				</div>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td><?php echo lang('label_tags'); ?></td>
			<td colspan="3">
			<?php 
				$field_tags = array(
								'name'        => 'tags',
								'id'          => 'tags',
								'value'       => $data->tags,
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_tags); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td colspan="3">
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<!-- td>Top Destination</td>
			<td colspan="3"><?php //echo form_checkbox('top_destination', '1', ($data->top_destination == '0') ? FALSE : TRUE); ?> </td-->
		</tr>
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => $data->publish_date_end,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3">
<?php 
		if(get_role() <= 3) {
			echo form_checkbox('status', '1', ($data->status == '0') ? FALSE : TRUE);
		} else {
			echo $data_status[$data->status];
		}
?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
<?php 
	echo form_submit('btnSave', lang('label_update'));
	if(get_role() <= $this->config->item('ge_max_admin_role')) {
		echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
	}

?>
				<input type="button" id="btnBack" value="<?php echo lang('label_close'); ?>" onclick="location.replace('<?php echo base_url('system/hotel'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
<script type="text/javascript">
<?php 
	if(get_role() <= $this->config->item('ge_max_admin_role')) {
?>
	$('#btnDelete').click(function() {	
		if(confirm("Are you sure, you want to remove hotel <?php echo $data->hotel_name; ?> ?")){
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "hotel_id=<?php echo $hotel_id; ?>&hotel_name=<?php echo $data->hotel_name; ?>&mode=delete&action=hotel_model.delete_hotel",
				success: function(data){//alert(typeof(data));
//					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/hotel/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
		}
	});
<?php 	
	}
?>
</script>
</div>