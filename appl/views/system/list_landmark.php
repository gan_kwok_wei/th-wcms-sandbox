<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	
	show_message();
	echo form_open('', 'id="form_list"'); 
	echo form_hidden('action', 'landmark_model.save_list_landmark');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/landmark/landmark_add'); ?>');"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_country" value="Search Country" class="search_init" /></th>
				<th class="ui-state-default"><input type="text" name="search_city" value="Search City" class="search_init" /></th>
                <th class="ui-state-default"><input type="text" name="search_city" value="Search Landmark" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
				<th width="60" class="ui-state-default"></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Country</th>
                <th>City</th>
				<th>Landmark</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->landmark_model->get_landmark_list();
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->landmark_id; ?></td>
				<td><a href="<?php echo base_url('system/landmark/landmark_edit/' . $row->landmark_id); ?>" title="<?php echo $row->city_name; ?>"><?php echo $row->country_name; ?></a></td>
				<td><a href="<?php echo base_url('system/landmark/landmark_edit/' . $row->landmark_id); ?>" title="<?php echo $row->city_name; ?>"><?php echo $row->city_name; ?></a></td>
                <td><a href="<?php echo base_url('system/landmark/landmark_edit/' . $row->landmark_id); ?>" title="<?php echo $row->landmark_name; ?>"><?php echo $row->landmark_name; ?></a></td>
				<td><?php echo $data_status[$row->status]; ?></td>
				<td><?php echo form_input('sort[' . $row->landmark_id . ']', $row->sort, ' class="inline_text"') . '<span style="display:none;"> ' . $row->sort . '</span>'; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Country</th>
                <th>City</th>
				<th>Landmark</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>