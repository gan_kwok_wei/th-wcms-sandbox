<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">

	function BrowseServer(startupPath, functionData) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}

	function SetFileField( fileUrl, data ) {
		$("#" + data["selectActionData"] ).val(fileUrl);
	}

	$().ready(function() {

		$("#form_gallery").validate({
			rules: {"gallery_category_name":"required",
					"title":"required"},
			messages: {"gallery_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>"}
		});
		
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	show_message();
	echo form_open('', 'id="form_gallery"'); 
	
	if($mode != 'add') {
		echo form_hidden('gallery_category_id', $gallery_category_id);
		$result = $this->cms_model->get_gallery_category($gallery_category_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/cms/gallery_category');
			exit;
		}
	}
	
	$category = $this->cms_model->get_parent_gallery();
	$language = $this->system_model->get_registered_lang();
	
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'cms_model.save_gallery_category');
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><strong><?php echo $data->gallery_category_id; ?></strong></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_gallery_category_name = array(
								'name'        => 'gallery_category_name',
								'id'          => 'gallery_category_name',
								'value'       => ($mode == 'add') ? set_value('gallery_category_name') : $data->gallery_category_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_gallery_category_name); 
			?>
			</td>
			<td><?php echo lang('label_title'); ?>*</td>
			<td>
			<?php 
				$field_title = array(
								'name'        => 'title',
								'id'          => 'title',
								'value'       => ($mode == 'add') ? set_value('title'): $data->title,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_title); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_language'); ?>*</td>
			<td colspan="3"><?php echo form_dropdown('language', $language, ($mode == 'add') ? set_value('language') : $data->language); ?></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_note'); ?></td>
			<td colspan="3"><?php echo $this->ckeditor->editor("note", (($mode == 'add') ? '' : $data->note)); ?></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE); ?></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="'.lang('label_delete').'" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/cms/gallery_category'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
<script type="text/javascript">
<?php 
	if($mode != 'add') {
?>
	$('#btnDelete').click(function() {	
		if(confirm("<?php echo lang('conf_delete') . $data->gallery_category_id; ?>")){ 
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "gallery_category_id=<?php echo $gallery_category_id ?>&mode=delete&action=cms_model.delete_gallery_category",
				success: function(data){//alert(typeof(data));
					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/cms/gallery_category/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
		}
	});
<?php 	
	}
?>
</script>
</div>