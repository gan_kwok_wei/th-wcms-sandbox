<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 

	define('ASSET_PATH', base_url().'assets/');
/*	$func = "get_$image_type";
	$result = $this->cms_model->$func($image_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
	} else {
		set_warning_message(lang('alert_undefined_data'));
	}
*/	
?>
<html xmlns="http://www.w3.org/1999/xhtml">

	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery-1.7.1.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery-ui-1.8.17.custom.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery.Jcrop.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/ui.spinner.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/jquery-ui-1.8.17.custom.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/jquery.Jcrop.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>js/jqueryui/ui.spinner.css" />
	<script language="Javascript">

		$().ready(function() {

			var img = window.opener.document.getElementById('<?php echo $component ?>').value;

			$('#src_image').attr('src', img);
			$('#resize_image').val(img);
			$('#crop_image').val(img);

			$('#src_image').Jcrop({
				//aspectRatio: 1,
				onSelect: updateCoords
			});
			
			$('#width').spinner({step: 1, largeStep: 1, min: 0, max: 999999});
			$('#height').spinner({step: 1, largeStep: 1, min: 0, max: 999999});
			$('#width').val($('#src_image').width());
			$('#height').val($('#src_image').height());
			$('#width').change(function() {
				$('#src_image').width($(this).val());

				$('#height').val($('#src_image').height());

				});
			$('#height').change(function() {
				$('#src_image').height($(this).val());
				
				$('#width').val($('#src_image').width());
				
				});
		});

		function updateCoords(c) {
			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#w').val(c.w);
			$('#h').val(c.h);
		};

		function checkCoords() {
			if (parseInt($('#w').val())) return true;
			alert('Please select a crop region then press submit.');
			return false;
		};

	</script>

</head>

<body style="font-family: arial,sans-serif;">

	<div id="outer">
		<div class="jcExample">
			<div class="article">
		
				<h2 style="border-bottom: 1px solid #CCCCCC; margin: 0 0 10px; padding: 5px 0; ">Image Edit</h2>
<?php 
	show_message();
	
?>		
				<!-- This is the image we're attaching Jcrop to -->
				<img id="src_image" src=""/>
		
				<!-- This is the form that our event handler fills -->
				<hr />
				
				<form action="" method="post" onsubmit="return checkCoords();">
					<input type="hidden" id="action" name="action" value="cms_model.image_edit" />
					<input type="hidden" id="crop_image" name="image" value=""/>
					<input type="hidden" id="x" name="x" />
					<input type="hidden" id="y" name="y" />
					<input type="hidden" id="w" name="w" />
					<input type="hidden" id="h" name="h" />
					<input type="submit" name="btnSave" value="Crop Image" />
				</form>
				<form action="" method="post">
					<input type="hidden" id="action" name="action" value="cms_model.image_edit" />
					<input type="hidden" id="resize_image" name="image" value=""/>
					W:<input type="text" id="width" name="width" size="10" /> 
					H:<input type="text" id="height" name="height" size="10" /> 
					<input type="submit" name="btnSave" value="Resize Image" />
				</form>
		
			</div>
		</div>
	</div>
</body>

</html>