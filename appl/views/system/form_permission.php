<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	$().ready(function() {
		
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$permission = $this->system_model->get_permission($permission_id);
		if($permission->num_rows() > 0) {
			$permission_data = $permission->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/security');
			exit;
		}
	}

	show_message();
	echo form_open('', 'id="form_permission"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'user_model.save_permission');
	echo ($mode != 'add') ? form_hidden('permission_id', $permission_id) : '';
?>
	<table width="100%">
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_name = array(
								'name'		=> 'name',
								'id'		=> 'name',
								'value'		=> ($mode == 'add') ? set_value('name') : $permission_data->name,
								'maxlength'	=> '45',
								'size'		=> '45'
							);
				 echo form_input($field_name); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_module'); ?>*</td>
			<td>
			<?php 
				$field_module = array(
								'name'		=> 'module',
								'id'		=> 'module',
								'value'		=> ($mode == 'add') ? set_value('module') : $permission_data->module,
								'maxlength'	=> '45',
								'size'		=> '45'
							);
				 echo form_input($field_module); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_class'); ?>*</td>
			<td>
			<?php 
				$field_class = array(
								'name'		=> 'class',
								'id'		=> 'class',
								'value'		=> ($mode == 'add') ? set_value('class') : $permission_data->class,
								'maxlength'	=> '45',
								'size'		=> '45'
							);
				 echo form_input($field_class); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_function'); ?>*</td>
			<td>
			<?php 
				$field_function = array(
								'name'		=> 'function',
								'id'		=> 'function',
								'value'		=> ($mode == 'add') ? set_value('function') : $permission_data->function,
								'maxlength'	=> '45',
								'size'		=> '45'
							);
				 echo form_input($field_function); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<tr>
			<td colspan="2">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', ($mode == 'add') ? lang('label_save') : lang('label_update'));
					
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="'.lang('label_delete').'" />';
				} else {
					
					echo form_submit('btnSave', ($mode == 'add') ? lang('label_save') : lang('label_update'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/security'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close();
?>
</div>
