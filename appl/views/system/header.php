<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*
|---------------------------------------------------------------
| DEFINE Assets path
|---------------------------------------------------------------
*/

define('ASSET_PATH', base_url('assets/'));
if(!isset($style_extras)) { $style_extras = array(); }
if(!isset($js_extras)) { $js_extras = array(); }

?>
<head>
	<title><?php echo $this->config->item('site_name'); ?></title>
    <meta name="author"      content="Budi Laxono">
	<meta name="copyright"   content="&copy; 2012 Green Lab Group" >
	<meta name="keywords"    content="Content Keywords">
	<meta name="description" content="Content Description">
	<meta name="template"    content="<?php echo $module . '.' . $class . '.' . $function;  ?>" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/admin.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/menu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/jquery-ui-1.8.17.custom.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/wysiwyg.css" />
	<?php foreach ($style_extras as $style_extra) { echo "<link rel='stylesheet' href='$style_extra'/>"; } ?>
	
	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery-1.7.1.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery-ui-1.8.17.custom.min.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery-ui-timepicker-addon.js"></script>
	<script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery-ui-sliderAccess.js"></script>
	<?php foreach ($js_extras as $js_extra) { echo "<script language='javascript' src='$js_extra'></script>"; } ?>
	<!-- script language="javascript" type="text/javascript" src="<?php echo ASSET_PATH ?>js/jqueryui/jquery.validate.js"></script -->
	
	<script language="javascript" type="text/javascript">
	
		$(document).ready(function(){

			$( ".datepicker" ).datepicker({
				changeMonth: true,
				changeYear: true,
				showOn: "button",
				buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd"
			});

			$( ".datetimepicker" ).datetimepicker({
				changeMonth: true,
				changeYear: true,
				showOn: "button",
				buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd",
				timeFormat: 'HH:mm',
				hourGrid: 4,
				minuteGrid: 10
			});
			
			<?php
				if(isset($js_function)) {
					if(count($js_function) > 0){
						foreach($js_function as $function){
							echo $function;
						}
					}
				}
			?>			

		});
		
	</script>
</head>

<body>
	<div id="container">
		
		<div id="header">
			<img src="<?php echo ASSET_PATH; ?>media/images/inside_logo.jpg" alt="" />
		</div>
		