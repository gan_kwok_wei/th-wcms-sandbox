<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		
		$("#form_banner").validate({
			rules: {"banner_name":"required",
					"title":"required",
					"banner_category_id":"required"},
			messages: {"banner_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"banner_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$banner = $this->cms_model->get_banner($banner_id);
		if($banner->num_rows() > 0) {
			$banner_data = $banner->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/cms/gallery');
			exit;
		}
	}
	
	$category = $this->cms_model->get_parent_banner();
	$language = $this->system_model->get_registered_lang();
	
	show_message();
	echo form_open('', 'id="form_banner"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'cms_model.save_banner');
	echo ($mode != 'add') ? form_hidden('banner_id', $banner_id) : '';
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $banner_data->banner_id; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_banner_name = array(
								'name'        => 'banner_name',
								'id'          => 'banner_name',
								'value'       => ($mode == 'add') ? set_value('banner_name') : $banner_data->banner_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_banner_name); 
			?>
			</td>
			<td><?php echo lang('label_title'); ?>*</td>
			<td>
			<?php 
				$field_title = array(
								'name'        => 'title',
								'id'          => 'title',
								'value'       => ($mode == 'add') ? set_value('title'): $banner_data->title,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_title); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_category'); ?>*
			</td>
			<td><?php echo form_dropdown('banner_category_id', $category, ($mode == 'add') ? set_value('category') : $banner_data->banner_category_id); ?></td>
			<td><?php echo lang('label_language'); ?></td>
			<td><?php echo ($mode == 'add') ? 'english' : $banner_data->language; ?></td>
		</tr>
		<tr>
			<td><?php echo lang('label_image'); ?></td>
			<td>
			<?php 
				$field_image = array(
								'name'        => 'image',
								'id'          => 'image',
								'value'       => ($mode == 'add') ? set_value('image'): $banner_data->image,
								'maxlength'   => '150',
								'size'        => '32'
							);
				echo form_input($field_image); 
			?><input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'image' );" />
			</td>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0: $banner_data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_link'); ?></td>
			<td colspan="3">
			<?php 
				$field_link = array(
								'name'        => 'link',
								'id'          => 'link',
								'value'       => ($mode == 'add') ? set_value('link'): $banner_data->link,
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_link); 
			?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_note'); ?></td>
			<td colspan="3"><?php echo $this->ckeditor->editor("note", ($mode == 'add') ? '' : $banner_data->note); ?></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($banner_data->created_id)->row();
		$modified_by = !$banner_data->modified_id ? FALSE : $this->user_model->get_user($banner_data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($banner_data->status == '0') ? FALSE : TRUE); ?></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $banner_data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $banner_data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
					echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url(); ?>system/cms/banner')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
<script type="text/javascript">
<?php 
	if($mode != 'add') {
?>
	$('#btnDelete').click(function() {	
		if(confirm("<?php echo lang('conf_delete') . $banner_id; ?>")){
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "banner_id=<?php echo $banner_id ?>&mode=delete&action=cms_model.delete_banner",
				success: function(data){//alert(typeof(data));
					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/cms/banner/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
		}
	});
<?php 	
	}
?>
</script>
</div>