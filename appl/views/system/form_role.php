<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	$().ready(function() {
<?php 
	if($mode != 'add') {
?>
		permTable = $('#permission_table').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
		x_permTable = $('#x_permission_table').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
<?php 
	}
?>			
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$role = $this->system_model->get_role($role_id);
		if($role->num_rows() > 0) {
			$role_data = $role->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/security');
			exit;
		}
	}

	show_message();
?>
	<table width="100%">
<?php
	echo form_open('', 'id="form_role"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'user_model.save_role');
	echo ($mode != 'add') ? form_hidden('role_id', $role_id) : '';
?>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_name = array(
								'name'		=> 'name',
								'id'		=> 'name',
								'value'		=> ($mode == 'add') ? set_value('name') : $role_data->name,
								'maxlength'	=> '45',
								'size'		=> '45'
							);
				 echo form_input($field_name); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<tr>
			<td colspan="2">
<?php 
				
	if($mode != 'add') {
		echo form_submit('btnSave', lang('label_update'));
		echo '<input type="button" id="btnDelete" name="btnDelete" value="'.lang('label_delete').'" />';
	} else {
		
		echo form_submit('btnSave', lang('label_save'));
	}
?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/security'); ?>')" />
			</td>
		</tr>
		
<?php 
	echo form_close();
	if($mode != 'add') {
?>		
		<tr>
			<td colspan="2"><div class="group_title"><?php echo lang('title_permission'); ?></div></td>
		</tr>
		<tr>
			<td colspan="2">
<?php 
		echo form_open('', 'id="form_role_permission"'); 
		echo form_hidden('action', 'user_model.update_role_permision');
		echo form_hidden('role_id', $role_id);
?>
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="permission_table">
					<thead>
						<tr>
							<th width="50">ID</th>
							<th>Name</th>
							<th>Module</th>
							<th>Class</th>
							<th>Function</th>
							<th width="50">Allow</th>
						</tr>
					</thead>
					<tbody>
<?php 
		$permission = $this->user_model->get_role_permission($role_id);
		if($permission->num_rows() > 0) {
			foreach($permission->result() as $row) {
?>
						<tr>
							<td><?php echo $row->permission_id; ?></td>
							<td><?php echo $row->name; ?></td>
							<td><?php echo $row->module; ?></td>
							<td><?php echo $row->class; ?></td>
							<td><?php echo $row->function; ?></td>
							<th><?php echo form_checkbox('role_permission[' . $row->entry_id . ']', 1, $row->allow_deny); ?></th>
						</tr>
<?php 
			}
		}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Module</th>
							<th>Class</th>
							<th>Function</th>
							<th>Allow</th>
						</tr>
					</tfoot>
				</table>
<?php 
	if($permission->num_rows() > 0) {
		echo form_submit('btnSave', lang('label_update_role_perm'));
	}
	echo form_close();
?>	
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="x_permission_table">
					<thead>
						<tr>
							<th width="50">ID</th>
							<th>Name</th>
							<th>Module</th>
							<th>Class</th>
							<th>Function</th>
						</tr>
					</thead>
					<tbody>
<?php 
		$x_permission = $this->user_model->get_x_role_permission($role_id);
		if($x_permission->num_rows() > 0) {
			foreach($x_permission->result() as $row) {
?>
						<tr>
							<td><a href="<?php echo base_url() . "system/security/role_add_permission/$role_id/" . $row->permission_id; ?>" title="Add this permission" ><?php echo $row->permission_id; ?></a></td>
							<td><a href="<?php echo base_url() . "system/security/role_add_permission/$role_id/" . $row->permission_id; ?>" title="Add this permission" ><?php echo $row->name; ?></a></td>
							<td><?php echo $row->module; ?></td>
							<td><?php echo $row->class; ?></td>
							<td><?php echo $row->function; ?></td>
						</tr>
<?php 
			}
		}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Module</th>
							<th>Class</th>
							<th>Function</th>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
<?php 
	}
?>
	</table>
</div>
