<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">

</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	show_message();
	echo form_open('', 'id="form_password"'); 
	echo form_hidden('action', 'user_model.update_password');
	echo form_hidden('user_id', $user_id);
?>
	<table>
		<tr>
			<td><?php echo lang('label_old_password'); ?>*</td>
			<td>
			<?php 
				$field_old_password = array(
								'name'        => 'old_password',
								'id'          => 'old_password',
								'maxlength'   => '50',
								'size'        => '50'
							);
				echo form_password($field_old_password).form_hidden('user_id', get_user_id());
			?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<tr>
			<td><?php echo lang('label_password'); ?>*</td>
			<td>
			<?php 
				$field_password = array(
								'name'        => 'password',
								'id'          => 'password',
								'maxlength'   => '50',
								'size'        => '50'
							);
				echo form_password($field_password); 
			?>
			</td>
		</tr>
		<tr>
			<td>
			<?php echo lang('label_conf_password'); ?>*
			</td>
			<td>
			<?php   
				$field_conf_password = array(
								'name'        => 'conf_password',
								'id'          => 'conf_password',
								'maxlength'   => '50',
								'size'        => '50'
							);
				echo form_password($field_conf_password); 
			?>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<?php echo form_submit('btnSave', lang('label_update')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url(); ?>system/user/user_edit/<?php echo get_user_id(); ?>')" />
			</td>
		</tr>
	</table>
<?php echo form_close(); ?>
</div>