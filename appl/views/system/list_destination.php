<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	
	show_message();
	echo form_open('', 'id="form_list"'); 
	echo form_hidden('action', 'hotel_model.save_list_destination');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/hotel/destination_add'); ?>');"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_hotel" value="Search Hotel" class="search_init" /></th>
				<th class="ui-state-default"><input type="text" name="search_package" value="Search Package" class="search_init" /></th>
				<th width="200" class="ui-state-default"></th>
				<th width="200" class="ui-state-default"></th>
				<th width="60" class="ui-state-default"></th>
				<th width="60" class="ui-state-default"></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Hotel</th>
				<th>Package</th>
				<th>Publish Start</th>
				<th>Publish End</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->hotel_model->get_destination_list();
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->destination_id; ?></td>
				<td><a href="<?php echo base_url('system/hotel/destination_edit/' . $row->destination_id); ?>" title="<?php echo $row->hotel_name; ?>"><?php echo $row->hotel_name; ?></a></td>
				<td><a href="<?php echo base_url('system/hotel/destination_edit/' . $row->destination_id); ?>" title="<?php echo $row->hotel_name; ?>"><?php echo $row->currency . ' ' . $row->price . ' ' . $row->package; ?></a></td>
				<td><?php echo $row->publish_date_start; ?></td>
				<td><?php echo $row->publish_date_end; ?></td>
				<td><?php echo $data_status[$row->status]; ?></td>
				<td><?php echo form_input('sort[' . $row->destination_id . ']', $row->sort, ' class="inline_text"') . '<span style="display:none;"> ' . $row->sort . '</span>'; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Hotel</th>
				<th>Package</th>
				<th>Publish Start</th>
				<th>Publish End</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>