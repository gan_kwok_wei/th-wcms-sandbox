<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<?php 
	$result = $this->hotel_model->get_inquiry($hotel_inquiry_id);
//	echo $this->db->last_query();
	if($result->num_rows() > 0) {
		$data = $result->row();
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel/inquiry');
		exit;
	}
	
?>
	<h2 id="content_title">Inquiry from : <?php echo $data->contact_person; ?> on <?php echo $data->date_send; ?></h2>
	<table width="100%">
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><strong><?php echo $data->hotel_inquiry_id; ?></strong></td>
		</tr>
		<tr>
			<td style="width:140px;">Company Name:</td>
			<td>
				<input type="text" name="company_name" value="<?php echo $data->company_name; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>Contact Person: </td>
			<td>
				<input type="text" name="contact_person" value="<?php echo $data->contact_person; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>Telephone:</td>
			<td>
				<input type="text" name="telephone" value="<?php echo $data->telephone; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>Mobile: </td>
			<td>
				<input type="text" name="mobile" value="<?php echo $data->mobile; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td style="vertical-align:top;"><label style="display:inline-block; margin-top:10px;">Address:</label></td>
			<td>
				<textarea name="address" class="input_light full_width" style="height:50px;" readonly="readonly"><?php echo $data->company_name; ?></textarea>
			<td>
		</tr>
		<tr>
			<td>Post Code:</td>
			<td>
				<input type="text" name="postcode" value="<?php echo $data->postcode; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>Country:</td>
			<td><input type="text" name="email" value="<?php echo $data->country_name; ?>" class="input_light full_width" readonly="readonly" /><td>
		</tr>
		<tr>
			<td>Fax Number:</td>
			<td>
				<input type="text" name="fax" value="<?php echo $data->fax; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>E-mail Address: </td>
			<td>
				<input type="text" name="email" value="<?php echo $data->email; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>Hotel: </td>
			<td><input type="text" name="email" value="<?php echo $data->hotel_name; ?>" class="input_light full_width" readonly="readonly" /><td>
		</tr>
		<tr>
			<td>Check In Date: </td>
			<td><input type="text" name="email" value="<?php echo $data->checkin; ?>" class="input_light full_width" readonly="readonly" /><td>
		</tr>
		<tr>
			<td>Check Out Date: </td>
			<td><input type="text" name="email" value="<?php echo $data->checkout; ?>" class="input_light full_width" readonly="readonly" /><td>
		</tr>
		<tr>
			<td>No of nights: </td>
			<td>
				<input type="text" name="no_of_night" value="<?php echo $data->no_of_night; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>No of rooms: </td>
			<td>
				<input type="text" name="no_of_room" value="<?php echo $data->no_of_room; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>No of pax:</td>
			<td>
				<input type="text" name="no_of_pax" value="<?php echo $data->no_of_pax; ?>" class="input_light full_width" readonly="readonly" />
			<td>
		</tr>
		<tr>
			<td>Room Type: </td>
			<td><input type="text" name="no_of_pax" value="<?php echo $data->room_name; ?>" class="input_light full_width" readonly="readonly" /><td>
		</tr>
		<tr>
			<td style="vertical-align:top;"><label style="display:inline-block; margin-top:10px;">Remarks:</td>
			<td>
				<textarea name="remarks" class="input_light full_width" style="height:50px;" readonly="readonly"><?php echo $data->remarks; ?></textarea>
			<td>
		</tr>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
	</table>
</div>