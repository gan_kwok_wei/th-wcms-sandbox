<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];

		featureTable = $('#feat_table').dataTable({"bJQueryUI": true, "bPaginate": false, "oLanguage": {"sSearch": "Search all columns:"}});
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
/*			
		$("#form_hotel_feature").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
*/
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
		
	});
	
</script>
<div id="main">
<?php 
	$result = $this->hotel_model->get_hotel($hotel_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
		if((get_role() > 3) && ($data->created_id != get_user_id())) {
    		set_warning_message(lang('alert_no_permission'));
			redirect('system/hotel');
			exit;
    	}
	    	
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel');
		exit;
	}
	echo '<h2 id="content_title">Edit Hotel : ' . $data->hotel_name . '</h2>';
	$category = $this->system_model->get_parent_city();
 
	show_message();
	echo form_open('', 'id="form_hotel_feature"');
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'hotel_model.update_hotel_feature');
	echo form_hidden('hotel_id', $hotel_id);
?>
	<div id="hotel_tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_edit/' . $hotel_id)  ?>">Hotel Detail</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_room_edit/' . $hotel_id); ?>">Features</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_gallery_edit/' . $hotel_id);  ?>">Gallery</a></li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="<?php echo base_url('system/hotel/hotel_feature_edit/' . $hotel_id);  ?>">What We Provide</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_package_edit/' . $hotel_id)  ?>">Packages</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_meta_edit/' . $hotel_id);  ?>">Meta Data Options</a></li>
		</ul>
	</div>
	
	<table width="100%">
		<tr>
			<td>URI name</td>
			<td><strong><?php echo $data->hotel_name; ?></strong></td>
			<td><?php echo lang('label_city'); ?></td>
			<td><?php echo form_dropdown('city_id', $category, $data->city_id, ' disabled="disabled"'); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Hotel Feature</div></td>
		</tr>
		<tr>
			<td colspan="4">
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="feat_table">
				<thead>
					<tr>
						<th width="60">ID</th>
						<th width="250">Feature type</th>
						<th>title</th>
						<th width="60">Available</th>
					</tr>
				</thead>
				<tbody>
<?php 
		$feature_list = $this->hotel_model->get_feature_ref_list(); 
		$hotel_feature = $this->hotel_model->get_hotel_feature($hotel_id); 
		if($feature_list->num_rows() > 0) {
			foreach($feature_list->result() as $row) {
				$check = (in_array($row->feature_ref_id, $hotel_feature));
?>
					<tr>
						<td><?php echo $row->feature_ref_id; ?></td>
						<td><?php echo $row->feature_type; ?></td>
						<td><?php echo $row->feature_name; ?></td>
						<td><?php echo form_checkbox('feature_ref[' . $row->feature_ref_id .']', '1', $check); ?></td>
					</tr>
<?php 
			}
		}
?>
				</tbody>
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Feature type</th>
						<th>title</th>
						<th>Available</th>
					</tr>
				</tfoot>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo form_submit('btnSave', lang('label_update')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_close'); ?>" onclick="location.replace('<?php echo base_url('system/hotel'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>