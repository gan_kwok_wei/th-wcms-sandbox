<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];

		featureTable = $('#feat_table').dataTable({"bJQueryUI": true, "bPaginate": false, "oLanguage": {"sSearch": "Search all columns:"}});
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
			
		$("#form_content").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
		
	});

	function delete_room(lang, id) {
		$('#room_' + lang + '_' + id).remove();
	}
	
//	roomCount = 1;
	function addRoom(lang) {
		eval('roomCount = roomCount_' + lang);
		
strRoom = '<table width="100%" id="room_' + lang + '_' + roomCount + '" style="border: 1px dashed #cccccc;">' +
'<input type="hidden" name="room_id[' + lang + '][' + roomCount + ']" id="quantity-' + lang + '-' + roomCount + '" value="0" >' +
//'	<tr>' +
//'		<td>Room type</td>' +
//'		<td><input type="text" size="32" name="room_type[' + lang + '][' + roomCount + ']" name="room_type-' + lang + '-' + roomCount + '"> </td>' +
//'		<td>Room name</td>' +
//'		<td><input type="text" size="32" name="room_name[' + lang + '][' + roomCount + ']" name="room_name-' + lang + '-' + roomCount + '"> </td>' +
//'	</tr>' +
'	<tr>' +
'		<td>Title</td>' +
'		<td><input type="text" size="32" name="room_title[' + lang + '][' + roomCount + ']" id="title-' + lang + '-' + roomCount + '"> </td>' +
'		<td>Quantity</td>' +
'		<td><input type="text" name="room_quantity[' + lang + '][' + roomCount + ']" id="quantity-' + lang + '-' + roomCount + '"> </td>' +
'	</tr>' +
'	<tr>' +
'		<td valign="top">Feature</td>' +
'		<td><textarea rows="3" cols="50" name="room_feature_text[' + lang + '][' + roomCount + ']"></textarea></td>' +
'		<td></td>' +
'		<td align="right" valign="bottom"><input type="button" value="Delete" onclick="delete_room(\'' + lang + '\', \'' + roomCount + '\')"></td>' +
'	</tr>' +
'</table>';
		
		$('#room_area_' + lang).append(strRoom);
		eval('roomCount_' + lang + '++');
	}

	function delete_gallery(id) {
		$('#gallery_' + id).remove();
	}
	
//	galleryCount = 1;
	function addGallery() {
		strGallery = '<tr id="gallery_' + galleryCount + '" style="border: 1px dashed #cccccc;">' +
'	<td>' +
'		Image <input type="text" size="32" name="gallery_image[' + galleryCount + ']" id="gallery-image-' + galleryCount + '">' +
'		<input type="button" value="Browse Server" onclick="BrowseServer(\'images:\/\', \'gallery-image-' + galleryCount + '\' )" />' +
'	</td>' +
'	<td valign="bottom">Title <input type="text" size="50" name="gallery_title[' + galleryCount + ']" id="gallery-title-' + galleryCount + '"></td>' +
'	<td valign="bottom"><input type="button" value="Delete" onclick="delete_gallery(\'' + galleryCount + '\')"></td>' +
'</tr>';
		$('#gallery_area').append(strGallery);
		galleryCount++;
	}
	
</script>
<div id="main">
<h2 id="content_title">Add New Hotel</h2>
<?php 
	$result = $this->hotel_model->get_hotel($hotel_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
		if((get_role() > 3) && ($data->created_id != get_user_id())) {
    		set_warning_message(lang('alert_no_permission'));
			redirect('system/hotel');
			exit;
    	}
	    	
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel');
		exit;
	}
	$category = $this->system_model->get_parent_city();
	
	show_message();
	echo form_open('', 'id="form_hotel"');
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'hotel_model.update_hotel');
	echo form_hidden('hotel_id', $hotel_id);
?>
	<table width="100%">
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_hotel_name = array(
								'name'        => 'hotel_name',
								'id'          => 'hotel_name',
								'value'       => $data->hotel_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_hotel_name); 
			?>
			</td>
			<td><?php echo lang('label_city'); ?>*</td>
			<td><?php echo form_dropdown('city_id', $category, $data->city_id); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title"><?php echo lang('label_contact'); ?></div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_phone'); ?> 1</td>
			<td>
			<?php 
				$field_phone1 = array(
								'name'        => 'phone1',
								'id'          => 'phone1',
								'value'       => $data->phone1,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone1); 
			?>
			</td>
			<td><?php echo lang('label_phone'); ?> 2</td>
			<td>
			<?php 
				$field_phone2 = array(
								'name'        => 'phone2',
								'id'          => 'phone2',
								'value'       => $data->phone2,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone2); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_fax'); ?></td>
			<td>
			<?php 
				$field_fax = array(
								'name'        => 'fax',
								'id'          => 'fax',
								'value'       => $data->fax,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_fax); 
			?>
			</td>
			<td><?php echo lang('label_email'); ?></td>
			<td>
			<?php 
				$field_email = array(
								'name'        => 'email',
								'id'          => 'email',
								'value'       => $data->email,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_email); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_latitude'); ?></td>
			<td>
			<?php 
				$field_latitude = array(
								'name'        => 'latitude',
								'id'          => 'latitude',
								'value'       => $data->latitude,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_latitude); 
			?>
			</td>
			<td><?php echo lang('label_longitude'); ?></td>
			<td>
			<?php 
				$field_longitude = array(
								'name'        => 'longitude',
								'id'          => 'longitude',
								'value'       => $data->longitude,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_longitude); 
			?>
			</td>
		</tr>
		
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
					</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		$result = $this->hotel_model->get_hotel_text($hotel_id, $lang_v);
		if($result->num_rows() > 0) {
			$text = $result->row();
		} else {
			$text = new stdClass();
			$text->hotel_text_id = "";
			$text->title = "";
			$text->icon = "";
			$text->address = "";
			$text->feature_text = "";
			$text->content = "";
			$text->note = "";
		}
		echo form_hidden("language[$lang_k]", $lang_v);
		echo form_hidden("hotel_text_id[$lang_k]", $text->hotel_text_id); 
?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table width="100%">
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_address'); ?></td>
								<td colspan="3"><?php echo $this->ckeditor->editor("address[$lang_k]", $text->address); ?></td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_icon'); ?></td>
								<td valign="bottom">
								<?php 
									if($text->icon) {
										echo '<img src="' . $text->icon . '" height="75"/> <br/>';
									}
									$field_icon = array(
													'name'        => "icon[$lang_k]",
													'id'          => "icon_$lang_k",
													'value'       => $text->icon,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_icon); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
								</td>
								<td valign="top"><?php echo lang('label_image'); ?></td>
								<td valign="bottom">
								<?php 
									if($text->image) {
										echo '<img src="' . $text->image . '" height="75"/> <br/>';
									}
									$field_image = array(
													'name'        => "image[$lang_k]",
													'id'          => "image_$lang_k",
													'value'       => $text->image,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_image); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'image_<?php echo $lang_k; ?>' );" />
								</td>
							</tr>
							<tr>
								<td valign="top">Feature text</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("feature_text[$lang_k]", $text->feature_text); ?></td>
							</tr>
							<tr>
								<td valign="top">&nbsp;</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("feature_text_1[$lang_k]", $text->feature_text_1); ?></td>
							</tr>
							<tr>
								<td valign="top">Note</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("note[$lang_k]", $text->note); ?></td>
							</tr>
							<tr>
								<td colspan="4"><div class="group_title">Room</div></td>
							</tr>
							<tr>
								<td colspan="4" id="room_area_<?php echo $lang_k; ?>">
									<div style="text-align:right; border-bottom: 1px solid #999999;">
										<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="addRoom('<?php echo $lang_k; ?>')"/>
									</div>
<?php 
		$room_list = $this->hotel_model->get_room_list($hotel_id, $lang_v);
		if($room_list->num_rows() > 0) {
			$i = 0;
?>
									<script type="text/javascript">
										<?php echo "roomCount_" . $lang_k . "= " . ($room_list->num_rows() + 1) . ";" ?>
									</script>
<?php 
			foreach($room_list->result() as $room_data) {
?>
									<table width="100%" id="room_<?php echo $lang_k . '_' . $i; ?>" style="border: 1px dashed #cccccc;">
										<input type="hidden" name="room_id[<?php echo $lang_k; ?>][<?php echo $i; ?>]" id="quantity-<?php echo $lang_k; ?>-<?php echo $i; ?>" value="<?php echo $room_data->room_id; ?>" >
										<!-- tr>
											<td>Room type</td>
											<td><input type="text" size="32" name="room_type[<?php echo $lang_k; ?>][<?php echo $i; ?>]" name="room_type-<?php echo $lang_k; ?>-<?php echo $i; ?>"> </td>
											<td>Room name</td>
											<td><input type="text" size="32" name="room_name[<?php echo $lang_k; ?>][<?php echo $i; ?>]" name="room_name-<?php echo $lang_k; ?>-<?php echo $i; ?>"> </td>
										</tr-->
										<tr>
											<td>Title</td>
											<td><input type="text" size="32" name="room_title[<?php echo $lang_k; ?>][<?php echo $i; ?>]" id="title-<?php echo $lang_k; ?>-<?php echo $i; ?>" value="<?php echo $room_data->title; ?>" > </td>
											<td>Quantity</td>
											<td><input type="text" name="room_quantity[<?php echo $lang_k; ?>][<?php echo $i; ?>]" id="quantity-<?php echo $lang_k; ?>-<?php echo $i; ?>" value="<?php echo $room_data->quantity; ?>" > </td>
										</tr>
										<tr>
											<td valign="top">Feature</td>
											<td>
												<textarea rows="3" cols="50" name="room_feature_text[<?php echo $lang_k; ?>][<?php echo $i; ?>]"><?php echo $room_data->feature_text; ?></textarea>
											</td>
											<td></td>											
											<td align="right" valign="bottom"><input type="button" value="Delete" onclick="delete_room(<?php echo "'$lang_k', '$i'"; ?>)"></td>
										</tr>
									</table>
<?php 
				$i++;
			}
		} else {
?>
									<script type="text/javascript">
										<?php echo "roomCount_" . $lang_k . "= 1;" ?>
									</script>
									<table width="100%" id="room_<?php echo $lang_k; ?>_0" style="border: 1px dashed #cccccc;">
										<!-- tr>
											<td>Room type</td>
											<td><input type="text" size="32" name="room_type[<?php echo $lang_k; ?>][0]" name="room_type-<?php echo $lang_k; ?>-0"> </td>
											<td>Room name</td>
											<td><input type="text" size="32" name="room_name[<?php echo $lang_k; ?>][0]" name="room_name-<?php echo $lang_k; ?>-0"> </td>
										</tr-->
										<tr>
											<td>Title</td>
											<td><input type="text" size="32" name="room_title[<?php echo $lang_k; ?>][0]" id="title-<?php echo $lang_k; ?>-0"> </td>
											<td>Quantity</td>
											<td><input type="text" name="room_quantity[<?php echo $lang_k; ?>][0]" id="quantity-<?php echo $lang_k; ?>-0"> </td>
										</tr>
										<tr>
											<td valign="top">Feature</td>
											<td colspan="3">
												<textarea rows="3" cols="50" name="room_feature_text[<?php echo $lang_k; ?>][0]"></textarea>
											</td>
										</tr>
									</table>
<?php 
		}
?>
								</td>
							</tr>
						</table>
					</div>
<?php 
	}
?>
				</div>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4"><div class="group_title">Hotel Gallery</div></td>
		</tr>
		<tr>
			<td colspan="4">
				<div style="text-align:right; border-bottom: 1px solid #999999;">
					<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="addGallery()"/>
				</div>
				<table width="100%" id="gallery_area">
<?php 
		$gallery_list = $this->hotel_model->get_hotel_gallery($hotel_id);
		if($gallery_list->num_rows() > 0) {
			$i = 0;
?>
					<script type="text/javascript">
						<?php echo "galleryCount = " . ($room_list->num_rows() + 1) . ";" ?>
					</script>
<?php 
			foreach($gallery_list->result() as $gallery_data) {
?>
					<tr id="gallery_<?php echo $i; ?>" style="border: 1px dashed #cccccc;">
						<td>
						<?php if($gallery_data->image) { echo '<img src="' . $gallery_data->image . '" height="75px">'; } ?>
							Image <input type="text" size="32" name="gallery_image[<?php echo $i; ?>]" id="gallery-image-<?php echo $i; ?>" value="<?php echo $gallery_data->image; ?>" >
							<input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'gallery-image-<?php echo $i; ?>' );" />
						</td>
						<td valign="bottom">Title <input type="text" size="50" name="gallery_title[<?php echo $i; ?>]" id="gallery-title-<?php echo $i; ?>" value="<?php echo $gallery_data->title; ?>" ></td>
						<td valign="bottom"></td>
					</tr>
<?php 
				$i++;
			}
		} else {
?>
					<script type="text/javascript">
						galleryCount = 1;
					</script>
					<tr style="border: 1px dashed #cccccc;">
						<td>
							Image <input type="text" size="32" name="gallery_image[0]" id="gallery-image-0">
							<input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'gallery-image-0' );" />
						</td>
						<td valign="bottom">Title <input type="text" size="50" name="gallery_title[0]" id="gallery-title-0"></td>
						<td valign="bottom"></td>
					</tr>
<?php 
		}
?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Hotel Feature</div></td>
		</tr>
		<tr>
			<td colspan="4">
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="feat_table">
				<thead>
					<tr>
						<th width="60">ID</th>
						<th width="250">Feature type</th>
						<th>title</th>
						<th width="60">Available</th>
					</tr>
				</thead>
				<tbody>
<?php 
		$feature_list = $this->hotel_model->get_feature_ref_list(); 
		$hotel_feature = $this->hotel_model->get_hotel_feature($hotel_id); 
		if($feature_list->num_rows() > 0) {
			foreach($feature_list->result() as $row) {
				$check = (in_array($row->feature_ref_id, $hotel_feature));
?>
					<tr>
						<td><?php echo $row->feature_ref_id; ?></td>
						<td><?php echo $row->feature_type; ?></td>
						<td><?php echo $row->feature_name; ?></td>
						<td><?php echo form_checkbox('feature_ref[' . $row->feature_ref_id .']', '1', $check); ?></td>
					</tr>
<?php 
			}
		}
?>
				</tbody>
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Feature type</th>
						<th>title</th>
						<th>Available</th>
					</tr>
				</tfoot>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td><?php echo lang('label_tags'); ?></td>
			<td colspan="3">
			<?php 
				$field_tags = array(
								'name'        => 'tags',
								'id'          => 'tags',
								'value'       => $data->tags,
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_tags); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<td>Top Destination</td>
			<td colspan="3"><?php echo form_checkbox('top_destination', '1', ($data->top_destination == '0') ? FALSE : TRUE); ?> </td>
		</tr>
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => $data->publish_date_end,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3">
<?php 
		if(get_role() <= 3) {
			echo form_checkbox('status', '1', ($data->status == '0') ? FALSE : TRUE);
		} else {
			echo $data_status[$row->status];
		}
?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo form_submit('btnSave', lang('label_update')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url(); ?>system/hotel')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>