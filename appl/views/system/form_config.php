<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
/* Global var for counter */
	
	function fnClickAddRow() {
		$('#data_table').dataTable().fnAddData( [
			'<input type="text" name="type[]" class="inline_text" />',
			'<input type="text" name="key[]" class="inline_text" />',
			'<input type="text" name="val[]" class="inline_text"/>',
			'1<input type="hidden" name="active[]" value="1">'] );
	}
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	show_message();
	echo form_open('', 'id="form_config"'); 
	echo form_hidden('action', 'system_model.save_config');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="fnClickAddRow();"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="300">Type</th>
				<th width="300">Key</th>
				<th>Value</th>
				<th width="70">Active</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$config = $this->system_model->get_config();
	if($config->num_rows() > 0) {
		foreach($config->result() as $row) {
?>
			<tr>
				<td><?php echo form_input('type[]', $row->type, ' class="inline_text"') . '<span style="display:none;"> ' . $row->type . '</span>'; ?></td>
				<td><?php echo form_input('key[]', $row->key, ' class="inline_text"') . '<span style="display:none;"> ' . $row->key . '</span>'; ?></td>
				<td><?php echo form_input('val[]', $row->val, ' class="inline_text"') . '<span style="display:none;"> ' . $row->val . '</span>'; ?></td>
				<td><?php echo form_dropdown('active[]', array(0, 1), $row->active, ' class="inline_text"') . '<span style="display:none;"> ' . $row->active . '</span>'; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>Type</th>
				<th>Key</th>
				<th>Value</th>
				<th>Active</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>