<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	
	show_message();
	echo form_open('', 'id="form_list"'); 
	echo form_hidden('action', 'cms_model.save_list_' . $table . '_category');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/cms/' . $table . '_category_add'); ?>');"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th width="60" class="ui-state-default"><input type="text" name="search_language" value="Search language" class="search_init" /></th>
				<th width="*" class="ui-state-default"><input type="text" name="search_title" value="Search title" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_parent" value="Search parent" size="5" class="search_init" /></th>
				<th width="60" class="ui-state-default"></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Language</th>
				<th>Title</th>
				<th>Status</th>
				<th>Parent</th>
				<th>Sort</th>
			</tr>
		</thead>
		<tbody>
<?php 
	eval('$contents = $this->cms_model->get_' . $table . '_category_list();');	
	$field_id = $table . '_category_id';
	$field_name = $table . '_category_name';
	
	if($contents->num_rows() > 0) {
		foreach($contents->result() as $row) {
?>
			<tr>
				<td><?php echo $row->$field_id; ?></td>
				<td><a href="<?php echo base_url('system/cms/' . $table . '_category_edit/' . $row->$field_id); ?>" title="<?php echo $row->$field_name; ?>"><?php echo $row->language; ?></a></td>
				<td><a href="<?php echo base_url('system/cms/' . $table . '_category_edit/' . $row->$field_id); ?>" title="<?php echo $row->$field_name; ?>"><?php echo $row->title; ?></a></td>
				<td><a href="<?php echo base_url('system/cms/' . $table . '_category_edit/' . $row->$field_id); ?>" title="<?php echo $row->$field_name; ?>"><?php echo $data_status[$row->status]; ?></a></td>
				<td><?php echo form_input('parent_id[' . $row->$field_id . ']', $row->parent_id, ' class="inline_text"') . '<span style="display:none;"> ' . $row->parent_id . '</span>'; ?></td>
				<td><?php echo form_input('sort[' . $row->$field_id . ']', $row->sort, ' class="inline_text"') . '<span style="display:none;"> ' . $row->sort . '</span>'; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Language</th>
				<th>Title</th>
				<th>Status</th>
				<th>Parent</th>
				<th>Sort</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>