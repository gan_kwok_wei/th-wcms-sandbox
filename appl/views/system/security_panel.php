<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
	$( "#security_tabs" ).tabs();
	roleTable = $('#role_table').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
	permTable = $('#permission_table').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
	
});
</script>
<div id="main">
	<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	show_message();
?>

	<div id="security_tabs">
		<ul>
			<li><a href="#tabs-1">Role</a></li>
			<li><a href="#tabs-2">Permission</a></li>
		</ul>
		<div id="tabs-1">
			<div style="text-align:right; padding-bottom:1em;">
				<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url(); ?>system/security/role_add');"/>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="role_table">
				<thead>
					<tr>
						<th width="50">ID</th>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
<?php 
	$role = $this->user_model->get_role_list();
	if($role->num_rows() > 0) {
		foreach($role->result() as $row) {
?>
					<tr>
						<th><?php echo $row->role_id; ?></th>
						<td><a href="<?php echo base_url() . 'system/security/role_edit/' . $row->role_id; ?>"><?php echo $row->name; ?></a></td>
					</tr>
<?php 
		}
	}
?>
				</tbody>
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Name</th>
					</tr>
				</tfoot>
			</table>
		</div>
		
		<div id="tabs-2">
			<div style="text-align:right; padding-bottom:1em;">
				<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url(); ?>system/security/permission_add');"/>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="permission_table">
				<thead>
					<tr>
						<th width="50">ID</th>
						<th>Name</th>
						<th>Module</th>
						<th>Class</th>
						<th>Function</th>
					</tr>
				</thead>
				<tbody>
<?php 
	$permission = $this->user_model->get_permission_list();
	if($permission->num_rows() > 0) {
		foreach($permission->result() as $row) {
?>
					<tr>
						<td><?php echo $row->permission_id; ?></td>
						<td><a href="<?php echo base_url() . 'system/security/permission_edit/' . $row->permission_id; ?>"><?php echo $row->name; ?></a></td>
						<td><a href="<?php echo base_url() . 'system/security/permission_edit/' . $row->permission_id; ?>"><?php echo $row->module; ?></a></td>
						<td><a href="<?php echo base_url() . 'system/security/permission_edit/' . $row->permission_id; ?>"><?php echo $row->class; ?></a></td>
						<td><a href="<?php echo base_url() . 'system/security/permission_edit/' . $row->permission_id; ?>"><?php echo $row->function; ?></a></td>
					</tr>
<?php 
		}
	}
?>
				</tbody>
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Module</th>
						<th>Class</th>
						<th>Function</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
		
	
</div>