
<div id="left_content">
	<div class="yui-b" id="mainmenu">
		<div class="small"></div>
		<ul class="menu">
			<li><a title="Personal Board" href="<?php echo base_url('system/dashboard'); ?>">DASHBOARD</a></li>
<?php 
	if(get_role() <= $this->config->item('ge_max_admin_role')) {
?>
			<li><a title="Master Setup" href="<?php echo base_url('system/user'); ?>">SYSTEM CONFIGURATION</a>
				<ul class="submenu">
					<li><a title="Role & Security" href="<?php echo base_url('system/security'); ?>">Role & Security</a></li>
					<li><a title="User Administration" href="<?php echo base_url('system/user'); ?>">User</a></li>
					<li><a title="Configuration" href="<?php echo base_url('system/dashboard/config'); ?>">Configuration</a></li>
				</ul>
			</li>
			<li><a title="Hotels" href="<?php echo base_url('system/hotel'); ?>">HOTEL MANAGEMENT</a>
				<ul class="submenu">
					<li><a title="Country & Language" href="<?php echo base_url('system/dashboard/country'); ?>">Country</a></li>
					<li><a title="Cities" href="<?php echo base_url('system/city'); ?>">City</a>
                    <li><a title="Landmarks" href="<?php echo base_url('system/landmark'); ?>">Nearby Attractions</a></li>
							<li><a title="Appetite" href="<?php echo base_url('system/culinary'); ?>">Culinary</a></li>
						
					
					<li><a title="Hotels" href="<?php echo base_url('system/hotel'); ?>">Hotel</a>
					<li><a title="Feature reference" href="<?php echo base_url('system/hotel/feature_ref_list'); ?>">Feature reference</a></li>
					<li><a title="Feature Package" href="<?php echo base_url('system/hotel/package'); ?>">Feature Package</a></li>
				</ul>
			</li>
			<li><a title="Inquiry" href="<?php echo base_url('system/hotel/inquiry'); ?>">INQUIRIES MANAGEMENT</a></li>
			<li><a title="Banner & Promo" href="<?php echo base_url('system/cms'); ?>">BANNERS & PROMOTIONS</a>
				<ul class="submenu">
					<li><a title="Hotel Promo" href="<?php echo base_url('system/promo'); ?>">Promotion</a></li>
					<li><a title="Hotel Campaign" href="<?php echo base_url('system/promo/campaign'); ?>">Campaign</a></li>
					<li><a title="Banner Category" href="<?php echo base_url('system/cms/banner_category'); ?>">Banner Categories</a></li>
					<li><a title="Banner" href="<?php echo base_url('system/cms/banner'); ?>">Banner</a></li>
					<li><a title="Top Destination" href="<?php echo base_url('system/hotel/destination_list'); ?>">Top Destination</a></li>
				</ul>
			</li>
<?php 
	}
?>
			<li><a title="CMS" href="<?php echo base_url('system/cms'); ?>">CONTENT CONFIGURATION</a>
				<ul class="submenu">
<?php 
	if(get_role() <= $this->config->item('ge_max_admin_role')) {
?>
					<li><a title="Menu" href="<?php echo base_url('system/dashboard/menu'); ?>">Menu</a></li>
					<li><a title="Content Category" href="<?php echo base_url('system/cms/content_category'); ?>">Contents Category</a></li>
					<li><a title="CMS" href="<?php echo base_url('system/cms'); ?>">Content</a>
					<!-- li><a title="Content" href="<?php echo base_url('system/cms/content'); ?>">Contents</a></li>
					<li><a title="Gallery Category" href="<?php echo base_url('system/cms/gallery_category'); ?>">Gallery Category</a></li>
					<li><a title="Gallery" href="<?php echo base_url('system/cms/gallery'); ?>">Gallery</a></li-->
<?php 
	}
?>	
				</ul>
			</li>
            <li><a title="Pages" href="<?php echo base_url('system/pages'); ?>">PAGES CONTENT</a>
						<ul class="submenu">
							<li><a title="Custom Page" href="<?php echo base_url('system/pages'); ?>">Custom Page</a></li>
							<!-- li><a title="Home" href="<?php echo base_url('system/pages/home'); ?>">Home</a></li>
							<li><a title="Our Hotels" href="<?php echo base_url('system/pages/our_hotels'); ?>">Our hotels</a></li>
							<li><a title="Promotions" href="<?php echo base_url('system/pages/promotions'); ?>">Promotions</a></li-->
							<li><a title="Corporate" href="<?php echo base_url('system/pages/corporate'); ?>">Corporate</a></li>
							<li><a title="About Us" href="<?php echo base_url('system/pages/about_us_management_team'); ?>">About Us</a></li>
							<li><a title="Help & Info" href="<?php echo base_url('system/pages/help_info_contact_us'); ?>">Help & Info</a></li>
						</ul>
					</li>
		</ul>
	</div>
	<div class="yui-b" id="user_id">
	<!-- <?php echo lang('label_language').' : '.$this->session->get_dec('lang'); ?><br/>
	<?php if($this->session->get_dec('lang') != 'english') { ?><a href="<?php echo site_url('../en/system/dashboard'); ?>"><?php } ?><img src="<?php echo ASSET_PATH."themes/admin/default/images/english.gif"; ?>" style="border:0" /><?php if($this->session->get_dec('lang') != 'english') { ?></a><?php } ?>&nbsp;
	<?php if($this->session->get_dec('lang') != 'indonesia') { ?><a href="<?php echo site_url('../id/system/dashboard'); ?>"><?php } ?><img src="<?php echo ASSET_PATH."themes/admin/default/images/indonesia.gif"; ?>" style="border:0" /><?php if($this->session->get_dec('lang') != 'indonesia') { ?></a><?php } ?>&nbsp;
	<hr/--> <br/>
<?php echo lang('label_welcome') . '<br/>'; ?>
		<a title="My Account" href="<?php echo base_url('system/user/account'); ?>"><?php echo get_user_data('name'); ?></a><br/><br/>
<?php echo lang('label_last_login_on') . ' : <br/>' . get_user_data('last_login') . '<br/><br/>'; ?>
		<a title="Logout from system" href="<?php echo base_url('login/logout'); ?>"><?php echo lang('label_logout')?></a>
	</div>
	<div class="clear"></div>
</div>
