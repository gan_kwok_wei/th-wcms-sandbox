<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">

</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$user = $this->user_model->get_user($user_id);
		if($user->num_rows() > 0) {
			$user_data = $user->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/user');
			exit;
		}
	}
	$role = $this->system_model->get_parent_role();
	$countries = $this->system_model->get_parent_country();
	
	show_message();
	echo form_open('', 'id="form_user"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'user_model.save_user');
	echo ($mode != 'add') ? form_hidden('user_id', $user_id) : '';
?>
	<table width="100%">
		<tr>
			<td><?php echo lang('label_user_id'); ?>*</td>
			<td>
			<?php 
				$field_person_id = array(
								'name'		=> 'external_id',
								'id'		=> 'external_id',
								'value'		=> ($mode == 'add') ? set_value('external_id') : $user_data->external_id,
								'maxlength'	=> '10',
								'size'		=> '10'
							);
				 echo form_input($field_person_id); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?php echo lang('label_user_name'); ?>*</td>
			<td>
			<?php 
				$field_user_name = array(
								'name'        => 'user_name',
								'id'          => 'user_name',
								'value'       => ($mode == 'add') ? set_value('user_name') : $user_data->user_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_user_name); 
			?>
			</td>
			<td><?php echo lang('label_email'); ?>*</td>
			<td>
			<?php 
				$field_email = array(
								'name'        => 'email',
								'id'          => 'email',
								'value'       => ($mode == 'add') ? set_value('email'): $user_data->email,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_email); 
			?>
			</td>
		</tr>
		<?php if($mode == 'add') { ?>
		<tr>
			<td><?php echo lang('label_password'); ?>*</td>
			<td>
			<?php 
				$field_password = array(
								'name'        => 'password',
								'id'          => 'password',
								'maxlength'   => '50',
								'size'        => '50'
							);
				echo form_password($field_password); 
			?>
			</td>
			<td>
			<?php echo lang('label_conf_password'); ?>*
			</td>
			<td>
			<?php   
				$field_conf_password = array(
								'name'        => 'conf_password',
								'id'          => 'conf_password',
								'maxlength'   => '50',
								'size'        => '50'
							);
				echo form_password($field_conf_password); 
			?>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="4"><div class="group_title"><?php echo lang('label_personal_info'); ?></div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_role_name'); ?>*</td>
			<td>
			<?php 
				if($mode != 'add') {
					if(get_role() <= $this->config->item('ge_max_admin_role')) {
						if($user_data->role_id < get_role()) {
							echo "<strong>" . $role[$user_data->role_id] . "</strong>";
						} else {
							if(get_role() != 1) {
								foreach($role as $k => $v) {
									if ($k < get_role()) {
										unset($role[$k]);
									}
								}
							}
							echo form_dropdown('role_id', $role, $user_data->role_id);
						} 
					} else {
						echo "<strong>" . $role[$user_data->role_id] . "</strong>";
					}
				} else {
					if(get_role() != 1) {
						foreach($role as $k => $v) {
							if ($k < get_role()) {
								unset($role[$k]);
							}
						}
					}
					echo form_dropdown('role_id', $role, set_value('role_id'));
				}
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?php echo lang('label_first_name'); ?>*
			</td>
			<td>
			<?php
				$field_first_name = array(
								'name'        => 'first_name',
								'id'          => 'first_name',
								'value'       => ($mode == 'add') ? set_value('first_name') : $user_data->first_name,
								'maxlength'   => '50',
								'size'        => '50'
							);
				echo form_input($field_first_name);  
			?>
			</td>
			<td><?php echo lang('label_last_name'); ?></td>
			<td>
			<?php 
				$field_last_name = array(
								'name'        => 'last_name',
								'id'          => 'last_name',
								'value'       => ($mode == 'add') ? set_value('last_name') : $user_data->last_name,
								'maxlength'   => '50',
								'size'        => '50'
							);
				echo form_input($field_last_name); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_email_other'); ?></td>
			<td>
			<?php 
				$field_email_other = array(
								'name'        => 'email_other',
								'id'          => 'email_other',
								'value'       => ($mode == 'add') ? set_value('email_other'): $user_data->email_other,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_email_other); 
			?>
			</td>
			<td><?php echo lang('label_job_title'); ?></td>
			<td>
			<?php 
				$field_job_title = array(
								'name'        => 'job_title',
								'id'          => 'job_title',
								'value'       => ($mode == 'add') ? set_value('job_title'): $user_data->job_title,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_job_title); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title"><?php echo lang('label_personal_contact'); ?></div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_address'); ?></td>
			<td colspan="3">
			<?php 
				$field_address1 = array(
								'name'        => 'address1',
								'id'          => 'address1',
								'value'       => ($mode == 'add') ? set_value('address1'): $user_data->address1,
								'maxlength'   => '150',
								'size'        => '100'
							);
				echo form_input($field_address1); 
			?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3">
			<?php 
				$field_address2 = array(
								'name'        => 'address2',
								'id'          => 'address2',
								'value'       => ($mode == 'add') ? set_value('address2'): $user_data->address2,
								'maxlength'   => '150',
								'size'        => '100'
							);
				echo form_input($field_address2); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_city'); ?></td>
			<td>
			<?php 
				$field_city = array(
								'name'        => 'city',
								'id'          => 'city',
								'value'       => ($mode == 'add') ? set_value('city'): $user_data->city,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_city); 
			?>
			</td>
			<td><?php echo lang('label_state'); ?></td>
			<td>
			<?php 
				$field_state = array(
								'name'        => 'state',
								'id'          => 'state',
								'value'       => ($mode == 'add') ? set_value('state'): $user_data->state,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_state); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_country'); ?></td>
			<td>
			<?php echo form_dropdown('country', $countries, ($mode == 'add') ? set_value('country') : $user_data->country); ?>
			</td>
			<td><?php echo lang('label_zip'); ?></td>
			<td>
			<?php 
				$field_zip = array(
								'name'        => 'zip',
								'id'          => 'zip',
								'value'       => ($mode == 'add') ? set_value('zip'): $user_data->zip,
								'maxlength'   => '5',
								'size'        => '7'
							);
				echo form_input($field_zip); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_work_phone'); ?></td>
			<td>
			<?php 
				$field_phone_work = array(
								'name'        => 'phone_work',
								'id'          => 'phone_work',
								'value'       => ($mode == 'add') ? set_value('phone_work'): $user_data->phone_work,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone_work); 
			?>
			</td>
			<td><?php echo lang('label_home_phone'); ?></td>
			<td>
			<?php 
				$field_phone_home = array(
								'name'        => 'phone_home',
								'id'          => 'phone_home',
								'value'       => ($mode == 'add') ? set_value('phone_home'): $user_data->phone_home,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone_home); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_mobile_phone'); ?></td>
			<td>
			<?php 
				$field_phone_mobile = array(
								'name'        => 'phone_mobile',
								'id'          => 'phone_mobile',
								'value'       => ($mode == 'add') ? set_value('phone_mobile'): $user_data->phone_mobile,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone_mobile); 
			?>
			</td>
			<td><?php echo lang('label_fax'); ?></td>
			<td>
			<?php 
				$field_fax = array(
								'name'        => 'fax',
								'id'          => 'fax',
								'value'       => ($mode == 'add') ? set_value('fax'): $user_data->fax,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_fax); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_im'); ?></td>
			<td>
			<?php 
				echo form_dropdown('im_type_id', $this->config->item('ge_im_type'), ($mode == 'add') ? set_value('im_type_id') : $user_data->im_type_id); 
				$field_im_id = array(
								'name'        => 'im_id',
								'id'          => 'im_id',
								'value'       => ($mode == 'add') ? set_value('im_id'): $user_data->im_id,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_im_id); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		if(($user_id != get_user_id()) && ($user_data->role_id >= get_role())) {
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td><?php echo form_checkbox('active', 1, ($mode == 'add') ? TRUE : ($user_data->active == 0) ? FALSE : TRUE); ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
		}
?>
		<tr>
			<td><?php echo lang('label_last_login'); ?></td>
			<td><?php echo '<strong>'.$user_data->last_login.'</strong>'; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					$val = ($user_id == get_user_id()) ? lang('label_change_password') : lang('label_reset_password');
					echo '<input type="button" id="btnPassword" name="btnPassword" value="' . $val . '" /><br/><br/>';
					
					echo form_submit('btnSave', lang('label_update'));
					if(($user_id != get_user_id()) && ($user_data->role_id >= get_role())) {
						echo '<input type="button" id="btnDelete" name="btnDelete" value="'.lang('label_delete').'" />';
					}
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/user'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
<script type="text/javascript">
<?php 
	if($mode != 'add') {
?>
	$('#btnDelete').click(function() {	
		if(confirm("<?php echo lang('conf_delete_user') . $user_data->user_id; ?>")){ 
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "user_id=<?php echo $user_id ?>&mode=delete&action=user_model.delete_user",
				success: function(data){//alert(data);
					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/user/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
//			location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/user/user_delete/' . $user_data->user_id)); ?>');
		}
	});
	$('#btnPassword').click(function() {
<?php 
		if($user_id == get_user_id()) { 
?>
		location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/user/change_password/' . $user_data->user_id)); ?>');
		
<?php 
		} else { 
?>
		if(confirm("<?php echo lang('conf_reset_password') . $user_data->user_id; ?>")){
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/user/reset_password/' . $user_data->user_id)); ?>",
				data: "user_id=<?php echo $user_id ?>",
				success: function(data){
				alert(data);
				}
			}); 
		}
		
<?php 	
		}
?>
	});

<?php 	
	}
?>
</script>
</div>