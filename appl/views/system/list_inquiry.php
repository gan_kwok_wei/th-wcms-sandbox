<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript">
<!--

//-->
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	$inquiry_type = array('general' => 'General', 'corporate' => 'Corporate');
	$inquiry_status = array('new', 'replied', 'passed');
	show_message();
	echo form_open('', 'id="form_list"'); 
	echo form_hidden('action', 'hotel_model.save_list_hotel');
?>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_type" value="Search Type" class="search_init" /></th>
				<th class="ui-state-default"><input type="text" name="search_name" value="Search Name" class="search_init" /></th>
				<th class="ui-state-default"><input type="text" name="search_email" value="Search Email" class="search_init" /></th>
				<th class="ui-state-default"><input type="text" name="search_hotel" value="Search Hotel" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Type</th>
				<th>Name</th>
				<th>Email</th>
				<th>Hotel</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->hotel_model->get_inquiry_list();
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->hotel_inquiry_id; ?></td>
				<td><?php echo humanize($row->hotel_inquiry_type); ?></td>
				<td><a href="<?php echo base_url('system/hotel/inquiry_show/' . $row->hotel_inquiry_id); ?>" title="<?php echo $row->hotel_name; ?>"><?php echo $row->contact_person; ?></a></td>
				<td><a href="<?php echo base_url('system/hotel/inquiry_show/' . $row->hotel_inquiry_id); ?>" title="<?php echo $row->hotel_name; ?>"><?php echo $row->email; ?></a></td>
				<td><a href="<?php echo base_url('system/hotel/inquiry_show/' . $row->hotel_inquiry_id); ?>" title="<?php echo $row->hotel_name; ?>"><?php echo $row->hotel_name; ?></a></td>
				<td><?php echo $inquiry_status[$row->status]; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Type</th>
				<th>Name</th>
				<th>Email</th>
				<th>Hotel</th>
				<th>Status</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>