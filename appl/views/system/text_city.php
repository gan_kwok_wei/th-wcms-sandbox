<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource content_text.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 9, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

?>
<script type="text/javascript" charset="utf-8">
	$().ready(function() {
		$( "#lang_tabs" ).tabs();
	});
</script>
<div id="lang_tabs">
	<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
	</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		if($mode != 'add') {
			$result = $this->system_model->get_content_text($content_text_type, $parent_id, $lang_v);
			if($result->num_rows() > 0) {
				$text = $result->row();
			} else {
				$text = new stdClass();
				$text->content_text_id = "0";
				$text->title = "";
				$text->icon = "";
				$text->image = "";
				$text->teaser = "";
				$text->content = "";
				$text->icon_1 = "";
				$text->image_1 = "";
				$text->teaser_1 = "";
				$text->content_1 = "";
				$text->link = "";
				$text->country = "";
			}
			echo form_hidden("content_text_id[$lang_k]", $text->content_text_id); 
		}
		echo form_hidden("language[$lang_k]", $lang_v); 
?>					
	<div id="tabs-<?php echo $lang_k; ?>">
		<table>		
			<tr>
				<td><?php echo lang('label_title'); ?>*</td>
				<td colspan="3">
				<?php 
					$field_title = array(
									'name'        => "title[$lang_k]",
									'id'          => "title_$lang_k",
									'value'       => ($mode == 'add') ? set_value('title'): $text->title,
									'maxlength'   => '150',
									'size'        => '120'
								);
					echo form_input($field_title); 
				?>
				</td>
			</tr>
			<tr>
				<td valign="top"><?php echo lang('label_teaser_' . $content_text_type); ?></td>
				<td colspan="3"><?php echo $this->ckeditor->editor("teaser[$lang_k]", ($mode == 'add') ? '' : $text->teaser); ?></td>
			</tr>
			<tr>
				<td><?php echo lang('label_link'); ?></td>
				<td colspan="3">
				<?php 
					$field_link = array(
									'name'        => "link[$lang_k]",
									'id'          => "link_$lang_k",
									'value'       => ($mode == 'add') ? set_value('link'): $text->link,
									'maxlength'   => '150',
									'size'        => '132'
								);
					echo form_input($field_link); 
				?>
				</td>
			</tr>
			<tr>
				<td><?php echo lang('label_country'); ?></td>
				<td colspan="3">
<?php 
		if($mode == 'add') {
			foreach ($this->system_model->get_registered_country() as $country_k => $country_v) {
				echo form_checkbox("country[$lang_k][]", $country_k, TRUE) . ucwords($country_v) . "|&nbsp;&nbsp;";
			}
		} else {
			$country_array = explode('|', $text->country);
			foreach ($this->system_model->get_registered_country() as $country_k => $country_v) {
				echo form_checkbox("country[$lang_k][]", $country_k, (in_array($country_k, $country_array)) ? TRUE : FALSE) . ucwords($country_v) . "|&nbsp;&nbsp;";
			}	
		}
?>
				</td>
			</tr>
		</table>
	</div>
<?php 
	}
?>				
</div>


<?php 
/**
 * End of file content_text.php 
 * Location: ./.../.../.../content_text.php 
 */