<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">

	function BrowseServer(startupPath, functionData) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}

	function SetFileField( fileUrl, data ) {
		$("#" + data["selectActionData"] ).val(fileUrl);
	}

	function addBanner(id, number, name) {
		$('#selected_banner_table').prepend('<tr id="selected_banner_' + id + '"> ' +
											'<td>' + number + '</td> ' +
											'<td>' + name + '</td> ' + 
											'<td><input type="hidden" name="banner_id[]" value="' + id + '"/><input type="button" onclick="javascript:removeBanner(' + id + ')" value="remove" /></td>' +
										'</tr>');
	}

	function removeBanner(id) {
		$('#selected_banner_' + id).remove();
	}

	function addPromo(id, hotel, name) {
		$('#selected_promo_table').prepend('<tr id="selected_promo_' + id + '"> ' +
											'<td>' + hotel + '</td> ' +
											'<td>' + name + '</td> ' +
											'<td><input type="hidden" name="promo_id[]" value="' + id + '"/><input type="button" onclick="javascript:removePromo(' + id + ')" value="remove" /></td> ' +
										'</tr>');
	}

	function removePromo(id) {
		$('#selected_promo_' + id).remove();
	}

	$().ready(function() {

		b1Table = $('#banner_table').dataTable({
			"bJQueryUI": true, 
			"sPaginationType": "full_numbers", 
			"oLanguage": {"sSearch": "Search all columns:"}
		});

		b2Table = $('#selected_banner_table').dataTable({
			"bJQueryUI": true, 
			"sPaginationType": "full_numbers", 
			"oLanguage": {"sSearch": "Search all columns:"}
		});

		p1Table = $('#promo_table').dataTable({
			"bJQueryUI": true, 
			"sPaginationType": "full_numbers", 
			"oLanguage": {"sSearch": "Search all columns:"}
		});

		p2Table = $('#selected_promo_table').dataTable({
			"bJQueryUI": true, 
			"sPaginationType": "full_numbers", 
			"oLanguage": {"sSearch": "Search all columns:"}
		});
		
		$("#form_banner").validate({
			rules: {"banner_category_name":"required",
					"title":"required"},
			messages: {"banner_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>"}
		});

		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	show_message();
	echo form_open('', 'id="form_campaign"'); 
	
	if($mode != 'add') {
		echo form_hidden('campaign_id', $campaign_id);
		$result = $this->promo_model->get_campaign($campaign_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/promo/campaign');
			exit;
		}
	}
	
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'promo_model.save_campaign');
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><strong><?php echo $data->campaign_id; ?></strong></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_title'); ?>*</td>
			<td>
			<?php 
				$field_title = array(
								'name'        => 'title',
								'id'          => 'title',
								'value'       => ($mode == 'add') ? set_value('title'): $data->title,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_title); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_note'); ?></td>
			<td colspan="3"><?php echo $this->ckeditor->editor("note", (($mode == 'add') ? '' : $data->note)); ?></td>
		</tr>
		<tr>
			<td><?php echo lang('label_valid_from'); ?></td>
			<td>
			<?php 
				$field_valid_from = array(
								'name'        => 'valid_from',
								'id'          => 'valid_from',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->valid_from,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_valid_from); 
			?>
			</td>
			<td><?php echo lang('label_valid_to'); ?></td>
			<td>
			<?php 
				$field_valid_to = array(
								'name'        => 'valid_to',
								'id'          => 'valid_to',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->valid_to,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_valid_to); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Banner</div></td>
		</tr>
		<tr>
			<td colspan="2" width="50%" valign="top">
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="banner_table">
					<thead>
						<tr>
							<th>Category</th>
							<th>Name</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<?php 
	$result = $this->promo_model->get_banner_list();
	if($result->num_rows() > 0) {
		foreach($result->result() as $row) {
?>
						<tr id="p_<?php echo $row->banner_id; ?>">
							<td><?php echo $row->category_title; ?></td>
							<td><?php echo $row->banner_name; ?></td>
							<td><input type="button" onclick="javascript:addBanner(<?php echo $row->banner_id; ?>, '<?php echo $row->category_title; ?>', '<?php echo $row->banner_name; ?>')" value="add" /></td>
						</tr>
<?php 
		}
	}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>Category</th>
							<th>Name</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td colspan="2" valign="top">
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="selected_banner_table">
					<thead>
						<tr>
							<th>Category</th>
							<th>Name</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<?php 
	
	$result = $this->promo_model->get_campaign_banner_list($campaign_id);
	if($result->num_rows() > 0) {
		foreach($result->result() as $row) {
?>
						<tr id="selected_banner_<?php echo $row->banner_id; ?>">
							<td><?php echo $row->category_title; ?></td>
							<td><?php echo $row->banner_name; ?></td>
							<td>
								<input type="hidden" name="banner_id[]" value="<?php echo $row->banner_id; ?>">
								<input type="button" onclick="javascript:removeBanner(<?php echo $row->banner_id; ?>)" value="remove" />
							</td>
						</tr>
<?php 
		}
	}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>Category</th>
							<th>Name</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Promo</div></td>
		</tr>
		<tr>
			<td colspan="2" valign="top">
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="promo_table">
					<thead>
						<tr>
							<th>Hotel</th>
							<th>Name</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<?php 
	$result = $this->promo_model->get_promo_list();
	if($result->num_rows() > 0) {
		foreach($result->result() as $row) {
?>
						<tr id="p_<?php echo $row->promo_id; ?>">
							<td><?php echo $row->hotel_name; ?></td>
							<td><?php echo $row->promo_name; ?></td>
							<td><input type="button" onclick="javascript:addPromo(<?php echo $row->promo_id; ?>, '<?php echo $row->hotel_name; ?>', '<?php echo $row->promo_name; ?>')" value="add" /></td>
						</tr>
<?php 
		}
	}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>Hotel</th>
							<th>Name</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td colspan="2" valign="top">
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="selected_promo_table">
					<thead>
						<tr>
							<th>Hotel</th>
							<th>Name</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<?php 
	$result = $this->promo_model->get_campaign_promo_list($campaign_id);
	if($result->num_rows() > 0) {
		foreach($result->result() as $row) {
?>
						<tr id="selected_promo_<?php echo $row->promo_id; ?>">
							<td><?php echo $row->hotel_name; ?></td>
							<td><?php echo $row->promo_name; ?></td>
							<td>
								<input type="hidden" name="promo_id[]" value="<?php echo $row->promo_id; ?>">
								<input type="button" onclick="javascript:removePromo(<?php echo $row->promo_id; ?>)" value="remove" />
							</td>
						</tr>
<?php 
		}
	}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>Hotel</th>
							<th>Name</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE); ?></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="'.lang('label_delete').'" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/promo/campaign'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>