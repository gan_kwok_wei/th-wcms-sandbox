<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php
	show_message();
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/dashboard/country_add/'); ?>');"/>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_country" value="Search Country" class="search_init" /></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Country</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->system_model->get_country_list();
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->country_id; ?></td>
				<td><a href="<?php echo base_url('system/dashboard/country_edit/' . $row->country_id); ?>" title="<?php echo $row->name; ?>"><?php echo $row->name; ?></a></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Country</th>
			</tr>
		</tfoot>
	</table>
</div>