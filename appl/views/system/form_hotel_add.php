<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		
		$( "#lang_tabs" ).tabs();
		
	});

</script>
<div id="main">
<h2 id="content_title">Add New Hotel</h2>
<?php 
	$category = $this->system_model->get_parent_city();
	
	show_message();
	echo form_open('', 'id="form_hotel"'); 
	echo form_hidden('mode', 'add');
	echo form_hidden('action', 'hotel_model.add_hotel');
?>
	<table width="100%">
		<tr>
			<td>URI name*</td>
			<td>
			<?php 
				$field_hotel_name = array(
								'name'        => 'hotel_name',
								'id'          => 'hotel_name',
								'value'       => set_value('hotel_name'),
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_hotel_name); 
			?>
			</td>
			<td><?php echo lang('label_city'); ?>*</td>
			<td><?php echo form_dropdown('city_id', $category, set_value('city_id')); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title"><?php echo lang('label_contact'); ?></div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_phone'); ?> 1</td>
			<td>
			<?php 
				$field_phone1 = array(
								'name'        => 'phone1',
								'id'          => 'phone1',
								'value'       => set_value('phone1'),
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone1); 
			?>
			</td>
			<td><?php echo lang('label_phone'); ?> 2</td>
			<td>
			<?php 
				$field_phone2 = array(
								'name'        => 'phone2',
								'id'          => 'phone2',
								'value'       => set_value('phone2'),
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_phone2); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_fax'); ?></td>
			<td>
			<?php 
				$field_fax = array(
								'name'        => 'fax',
								'id'          => 'fax',
								'value'       => set_value('fax'),
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_fax); 
			?>
			</td>
			<td><?php echo lang('label_email'); ?></td>
			<td>
			<?php 
				$field_email = array(
								'name'        => 'email',
								'id'          => 'email',
								'value'       => set_value('email'),
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_email); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Google Map</div></td>
		</tr>
		<tr>
			<td><?php echo lang('label_latitude'); ?></td>
			<td>
			<?php 
				$field_latitude = array(
								'name'        => 'latitude',
								'id'          => 'latitude',
								'value'       => set_value('latitude'),
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_latitude); 
			?>
			</td>
			<td><?php echo lang('label_longitude'); ?></td>
			<td>
			<?php 
				$field_longitude = array(
								'name'        => 'longitude',
								'id'          => 'longitude',
								'value'       => set_value('longitude'),
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_longitude); 
			?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_icon'); ?></td>
			<td valign="bottom">
			<?php 
				$field_icon = array(
								'name'        => "google_icon",
								'id'          => "google_icon",
								'value'       => set_value('google_icon'),
								'maxlength'   => '150',
								'size'        => '32'
							);
				echo form_input($field_icon); 
			?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'google_icon' );" />
			</td>
			<td valign="top"></td>
			<td valign="bottom"></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_desc'); ?></td>
			<td colspan="3"><?php echo $this->ckeditor->editor("google_note", set_value('note')); ?></td>
		</tr>
		
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
					</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		echo form_hidden("language[$lang_k]", $lang_v); 
?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table width="100%">
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => set_value('title'),
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_address'); ?></td>
								<td colspan="3"><?php echo $this->ckeditor->editor("address[$lang_k]"); ?></td>
							</tr>
							<tr>
								<td><?php echo lang('label_icon'); ?></td>
								<td>
								<?php 
									$field_icon = array(
													'name'        => "icon[$lang_k]",
													'id'          => "icon_$lang_k",
													'value'       => set_value('icon'),
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_icon); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
								</td>
								<td><?php echo lang('label_image'); ?></td>
								<td>
								<?php 
									$field_image = array(
													'name'        => "image[$lang_k]",
													'id'          => "image_$lang_k",
													'value'       => set_value('image'),
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_image); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'image_<?php echo $lang_k; ?>' );" />
								</td>
							</tr>
							<tr>
								<td valign="top">Feature text</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("feature_text[$lang_k]"); ?></td>
							</tr>
							<tr>
								<td valign="top">How to get Here</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("note[$lang_k]"); ?></td>
							</tr>
						</table>
					</div>
<?php 
	}
?>
				</div>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td><?php echo lang('label_tags'); ?></td>
			<td colspan="3">
			<?php 
				$field_tags = array(
								'name'        => 'tags',
								'id'          => 'tags',
								'value'       => set_value('tags'),
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_tags); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td colspan="3">
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => 0,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<!-- td>Top Destination</td>
			<td colspan="3"><?php // echo form_checkbox('top_destination', '1'); ?> </td-->
		</tr>
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => '0000-00-00 00:00',
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => '0000-00-00 00:00',
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo form_submit('btnSave', lang('label_save')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/hotel'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>