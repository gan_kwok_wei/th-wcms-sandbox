<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	$().ready(function() {
		
		
	});

</script>
<div id="main">
<?php 
	$result = $this->hotel_model->get_hotel($hotel_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
		if((get_role() > 3) && ($data->created_id != get_user_id())) {
    		set_warning_message(lang('alert_no_permission'));
			redirect('system/hotel');
			exit;
    	}
	    	
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel');
		exit;
	}
	echo '<h2 id="content_title">Edit Hotel : ' . $data->hotel_name . '</h2>';
	$category = $this->system_model->get_parent_city();
 
	show_message();
	echo form_open('', 'id="form_hotel"');
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'hotel_model.update_hotel_meta');
	echo form_hidden('hotel_id', $hotel_id);
?>
	<div id="hotel_tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_edit/' . $hotel_id)  ?>">Hotel Detail</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_room_edit/' . $hotel_id); ?>">Features</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_gallery_edit/' . $hotel_id);  ?>">Gallery</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_feature_edit/' . $hotel_id);  ?>">What We Provide</a></li>
			<li class="ui-state-default ui-corner-top"><a href="<?php echo base_url('system/hotel/hotel_package_edit/' . $hotel_id);  ?>">Packages</a></li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="<?php echo base_url('system/hotel/hotel_meta_edit/' . $hotel_id);  ?>">Meta Data Options</a></li>
		</ul>
	</div>
	
	<table width="100%">
		<tr>
			<td>URI name</td>
			<td><strong><?php echo $data->hotel_name; ?></strong></td>
			<td><?php echo lang('label_city'); ?></td>
			<td><?php echo form_dropdown('city_id', $category, $data->city_id, ' disabled="disabled"'); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Keywords</div></td>
		</tr>
		<tr>
		<tr>
			<td colspan="4"><?php echo $this->ckeditor->editor("meta_keywords", $data->meta_keywords); ?></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title">Description</div></td>
		</tr>
		<tr>
		<tr>
			<td colspan="4"><?php echo $this->ckeditor->editor("meta_description", $data->meta_description); ?></td>
		</tr>
		
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php echo form_submit('btnSave', lang('label_update')); ?>
				<input type="button" id="btnBack" value="<?php echo lang('label_close'); ?>" onclick="location.replace('<?php echo base_url('system/hotel'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>