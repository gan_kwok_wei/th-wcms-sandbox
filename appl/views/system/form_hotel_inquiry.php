<?php 
	$result = $this->hotel_model->get_inquiry($hotel_inquiry_id);
//	echo $this->db->last_query();
	if($result->num_rows() > 0) {
		$data = $result->row();
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/hotel/inquiry');
		exit;
	}
	
?>
<!-- div id="breadcrumb">
	
</div-->
<div id="main">
	<h2 id="content_title">Inquiry from : <?php echo $data->contact_person; ?> on <?php echo $data->date_send; ?></h2>
<?php 
	show_message();
	echo form_open('', 'id="form_inquiry"'); 
	echo form_hidden('action', 'hotel_model.response_inquiry');
	echo form_hidden('hotel_inquiry_id', $hotel_inquiry_id);
	echo form_hidden('to', $data->email);
	echo form_hidden('contact_person', $data->contact_person);

?>
	<table width="100%">
		<tr>
			<td style="width:140px;"><?php echo lang('label_id'); ?></td>
			<td><strong><?php echo $data->hotel_inquiry_id; ?></strong></td>
			<td>Hotel: </td>
			<td><strong><?php echo $data->hotel_name; ?></strong></td>
		</tr>
		<tr>
			<td>Contact Name: </td>
			<td><strong><?php echo $data->contact_person; ?></strong></td>
			<td>E-mail Address: </td>
			<td><strong><?php echo $data->email; ?></strong>
		</tr>
		<tr>
			<td>Country:</td>
			<td><strong><?php echo $data->country_name; ?></strong></td>
			<td>Post Code:</td>
			<td><strong><?php echo $data->postcode; ?></strong></td>
		</tr>
		<tr>
			<td>Address:</td>
			<td colspan="3"><strong><?php echo $data->company_name; ?></strong></td>
		</tr>
		<tr>
			<td>Remarks:</td>
			<td colspan="3"><strong><?php echo $data->remarks; ?></strong></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td style="vertical-align:top;">Response:</td>
			<td colspan="3"><?php echo ($data->status == 0) ? $this->ckeditor->editor("response", $data->response) : '<strong>' . $data->response . '</strong>'; ?><td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($data->status == 1) {	
		$response_by = $this->user_model->get_user($data->response_id)->row();
?>

					<tr>
						<td><?php echo 'Response by'; ?></td>
						<td colspan="3"><strong><?php echo $response_by->first_name . ' ' . $response_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td><?php echo 'Response Time'; ?></td>
						<td colspan="3"><strong><?php echo $data->response_time; ?></strong></td>
					</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
<?php
	if($data->status == 0) {
		echo form_submit('btnSave', 'Reply');
	}
?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/hotel/inquiry'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>