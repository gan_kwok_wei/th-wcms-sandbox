<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		
		$("#form_banner").validate({
			rules: {"banner_name":"required",
					"title":"required",
					"banner_category_id":"required"},
			messages: {"banner_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"banner_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$result = $this->hotel_model->get_destination($destination_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/hotel/destination_list');
			exit;
		}
	}
	
	$hotel = $this->system_model->get_parent_hotel();
	$package = array('per_night' => 'per Night');
	
	show_message();
	echo form_open('', 'id="form_destination"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'hotel_model.save_destination');
	echo ($mode != 'add') ? form_hidden('destination_id', $destination_id) : '';
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $data->destination_id; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_hotel'); ?>*</td>
			<td><?php echo form_dropdown('hotel_id', $hotel, ($mode == 'add') ? set_value('hotel_id') : $data->hotel_id); ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?php echo lang('label_price'); ?></td>
			<td colspan="3">
			<?php 
				$field_price = array(
								'name'        => 'price',
								'id'          => 'price',
								'value'       => ($mode == 'add') ? set_value('price'): $data->price,
								'maxlength'   => '150',
								'size'        => '32'
							);
				echo form_input($field_price); 
				echo form_dropdown('package', $package, ($mode == 'add') ? set_value('package') : $data->package);
			?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_note'); ?></td>
			<td colspan="3"><?php echo $this->ckeditor->editor("note", ($mode == 'add') ? '' : $data->note); ?></td>
		</tr>
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_end,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_link'); ?></td>
			<td colspan="3">
			<?php 
				$field_link = array(
								'name'        => 'link',
								'id'          => 'link',
								'value'       => ($mode == 'add') ? set_value('link'): $data->link,
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_link); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0: $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE); ?></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/hotel/destination_list'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
<script type="text/javascript">
<?php 
	if($mode != 'add') {
?>
	$('#btnDelete').click(function() {	
		if(confirm("<?php echo lang('conf_delete') . $banner_id; ?>")){
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "banner_id=<?php echo $banner_id ?>&mode=delete&action=cms_model.delete_banner",
				success: function(data){//alert(typeof(data));
					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/cms/banner/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
		}
	});
<?php 	
	}
?>
</script>
</div>