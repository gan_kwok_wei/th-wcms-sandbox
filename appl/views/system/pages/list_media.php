<!-- div id="breadcrumb">
	
</div-->
<?php 

	$list = $this->cms_model->get_content_category_list($content_category_id);
	$category_list = $list->result();
	
?>

<script type="text/javascript">
<!--

	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
<?php 
	foreach($category_list as $category) {
?>		
	<?php echo $category->content_category_name . 'Table'; ?> = $('<?php echo '#' . $category->content_category_name . '_table'; ?>').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
		$('<?php echo '#form_' . $category->content_category_name . '_list'; ?>').submit( function() { if(confirm("Save list?")) { return true; } else { return false; } });
<?php 
	}
?>
		var asInitVals = new Array();
		$("thead input").keyup( function () { oTable.fnFilter( this.value, $("thead input").index(this) + 1 ); } );
		$("thead input").each( function (i) { asInitVals[i] = this.value; } );
		$("thead input").focus( function () { if( this.className == "search_init" ) { this.className = ""; this.value = ""; } } );
		$("thead input").blur( function (i) { if( this.value == "" ) { this.className = "search_init"; this.value = asInitVals[$("thead input").index(this)]; } } );
	});
//-->
</script>
<div id="main">
<?php 
	$this->load->view('system/pages/' . $menu, array('sub_about' => $sub_about));
	show_message();
	$result = $this->cms_model->get_content($content_id);
	$data = $result->row();
	echo form_open('', 'id="form_content"'); 
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'cms_model.save_content');
	echo form_hidden('content_id', $content_id);
	echo form_hidden('content_category_id', $data->content_category_id);
	echo form_hidden('content_name', $data->content_name);
	echo form_hidden('tags', $data->tags);
	echo form_hidden('sort', $data->sort);
	echo form_hidden('status', 1);
	echo form_hidden('publish_date_start', $data->publish_date_start);
	echo form_hidden('publish_date_end', $data->publish_date_end);
	echo form_hidden('ref', "system/pages/$ref");
?>
	<table width="100%">
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
				<?php 
					foreach ($this->config->item('ge_language') as $lang_k => $lang_v) { 
						echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
					}
				?>					
					</ul>
				<?php 
					foreach ($this->config->item('ge_language') as $lang_k => $lang_v) {
						
						$result = $this->system_model->get_content_text('content', $content_id, $lang_v);
						if($result->num_rows() > 0) {
							$text = $result->row();
						} else {
							$text = new stdClass();
							$text->content_text_id = "0";
							$text->title = "";
							$text->icon = "";
							$text->image = "";
							$text->teaser = "";
							$text->content = "";
							$text->link = "";
							$text->country = "";
						}
						echo form_hidden("content_text_id[$lang_k]", $text->content_text_id); 
						echo form_hidden("language[$lang_k]", $lang_v); 
						foreach ($this->system_model->get_registered_country() as $country_k => $country_v) {
							echo form_hidden("country[$lang_k][]", $country_k);
						}
				?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table>		
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_content_content'); ?></td>
								<td colspan="3"><?php echo $this->ckeditor->editor("content[$lang_k]", $text->content); ?></td>
							</tr>
							<tr>
								<td><?php echo lang('label_link'); ?></td>
								<td colspan="3">
								<?php 
									$field_link = array(
													'name'        => "link[$lang_k]",
													'id'          => "link_$lang_k",
													'value'       => $text->link,
													'maxlength'   => '150',
													'size'        => '132'
												);
									echo form_input($field_link); 
								?>
								</td>
							</tr>
						</table>
					</div>
				<?php 
					}
				?>				
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="4"><?php echo form_submit('btnSave', lang('label_save')); ?> </td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
	</table>
<?php 
	echo form_close(); 

	foreach($category_list as $category) {
		echo form_open('', 'id="form_' . $category->content_category_name . '_list"'); 
		echo form_hidden('action', 'cms_model.save_list_content');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<strong><?php echo $category->title; ?></strong>
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/pages/content_add/' . $ref . '/' . $category->content_category_id); ?>');"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="<?php echo $category->content_category_name . '_table'; ?>">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_title" value="Search title" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
				<th width="60" class="ui-state-default"></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</thead>
		<tbody>
<?php 
		$list = $this->cms_model->get_content_list($category->content_category_id);
		if($list->num_rows() > 0) {
			foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->content_id; ?></td>
				<td><a href="<?php echo base_url('system/pages/content_edit/' . $ref . '/' . $row->content_id); ?>" title="<?php echo $row->content_name; ?>"><?php echo $row->content_name; ?></a></td>
				<td><?php echo $data_status[$row->status]; ?></td>
				<td><?php echo form_input('sort[' . $row->content_id . ']', $row->sort, ' class="inline_text"') . '<span style="display:none;"> ' . $row->sort . '</span>'; ?></td>
			</tr>
<?php 
			}
		}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</tfoot>
	</table>
	<hr/>
	<br/>
<?php 
		echo form_close();
	}
?>
</div>