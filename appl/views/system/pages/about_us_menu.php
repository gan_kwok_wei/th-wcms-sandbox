<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource about_us_menu.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 22, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */
?>
<div class="menu_red">
	<div class="menu_red_left f_left"></div>
	<div class="menu_red_center f_left">
		<div class="menu_red_content f_left"><h1>About Us</h1></div>
		<div class="menu_red_content2 f_left">
			<ul>
				<li>
					<a href="<?php echo base_url('system/pages/about_us_management_team');?>" class="<?php echo ($sub_about == "management_team" ? "selected" : "");?>">Management Team</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/about_us_shareholders');?>" class="<?php echo ($sub_about == "shareholders" ? "selected" : "");?>">Shareholders</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/about_us_career');?>" class="<?php echo ($sub_about == "career" ? "selected" : "");?>">Career</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/about_us_concept');?>" class="<?php echo ($sub_about == "concept" ? "selected" : "");?>">Concept</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/about_us_media');?>" class="<?php echo ($sub_about == "media" ? "selected" : "");?>">Media</a>
				</li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
	<div class="menu_red_right f_left"></div>
	<div class="clear"></div>
</div>
<div class="clear"></div>

<?php 
/**
 * End of file about_us_menu.php 
 * Location: ./.../.../.../about_us_menu.php 
 */