<?php
	define('ASSET_PATH', base_url().'assets/');
//	include_once('../conn.php');
//	include_once('function.php');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
	<title>About Ace Hardware Stores Indonesia | Browse for Hardware, Home Improvement, and Tools.</title>
	<meta name="description" content="Let Ace Hardware provide you with great hardware products and advice from our official online home.  Whether you are looking for paint, lawn &amp; garden supplies, hardware or tools, Ace Hardware has everything you need!" />
	<meta name="keywords" content="hardware for the home, home hardware, buy hardware, purchase hardware, shop for hardware, shop hardware, hardware shop, online hardware store, online hardware stores, hardware shops, hardware store, hardware stores, hardware shop online, hardware shops online, hardware store online, hardware stores online, hardware retailer, home improvement, home improvements, home improvement store, home improvement stores, home improvement retailers" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7, IE=9" />
	<meta name="Author" content="Ace Hardware Indonesia" />
	<meta name="viewport" content="width=1024" />

	<link rel="shortcut icon" href="http://ACE.imageg.net/favicon.ico" />
<!-- Stylesheets -->
	<link href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/navigation.css" type="text/css" id="globalheader-stylesheet" rel="stylesheet"  />
	<link href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/style.css"  rel="stylesheet" type="text/css" />

<!-- Javascripts -->
	<script type="text/javascript" src="http://platform.twitter.com/anywhere.js?id&#61;akzPZxSf6sr30votc95ySQ&amp;v&#61;1"></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery.js" type="text/javascript" ></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery.color.js" type="text/javascript" ></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery.tools.min.js" type="text/javascript" ></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/acehardware.js" type="text/javascript" ></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery.hoverIntent.minified.js" type="text/javascript"></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery.dcverticalmegamenu.1.1.js" type="text/javascript"></script>           
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery.easing.1.3.js" type="text/javascript"></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery.jBreadCrumb.1.1.js" type="text/javascript" language="JavaScript"></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo ASSET_PATH; ?>js/pages/globalnav.js" type="text/javascript" charset="utf-8"></script>
<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="ie8.css" />
	<script src="js/png.js"></script>
	<script>
	DD_belatedPNG.fix('.png_bg');
	</script>
<![endif]--> 
	<script>
        window.fbAsyncInit = function() {
          FB.init({
            appId      : '138112892948100',
            status     : true, 
            cookie     : true,
            xfbml      : true,
            oauth      : true,
          });
        };
        (function(d){
           var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/en_US/all.js";
           d.getElementsByTagName('head')[0].appendChild(js);
         }(document));
        
    	$().ready(function() { 
    		$('#preview_title').html(window.opener.document.forms[0].title.value);
    		$('#preview_image').attr('src', window.opener.document.forms[0].image.value);
    		$('#preview_content').html(window.opener.document.forms[0].content.value);
    		
        });    
	</script>
      
<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<script type="text/javascript" src="js/twitter-widget.js"></script>

</head>
<body>

<!-- Wrapper -->
	<div id="wrapper_sec">
		<!-- Master Head -->
	    <div id="masthead">
	    	<div class="inner" style="padding-top:10px">
		    	<div class="clear"></div>
	        </div><!-- end inner -->
		</div>
		
		<div id="content_sec">
			<div class="inner">
			<div class="clear"></div>
				<!-- Curved Box -->
				<div class="sections">
					<div class="sectopwhite">&nbsp;</div>
					<div class="seccentwhite">
					<!-- Column 3 -->
						<div class="col3">
							<!-- Static Page -->
							<div class="static">
							
								<h5 id="preview_title"></h5>
								<div class="hairlineTips"></div>
								<h3 class="wclm">
									<img id="preview_image" src="image">
								</h3>
							
							
								<div class="sec10">
								<div id="content">  	
									<br>
									<div class="tips colr">
										<div id="preview_content"></div>
									
										<div class="share">
											Share : 
											<a href="#"><img src="images/share/fb.jpg" title="Facebook" width="13px" alt="rss"></a>
											<a href="#"><img src="images/share/tw.jpg" title="twitter" width="13px" alt="rss"></a>
											<a href="#"><img src="images/share/email.jpg" title="email" alt="email"></a>
											<a href="#"><img src="images/share/print.jpg" title="print" alt="print"></a>
										</div>		
									</div>
									<div class="clear"></div>
								</div><!-- end content -->
							</div><!-- end sec 2-->
							
							
							<div class="sec9">
								<div class="corporate">
									<h2 class="white headingSmall" style="line-height: 10px;" ><span id="titleArticle">XXXXX</span></h2>
									<br>
								
									<ul id="nav">
										<li><a href="#">-xxxxx</a></li>
										<li><a href="#">-xxxxx</a></li>
										<li><a href="#">-xxxxx</a></li>
										<li><a href="#">-xxxxx</a></li>
									</ul>
									
									<div class="showall red">SHOW ALL</div>
								</div>
								<div class="corporate_footer"></div>
								<br>
								<div class="corporate">
									<h2 class="white headingSmall" style="line-height: 10px;"><span id="titleTool">XXXXX</span></h2>
									<br>
								
									<ul id="nav">
										<li><a href="#">-xxxxx</a></li>
										<li><a href="#">-xxxxx</a></li>
										<li><a href="#">-xxxxx</a></li>
										<li><a href="#">-xxxxx</a></li>
									</ul>
									<div class="showall red">SHOW ALL</div>
								</div>
								<div class="corporate_footer"></div>
								<div class="clear"></div>
							</div><!-- end sec1 -->
							</div>
						</div>
					</div>
					<div class="secbotwhite">&nbsp;</div>
				</div>
		        <div class="clear"></div>     
		    </div>
		</div>  
	</div>
</body>
</html>    