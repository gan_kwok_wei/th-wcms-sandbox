<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
			
		$("#form_content").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$result = $this->cms_model->get_content($content_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/dashboard');
			exit;
		}
	}
	
	$category = $this->cms_model->get_parent_content();
	$language = $this->system_model->get_registered_lang();
	
	show_message();
	echo form_open('', 'id="form_content"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'cms_model.save_content');
	echo ($mode != 'add') ? form_hidden('content_id', $content_id) : '';
	echo form_hidden('content_category_id', ($mode != 'add') ? $data->content_category_id : $content_category_id);
	echo form_hidden('ref', 'system/pages/content_edit/' . $ref . (($mode != 'add') ? "/$content_id/" : "/"));
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $data->content_id; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_content_name = array(
								'name'        => 'content_name',
								'id'          => 'content_name',
								'value'       => ($mode == 'add') ? set_value('content_name') : $data->content_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_content_name); 
			?>
			</td>
			<td><?php echo lang('label_category'); ?></td>
			<td><strong><?php echo ($mode == 'add') ? $category[$content_category_id] : $category[$data->content_category_id]; ?></strong></td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
				<?php 
					foreach ($this->config->item('ge_language') as $lang_k => $lang_v) { 
						echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
					}
				?>					
					</ul>
				<?php 
					foreach ($this->config->item('ge_language') as $lang_k => $lang_v) {
						if($mode != 'add') {
							$result = $this->system_model->get_content_text('content', $content_id, $lang_v);
							if($result->num_rows() > 0) {
								$text = $result->row();
							} else {
								$text = new stdClass();
								$text->content_text_id = "0";
								$text->title = "";
								$text->icon = "";
								$text->image = "";
								$text->teaser = "";
								$text->content = "";
								$text->content_1 = "";
								$text->link = "";
								$text->country = "";
							}
							echo form_hidden("content_text_id[$lang_k]", $text->content_text_id); 
						}
						echo form_hidden("language[$lang_k]", $lang_v); 
				?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table>		
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => ($mode == 'add') ? set_value('title'): $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_icon_content'); ?></td>
								<td valign="bottom">
								<?php 
									if(($mode != 'add') && ($text->icon)) {
										echo '<img src="' . $text->icon . '" height="75"/> <br/>';
									}
									$field_icon = array(
													'name'        => "icon[$lang_k]",
													'id'          => "icon_$lang_k",
													'value'       => ($mode == 'add') ? set_value('icon'): $text->icon,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_icon); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
								</td>
								<td valign="top"><?php echo lang('label_image_content'); ?></td>
								<td valign="bottom">
								<?php  
									if(($mode != 'add') && ($text->image)) {
										echo '<img src="' . $text->image . '" height="100"/> <br/>';
									}
									$field_image = array(
													'name'        => "image[$lang_k]",
													'id'          => "image_$lang_k",
													'value'       => ($mode == 'add') ? set_value('image'): $text->image,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_image); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'image_<?php echo $lang_k; ?>' );" />
								</td>
							</tr>
							<tr>
								<td valign="top">Question</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("content[$lang_k]", ($mode == 'add') ? '' : $text->content); ?></td>
							</tr>
							<tr>
								<td valign="top">Answer</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("content_1[$lang_k]", ($mode == 'add') ? '' : $text->content_1); ?></td>
							</tr>
							<tr>
								<td><?php echo lang('label_link'); ?></td>
								<td colspan="3">
								<?php 
									$field_link = array(
													'name'        => "link[$lang_k]",
													'id'          => "link_$lang_k",
													'value'       => ($mode == 'add') ? set_value('link'): $text->link,
													'maxlength'   => '150',
													'size'        => '132'
												);
									echo form_input($field_link); 
								?>
								</td>
							</tr>
							<tr>
								<td><?php echo lang('label_country'); ?></td>
								<td colspan="3">
				<?php 
						if($mode == 'add') {
							foreach ($this->config->item('ge_country') as $country_k => $country_v) {
								echo form_checkbox("country[$lang_k][]", $country_k, TRUE) . ucwords($country_v) . "|&nbsp;&nbsp;";
							}
						} else {
							$country_array = explode('|', $text->country);
							foreach ($this->config->item('ge_country') as $country_k => $country_v) {
								echo form_checkbox("country[$lang_k][]", $country_k, (in_array($country_k, $country_array)) ? TRUE : FALSE) . ucwords($country_v) . "|&nbsp;&nbsp;";
							}	
						}
				?>
								</td>
							</tr>
						</table>
					</div>
				<?php 
					}
				?>				
				</div>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td><?php echo lang('label_tags'); ?></td>
			<td colspan="3">
			<?php 
				$field_tags = array(
								'name'        => 'tags',
								'id'          => 'tags',
								'value'       => ($mode == 'add') ? set_value('tags'): $data->tags,
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_tags); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0: $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_end,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3">
<?php 
		if(get_role() <= 3) {
			echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE);
		} else {
			echo $data_status[$data->status];
		}
?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/pages/' . $ref); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>

</div>