<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
			
		$("#form_content").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
	});
</script>
<div id="main">
<?php 
	$this->load->view('system/pages/help_info_menu', array('sub_about' => $sub_about));

	$result = $this->cms_model->get_content($content_id);
	if($result->num_rows() > 0) {
		$data = $result->row();
	} else {
		set_warning_message(lang('alert_undefined_data'));
		redirect('system/dashboard');
		exit;
	}
	
	$category = $this->cms_model->get_parent_content();
	$language = $this->system_model->get_registered_lang();
	
	show_message();
	echo form_open('', 'id="form_content"'); 
	echo form_hidden('mode', 'edit');
	echo form_hidden('action', 'cms_model.save_content');
	echo form_hidden('content_id', $content_id);
	echo form_hidden('content_name', $data->content_name);
	echo form_hidden('content_category_id', $data->content_category_id);
	echo form_hidden('tags', $data->tags);
	echo form_hidden('sort', $data->sort);
	echo form_hidden('ref', 'system/pages/help_info_contact_us');
?>
	<table width="100%">
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $data->content_id; ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?php echo lang('label_name'); ?></td>
			<td><strong><?php echo $data->content_name;	?></strong></td>
			<td><?php echo lang('label_category'); ?></td>
			<td><strong><?php echo $category[$data->content_category_id]; ?></strong></td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
				<?php 
					foreach ($this->config->item('ge_language') as $lang_k => $lang_v) { 
						echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
					}
				?>					
					</ul>
				<?php 
					foreach ($this->config->item('ge_language') as $lang_k => $lang_v) {
					
						$result = $this->system_model->get_content_text('content', $content_id, $lang_v);
						if($result->num_rows() > 0) {
							$text = $result->row();
						} else {
							$text = new stdClass();
							$text->content_text_id = "0";
							$text->title = "";
							$text->icon = "";
							$text->image = "";
							$text->teaser = "";
							$text->content = "";
							$text->icon_1 = "";
							$text->image_1 = "";
							$text->teaser_1 = "";
							$text->content_1 = "";
							$text->link = "";
							$text->country = "";
						}
						echo form_hidden("content_text_id[$lang_k]", $text->content_text_id); 
						echo form_hidden("icon[$lang_k]", $text->icon); 
						echo form_hidden("image[$lang_k]", $text->image); 
						echo form_hidden("teaser[$lang_k]", $text->teaser); 
						echo form_hidden("icon_1[$lang_k]", $text->icon_1); 
						echo form_hidden("image_1[$lang_k]", $text->image_1); 
						echo form_hidden("teaser_1[$lang_k]", $text->teaser_1); 
						echo form_hidden("content_1[$lang_k]", $text->content_1); 
						echo form_hidden("link[$lang_k]", $text->link); 
						echo form_hidden("country[$lang_k]", $text->country); 
						echo form_hidden("language[$lang_k]", $lang_v); 
				?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table>		
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_content_content'); ?></td>
								<td colspan="3"><?php echo $this->ckeditor->editor("content[$lang_k]", $text->content); ?></td>
							</tr>
							<tr>
								<td><?php echo lang('label_country'); ?></td>
								<td colspan="3">
				<?php 
						$country_array = explode('|', $text->country);
						foreach ($this->system_model->get_registered_country() as $country_k => $country_v) {
							echo form_checkbox("country[$lang_k][]", $country_k, (in_array($country_k, $country_array)) ? TRUE : FALSE) . ucwords($country_v) . "|&nbsp;&nbsp;";
						}	
				?>
								</td>
							</tr>
						</table>
					</div>
				<?php 
					}
				?>				
				</div>

			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => $data->publish_date_end,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	$create_by = $this->user_model->get_user($data->created_id)->row();
	$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3">
<?php 
	if(get_role() <= 3) {
		echo form_checkbox('status', '1', ($data->status == '0') ? FALSE : TRUE);
	} else {
		echo $data_status[$data->status];
	}
?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_hits'); ?></td>
						<td valign="top"><strong><?php echo $data->hits; ?></strong></td>
					</tr>
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
			<?php 
					echo form_submit('btnSave', lang('label_update'));
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/cms/content'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>