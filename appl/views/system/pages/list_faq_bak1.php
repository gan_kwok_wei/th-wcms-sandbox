<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript">
<!--
	$().ready(function() {
		oTable = $('#data_table').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
		$('#form_list').submit( function() { if(confirm("Save list?")) { return true; } else { return false; } });
		var asInitVals = new Array();
		$("thead input").keyup( function () { oTable.fnFilter( this.value, $("thead input").index(this) + 1 ); } );
		$("thead input").each( function (i) { asInitVals[i] = this.value; } );
		$("thead input").focus( function () { if( this.className == "search_init" ) { this.className = ""; this.value = ""; } } );
		$("thead input").blur( function (i) { if( this.value == "" ) { this.className = "search_init"; this.value = asInitVals[$("thead input").index(this)]; } } );
	});
//-->
</script>
<div id="main">
<?php 
	$this->load->view('system/pages/' . $menu, array('sub_about' => $sub_about));
	show_message();
	echo form_open('', 'id="form_list"'); 
	echo form_hidden('action', 'cms_model.save_list_content');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/pages/content_add/' . $ref . '/' . $content_category_id); ?>');"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_title" value="Search title" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
				<th width="60" class="ui-state-default"></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->cms_model->get_content_list($content_category_id);
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->content_id; ?></td>
				<td><a href="<?php echo base_url('system/pages/content_edit/' . $ref . '/' . $row->content_id); ?>" title="<?php echo $row->content_name; ?>"><?php echo $row->content_name; ?></a></td>
				<td><?php echo $data_status[$row->status]; ?></td>
				<td><?php echo form_input('sort[' . $row->content_id . ']', $row->sort, ' class="inline_text"') . '<span style="display:none;"> ' . $row->sort . '</span>'; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>