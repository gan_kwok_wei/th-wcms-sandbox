<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource about_us_menu.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 22, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */
?>
<div class="menu_red">
	<div class="menu_red_left f_left"></div>
	<div class="menu_red_center f_left">
		<div class="menu_red_content f_left"><h1>Help & Info</h1></div>
		<div class="menu_red_content2 f_left">
			<ul>
				<li>
					<a href="<?php echo base_url('system/pages/help_info_contact_us'); ?>" class="<?php echo ($sub_about == "contact_us" ? "selected" : ""); ?>">Contact Us</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/help_info_booking_cancelation'); ?>" class="<?php echo ($sub_about == "booking_cancelation" ? "selected" : ""); ?>">Booking & Cancelation</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/help_info_terms_condition'); ?>" class="<?php echo ($sub_about == "terms_condition" ? "selected" : ""); ?>">Terms & Condition</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/help_info_fee_hotel_schedule'); ?>" class="<?php echo ($sub_about == "fee_hotel_schedule" ? "selected" : ""); ?>">Fee & Hotel Schedule</a>
				</li>
				<li>
					<a href="<?php echo base_url('system/pages/help_info_faq'); ?>" class="<?php echo ($sub_about == "faq" ? "selected" : ""); ?>">FAQ</a>
				</li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
	<div class="menu_red_right f_left"></div>
	<div class="clear"></div>
</div>
<div class="clear"></div>


<?php 
/**
 * End of file about_us_menu.php 
 * Location: ./.../.../.../about_us_menu.php 
 */