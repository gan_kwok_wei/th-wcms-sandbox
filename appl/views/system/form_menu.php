<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$menu = $this->system_model->get_menu($menu_id);
		if($menu->num_rows() > 0) {
			$data = $menu->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/dashboard/menu');
			exit;
		}
	}
	
//	$category = $this->system_model->get_parent_menu();
//	$language = $this->system_model->get_registered_lang();
	$parent = $this->system_model->get_parent_menu();
	
	show_message();
	echo form_open('', 'id="form_menu"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'system_model.save_menu');
	echo ($mode != 'add') ? form_hidden('menu_id', $menu_id) : '';
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $data->menu_id; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_menu_name = array(
								'name'        => 'menu_name',
								'id'          => 'menu_name',
								'value'       => ($mode == 'add') ? set_value('menu_name') : $data->menu_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_menu_name); 
			?>
			</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="4"><div class="group_title"><?php echo lang('label_title'); ?>*</div></td>
		</tr>
<!-- *********************************************************************************** -->
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {

		if($mode != 'add') {
			$result = $this->system_model->get_menu_text($menu_id, $lang_v);
			if($result->num_rows() > 0) {
				$text = $result->row();
			} else {
				$text = new stdClass();
				$text->menu_text_id = "0";
				$text->title = "";
				$text->note = "";
			}
			echo form_hidden("menu_text_id[$lang_k]", $text->menu_text_id); 
		}
		echo form_hidden("language[$lang_k]", $lang_v);
?>
		<tr>
			<td><?php echo humanize($lang_v); ?></td>
			<td colspan="3">
				<?php 
					$field_title = array(
									'name'        => "title[$lang_k]",
									'id'          => "title_$lang_k",
									'value'       => ($mode == 'add') ? set_value("title[$lang_k]") : $text->title,
									'maxlength'   => '150',
									'size'        => '120'
								);
					echo form_input($field_title); 
					
				?>
			</td>
		</tr>
<?php 
	} 
?>
<!-- *********************************************************************************** -->
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td><?php echo lang('label_icon'); ?></td>
			<td>
			<?php 
				$field_icon = array(
								'name'        => 'icon',
								'id'          => 'icon',
								'value'       => ($mode == 'add') ? set_value('icon'): $data->icon,
								'maxlength'   => '150',
								'size'        => '32'
							);
				echo form_input($field_icon); 
			?><input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'icon' );" />
			</td>
			<td><?php echo lang('label_image'); ?></td>
			<td>
			<?php 
				$field_image = array(
								'name'        => 'image',
								'id'          => 'image',
								'value'       => ($mode == 'add') ? set_value('image'): $data->image,
								'maxlength'   => '150',
								'size'        => '32'
							);
				echo form_input($field_image); 
			?><input type="button" value="Browse Server" onclick="BrowseServer( 'images:/', 'image' );" />
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_link'); ?></td>
			<td colspan="3">
			<?php 
				$field_link = array(
								'name'        => 'link',
								'id'          => 'link',
								'value'       => ($mode == 'add') ? set_value('link'): $data->link,
								'maxlength'   => '150',
								'size'        => '132'
							);
				echo form_input($field_link); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_parent'); ?></td>
			<td>
			<?php 
				echo form_dropdown('parent_id', $parent, ($mode == 'add') ? 0: $data->parent_id);
			?>
			</td>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0: $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_note'); ?></td>
			<td colspan="3"><?php echo $this->ckeditor->editor("note", ($mode == 'add') ? '' : $data->note); ?></td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE); ?></td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="'.lang('label_delete').'" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/dashboard/menu'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>