<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];
	
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
/*			
		$("#form_hotel_room").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
*/		
	});

</script>
<div id="main">
<h2 id="content_title">Package Manager</h2>
<?php 
	show_message();
?>
	<table width="100%">
<!-- *************************************** -->

		<tr>
			<td colspan="4">
				<div style="text-align:right; border-bottom: 1px solid #999999;">
					<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/hotel/package_detail_add'); ?>');"/>
				</div>
<?php 
	$package_list = $this->hotel_model->get_package_list();
	if($package_list->num_rows() > 0) {
		$i = 0;
		foreach($package_list->result() as $package_data) {
?>
				<table width="100%" id="package_<?php echo $i; ?>" style="border: 1px dashed #cccccc;">
					<tr>
						<td valign="top" width="50%"><strong><?php echo $package_data->package_name; ?></strong></td>
						<td valign="top">
							
<?php 
			$package_feature_list = $this->hotel_model->get_package_feature($package_data->package_id);
			if($package_feature_list->num_rows() > 0) {
				echo '<table width="100%">';
				foreach($package_feature_list->result() as $package_feature) {
?>
								<tr>
									<td>
<?php 
					if($package_feature->icon) {
						echo '<img src="' . $package_feature->icon . '"><br/>';
					}
					echo $package_feature->feature_name;
?>
									</td>
								</tr>
<?php 
				}
?>
					
							</table>
<?php 
			}
?>
							<div style="text-align:right;">
								<input type="button" name="btnEdit" value="<?php echo lang('label_edit'); ?>" onclick="location.assign('<?php echo base_url('system/hotel/package_detail_edit/' . $package_data->package_id); ?>');"/>
							</div>
						</td>
					</tr>
				</table>
<?php 
			$i++;
		}
	} else { 
		echo '<center>No Package Available!</center>';
		
	}
?>
			</td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
	</table>
</div>