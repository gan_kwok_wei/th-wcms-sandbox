<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	
	show_message();
	echo form_open('', 'id="form_list"'); 
	echo form_hidden('action', 'promo_model.save_list_promo');
?>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/promo/promo_add'); ?>');"/>
		<input type="submit" name="btnSave" value="<?php echo lang('label_send'); ?>" />
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_hotel" value="Search Hotel" class="search_init" /></th>
				<th class="ui-state-default"><input type="text" name="search_Title" value="Search Title" class="search_init" /></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
				<th width="60" class="ui-state-default"></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Hotel</th>
				<th>Title</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->promo_model->get_promo_list();
//	echo $this->db->last_query();
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->promo_id; ?></td>
				<td><a href="<?php echo base_url('system/promo/promo_edit/' . $row->promo_id); ?>" title="<?php echo $row->promo_name; ?>"><?php echo $row->hotel_name; ?></a></td>
				<td><a href="<?php echo base_url('system/promo/promo_edit/' . $row->promo_id); ?>" title="<?php echo $row->promo_name; ?>"><?php echo $row->promo_name; ?></a></td>
				<td><?php echo $data_status[$row->status]; ?></td>
				<td><?php echo form_input('sort[' . $row->promo_id . ']', $row->sort, ' class="inline_text"') . '<span style="display:none;"> ' . $row->sort . '</span>'; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Hotel</th>
				<th>Title</th>
				<th>Status</th>
				<th>Sort</th>
			</tr>
		</tfoot>
	</table>
<?php 
	echo form_close();
?>
</div>