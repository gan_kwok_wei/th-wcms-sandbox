<!-- div id="breadcrumb">
	
</div-->
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
	<div style="text-align:right; padding-bottom:1em;">
		<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="location.assign('<?php echo base_url('system/promo/campaign_add'); ?>');"/>
	</div>
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="data_table">
		<thead>
			<tr>
				<th width="20" class="ui-state-default"></th>
				<th class="ui-state-default"><input type="text" name="search_title" value="Search Title" class="search_init" /></th>
				<th width="100" class="ui-state-default"></th>
				<th width="100" class="ui-state-default"></th>
				<th width="50" class="ui-state-default"><input type="text" name="search_status" value="Search status" class="search_init" /></th>
			</tr>
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Start</th>
				<th>End</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
<?php 
	$list = $this->promo_model->get_campaign_list();
//	echo $this->db->last_query();
	if($list->num_rows() > 0) {
		foreach($list->result() as $row) {
?>
			<tr>
				<td><?php echo $row->campaign_id; ?></td>
				<td><a href="<?php echo base_url('system/promo/campaign_edit/' . $row->campaign_id); ?>" title="<?php echo $row->campaign_name; ?>"><?php echo $row->campaign_name; ?></a></td>
				<td><?php echo $row->valid_from; ?></td>
				<td><?php echo $row->valid_to; ?></td>
				<td><?php echo $data_status[$row->status]; ?></td>
			</tr>
<?php 
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Start</th>
				<th>End</th>
				<th>Status</th>
			</tr>
		</tfoot>
	</table>
</div>