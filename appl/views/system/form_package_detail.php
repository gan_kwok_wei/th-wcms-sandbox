<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];

		featureTable = $('#feat_table').dataTable({"bJQueryUI": true, "bPaginate": false, "oLanguage": {"sSearch": "Search all columns:"}});
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
/*			
		$("#form_room").validate({
			rules: {"content_name":"required",
					"title":"required",
					"content_category_id":"required"},
			messages: {"content_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"content_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
*/		
		$( ".datepicker" ).datepicker({
			showOn: "button",
			buttonImage: "<?php echo ASSET_PATH; ?>themes/admin/<?php echo $this->system_model->get_admin_theme(); ?>/css/images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "yy-mm-dd"
		});
		
	});

</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	show_message();

	if($mode != 'add') {
		$result = $this->hotel_model->get_package($package_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
			if((get_role() > 3) && ($data->created_id != get_user_id())) {
	    		set_warning_message(lang('alert_no_permission'));
				redirect('system/hotel/package/');
				exit;
	    	}
		    
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/hotel/package');
			exit;
		}
	}
	
	echo form_open('', 'id="form_package"');
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'hotel_model.save_package');
	echo ($mode != 'add') ? form_hidden('package_id', $package_id) : '';
?>
	<table width="100%">
		<tr>
			<td>Package name *</td>
			<td colspan="3">
			<?php 
				$field_package_name = array(
								'name'        => "package_name",
								'id'          => "package_name",
								'value'       => ($mode == 'add') ? set_value('package_name') : $data->package_name,
								'maxlength'   => '150',
								'size'        => '120'
							);
				echo form_input($field_package_name); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td>
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0 : $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
			<td>Active</td>
			<td colspan="3"><?php echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : $data->status); ?> </td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4">
				<script type="text/javascript" charset="utf-8">
					$().ready(function() {
						$( "#lang_tabs" ).tabs();
					});
				</script>
				<div id="lang_tabs">
					<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
					</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		$result = $this->hotel_model->get_package_text($package_id, $lang_v);
//		echo $this->db->last_query();
		if($result->num_rows() > 0) {
			$text = $result->row();
		} else {
			$text = new stdClass();
			$text->package_text_id = "0";
			$text->title = "";
			$text->icon = "";
			$text->note = "";
		}
		echo form_hidden("language[$lang_k]", $lang_v);
		echo form_hidden("package_text_id[$lang_k]", $text->package_text_id); 
?>					
					<div id="tabs-<?php echo $lang_k; ?>">
						<table width="100%">
							<tr>
								<td><?php echo lang('label_title'); ?>*</td>
								<td colspan="3">
								<?php 
									$field_title = array(
													'name'        => "title[$lang_k]",
													'id'          => "title_$lang_k",
													'value'       => $text->title,
													'maxlength'   => '150',
													'size'        => '120'
												);
									echo form_input($field_title); 
								?>
								</td>
							</tr>
							<tr>
								<td valign="top"><?php echo lang('label_icon'); ?></td>
								<td valign="bottom">
								<?php 
									if($text->icon) {
										echo '<img src="' . $text->icon . '" height="75"/> <br/>';
									}
									$field_icon = array(
													'name'        => "icon[$lang_k]",
													'id'          => "icon_$lang_k",
													'value'       => $text->icon,
													'maxlength'   => '150',
													'size'        => '32'
												);
									echo form_input($field_icon); 
								?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td valign="top">Note</td>
								<td colspan="3"><?php echo $this->ckeditor->editor("note[$lang_k]", $text->note); ?></td>
							</tr>
						</table>
					</div>
<?php 
	}
?>
				</div>
			</td>
		</tr>
		
<!-- *************************************** -->

		<tr>
			<td colspan="4"><div class="group_title">Hotel Feature</div></td>
		</tr>
		<tr>
			<td colspan="4">
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="feat_table">
				<thead>
					<tr>
						<th width="60">ID</th>
						<th width="250">Feature type</th>
						<th>title</th>
						<th width="60">Available</th>
					</tr>
				</thead>
				<tbody>
<?php 
		$feature_list = $this->hotel_model->get_feature_ref_list(); 
		if($mode != 'add') {
			$package_feature = $this->hotel_model->get_array_package_feature($package_id);
		}
		
		if($feature_list->num_rows() > 0) {
			foreach($feature_list->result() as $row) {
				if($mode != 'add') {
					$check = (in_array($row->feature_ref_id, $package_feature));
				}
?>
					<tr>
						<td><?php echo $row->feature_ref_id; ?></td>
						<td><?php echo $row->feature_type; ?></td>
						<td><?php echo $row->feature_name; ?></td>
						<td><?php echo form_checkbox('feature_ref[' . $row->feature_ref_id .']', '1', (($mode != 'add') ? $check : FALSE)); ?></td>
					</tr>
<?php 
			}
		}
?>
				</tbody>
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Feature type</th>
						<th>title</th>
						<th>Available</th>
					</tr>
				</tfoot>
			</table>
			</td>
		</tr>
<?php 
	if($mode != 'add') {
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3">
<?php 
		if(get_role() <= 3) {
			echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE);
		} else {
			echo $data_status[$data->status];
		}
?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
		<tr>
			<td colspan="4">
				<?php 
					
					if($mode != 'add') {
						echo form_submit('btnSave', lang('label_update'));
					//	echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
					} else {
						
						echo form_submit('btnSave', lang('label_save'));
					}
				?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/hotel/package/'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>