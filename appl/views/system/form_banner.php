<!-- div id="breadcrumb">
	
</div-->
<?php 
	if($mode != 'add') {
		$banner = $this->cms_model->get_banner($banner_id);
		if($banner->num_rows() > 0) {
			$data = $banner->row();
			if((get_role() > 3) && ($data->created_id != get_user_id())) {
	    		set_warning_message(lang('alert_no_permission'));
				redirect('system/cms/banner');
				exit;
	    	}
	    	
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/cms/banner');
			exit;
		}
	}
	
	$category = $this->cms_model->get_parent_banner();
	$pages = $this->config->item('ge_page');
	$hotels = array('0' => '-ALL-') + array_slice($this->system_model->get_parent_hotel(), 1);
	$display_pos = array('' => '-Select position-', 
						'top' => 'Top main banner', 
						'left_1' => 'Left column baner', 
						'left_2' => 'Left 2 banner', 
						'mid_1' => 'Home whats hot banner', 
						'mid_2' => 'Home middle banner', 
						'right_1' => 'Right column baner', 
						'right_2' => 'Right 2 banner', 
						'bottom' => 'Home page bottom baner');
	
?>
<style>
<!--
	.hotel_options{
		display: none;
	}
-->
</style>
<script type="text/javascript" charset="utf-8">
	
	function BrowseServer( startupPath, functionData ) {
		var finder = new CKFinder();
	
		finder.basePath = '../../assets/js/ckfinder';
		finder.startupPath = startupPath;
		finder.selectActionFunction = SetFileField;
		finder.selectActionData = functionData;
		finder.popup();
	}
	
	function SetFileField( fileUrl, data ) {
		document.getElementById( data["selectActionData"] ).value = fileUrl;
	}

	$().ready(function() {
		var availableTags = [<?php echo $this->system_model->get_available_tags(); ?>];
		
		function split( val ) {
			return val.split( /;\s*/ );
		}
		
		function extractLast( term ) {
			return split( term ).pop();
		}

		$( "#lang_tabs" ).tabs();
		
		$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
					availableTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( "; " );
					return false;
				}
			});
		
		$("#form_banner").validate({
			rules: {"banner_name":"required",
					"title":"required",
					"banner_category_id":"required"},
			messages: {"banner_name": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"title": "<span class='alert'><?php echo lang('alert_required'); ?></span>",
						"banner_category_id": "<span class='alert'><?php echo lang('alert_no_selected'); ?></span>"}
		});
		
		$('#position').change(displayPositionChange);
		
	});

	function delete_display(id, display_id) {
		$('#display_' + id).remove();
		if(display_id != 0) {
			strDel = '<input type="hidden" name="banner_display_id[' + id + ']" value="' + display_id + '" >' +
					'<input type="hidden" name="page_id[' + id + ']" value="" >';
			$('#display_area').append(strDel);
		}
	}
	
//	galleryCount = 1;
	function addDisplay() {
		strDisplay = '<tr id="display_' + displayCount + '" style="border: 1px dashed #cccccc;">' +
'	<td>' +
'		<?php echo preg_replace('/\s+/', ' ', trim(form_dropdown('page_id[\' + displayCount + \']', $pages, array(), 'id="page_\' + displayCount + \'" onchange="pageChange(\' + displayCount + \')"'))); ?> ' +
'		<?php echo preg_replace('/\s+/', ' ', trim(form_dropdown('hotel_id[\' + displayCount + \']', $hotels, array(), 'id="hotel_\' + displayCount + \'" class="hotel_options"'))); ?>' +
'	</td>' +
'	<td class="pos_col">' +
'		<?php //echo preg_replace('/\s+/', ' ', trim(form_dropdown('display_pos[\' + displayCount + \']', $display_pos))); ?>' +
'		<input type="hidden" name="display_pos[' + displayCount + ']" value="' + $('#position option:selected').val() + '"><span>' + $('#position option:selected').text() + '</span> ' +
'	</td>' +
'	<td>' +
'		<input type="hidden" name="banner_display_id[' + displayCount + ']" value="0" />' +
'		<input type="button" value="Delete" onclick="delete_display(\'' + displayCount + '\', \'0\')">' +
'	</td>' +
'</tr>';
		$('#display_area').append(strDisplay);
		displayCount++;
	}

	function pageChange(pid) {
		if($('#page_' + pid).val() == 2) {
			$('#hotel_' + pid).show();
		} else {
			$('#hotel_' + pid).hide();
		}
	}
	
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 

	show_message();
	echo form_open('', 'id="form_banner"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'cms_model.save_banner');
	echo ($mode != 'add') ? form_hidden('banner_id', $banner_id) : '';
?>
	<table width="100%">
<?php 
	if($mode != 'add') { 
?>
		<tr>
			<td><?php echo lang('label_id'); ?></td>
			<td><?php echo $data->banner_id; ?></td>
			<td></td>
			<td></td>
		</tr>
<?php 
	}
?>
		<tr>
			<td>Position *</td>
			<td><?php echo form_dropdown('position', $display_pos, (($mode == 'add') ? set_value('display_pos') : $data->display_pos), 'id="position"'); ?></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><?php echo lang('label_name'); ?>*</td>
			<td>
			<?php 
				$field_banner_name = array(
								'name'        => 'banner_name',
								'id'          => 'banner_name',
								'value'       => ($mode == 'add') ? set_value('banner_name') : $data->banner_name,
								'maxlength'   => '50',
								'size'        => '50',
								'class'		=> 'k-input'
							);
				echo form_input($field_banner_name); 
			?>
			</td>
			<td><?php echo lang('label_category'); ?>*</td>
			<td><?php echo form_dropdown('banner_category_id', $category, ($mode == 'add') ? set_value('category') : $data->banner_category_id); ?></td>
		</tr>
<!-- *************************************** -->
		<tr>
			<td colspan="4">
			<?php 
				if($mode == 'add') {
					$this->load->view('system/text_banner', array('mode' => $mode, 'content_text_type' => 'banner'));
				} else {
					$this->load->view('system/text_banner', array('mode' => $mode, 'content_text_type' => 'banner', 'parent_id' => $banner_id));
				} 
			?>
			</td>
		</tr>
<!-- *************************************** -->	
		<tr class="display_pages">
			<td colspan="4"><div class="group_title">Display</div></td>
		</tr>
		<tr class="display_pages">
			<td colspan="4">
				<div style="text-align:right; border-bottom: 1px solid #999999;">
					<input type="button" name="btnAdd" value="<?php echo lang('label_add'); ?>" onclick="addDisplay()"/>
				</div>
				<table width="100%" id="display_area">
					<tr style="border-collapse:collapse; background: #cccccc;">
						<td width="50%"><strong>Page</strong></td>
						<td><strong>Position</strong></td>
						<td width="200"></td>
					</tr>
<?php 
		$display_list = $this->cms_model->get_banner_display_list($banner_id);
		if($display_list->num_rows() > 0) {
			$i = 0;
?>
					<script type="text/javascript">
						<?php echo "displayCount = " . ($display_list->num_rows() + 1) . ";" ?>

						
					</script>
<?php 
			foreach($display_list->result() as $display_data) {
				if($display_data->page_id == 2) {
					$check_page = '';
				} else {
					$check_page = ' class="hotel_options"';
				}
?>
					<tr id="display_<?php echo $i; ?>" style="border: 1px dashed #cccccc;">
						<td>
<?php 
				echo form_dropdown('page_id[' . $i . ']', $pages, (($mode == 'add') ? set_value('page_id[' . $i . ']') : $display_data->page_id), 'id="page_' . $i . '" onchange="pageChange(' . $i . ')"');
				echo form_dropdown('hotel_id[' . $i . ']', $hotels, (($mode == 'add') ? set_value('hotel_id[' . $i . ']') : $display_data->hotel_id), 'id="hotel_' . $i . '"' . $check_page);
?>
						</td>
						<td class="pos_col">
<?php 
				//echo form_dropdown('display_pos[' . $i . ']', $display_pos, ($mode == 'add') ? set_value('display_pos') : $display_data->display_pos);
				echo form_hidden('display_pos[' . $i . ']', ($mode == 'add') ? set_value('display_pos') : $display_data->display_pos);
				echo '<span>' . $display_pos[$display_data->display_pos] . '</span>';
?>
						</td>
						<td>
							<input type="hidden" name="banner_display_id[<?php echo $i; ?>]" value="<?php echo $display_data->banner_display_id; ?>" />
							<input type="button" value="Delete" onclick="delete_display('<?php echo $i; ?>', '<?php echo $display_data->banner_display_id; ?>')">
						</td>
					</tr>
<?php 
				$i++;
			}
		} else {
?>
					<script type="text/javascript">
						displayCount = 1;
					</script>
					<tr style="border: 1px dashed #cccccc;">
						<tr style="border: 1px dashed #cccccc;">
						<td>
							<?php echo form_dropdown('page_id[0]', $pages, 0, 'id="page_0" onchange="pageChange(0)"'); ?>
							<?php echo form_dropdown('hotel_id[0]', $hotels, 0, 'id="hotel_0" class="hotel_options"'); ?>
						</td>
						<td class="pos_col"><?php echo form_hidden('display_pos[0]', ''); ?><span></span></td>
						<td><input type="hidden" name="banner_display_id[0]" value="0" /></td>
					</tr>
<?php 
		}
?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>	
		<tr>
			<td><?php echo lang('label_sort'); ?></td>
			<td colspan="3">
			<?php 
				$field_sort = array(
								'name'        => 'sort',
								'id'          => 'sort',
								'value'       => ($mode == 'add') ? 0: $data->sort,
								'maxlength'   => '150',
								'size'        => '50'
							);
				echo form_input($field_sort); 
			?>
			</td>
		</tr>
		<tr>
			<td><?php echo lang('label_publish_start'); ?></td>
			<td>
			<?php 
				$field_publish_start = array(
								'name'        => 'publish_date_start',
								'id'          => 'publish_date_start',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_start,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_start); 
			?>
			</td>
			<td><?php echo lang('label_publish_end'); ?></td>
			<td>
			<?php 
				$field_publish_end = array(
								'name'        => 'publish_date_end',
								'id'          => 'publish_date_end',
								'value'       => ($mode == 'add') ? '0000-00-00 00:00' : $data->publish_date_end,
								'maxlength'   => '150',
								'size'        => '50',
								'class'	      => 'datetimepicker'
							);
				echo form_input($field_publish_end); 
			?>
			</td>
		</tr>
		<tr>
			<td colspan="4"><hr/></td>
		</tr>
<?php 
	if($mode != 'add') { 
		$create_by = $this->user_model->get_user($data->created_id)->row();
		$modified_by = !$data->modified_id ? FALSE : $this->user_model->get_user($data->modified_id)->row();
		
?>
		<tr>
			<td><?php echo lang('label_active'); ?></td>
			<td colspan="3">
<?php 
		if(get_role() <= 3) {
			echo form_checkbox('status', '1', ($mode == 'add') ? TRUE : ($data->status == '0') ? FALSE : TRUE);
		} else {
			echo $data_status[$data->status];
		}
?>
			</td>
		</tr>
		<tr>
			<td valign="top"><?php echo lang('label_info'); ?></td>
			<td colspan="3">
				<table width="100%">
					<tr>
						<td valign="top" width="30%"><?php echo lang('label_created_by'); ?></td>
						<td valign="top"><strong><?php echo $create_by->first_name . ' ' . $create_by->last_name; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_time_created'); ?></td>
						<td valign="top"><strong><?php echo $data->created_time; ?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_modified_by'); ?></td>
						<td valign="top"><strong><?php echo $modified_by ? $modified_by->first_name . ' ' . $modified_by->last_name : "" ;?></strong></td>
					</tr>
					<tr>
						<td valign="top"><?php echo lang('label_last_modified'); ?></td>
						<td valign="top"><strong><?php echo $data->modified_time; ?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
<?php 
	}
?>
		<tr>
			<td colspan="4">
			<?php 
				
				if($mode != 'add') {
					echo form_submit('btnSave', lang('label_update'));
//					echo '<input type="button" id="btnDelete" name="btnDelete" value="' . lang('label_delete') . '" />';
				} else {
					
					echo form_submit('btnSave', lang('label_save'));
				}
			?>
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/cms/banner'); ?>')" />
			</td>
		</tr>
	</table>
<?php 
	echo form_close(); 
?>
</div>