<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource content_text.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 9, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

?>
<script type="text/javascript" charset="utf-8">
	$().ready(function() {
		$( "#lang_tabs" ).tabs();
		
		$( "#lang_tabs" ).hide();
		$( ".fields_banner" ).hide();
		$( ".display_pages" ).hide();

		displayPositionChange();
		
	});

	function displayPositionChange() {

		$( "#lang_tabs" ).hide();
		$( ".fields_banner" ).hide();
		$( ".display_pages" ).hide();

		pos = $('#position option:selected').val();
		if(pos != "") {
			switch(pos) {
			case 'top' :
				$("#lang_tabs" ).show();
				$(".fields_top" ).show();
				$("").each(function() {  });
				break;
			case 'left_1' :
				$("#lang_tabs" ).show();
				$(".fields_left_1" ).show();
				
				break;
			case 'left_2' :
				$("#lang_tabs" ).show();
				$(".fields_left_2" ).show();
				
				break;
			case 'mid_1' :
				$("#lang_tabs" ).show();
				$(".fields_mid_1" ).show();
				
				break;
			case 'mid_2' :
				$("#lang_tabs" ).show();
				$(".fields_mid_2" ).show();
				
				break;
			case 'right_1' :
				$("#lang_tabs" ).show();
				$(".fields_right_1" ).show();
				
				break;
			case 'right_2' :
				$("#lang_tabs" ).show();
				$(".fields_right_2" ).show();
				
				break;
			case 'bottom' :
				$("#lang_tabs" ).show();
				$(".fields_bottom" ).show();
				
				break;
			}
			$(".pos_col input").each(function() { $(this).val(pos); });
			$(".pos_col span").each(function() { $(this).html($('#position option:selected').text()); });
			
			$( ".display_pages" ).show();
		}
	}
	
</script>
<div id="lang_tabs">
	<ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) { 
		echo '<li><a href="#tabs-' . $lang_k . '">' . ucfirst($lang_v) . '</a></li>';
	}
?>					
	</ul>
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {
		if($mode != 'add') {
			$result = $this->system_model->get_content_text($content_text_type, $parent_id, $lang_v);
			if($result->num_rows() > 0) {
				$text = $result->row();
			} else {
				$text = new stdClass();
				$text->content_text_id = "0";
				$text->title = "";
				$text->icon = "";
				$text->image = "";
				$text->teaser = "";
				$text->content = "";
				$text->icon_1 = "";
				$text->image_1 = "";
				$text->teaser_1 = "";
				$text->content_1 = "";
				$text->link = "";
				$text->country = "";
			}
			echo form_hidden("content_text_id[$lang_k]", $text->content_text_id); 
		}
		echo form_hidden("language[$lang_k]", $lang_v); 
?>					
	<div id="tabs-<?php echo $lang_k; ?>">
		<table>		
			<tr>
				<td><?php echo lang('label_title'); ?>*</td>
				<td>
				<?php 
					$field_title = array(
									'name'        => "title[$lang_k]",
									'id'          => "title_$lang_k",
									'value'       => ($mode == 'add') ? set_value('title'): $text->title,
									'maxlength'   => '150',
									'size'        => '120'
								);
					echo form_input($field_title); 
				?>
				</td>
			</tr>
			<tr class="fields_banner fields_top">
				<td valign="top"><span>Icon (31px X 33px)</span></td>
				<td valign="bottom">
				<?php 
					if(($mode != 'add') && ($text->icon)) {
						echo '<img src="' . $text->icon . '" height="75"/> <br/>';
					}
					$field_icon = array(
									'name'        => "icon[$lang_k]",
									'id'          => "icon_$lang_k",
									'value'       => ($mode == 'add') ? set_value('icon'): $text->icon,
									'maxlength'   => '150',
									'size'        => '32'
								);
					echo form_input($field_icon); 
				?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'icon_<?php echo $lang_k; ?>' );" />
				</td>
			</tr>
			<tr>
				<td valign="top">
					Image
					<span class="fields_banner fields_top">(980px X 487px)</span>
					<span class="fields_banner fields_mid_1">(316px X 204px)</span>
					<span class="fields_banner fields_mid_2 fields_left_1  fields_left_2">(316px X 178px)</span>
					<span class="fields_banner fields_right_1 fields_right_2">(232px X 223px)</span>
					<span class="fields_banner fields_bottom">(162px X 85px)</span>
				</td>
				<td valign="bottom">
				<?php  
					if(($mode != 'add') && ($text->image)) {
						echo '<img src="' . $text->image . '" height="100"/> <br/>';
					}
					$field_image = array(
									'name'        => "image[$lang_k]",
									'id'          => "image_$lang_k",
									'value'       => ($mode == 'add') ? set_value('image'): $text->image,
									'maxlength'   => '150',
									'size'        => '32'
								);
					echo form_input($field_image); 
				?><input type="button" value="Browse Server" onclick="BrowseServer( 'Files:/images', 'image_<?php echo $lang_k; ?>' );" />
				</td>
			</tr>
			<tr class="fields_banner fields_right_1  fields_right_2">
				<td valign="top"><?php echo lang('label_teaser_' . $content_text_type); ?></td>
				<td><?php echo $this->ckeditor->editor("teaser[$lang_k]", ($mode == 'add') ? '' : $text->teaser); ?></td>
			</tr>
			<tr>
				<td><?php echo lang('label_link'); ?></td>
				<td colspan="3">
				<?php 
					$field_link = array(
									'name'        => "link[$lang_k]",
									'id'          => "link_$lang_k",
									'value'       => ($mode == 'add') ? set_value('link'): $text->link,
									'maxlength'   => '150',
									'size'        => '132'
								);
					echo form_input($field_link); 
				?>
				</td>
			</tr>
			<tr>
				<td><?php echo lang('label_country'); ?></td>
				<td>
<?php 
		if($mode == 'add') {
			foreach ($this->system_model->get_registered_country() as $country_k => $country_v) {
				echo form_checkbox("country[$lang_k][]", $country_k, TRUE) . ucwords($country_v) . "|&nbsp;&nbsp;";
			}
		} else {
			$country_array = explode('|', $text->country);
			foreach ($this->system_model->get_registered_country() as $country_k => $country_v) {
				echo form_checkbox("country[$lang_k][]", $country_k, (in_array($country_k, $country_array)) ? TRUE : FALSE) . ucwords($country_v) . "|&nbsp;&nbsp;";
			}	
		}
?>
				</td>
			</tr>
		</table>
	</div>
<?php 
	}
?>				
</div>


<?php 
/**
 * End of file content_text.php 
 * Location: ./.../.../.../content_text.php 
 */