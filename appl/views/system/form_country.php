<!-- div id="breadcrumb">
	
</div-->
<script type="text/javascript" charset="utf-8">
	$().ready(function() {
<?php 
	if($mode != 'add') {
?>
		languageTable = $('#language_table').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
		x_languageTable = $('#x_language_table').dataTable({"bJQueryUI": true, "sPaginationType": "full_numbers", "oLanguage": {"sSearch": "Search all columns:"}});
<?php 
	}
?>			
	});
</script>
<div id="main">
<h2 id="content_title"><?php echo $title; ?></h2>
<?php 
	if($mode != 'add') {
		$result = $this->system_model->get_country($country_id);
		if($result->num_rows() > 0) {
			$data = $result->row();
		} else {
			set_warning_message(lang('alert_undefined_data'));
			redirect('system/dashboard/country');
			exit;
		}
	}

	$x_country = $this->system_model->get_x_country();
	
	show_message();
?>
	<table width="100%">
<?php
	echo form_open('', 'id="form_country"'); 
	echo form_hidden('mode', $mode);
	echo form_hidden('action', 'system_model.save_country');
	echo ($mode != 'add') ? form_hidden('country_id', $country_id) : '';
?>
		<tr>
			<td colspan="2">
				<table width="100%">
					<tr>
						<td>Country</td>
						<td><?php echo form_dropdown('x_country_id', $x_country); ?></td>
					</tr>
<?php if($mode != 'add') { ?>					
					<tr>
						<td><?php echo lang('label_name'); ?></td>
						<td><strong><?php echo $data->letter_code . '/' . $data->name; ?></strong></td>
					</tr>
<?php } ?>
					<tr>
						<td colspan="2"><div class="group_title"><?php echo lang('label_title'); ?>*</div></td>
					</tr>
<!-- *********************************************************************************** -->
<?php 
	foreach ($this->system_model->get_registered_lang() as $lang_k => $lang_v) {

		if($mode != 'add') {
			$result = $this->system_model->get_country_text($country_id, $lang_v);
			if($result->num_rows() > 0) {
				$text = $result->row();
			} else {
				$text = new stdClass();
				$text->country_text_id = "0";
				$text->title = "";
				$text->note = "";
			}
			echo form_hidden("country_text_id[$lang_k]", $text->country_text_id); 
		}
		echo form_hidden("language[$lang_k]", $lang_v);
?>
					<tr>
						<td><?php echo humanize($lang_v); ?></td>
						<td>
							<?php 
								$field_title = array(
												'name'        => "title[$lang_k]",
												'id'          => "title_$lang_k",
												'value'       => ($mode == 'add') ? set_value("title[$lang_k]") : $text->title,
												'maxlength'   => '150',
												'size'        => '120'
											);
								echo form_input($field_title); 
								
							?>
						</td>
					</tr>
<?php 
	} 
?>
<!-- *********************************************************************************** -->
	
				</table>
			</td>
		</tr>	
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<tr>
			<td colspan="2">
<?php 
				
	if($mode != 'add') {
		echo form_submit('btnSave', lang('label_update'));
		echo '<input type="button" id="btnDelete" name="btnDelete" value="'.lang('label_delete').'" />';
	} else {
		
		echo form_submit('btnSave', lang('label_save'));
	}
?>				
				<input type="button" id="btnBack" value="<?php echo lang('label_back'); ?>" onclick="location.replace('<?php echo base_url('system/dashboard/country'); ?>')" />
			</td>
		</tr>
		
<?php 
	echo form_close();
	if($mode != 'add') {
?>		
		<tr>
			<td colspan="2"><div class="group_title">Language</div></td>
		</tr>
		<tr>
			<td valign="top">
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="x_language_table">
					<thead>
						<tr>
							<th width="50">ID</th>
							<th>Name</th>
						</tr>
					</thead>
					<tbody>
<?php 
		$x_language = $this->system_model->get_x_language($country_id);
		if($x_language->num_rows() > 0) {
			foreach($x_language->result() as $row) {
?>
						<tr>
							<td><a href="<?php echo base_url() . "system/dashboard/country_add_language/$country_id/" . $row->key; ?>" title="Add this language" ><?php echo $row->key; ?></a></td>
							<td><a href="<?php echo base_url() . "system/dashboard/country_add_language/$country_id/" . $row->key; ?>" title="Add this language" ><?php echo $row->val; ?></a></td>
						</tr>
<?php 
			}
		}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>ID</th>
							<th>Name</th>
						</tr>
					</tfoot>
				</table>
			</td>
			<td valign="top">
<?php 
		echo form_open('', 'id="form_country_language"'); 
		echo form_hidden('action', 'system_model.update_country_language');
		echo form_hidden('country_id', $country_id);
?>
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="language_table">
					<thead>
						<tr>
							<th width="50">ID</th>
							<th>Name</th>
							<th>Default</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<?php 
		$language = $this->system_model->get_country_language($country_id);
		if($language->num_rows() > 0) {
			foreach($language->result() as $row) {
?>
						<tr>
							<td><?php echo $row->key; ?></td>
							<td><?php echo $row->val; ?></td>
							<td><?php echo form_radio('default', $row->key, $row->default); ?></td>
							<td><a href="<?php echo base_url() . "system/dashboard/country_remove_language/$country_id/" . $row->key; ?>" title="Remove this language" >Remove</a></td>
						</tr>
<?php 
			}
		}
?>
					</tbody>
					<tfoot>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Default</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
<?php 
	if($language->num_rows() > 0) {
		echo form_submit('btnSave', 'Update country language');
	}
	echo form_close();
?>	
			</td>
		</tr>
<?php 
	}
?>
	</table>
	
<script type="text/javascript">
<?php 
	if($mode != 'add') {
?>
	$('#btnDelete').click(function() {	
		if(confirm("<?php echo lang('conf_delete') . $country_id; ?>")){
			$.ajax({
				type: "POST",
				url: "<?php echo str_replace($this->config->item('url_suffix'), "", site_url("system/dashboard/ajax_handler")); ?>",
				data: "country_id=<?php echo $country_id ?>&mode=delete&action=system_model.delete_country",
				success: function(data){//alert(typeof(data));
					eval('var data=' + data);
					if(typeof(data.error) != 'undefined') {
						if(data.error != '') {
							alert(data.error);
						} else {
							alert(data.msg);
							location.assign('<?php echo str_replace($this->config->item('url_suffix'), "", site_url('system/dashboard/country/')); ?>');
						}
					} else {
						alert('Data transfer error!');
					}
				}
			});  
		}
	});
<?php 	
	}
?>
</script>
</div>
