	<div class="row">
		<div class="twelve">
			<div class="hr">
			</div>
		</div>
	</div>
	
	<!-- START FOOTER CONTAINER -->
	<div class="footer bgc-grayDark " id="th-footer">
		<div class="row six clearfix"></div>
		<div class="row">
			<div class="footer_content">
				<div class="two clearfix">
					<div class="col1 first" style="width: 490px;">
						<div class="footer_content_left1 f_left">
							<h1><a href="<?php echo base_url('hotel/');?>" style="color: #E91126;"><?php echo lang('footer_our_hotel');?></a></h1>
							<table>
								<tr>
									<td>
<?php
	
	$x = 1;
	foreach($country_list as $row){
		if($x == 4){ echo '</td><td>'; }
		echo '<div class="foot_list_hotel "><a href="' . base_url('hotel/hotels/' . $row['country_id'] . '/' . underscore($row['name'])) . '" ><h2>' . $row['name'] . '</h2></a> ';
		
		if(count($row['hotel_list']) > 0){
			$i = 1;
			foreach($row['hotel_list'] as $hotel) {
				echo ($i != 1 ? ' / ' : '');
				echo '<a href="' . base_url('our-hotel/' . underscore($hotel['name'])) . '">' . $hotel['title'] . '</a>';
				$i++;
			}

		}
		echo '</div>';
		$x ++;
	}
?>
									</td>
								</tr>
							</table>
						</div>
					</div>
	
					<div class="footer_content">
						<div class="col1">
							<div class="footer_content_right1 f_left">
								<div class="foot_list_hotel f_left">
									<div class="foot_list_hotel_isi">
										<h1><a href="<?php echo base_url('about_us/');?>" style="color: #E91126;"><?php echo lang('footer_about_us');?></a></h1>
										<span>
<?php 
	if(isset($sub_menus['about_us'])) {
		foreach ($sub_menus['about_us'] as $sub_menu) {
?>
											<a href="<?php echo base_url($sub_menu->link); ?>"><?php echo (($sub_menu->text_title) ? $sub_menu->text_title : $sub_menu->def_title); ?></a> /
											
<?php 
		}
	}
?>
										</span>
									</div>
									<div class="foot_list_hotel_isi">
										<h1><a href="<?php echo base_url('help_info/');?>" style="color: #E91126;"><?php echo lang('footer_help_info');?></a></h1>
										<span>
<?php 
	if(isset($sub_menus['help_info'])) {
		foreach ($sub_menus['help_info'] as $sub_menu) {
?>
											<a href="<?php echo base_url($sub_menu->link); ?>"><?php echo (($sub_menu->text_title) ? $sub_menu->text_title : $sub_menu->def_title); ?></a> /
											
<?php 
		}
	}
?>
										</span>
									</div>
									<div class="foot_list_hotel_isi">
										<h1><a href="<?php echo base_url('promotion/');?>" style="color: #E91126;"><?php echo lang('footer_promotion');?></a></h1>
										<span>
<?php 
	if(isset($sub_menus['promotion'])) {
		foreach ($sub_menus['promotion'] as $sub_menu) {
?>
											<a href="<?php echo base_url($sub_menu->link); ?>"><?php echo (($sub_menu->text_title) ? $sub_menu->text_title : $sub_menu->def_title); ?></a> /
											
<?php 
		}
	}
?>
										</span>
									</div>
								</div>
								<div class="foot_list_hotel f_left">
									<div class="foot_list_hotel_isi">
										<h1><a href="<?php echo base_url('corporate/');?>" style="color: #E91126;"><?php echo lang('footer_corporate');?></a></h1>
<?php 
	if(isset($sub_menus['corporate'])) {
		foreach ($sub_menus['corporate'] as $sub_menu) {
?>
											<a href="<?php echo base_url($sub_menu->link); ?>"><?php echo (($sub_menu->text_title) ? $sub_menu->text_title : $sub_menu->def_title); ?></a> /
											
<?php 
		}
	}
?>
									</div>
									<div class="foot_list_hotel_isi" style="width:250px;">
										<h1><?php echo lang('footer_follow_us');?></h1>
										<div class="follow_media f_left">
											<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/fb_icon.png" class="f_left"/>
											<h3>Facebook</h3>
											<h4>Like</h4>
										</div>
		
										<div class="follow_media f_left">
											<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/pin_icon.png" class="f_left"/>
											<h3>Pinterest</h3>
											<h4>Pinned</h4>
										</div>
		
										<div class="follow_media f_left">
											<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/tw_icon.png" class="f_left"/>
											<h3>Twitter</h3>
											<h4>Follow</h4>
										</div>
										<div class="follow_media f_left">
											<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/yt_icon.png" class="f_left"/>
											<h3>Youtube</h3>
											<h4>Subscribe</h4>
										</div>
									</div>
								</div>
							</div>
		
						</div>
					</div>
				</div>
				<div class="clean"></div>
				<hr class="hr_one"/>
				<div class="footer_content">
					<div class="footer_content_left2 f_left">
						<h5><?php echo lang('footer_secure_by');?></h5>
						<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/secure_by.png" />
					</div>
					<div class="footer_content_right2 f_left">
						<h5><?php echo lang('footer_our_partner');?></h5>
						<img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/our_partner.png" />
					</div>
					<div class="clean"></div>
				</div>
	
				<hr class="hr_one"/>
				<div class="footer_content">
					<div class="footer_text">
						<?php echo lang('footer_text');?>
					</div>
				</div>
				<div class="clean"></div>
			</div>
		</div>
	</div>
	<!-- END FOOTER CONTAINER -->
	
	<script type="text/javascript">
		var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>

</body>
</html>