<div id="container" style="padding-top:30px;">
	<div class="container clearfix">
		<div class="four clearfix">

			<div class="col3 first left">
				<div id="about_content_detail_container">
					<?php
						$this->load->view('component/' . $sub_about);
					?>
				</div>
			</div>

			<div class="col1">

			<!--menu-->
			<div class="light-grey round clearfix" style="border: 1px solid #EAEAEA; -webkit-border-radius: 8px; -moz-border-radius: 8px; -ms-border-radius: 8px; -o-border-radius: 8px; border-radius: 8px; padding: 8px;">

									<ul class="menu inner"  id="sidebar-about" >
										<li style="padding:4px 0; text-transform:uppercase; font-style:bold;">
											<a href="<?php echo base_url('about_us/management_team');?>" class="<?php echo ($sub_about == "management_team" ? "selected" : "");?>"><?php echo lang('about_management_team');?></a>
										</li>

										<li style="padding:4px 0; text-transform:uppercase; font-style:bold;">
											<a href="<?php echo base_url('about_us/career');?>" class="<?php echo ($sub_about == "career" ? "selected" : "");?>"><?php echo lang('about_career');?></a>
										</li>
										<li style="padding:4px 0; text-transform:uppercase; font-style:bold;">
											<a href="<?php echo base_url('about_us/concept');?>" class="<?php echo ($sub_about == "concept" ? "selected" : "");?>"><?php echo lang('about_concept');?></a>
										</li>
									</ul>

			</div>	
			<div class="gutter"></div>				
			<!--menu-->	
				<div class="light-grey round clearfix">
				<?php
						$this->load->view('component/contact_us_medium.php');
					?>

					<?php
						$this->output_data['page_id'] = 5;
						$this->output_data['position1'] = 'left_1';
						$this->output_data['position2'] = 'left_2';
						$this->load->view('component/image_promo_medium.php', $this->output_data);
					?>
				</div>
				<div class="gutter"></div>
				
			</div>
		</div>
	</div>
					

</div>
<!-- end body container -->