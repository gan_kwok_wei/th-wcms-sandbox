<div class="lcs-main-wrap">
	<!--start /fwslider -->
	<div class="hotel-slider" style="height:420px;overflow:hidden;background-color:#191919;">
		<div class="container clearfix">
			<div id="booker" class="bookform round" style="overflow: hidden;">
<?php 
	switch ($this->session->get_dec('lang')) {
		case 'id':
			$book_lang = "ID";
		break;
		case 'ch':
			$book_lang = "CN";
		break;
		case 'th':
			$book_lang = "TH";
		break;
		default:
			$book_lang = "GB";
		break;
	}
?>
				<iframe src="http://demo.ntest.com/TuneHotels/Booking/TUNEBookME2.asp?Lang=<?php echo $book_lang; ?>" width="320" height="380" style="overflow: hidden;" scrolling="no"></iframe>
			</div>
		</div>
		<div id="trace">
<?php
	$this->output_data['page_id'] = 0;
//	$this->load->view('component/home-billboard-02', $this->output_data);
	$this->load->view('component/big_slider', $this->output_data);
?>
		</div>
	</div>

</div>
<div class="gutter"></div>
<div class="container clearfix" id="container">
	<div class="six clearfix">
		<div class="col2 first">
			<?php $this->load->view('component/top_destination'); ?>
		</div>
		<div class="col2">
			<!-- start whats hot -->
			<div style="display:block; margin-bottom:16px;">
<?php
	$this->output_data['page_id'] = 0;
	$this->load->view('component/whats_hot', $this->output_data);
?>
			</div>
			<div class="clear"></div>
			<div style="display:block; margin-bottom:16px;">
<?php
	$this->output_data['page_id'] = 0;
	$this->output_data['position'] = 'mid_2';
	$this->load->view('component/image_promo1', $this->output_data);
?>
			</div>
			<!-- end promo -->
		
		</div>
		<div class="col2">
			<!-- start social -->
			<div class="social_container bgc-lightGrey border-color-grey round">
				<?php $this->load->view('component/social_follow'); ?>
			</div>
			<!--end social-->
			<!-- start newsletter -->
			<div class="newsletter_container bgc-lightGrey box-shade round" style="margin-top:20px;">
				<?php $this->load->view('component/form_newsletter'); ?>
			</div>
			<!--end newsletter-->
		</div>
	
	</div>
	<div class="gutter"></div>
</div>
<!--<div class="social-links">
  <div class="container">
    <div class="row">
      <div class="span12">
        <p class="big"><span>Follow Us On</span> <a href="#"><i class="icon-facebook"></i>Facebook</a> <a href="#"><i class="icon-twitter"></i>Twitter</a> <a href="#"><i class="icon-google-plus"></i>Google Plus</a> <a href="#"><i class="icon-linkedin"></i>LinkedIn</a></p>
      </div>
    </div>    
  </div>
</div>-->