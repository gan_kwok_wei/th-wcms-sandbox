<?php 
define('ASSET_PATH', base_url().'assets/');
if(!isset($style_extras)) { $style_extras = array(); }
	if(!isset($js_extras)) { $js_extras = array(); }
	
	$ge_page = $this->config->item('ge_page');

?>

		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/style.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/page_hotel.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/font.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/flight-calendar.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/css/uni_form.css" />
		
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
		
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.tools.min.js"></script>
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.ddslick.js"></script>
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.uniform.js"></script>
		<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery.dateFormat-1.0.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
		
		<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
		<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
		
		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "ur-bc6e7663-6ee9-a9fc-9231-1c3420c75ce3"});</script>		
<script>
	$(document).ready(function(){
		$('error_span').hide();
		
		 $( "#fields_dob" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "1920:2003",
			dateFormat: "dd/mm/yy"
		}); 
		
	});

	
	function send_newsletter_form(){
		$('.input_error').addClass('input_light');
		$('.input_error').removeClass('input_error');
		$('.error_span').hide();
		
		var news_email = $('input[name="fields_email"]').val();
		var news_fname = $('input[name="fields_fname"]').val();
		var news_lname = $('input[name="fields_lname"]').val();
		var news_gender = $('input[name="fields_gender"]:checked').val();
		var news_country_id = $('select[name="fields_country"]').val();
		var news_birth = $('input[name="fields_dob"]').val();
		var news_phone = $('input[name="fields_phone"]').val();
		
		var next = '1';
		
		if($.trim(news_email) == ""){
			$('input[name="fields_email"]').addClass('input_error');
			$('input[name="fields_email"]').removeClass('input_light');
			$('.error_email').show();
			var next = '0';
		}
		if($.trim(news_fname) == ""){
			$('input[name="fields_fname"]').addClass('input_error');
			$('input[name="fields_fname"]').removeClass('input_light');
			$('.error_name').show();
			var next = '0';
		}
		if($.trim(news_gender) == ""){
			$('.error_gender').show();
			var next = '0';
		}
		if($.trim(news_country_id) == ""){
			$('.error_hotel_news').show();
			var next = '0';
		}
		if($.trim(news_birth) == ""){
			$('.error_hotel_dob').show();
			var next = '0';
		}
		
		if(next == '1'){
			return true;
		} else {
			return false;
		}
	}
	
	
</script>

<div class="e_news_later_sign_up" align="center">
	<div class="box_content">
		<form method=post action="https://app.icontact.com/icp/signup.php" name="icpsignup" id="icpsignup264" accept-charset="UTF-8" onsubmit="return send_newsletter_form();" >
			<input type=hidden name=redirect value="<?php echo base_url('help_info/news_sign_up_response/ok'); ?>" />
			<input type=hidden name=errorredirect value="<?php echo base_url('help_info/news_sign_up_response/failed'); ?>" />
			
			<input type=hidden name="listid" value="14623">
			<input type=hidden name="specialid:14623" value="D2H8">
			<input type=hidden name=clientid value="773175">
			<input type=hidden name=formid value="264">
			<input type=hidden name=reallistid value="1">
			<input type=hidden name=doubleopt value="1">	
			
			<table style="font-size:12px;">
				<tr>
					<td style="width:140px;"><?php echo lang('e_news_email');?>: <span class="red">*</span></td>
					<td>
						<input type="text" id="fields_email" name="fields_email" class="input_light" style="width:280px;" />
						<span class="span_input_error error_span error_email" style="display:none;">
							<?php echo lang('false_field_email'); ?>
						</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_fname');?>: <span class="red">*</span></td>
					<td>
						<input type="text" id="fields_fname" name="fields_fname" class="input_light" style="width:280px;" />
						<span class="span_input_error error_span error_name" style="display:none;">
							<?php echo lang('false_field_name'); ?>
						</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_lname');?>:</td>
					<td>
						<input type="text" id="fields_lname" name="fields_lname" class="input_light" style="width:280px;" />
					<td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('e_news_gender');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;">
						<input type="radio" id="options:gender:M" name="fields_gender" value="M" checked="checked" /> <label for="options:gender:M"><?php echo lang('e_news_gender_m');?></label> 
						<input type="radio" id="options:gender:F" name="fields_gender" value="F" /> <label for="options:gender:F"><?php echo lang('e_news_gender_f');?></label>
						<span class="span_input_error error_span error_gender" style="display:none;">
							<?php echo lang('false_field'); ?>
						</span>
					<td>
				</tr>
				<tr>
					<td height="34" style="vertical-align:middle;"><?php echo lang('corp_country');?>: <span class="red">*</span></td>
					<td height="34" style="vertical-align:middle;">
						<select class="myselect" id="fields_country" name="fields_country" style="width:150px;">
							<option value=""><?php echo lang('corp_sel_country');?></option>
<?php
	$countries = array('MY' => 'Malaysia',
                  'ID' => 'Indonesia',
                  'GB' => 'United Kingdom',
                  'CH' => 'China',
                  'SG' => 'Singapore',
                  'AU' => 'Australia',
                  'HK' => 'Hong Kong',
                  'TH' => 'Thailand',
                  'IN' => 'India',
                  'OA' => 'Other Asia Pacific Countries',
                  'OE' => 'Other European Countries',
                  'OT' => 'Other Continents',);
	foreach($countries as $k => $v){
		echo '<option value="' . $k . '">' . $v . '</option>';
	}
?>
						</select>
						<span class="span_input_error error_span error_hotel_news" style="display:none;">
							<?php echo lang('false_field'); ?>
						</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_birth');?>: <span class="red">*</span></td>
					<td>
						<input type="text" id="fields_dob" name="fields_dob" class="date_clean datepicker" style="" />
						<span class="span_input_error error_span error_hotel_dob" style="display:none;">
							<?php echo lang('false_field_date'); ?>
						</span>
					<td>
				</tr>
				<tr>
					<td><?php echo lang('e_news_phone');?>:</td>
					<td>
						<input type="text" id="fields_phone" name="fields_phone" class="input_light" style="width:280px;" />
						<input type=text name="fields_channel" value="website" style="display: none" readonly="readonly">
					<td>
				</tr>
				<tr>
					<td>
						<br/><br/>
						<input id="btn_submit" class="btn_see_detail btn_book_box f_left" type="submit" value="<?php echo lang('sign_up');?>" > <span style="display:none;" class="loading_img f_left"><img  src="/tune_media/images/img/loader.gif" /> Sending Data...</span>
						<div class="clean"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>