
<div class="clear">

	<div class="hr"></div>

</div>
<!-- START FOOTER CONTAINER -->

<div id="th-footer">

<div class="six body-wrap clearfix">




				<div class="sitemap-wrap col3 first">
						<?php
					$query_country = $this->pages_model->get_country();

					$x = 1;
					foreach($query_country->result() as $data_country){
						if($x == 1){
							//echo '</li><li>';
						}
						echo '<ul>
								<a href="' . base_url('hotel/hotels/' . $data_country->country_id . '/' . underscore(strtolower($data_country->name))) . '" ><li class="list-head">' . (($data_country->text_title) ? $data_country->text_title : $data_country->def_title) . '</li></a> ';
							$query_hotel_by_country = $this->pages_model->get_hotel_by_country($data_country->country_id);
							if($query_hotel_by_country->num_rows() > 0){

								$i = 1;
								foreach($query_hotel_by_country->result() as $data_hotel_by_country){
									echo ($i != 0 ? '<li>' : '');
									echo '<a href="' . base_url('hotel/hotel_detail/') . $data_hotel_by_country->hotel_id . "/" . underscore(strtolower($data_hotel_by_country->hotel_name)) . '">' . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . '</a></li>';

									/* echo "<li>
										<a href=\"" . base_url('hotel/hotel_detail/') . $data_hotel_by_country->hotel_id . "/" . underscore(strtolower($data_hotel_by_country->hotel_name)) . "\">" . (($data_hotel_by_country->text_title) ? $data_hotel_by_country->text_title : $data_hotel_by_country->def_title) . "</a>
									</li>"; */
									$i++;
								}

							}
						echo '</ul>';
					$x ++;
					}
				?>
				</div>

				<!--about us -->
				<div class="col1">

						<ul>
								<a href="<?php echo base_url('/corporate/');?>"><h5><?php echo lang('footer_corporate');?></h5></a>
						</ul>
				<div class="gutter"></div>

				<ul>
							<a href="<?php echo base_url('about_us/');?>" ><h5><?php echo lang('footer_about_us');?></h5></a>

							<?php
									$sub_menus = array();
									$list = $this->pages_model->get_menu_list(5);
									//foreach($list->result() as $main_menu){
									//	$sub_list1 = $this->pages_model->get_menu_list_aboutus($main_menu='5');
										if(($list->num_rows() > 0) || ($main_menu->menu_name == 'hotel')) {
											$link = '#';
											$icon_class = 'i_child';
											$sub_menus[$main_menu='5'] = $list->result();
										} else {
											$icon_class = '';
											$link = base_url($main_menu->link);
										}
									//}
									
										if(count($sub_menus) > 0) {
										unset($sub_menus['hotel']);
										foreach ($sub_menus as $sub_key => $sub_menu) {
											foreach($sub_menu as $menu_data) {			
                      		?>
                      							<li><a href="<?php echo base_url($menu_data->link); ?>"><?php echo (($menu_data->text_title) ? $menu_data->text_title : $menu_data->def_title); ?></a> </li>
									<?php 
											}
										}
									}
                      				?>
						</ul>



				</div>
				<!--about us -->

				<div class="col1">
							<ul>
<a href="<?php echo base_url('help_info/');?>"><h5><?php echo lang('footer_help_info');?></h5></a>
								<?php
									$sub_menus = array();
									$list = $this->pages_model->get_menu_list(6);
									//foreach($list->result() as $main_menu){
									//	$sub_list1 = $this->pages_model->get_menu_list_help_info($main_menu='6');
										if(($list->num_rows() > 0) || ($main_menu->menu_name == 'hotel')) {
											$link = '#';
											$icon_class = 'i_child';
											$sub_menus[$main_menu='5'] = $list->result();
										} else {
											$icon_class = '';
											$link = base_url($main_menu->link);
										}
									//}
									
										if(count($sub_menus) > 0) {
										unset($sub_menus['hotel']);
										foreach ($sub_menus as $sub_key => $sub_menu) {
											foreach($sub_menu as $menu_data) {
									?>
												<li><a href="<?php echo base_url($menu_data->link); ?>"><?php echo (($menu_data->text_title) ? $menu_data->text_title : $menu_data->def_title); ?></a> </li>
									
									<?php 
											}
										}
									}
                      				?>						
							</ul>
				</div>

				<div class="col1">

					<div>
					<h5 class="list-head"><strong><?php echo lang('footer_follow_us');?></strong></h5>
								<div class="follow_media f_left">
									<a href="http://www.facebook.com/tunehotels" target="_blank"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/fb_icon.png" class="f_left"/>
									<p>Facebook</p>
									<p>Like</p></a>
					</div>

				</div>

				<div class="follow_media f_left">
									<a href="http://pinterest.com/tunehotels/" target="_blank"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/pin_icon.png" class="f_left"/>
									<p>Pinterest</p>
									<p>Pinned</p></a>
								</div>


				<div class="follow_media f_left">
									<a href="https://twitter.com/tunehotels" target="_blank"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/tw_icon.png" class="f_left"/>
									<p>Twitter</p>
									<p>Follow</p></a>
								</div>
								<div class="follow_media f_left">
									<a href="http://www.youtube.com/user/tunehotel" target="_blank"><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/yt_icon.png" class="f_left"/>
									<p>Youtube</p>
									<p>Subscribe</p></a>
								</div>



				</div>







				</div>



		<div class="row-footer">
	<hr class="hr_one"/>
		<img src="/assets/img/ui/footer-badges.gif" style="width:980px; margin:0 auto; padding:0; display:block;">
		<hr class="hr_one"/>
		<div class="footer_content align-center">
				<div class="footer_text"><p> <?php echo lang('footer_text');?>
				<small>© Copyright2013 Tune Hotels. All rights reservedPrivacy Policy Terms and Conditions Cookie Statement Mobile site Optional service fees</small> </p></div>
		</div>
		<div class="clear"></div>
		</div>

</div>


<div class="clear"></div>

<div class="yellow"><strong>{elapsed_time}</strong> </div>



</body>
</html>
