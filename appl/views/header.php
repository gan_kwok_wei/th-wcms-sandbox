<?php
define('ASSET_PATH', base_url().'assets/');
if(!isset($style_extras)) { $style_extras = array(); }
if(!isset($js_extras)) { $js_extras = array(); }

$ge_page = $this->config->item('ge_page');

?>
<!DOCTYPE html>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<!--[if lt IE 7]>   <html class="no-js ie6 oldie" lang="en" itemscope itemtype="http://schema.org/Article"> <![endif]-->
<!--[if IE 7]>      <html class="no-js ie7 oldie" lang="en" itemscope itemtype="http://schema.org/Article"> <![endif]-->
<!--[if IE 8]>      <html class="no-js ie8 oldie" lang="en" itemscope itemtype="http://schema.org/Article"> <![endif]-->
<!--[if IE 9]>      <html class="no-js ie9" lang="en" itemscope itemtype="http://schema.org/Article">       <![endif]-->
<!--[if (gt IE 9)|!(IE)]<!-->
<html style="" class=" js no-flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" itemscope="" itemtype="http://schema.org/Article" lang="en">
<!--<![endif]-->
<head>

	<link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="TuneHotels.com, Tune Hotels, Budget Hotels, Boutique Hotels<?php echo (isset($keywords)) ? (', ' . $keywords) : '' ; ?>" />
	<meta name="description" content="Tune Hotel<?php echo (isset($description)) ? (' - ' . $description) : '' ; ?>" />

	<?php foreach ($style_extras as $style_extra) { echo '<link rel="stylesheet" type="text/css" media="screen" href="' . $style_extra . '" />'; } ?>

	<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/modernizr.js"></script>

	<?php foreach ($js_extras as $js_extra) { echo '<script type="text/javascript" src="' . $js_extra . '"></script>'; } ?>


	<script type="text/javascript" src="<?php echo ASSET_PATH ?>js/custom.js"></script>

	<script type="text/javascript">var switchTo5x=true;</script>
	<!--<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>-->
	<!--<script type="text/javascript">stLight.options({publisher: "ur-bc6e7663-6ee9-a9fc-9231-1c3420c75ce3"});</script>-->

	<script type="text/javascript">

	$(document).ready(function(){
            //change language
            $('#for_lang>ul>li>a.dd-option').click(function() {
            	var country = $('#for_country>div>input.dd-selected-value').val();
            	var lang = $('#for_lang>div>input.dd-selected-value').val();
            	if(country != ""){
            		window.location='<?php echo "http://" . $_SERVER['HTTP_HOST'];?>/' + country + '/' + lang;
            	} else {
            		alert('Please select country first!');
            	}
            });

          });
	</script>
	<title><?php echo ((isset($title)) ? ($title . ' | ') : '') . $this->config->item('site_title') ; ?></title>

</head>
<body>
	<!-- start body container -->
	
	<div id="th-header">
		<div class="wrapper">
			<div id="header_top" style="height:144px">
				<a href="<?php echo base_url();?>"><div id="logo"></div></a>
				<div id="header_nav">

					<div id="header_menu">

						<div class="f_right red">
                            <!--select name="language_id" tabindex="2" class="f_left" id="for_lang">
                                <option value="" data-imagesrc="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/flag/en.png"><?php echo lang('select_language');?></option>
                                <?php
                                    $this_lang = $this->uri->segment(2);

                                    $tune_lang = $this->config->item('ge_language');
                                    foreach($tune_lang as $tune_langs){
                                        $tune_lang_key = array_keys($tune_lang, $tune_langs);
                                        echo '<option value="' . $tune_lang_key[0] . '" data-imagesrc="' . ASSET_PATH . 'themes/publish/' . $this->system_model->get_publish_theme() . '/images/assets/flag/' . $tune_lang_key[0] . '.png" ' . ($this_lang == $tune_lang_key[0] ? 'selected' : '') . ' >' . $tune_langs . '</option>';
                                    }
                                ?>
                              </select-->
                            </div>
                            <div class="f_right" style="margin-top:40px;">
                            <!--select name="country_id" tabindex="1" class="f_left" id="for_country">
                                <option value="" data-imagesrc="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/flag/en.png"><?php echo lang('select_country');?></option>
                                <?php
                                    $this_country = $this->uri->segment(1);

                                    $tune_country = $this->config->item('ge_country');
                                    foreach($tune_country as $tune_countrys){
                                        $tune_countrys_key = array_keys($tune_country, $tune_countrys);
                                        echo '<option value="' . $tune_countrys_key[0] . '" data-imagesrc="' . ASSET_PATH . 'themes/publish/' . $this->system_model->get_publish_theme() . '/images/assets/flag/' . $tune_countrys_key[0] . '.png" ' . ($this_country == $tune_countrys_key[0] ? 'selected' : '') . ' >' . $tune_countrys . '</option>';
                                    }
                                ?>
                              </select-->

                              <div id="this_country_lang">
                              	<?php
                              	$full_session_country = $this->db->get_where('country', array('letter_code' => strtoupper($this->session->get_dec('country'))));
                              	$data_full_session_country = $full_session_country->row();

                              	$tune_lang = $this->config->item('ge_language');
                              	echo $data_full_session_country->name . ' ' . ucwords($tune_lang[$this->session->get_dec('lang')]);
                              	?>
                              </div>
							
							<div class="backdrop"></div>
                              <div class="box1">
                              	<div class="box_title1">
                              		<span><?php echo lang('select_country_language') ?></span>
                              		<div class="close1"></div>
                              	</div>
                              	<div class="box_content1">
                              		<div class="boc_cont_cont1">
                              			<ul id="list_language">
                              				<?php
                              				$get_country_list = $this->system_model->get_country_list();
                              				foreach($get_country_list->result() as $data_get_country_list){
                              					?>
                              					<li>
                              						<span><img src="<?php echo ASSET_PATH; ?>themes/publish/<?php echo $this->system_model->get_publish_theme(); ?>/images/assets/flag/<?php echo strtolower($data_get_country_list->letter_code);?>.png" style="width:16px; height: 11px;" width="16" height="11" />
                              							&nbsp;<?php echo $data_get_country_list->name;?></span>
                              							<ul>
                              								<?php
                              								$get_country_language = $this->system_model->get_country_language($data_get_country_list->country_id);
                              								foreach($get_country_language->result() as $data_get_country_language){
                              									echo '<li><a href="http://' . $_SERVER['HTTP_HOST'] . '/' . strtolower($data_get_country_list->letter_code) . '/' . $data_get_country_language->key . '">' . lang('lang_' . $data_get_country_language->val) . '</a></li>';
                              								}
                              								?>
                              							</ul>
                              						</li>
                              						<?php
                              					}
                              					?>
                              				</ul>
                              				<div class="clear"></div>
                              			</div>
                              		</div>
                              	</div>
                              	<?php //$this->load->view('component/popup_newsletter'); ?>
                              </div>
                            </div>
                          </div>
                        </div>


                      </div>
                      <div class="clear"></div>
                      <div class="container">
                      	<?php
                      	$this->load->view('component/main-navigation-top.php');
                      	?>
                      </div>

                      <div  class="navbar">
                      	<div class="nav-row container">
                      		<?php
                      		$this_page = (in_array($this->uri->segment(1), $this->system_model->get_registered_country(TRUE))) ?  $this->uri->segment(3) : $this->uri->segment(1);
                      		if(!$this_page){
                      			$this_page = "home";
                      		}
                      		?>

                      		<ul>
                      			<?php
                      			$sub_menus = array();
                      			$list = $this->pages_model->get_menu_list();
                      			foreach($list->result() as $main_menu){
                      				$sub_list1 = $this->pages_model->get_menu_list($main_menu->menu_id);
                      				if(($sub_list1->num_rows() > 0) || ($main_menu->menu_name == 'hotel')) {
                      					$link = '#';
                      					$icon_class = 'i_child';
                      					$sub_menus[$main_menu->menu_name] = $sub_list1->result();
                      				} else {
                      					$icon_class = '';
                      					$link = base_url($main_menu->link);
                      				}
                      				?>
                      				<li><a href="<?php echo $link; ?>" id="menu_<?php echo $main_menu->menu_name; ?>"
                      					class="<?php echo $icon_class . ' ' . $main_menu->menu_name . ' a_first ' . (($this_page == $main_menu->menu_name) ? ($main_menu->menu_name . '_hover') : ""); ?> ">

                      					<?php echo (($main_menu->text_title) ? $main_menu->text_title : $main_menu->def_title); ?></a></li>

                      					<?php
                      				}

                      				?>
                      			</ul>
                      		</div>
                      	</div>


                      	<div class="clear"></div>



                      	<div id="sub_hotel" class="nav_drop" style="display: none;">
                      		<div class="wrapper twelve clearfix">
                      			<?php
                      			$res_country = $this->pages_model->get_country();
                      			foreach($res_country->result() as $data_country) {
                      				?>

                      				<ul class="col2">
                      					<a href="<?php echo base_url('hotel/hotels/' . $data_country->country_id . '/' . underscore(strtolower($data_country->name))); ?>"  class="nav_drop_tab"><h2><?php echo (($data_country->text_title) ? $data_country->text_title : $data_country->def_title); ?></h2></a>

                      					<?php
                      					$res_country_hotel = $this->pages_model->get_hotel_by_country($data_country->country_id);
                      					if($res_country_hotel->num_rows() > 0){
                      						echo '<div class="nav_drop_list">';
                      						foreach($res_country_hotel->result() as $data_hotel) {
                      							?>
                      							<!-- li><a href="<?php echo base_url('our-hotel/' . $data_hotel->hotel_name); ?>" onclick="#"><?php echo (($data_hotel->text_title) ? $data_hotel->text_title : $data_hotel->def_title); ?></a></li-->
                      							<li class="nav_drop_list" >
                      								<a href="<?php echo base_url('hotel/hotel_detail/' . $data_hotel->hotel_id . '/' . $data_hotel->hotel_name); ?>" onclick="#">
                      									<?php echo (($data_hotel->text_title) ? $data_hotel->text_title : $data_hotel->def_title); ?>
                      								</a>
                      							</li>
                      							<?php
                      						}
                      						echo '</div>';
                      					}
                      					?>

                      				</ul>

                      				<?php
                      			}
                      			?>

                      		</div>
                      	</div>


                      	<!--main subnav disabled-->
                      	<?php 
                      	if(count($sub_menus) > 0) {
                      		unset($sub_menus['hotel']);
                      		foreach ($sub_menus as $sub_key => $sub_menu) {
                      			?>

                      			<div id="sub_<?php echo $sub_key; ?>" class="nav_drop black" style="display:none; ">
                      				<div class="wrapper twelve clearfix">
                      					<ul>
                      						<?php 

                      						foreach($sub_menu as $menu_data) {
                      							?>
                      							<li class="col2"><a href="<?php echo base_url($menu_data->link); ?>" onclick="#"><?php echo (($menu_data->text_title) ? $menu_data->text_title : $menu_data->def_title); ?></a></li>
                      							<?php 
                      						}
                      						?>
                      					</ul>
                      				</div>
                      			</div>
                      			<?php 
                      		}
                      	}
                      	?>

                      </div>


