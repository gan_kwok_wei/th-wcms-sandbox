<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource pages_lang.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Aug 27, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

/** language **/
$lang['lang_english']		= "english";
$lang['lang_indonesia']		= "indonesia";
$lang['lang_chinese']		= "中国语言";
$lang['lang_thailand']		= "ภาษาไทย";

/** HEADER **/
$lang['home'] ="首页";
$lang['our_hotel'] ="我们的酒店";
$lang['promotions'] ="促销“促销活动";
$lang['corporate'] ="公司";
$lang['about_us'] ="关于我们";
$lang['help_info'] ="帮助信息";

$lang['select_country'] ="选择国家";
$lang['select_language'] ="选择语言";

$lang['search'] ="搜索";
$lang['search_result'] ="搜索结果";
$lang['search_readmore'] ="了解更多";

$lang['select_country_language'] = "请选择您的国家和语文";

$lang['title_404'] ="404";
$lang['page_not_found'] ="未发现";
$lang['404_text'] ="很抱歉，您要求的网页可能已被移动或删除。";

/** booking search **/
$lang['book_hello'] ="你好";
$lang['book_city'] ="城";
$lang['book_checkin'] ="入住";
$lang['book_ckeckout'] ="退房";
$lang['book_rooms'] ="房间";
$lang['book_adults'] ="成人";
$lang['book_promotion_code'] ="促销代码（可选）";
$lang['book_search'] ="搜索";
$lang['booking_managed_confirm'] ="管理确认预订";

/** top destination **/
$lang['top_top_destination'] ="目的地";
$lang['top_from'] ="从";
$lang['top_per_Night'] ="每晚";

/** whats hot **/
$lang['whats_whats_hot'] ="什么？";

/** follow us **/
$lang['follow_follow_us'] ="关注我们";

/** e-newsletter **/
$lang['e_news_enewsletter'] ="E-通讯";
$lang['e_news_text'] ="注册为我们的通讯，是第一次听到关于我们的最新优惠。";
$lang['e_news_your_email'] ="你的电子邮件地址";
$lang['e_news_subscribe'] ="立即申请";

$lang['e_news_signup'] ="注册E-通讯";

$lang['e_news_email'] ="电子邮件";
$lang['e_news_fname'] ="名";
$lang['e_news_lname'] ="姓";
$lang['e_news_gender'] ="性别";
$lang['e_news_gender_m'] ="男";
$lang['e_news_gender_f'] ="女";
$lang['e_news_birth'] ="出生日期";
$lang['e_news_phone'] ="电话";
$lang['sign_up'] ="注册";
$lang['e_news_success'] 	= "成功注册电子报";
$lang['e_news_mail_success'] 	= "您已成功签订了关于调整酒店电子通讯。";
$lang['e_news_failed'] 		= "注册失败！";
$lang['e_news_duplicated'] 	= "您的电子邮件地址已被注册。";
$lang['invalid_email']= "无效的电子邮件";
$lang['false_field_email']	= "请填写的电子邮件场！";
$lang['false_field_name']	= "请填写名称字段中！";
$lang['false_field']		= "请选择一个！";
$lang['false_field_date']	= "请有效日期！";
$lang['false_field_room']	= "请选择房间类型！";

/** foot home **/
$lang['foot_home_find_us'] ="查找";
$lang['foot_home_see_video'] ="查看我们的视频";
$lang['foot_home_corporate'] ="公司";
$lang['foot_home_guest_review'] ="游客的评价";

/** footer **/
$lang['footer_our_hotel'] ="我们的酒店";
$lang['footer_about_us'] ="关于我们";
$lang['footer_help_info'] ="帮助信息";
$lang['footer_promotion'] ="促销";
$lang['footer_corporate'] ="商业";
$lang['footer_follow_us'] ="关注我们";
$lang['footer_secure_by'] ="抵押";
$lang['footer_our_partner'] ="我们的合作伙伴";
$lang['footer_text'] ="使用Tune酒店网站表示您同意遵守我们的服务条款和隐私权政策。<BR/>©2012 Tune酒店。最佳浏览解析度1024 x 768的分辨率，MSIE 8节或火狐3.​​5 +";


/** HOTEL PAGE **/

/** map hotel **/
$lang['map_hotel_tune_hotel'] ="Tune酒店";
$lang['map_hotel_our_hotel'] ="我们的酒店";
$lang['map_hotel_tune_hotel_so_far'] ="Tune酒店至今";
$lang['map_overview']				= "概观";

/** list hotel **/
$lang['list_hotel_all_hotel'] ="所有酒店";
$lang['list_hotel_tune_hotel'] ="Tune酒店";

$lang['book_now'] ="提交";
$lang['see_detail'] ="查看详细信息";
$lang['read_more'] ="了解更多";
$lang['lets_go'] ="让我们去";

/** detail hotel **/
$lang['det_hotel_hotel_information'] ="酒店信息";
$lang['det_hotel_gallery'] ="画廊";
$lang['det_hotel_nearby_attraction'] ="附近的景点";
$lang['det_hotel_how_to_here'] ="如何到达这里";
$lang['det_hotel_customer_review'] ="顾客";

/** hotel information **/
$lang['h_info_address'] ="地址";
$lang['h_info_feature'] ="特色";
$lang['h_info_what_we_provide'] ="我们提供什么";
$lang['h_info_addon'] ="添加";
$lang['h_info_package'] ="包";

/** nearby attraction **/
$lang['nearby_must_do_think'] ="必须做的事情";
$lang['nearby_place_interes'] ="景点";
$lang['nearby_appetite'] ="食欲";

/** promotion **/
$lang['promo_promotion'] ="推广";
$lang['promo_sel_cc'] ="请选择国家和城市";
$lang['promo_sel_country'] ="选择国家";
$lang['promo_all_country'] ="所有国家";
$lang['promo_sel_city'] ="选择城市";
$lang['promo_all_city'] ="所有城市";
$lang['promo_no_found'] ="无促销的发现！";
$lang['promo_tune_promo'] ="Tune酒店促销";
$lang['social_buzz'] 	= "我們的客人說我們！";

/** corp **/
$lang['corp_inquiry_form'] ="查询表";
$lang['corp_company_name'] ="公司名称";
$lang['corp_contact_person'] ="联系人";
$lang['corp_telp'] ="电话";
$lang['corp_mobile'] ="移动";
$lang['corp_addr'] ="地址";
$lang['corp_post_code'] ="邮政编码";
$lang['corp_country'] ="国家";
$lang['corp_sel_country'] ="选择国家";
$lang['corp_fax'] ="传真号码";
$lang['corp_email'] ="电子邮件地址";
$lang['corp_hotel'] ="酒店";
$lang['corp_sel_hotel'] ="选择酒店";
$lang['corp_checkin'] ="检查日期";
$lang['corp_year'] ="年份";
$lang['corp_month的'] ="月";
$lang['corp_day'] ="日";
$lang['corp_checkout'] ="退房日期";
$lang['corp_n_night'] ="不夜";
$lang['corp_no_room'] ="无房";
$lang['corp_no_pax'] ="百富";
$lang['corp_room_type'] ="房间类型";
$lang['corp_sel_room'] ="选择 房间";
$lang['corp_remarks'] ="备注";

/** about us **/
$lang['about_management_team'] ="管理团队";
$lang['about_shareholders'] ="股东";
$lang['about_career'] ="职业";
$lang['about_concept'] ="概念";
$lang['about_media'] ="媒体";
$lang['about_career_with_tune'] ="职业与Tune酒店";
$lang['about_vacancy'] ="缺位";
$lang['about_medi_type方向'] ="媒体类型";

/** help & info **/
$lang['help_contact_us'] ="联系我们";
$lang['help_booking_cancelation'] ="预订及对消";
$lang['help_terms_condition'] ="条款及条件";
$lang['help_fee_hotel_schedule'] ="费及酒店日程安排";
$lang['help_faq'] ="常见问题";

$lang['help_c_gen_in'] ="一般查询";
$lang['help_c_adv_tune'] ="广告Tune酒店";
$lang['help_c_enq_fra'] ="查询有关Tune酒店的特许经营计划";
$lang['help_c_feed'] ="意见/建议";
$lang['help_c_sell'] ="出售/租的物业给我们呢？";
$lang['help_c_name'] ="名称";
$lang['help_c_email'] ="电子邮件地址";
$lang['help_c_addr'] ="地址";
$lang['help_c_post'] ="邮政编码";
$lang['help_c_countr'] ="国家";
$lang['help_c_sel_cn'] ="选择国家";
$lang['help_c_htl'] ="酒店";
$lang['help_c_sel_htl'] ="选择酒店";
$lang['help_c_remarks'] ="备注";
$lang['help_c_submit'] ="提交";

$lang['response_inquiry_title'] 	= "Tune酒店查询";
$lang['response_text_success'] 		= "感谢您的垂询，我们将恢复在72小时内给您。";
$lang['response_text_failed'] 		= "无法发送询盘。";




$lang['help_c_sugest'] ="建议，问题或意见";
$lang['help_c_telepon']="电话";
$lang['false_field_telepon'] ="请填写电话领域!";

$lang['help_c_mobile'] ="移动";
$lang['false_field_mobile'] ="请填写在移动领域!";

$lang['help_c_property'] ="属性名称";
$lang['false_field_property'] ="请填写属性名称字段中!";

$lang['false_field_city'] ="请填写城市场!";

$lang['help_c_exisiting']="重新审视";
$lang['help_c_exi_cn_bu'] ="大厦";
$lang['help_c_exi_cn_la']="土地";

$lang['help_c_faciliti'] ="设施";
$lang['false_field_facility'] ="请填写设施";

$lang['help_c_category']="类别";
$lang['help_c_exi_cn_citicentre'] ="城市中心";
$lang['help_c_exi_cn_citylimits'] ="城市极限";
$lang['help_c_exi_cn_outside']="外城";
$lang['help_c_exi_cn_resort'] ="海滩度假村";
$lang['help_c_exi_cn_resort_others'] ="度假村";
$lang['help_c_exi_cn_resort_eco'] ="生态度假村";

$lang['help_c_land_title'] ="土地所有权";
$lang['help_c_exi_cn_freehold'] ="永久业权";
$lang['help_c_exi_cn_strata'] ="层";
$lang['help_c_exi_cn_leasehold'] ="租赁";
$lang['help_c_exi_cn_native'] ="本土";

$lang['help_c_land_size'] ="占地面积";
$lang['help_c_built_up']="生成";
$lang['help_c_no_floor']="无地板";


$lang['help_c_marketprice'] ="市场价格";
$lang['help_c_askingprice'] ="售价";
$lang['false_field_asking'] ="请填写要价";
$lang['false_field_market'] ="请填补了市场价格";


/**
 * End of file pages_lang.php 
 * Location: ./.../.../.../pages_lang.php 
 */