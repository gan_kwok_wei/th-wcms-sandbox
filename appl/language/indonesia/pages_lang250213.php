<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource pages_lang.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Aug 27, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

/** language **/
$lang['lang_english']		= "english";
$lang['lang_indonesia']		= "indonesia";
$lang['lang_chinese']		= "中国语言";
$lang['lang_thailand']		= "ภาษาไทย";

/** HEADER **/
$lang['home'] = "Home";
$lang['our_hotel'] = "Hotel kami";
$lang['promosi'] = "Promosi";
$lang['perusahaan'] = "Perusahaan";
$lang['about_us'] = "Tentang Kami";
$lang['help_info'] = "Bantuan & Info";

$lang['select_country'] = "Pilih Negara";
$lang['select_language'] = "Pilih Bahasa";

$lang['search'] = "Cari";
$lang['search_result'] = "Search Result";
$lang['search_readmore'] = "Read More";

$lang['title_404'] = "404";
$lang['page_not_found'] = "Halaman tidak ditemukan";
$lang['404_text '] = "Maaf halaman yang Anda minta mungkin telah pindah atau dihapus.";

/** Pencarian pemesanan **/
$lang['book_hello'] = "Hello.";
$lang['book_city'] = "Kota";
$lang['book_checkin'] = "Check-In";
$lang['book_ckeckout'] = "Check-Out";
$lang['book_rooms'] = "Kamar";
$lang['book_adults'] = "Dewasa";
$lang['book_promotion_code'] = "Kode Promosi (Opsional)";
$lang['book_search'] = "Cari";
$lang['booking_managed_confirm'] = "Kelola pemesanan dikonfirmasi";

/** Tujuan utama **/
$lang['top_top_destination'] = "Tujuan Populer";
$lang['top_from'] = "Dari";
$lang['top_per_Night'] = "Per Malam";

/** Whats hot **/
$lang['whats_whats_hot'] = "Apa yang Hot?";

/** Ikuti kami **/
$lang['follow_follow_us'] = "Ikuti Kami Di";

/** E-newsletter **/
$lang['e_news_enewsletter'] = "E-Newsletter";
$lang['e_news_text'] = "Sign up untuk newsletter kami dan jadilah yang pertama mendengar tentang penawaran terbaru kami.";
$lang['e_news_your_email'] = "Alamat Email Anda";
$lang['e_news_subscribe'] = "Berlangganan Sekarang";

$lang['e_news_signup'] = "Daftar e-Newsletter";

$lang['e_news_email'] = "Email";
$lang['e_news_fname'] = "Nama Depan";
$lang['e_news_lname'] = "Nama Belakang";
$lang['e_news_gender'] = "Gender";
$lang['e_news_gender_m'] = "Pria";
$lang['e_news_gender_f'] = "Wanita";
$lang['e_news_birth'] = "Tanggal lahir";
$lang['e_news_phone'] = "Telepon";
$lang['sign_up'] = "Daftar";
$lang['invalid_email']	= "Email Tidak Benar";
$lang['false_field']		= "Pilih Salah Satu!";
$lang['false_field_date']	= "Pilih Tgl yang Benar!";
$lang['false_field_room']	= "Pilih Kamar yang Benar!";

/** Rumah kaki **/
$lang['foot_home_find_us'] = "Temukan Kami";
$lang['foot_home_see_video'] = "Lihat Video kami";
$lang['foot_home_corporate'] = "Perusahaan";
$lang['foot_home_guest_review'] = "Guest Review";

/** Footer **/
$lang['footer_our_hotel'] = "HOTEL KAMI";
$lang['footer_about_us'] = "TENTANG KAMI";
$lang['footer_help_info'] = "BANTUAN & INFO";
$lang['footer_promotion'] = "PROMOSI";
$lang['footer_corporate'] = "PERUSAHAAN";
$lang['footer_follow_us'] = "IKUTI KAMI";
$lang['footer_secure_by'] = "Dijamin oleh";
$lang['footer_our_partner'] = "Mitra kami";
$lang['footer_text'] = "Penggunaan situs Tune Hotels Anda menyatakan telah memenuhi Syarat & Ketentuan dan Kebijakan Privasi <br/> � 2012 Hotel Tune Untuk gambar terbaik gunakan resolusi 1024 x 768,. MSIE v.8 atau Firefox 3.5 + ";


/** HALAMAN HOTEL **/

/** Peta Hotel **/
$lang['map_hotel_tune_hotel'] = "Tune Hotel";
$lang['map_hotel_our_hotel'] = "Hotel kami";
$lang['map_hotel_tune_hotel_so_far'] = "Tune Hotel So Far";
$lang['map_overview']				= "Ikhtisar";

/** Hotel Daftar **/
$lang['list_hotel_all_hotel'] = "Semua Hotel";
$lang['list_hotel_tune_hotel'] = "Tune Hotel";

$lang['book_now'] = "Pesan Sekarang";
$lang['see_detail'] = "Lihat Detail";
$lang['read_more'] = "Read More";
$lang['lets_go'] = "Ayo";

/** Hotel rinci **/
$lang['det_hotel_hotel_information'] = "Hotel Informasi";
$lang['det_hotel_gallery'] = "Galeri";
$lang['det_hotel_nearby_attraction'] = "objek Terdekat";
$lang['det_hotel_how_to_here'] = "Cara Dapatkan Disini";
$lang['det_hotel_customer_review'] = "dari pelanggan";

/** Informasi hotel **/
$lang['h_info_address'] = "Alamat";
$lang['h_info_feature'] = "Fitur";
$lang['h_info_what_we_provide'] = "Apa yang Kami Sediakan";
$lang['h_info_addon'] = "Add-On";
$lang['h_info_package'] = "Paket";

/** Terdekat tarik **/
$lang['nearby_must_do_think'] = "Harus Lakukan hal-hal di";
$lang['nearby_place_interes'] = "Tempat Menarik";
$lang['nearby_appetite'] = "Appetite";

/** Promosi **/
$lang['promo_promotion'] = "Promosi";
$lang['promo_sel_cc'] = "Silahkan Pilih Negara dan Kota";
$lang['promo_sel_country'] = "Pilih Negara";
$lang['promo_sel_city'] = "Pilih Kota";
$lang['promo_sel_country'] 		= "Pilih Negara";
$lang['promo_all_country'] 		= "Semua Negara";
$lang['promo_sel_city'] 		= "Pilih Kota";
$lang['promo_all_city'] 		= "Semua Kota";
$lang['promo_no_found'] = "Tidak ada Promo ditemukan!";
$lang['promo_tune_promo'] = "Tune Hotel Promo";

/** Corp **/
$lang['corp_inquiry_form'] = "Formulir Permintaan";
$lang['corp_company_name'] = "Nama Perusahaan";
$lang['corp_contact_person'] = "Contact Person";
$lang['corp_telp'] = "Telepon";
$lang['corp_mobile'] = "Mobile";
$lang['corp_addr'] = "Alamat";
$lang['corp_post_code'] = "Kode Pos";
$lang['corp_country'] = "Negara";
$lang['corp_sel_country'] = "Pilih Negara";
$lang['corp_fax'] = "Nomor Faks";
$lang['corp_email'] = "Alamat E-mail";
$lang['corp_hotel'] = "Hotel";
$lang['corp_sel_hotel'] = "Pilih Hotel";
$lang['corp_checkin'] = "Check In Tanggal";
$lang['corp_year'] = "Tahun";
$lang['corp_month'] = "Bulan";
$lang['corp_day'] = "Hari";
$lang['corp_checkout'] = "Check Out Tanggal";
$lang['corp_n_night'] = "Tidak ada dari malam";
$lang['corp_no_room'] = "Tidak ada kamar";
$lang['corp_no_pax'] = "Tidak ada dari pax";
$lang['corp_room_type'] = "Jenis kamar";
$lang['corp_sel_room'] = "Pilih Kamar";
$lang['corp_remarks'] = "Keterangan";

/** Tentang kami **/
$lang['about_management_team'] = "Tim Manajemen";
$lang['about_shareholders'] = "Pemegang Saham";
$lang['about_career'] = "Karir";
$lang['about_concept'] = "Konsep";
$lang['about_media'] = "Media";
$lang['about_career_with_tune'] = "Karir Dengan Tune Hotels";
$lang['about_vacancy'] = "Vacancy";
$lang['about_medi_type'] = "Media Jenis";

/** Membantu & Info **/
$lang['help_contact_us'] = "Hubungi Kami";
$lang['help_booking_cancelation'] = "Pemesanan & Pembatalan";
$lang['help_terms_condition'] = "Syarat & Kondisi";
$lang['help_fee_hotel_schedule'] = "Fee & Jadwal";
$lang['help_faq'] = "FAQ";

$lang['help_c_gen_in'] = "Kirim Umum";
$lang['help_c_adv_tune'] = "Beriklan Dengan Hotel Tune";
$lang['help_c_enq_fra'] = "Pertanyaan tentang Program Waralaba dengan Tune Hotels";
$lang['help_c_feed'] = "Feedback / Saran";
$lang['help_c_sell'] = "Jual / Sewa properti kepada kami?";
$lang['help_c_name'] = "Nama";
$lang['help_c_email'] = "Alamat Email";
$lang['help_c_addr'] = "Alamat";
$lang['help_c_post'] = "Kode Pos";
$lang['help_c_countr'] = "Negara";
$lang['help_c_sel_cn'] = "Pilih Negara";
$lang['help_c_htl'] = "Hotel";
$lang['help_c_sel_htl'] = "Pilih Hotel";
$lang['help_c_remarks'] = "Keterangan";
$lang['help_c_submit'] = "Kirim";
/**
 * End of file pages_lang.php 
 * Location: ./.../.../.../pages_lang.php 
 */