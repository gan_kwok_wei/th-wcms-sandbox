<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource pages_lang.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Aug 27, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

/** language **/
$lang['lang_english']		= "english";
$lang['lang_indonesia']		= "indonesia";
$lang['lang_chinese']		= "中国语言";
$lang['lang_thailand']		= "ภาษาไทย";

/** HEADER **/
$lang['home']				= "Home";
$lang['our_hotel']			= "Our Hotels";
$lang['promotions']			= "Promotions";
$lang['corporate']			= "Corporate";
$lang['about_us']			= "About Us";
$lang['help_info']			= "Help &amp; Info";

$lang['select_country']		= "Select Country";
$lang['select_language']	= "Select Language";

$lang['search']				= "Search";
$lang['search_result']		= "Search Result";
$lang['search_readmore']	= "Read More";

$lang['title_404']				= "404";
$lang['page_not_found']		= "Page not found";
$lang['404_text']			= "Sorry the page you requested may have been move or deleted.";

/** booking search **/
$lang['book_hello']			= "Hello.";
$lang['book_city']			= "City";
$lang['book_checkin']		= "Check-In";
$lang['book_ckeckout']		= "Check-Out";
$lang['book_rooms']			= "Rooms";
$lang['book_adults']		= "Adults";
$lang['book_promotion_code']= "Promotional Code (Optional)";
$lang['book_search']		= "Search";
$lang['booking_managed_confirm']= "Manage confirmed bookings";

/** top destination **/
$lang['top_top_destination']= "Top Destination";
$lang['top_from']			= "From";
$lang['top_per_Night']		= "Per Night";

/** whats hot **/
$lang['whats_whats_hot']	= "What's Hot ?";

/** follow us **/
$lang['follow_follow_us']	= "Follow Us On";

/** e-newsletter **/
$lang['e_news_enewsletter']	= "E-Newsletter";
$lang['e_news_text']		= "Sign up for our newsletter and be the first to hear about our latest offers.";
$lang['e_news_your_email']	= "Your Email Address";
$lang['e_news_subscribe']	= "Subscribe Now";

$lang['e_news_signup']		= "e-Newsletter Sign Up";

$lang['e_news_email'] 		= "Email";
$lang['e_news_fname'] 		= "First Name";
$lang['e_news_lname'] 		= "Last Name";
$lang['e_news_gender'] 		= "Gender";
$lang['e_news_gender_m'] 		= "Male";
$lang['e_news_gender_f'] 		= "Female";
$lang['e_news_birth'] 		= "Date of birth";
$lang['e_news_phone'] 		= "Phone";
$lang['sign_up']			= "Sign Up";
$lang['invalid_email']		= "Email not valid";
$lang['false_field']		= "Please select one!";
$lang['false_field_date']	= "Please valid date!";
$lang['false_field_room']	= "Please select room type!";

/** foot home **/
$lang['foot_home_find_us']	= "Find Us";
$lang['foot_home_see_video']= "See Our Video";
$lang['foot_home_corporate']= "Corporate";
$lang['foot_home_guest_review']	= "Guest Review";

/** footer **/
$lang['footer_our_hotel']	= "OUR HOTELS";
$lang['footer_about_us']	= "ABOUT US";
$lang['footer_help_info']	= "HELP &amp; INFO";
$lang['footer_promotion']	= "PROMOTIONS";
$lang['footer_corporate']	= "CORPORATE";
$lang['footer_follow_us']	= "FOLLOW US";
$lang['footer_secure_by']	= "Secured By";
$lang['footer_our_partner']	= "Our Partners";
$lang['footer_text']		= "Usage of Tune Hotels website states your compliance of our Terms & Conditions and Privacy Policy.<br/>&copy; 2012 Tune Hotels. For best view use 1024 x 768 resolution; MSIE v.8 or Firefox 3.5+";


/** HOTEL PAGE **/

/** map hotel **/
$lang['map_hotel_tune_hotel'] 		= "Tune Hotel";
$lang['map_hotel_our_hotel'] 		= "Our Hotel";
$lang['map_hotel_tune_hotel_so_far']= "Tune Hotel So Far";
$lang['map_overview']				= "Overview";

/** list hotel **/
$lang['list_hotel_all_hotel'] 		= "All Hotels";
$lang['list_hotel_tune_hotel'] 		= "Tune Hotel";

$lang['book_now'] 					= "Book Now";
$lang['see_detail'] 				= "See Detail";
$lang['read_more']					= "Read More";
$lang['lets_go']					= "Let's Go";

/** detail hotel **/
$lang['det_hotel_hotel_information']= "Hotel Information";
$lang['det_hotel_gallery'] 			= "Gallery";
$lang['det_hotel_nearby_attraction']= "Nearby Attraction";
$lang['det_hotel_how_to_here'] 		= "How To Get Here";
$lang['det_hotel_customer_review'] 	= "Customer Review";

/** hotel information **/
$lang['h_info_address'] 			= "Address";
$lang['h_info_feature'] 			= "Features";
$lang['h_info_what_we_provide'] 	= "What We Provide";
$lang['h_info_addon'] 				= "Add-On";
$lang['h_info_package'] 			= "Package";

/** nearby attraction **/
$lang['nearby_must_do_think'] 		= "Must Do things in";
$lang['nearby_place_interes'] 		= "Place Of Interest";
$lang['nearby_appetite'] 			= "Appetite";

/** promotion **/
$lang['promo_promotion'] 		= "Promotion";
$lang['promo_sel_cc'] 			= "Please Select Country and City";
$lang['promo_sel_country'] 		= "Select Country";
$lang['promo_all_country'] 		= "All Country";
$lang['promo_sel_city'] 		= "Select City";
$lang['promo_all_city'] 		= "All City";
$lang['promo_no_found'] 		= "No Promo found!";
$lang['promo_tune_promo'] 		= "Tune Hotel Promo";

/** corp **/
$lang['corp_inquiry_form'] 		= "Inquiry Form";
$lang['corp_company_name'] 		= "Company Name";
$lang['corp_contact_person'] 	= "Contact Person";
$lang['corp_telp'] 				= "Telephone";
$lang['corp_mobile'] 			= "Mobile";
$lang['corp_addr'] 				= "Address";
$lang['corp_post_code'] 		= "Post Code";
$lang['corp_country'] 			= "Country";
$lang['corp_sel_country'] 		= "Select Country";
$lang['corp_fax'] 				= "Fax Number";
$lang['corp_email'] 			= "E-mail Address";
$lang['corp_hotel'] 			= "Hotel";
$lang['corp_sel_hotel'] 		= "Select Hotel";
$lang['corp_checkin'] 			= "Check In Date";
$lang['corp_year'] 				= "Year";
$lang['corp_month'] 			= "Month";
$lang['corp_day'] 				= "Day";
$lang['corp_checkout'] 			= "Check Out Date";
$lang['corp_n_night'] 			= "No of nights";
$lang['corp_no_room'] 			= "No of rooms";
$lang['corp_no_pax'] 			= "No of pax";
$lang['corp_room_type'] 		= "Room Type";
$lang['corp_sel_room'] 			= "Select Room";
$lang['corp_remarks'] 			= "Remarks";

/** about us **/
$lang['about_management_team'] 		= "Management Team";
$lang['about_shareholders'] 		= "Shareholders";
$lang['about_career'] 		= "Career";
$lang['about_concept'] 		= "Concept";
$lang['about_media'] 		= "Media";
$lang['about_career_with_tune'] 		= "Career With Tune Hotels";
$lang['about_vacancy'] 		= "Vacancy";
$lang['about_medi_type'] 		= "Media Types";

/** help & info **/
$lang['help_contact_us'] 		= "Contact Us";
$lang['help_booking_cancelation'] 		= "Bookings &amp; Cancellation";
$lang['help_terms_condition'] 		= "Terms &amp; Condition";
$lang['help_fee_hotel_schedule'] 		= "Fee &amp; Hotel Schedule";
$lang['help_faq'] 		= "FAQ";

$lang['help_c_gen_in'] 		= "General Inquiry";
$lang['help_c_adv_tune'] 		= "Advertise With Tune Hotel";
$lang['help_c_enq_fra'] 		= "Enquiry about Franchise Programme with Tune Hotels";
$lang['help_c_feed'] 		= "Feedback / Suggestions";
$lang['help_c_sell'] 		= "Sell / Rent a property to us ?";
$lang['help_c_name'] 		= "Name";
$lang['help_c_email'] 		= "Email Address";
$lang['help_c_addr'] 		= "Address";
$lang['help_c_post'] 		= "Post Code";
$lang['help_c_countr'] 		= "Country";
$lang['help_c_sel_cn'] 		= "Select Country";
$lang['help_c_htl'] 		= "Hotel";
$lang['help_c_sel_htl'] 		= "Select Hotel";
$lang['help_c_remarks'] 		= "Remarks";
$lang['help_c_submit'] 		= "Submit";

/**
 * End of file pages_lang.php 
 * Location: ./.../.../.../pages_lang.php 
 */