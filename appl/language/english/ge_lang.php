<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ge_lang.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Aug 28, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

/*	
 *	header lable
 */
$lang['label_welcome']				= "Welcome";
$lang['label_logout']				= "Logout";
$lang['label_language']				= "Language";

/*	
 *	title
 */
$lang['title_news']					= "News";
$lang['title_add_news']				= "Add News";
$lang['title_edit_news']			= "Edit News";
$lang['title_role']					= "Role";
$lang['title_add_role']				= "Add Role";
$lang['title_edit_role']			= "Edit Role";
$lang['title_permission']			= "Permission";
$lang['title_add_permission']		= "Add Permission";
$lang['title_edit_permission']		= "Edit Permission";
$lang['title_add_user']				= "Add New Account";
$lang['title_edit_user']			= "Edit Account";
$lang['title_add_contact']			= "Add New Contact";
$lang['title_edit_contact']			= "Edit Contact";
$lang['title_add_supplier']			= "Add New Supplier";
$lang['title_edit_supplier']		= "Edit Supplier";
$lang['title_company_profile']		= "Company Profile";
$lang['title_add_company']			= "Add New Company";
$lang['title_edit_company']			= "Edit Company";
$lang['title_product_category']		= "Product Category";
$lang['title_add_category']			= "Add New Category";
$lang['title_edit_category']		= "Edit Category";
$lang['title_project']				= "Project";
$lang['title_add_project']			= "Add New Project";
$lang['title_edit_project']			= "Edit Project";
$lang['title_product']				= "Product";
$lang['title_add_product']			= "Add New Product";
$lang['title_edit_product']			= "Edit Product";
$lang['title_request']				= "Request";
$lang['title_add_request']			= "Add New Request";
$lang['title_edit_request']			= "Edit Request";
$lang['title_my_request']			= "My Request";
$lang['title_catalogue']			= "Catalogue";
$lang['title_agreement']			= "Agreement";
$lang['title_add_menu']				= "Add New Menu";
$lang['title_edit_menu']			= "Edit Menu";
$lang['title_add_content']			= "Add New Content";
$lang['title_edit_content']			= "Edit Content";
$lang['title_tender']				= "Tender";
$lang['title_create_tender']		= "Create Tender";
$lang['title_bid_tender']			= "Bid Tender";
$lang['title_edit_tender']			= "Edit Tender";
$lang['title_dashboard']			= "Dashboard";
$lang['title_undefined']			= "Undefined";

/*	
 *	content text banner
 */

$lang['label_icon_banner']			= "Icon<br/>(For Top Position 31px X 33px)";
$lang['label_icon_1_banner']		= "(Not Used)";
$lang['label_image_banner']			= "Image Banner<br/>(Top : 980px X 487px)<br/>(Middle 1 : 316px X 204px)<br/>(Middle 2 / Left 1 / Left 2 : 316px X 178px)<br/>(Right 1 / Right 2 : 232px X 223px)";
$lang['label_image_1_banner']		= "(Not Used)";
$lang['label_teaser_banner']		= "Text Whats Hot<br/>(For Right Position)";
$lang['label_teaser_1_banner']		= "(Not Used)";
$lang['label_content_banner']		= "(Not Used)";
$lang['label_content_1_banner']		= "(Not Used)";

/*	
 *	content text content
 */

$lang['label_icon_content']			= "Icon";
$lang['label_icon_1_content']		= "";
$lang['label_image_content']		= "Image";
$lang['label_image_1_content']		= "";
$lang['label_teaser_content']		= "Teaser";
$lang['label_teaser_1_content']		= "";
$lang['label_content_content']		= "Content";
$lang['label_content_1_content']	= "";

/*	
 *	content text gallery
 */

$lang['label_icon_gallery']			= "Icon";
$lang['label_icon_1_gallery']		= "";
$lang['label_image_gallery']		= "Image";
$lang['label_image_1_gallery']		= "";
$lang['label_teaser_gallery']		= "Teaser";
$lang['label_teaser_1_gallery']		= "";
$lang['label_content_gallery']		= "Content";
$lang['label_content_1_gallery']	= "";

/*	
 *	content text city
 */

$lang['label_icon_city']			= "(Not used)";
$lang['label_icon_1_city']			= "(Not used)";
$lang['label_image_city']			= "(Not used)";
$lang['label_image_1_city']			= "(Not used)";
$lang['label_teaser_city']			= "Must do things in";
$lang['label_teaser_1_city']		= "(Not used)";
$lang['label_content_city']			= "(Not used)";
$lang['label_content_1_city']		= "(Not used)";

/*	
 *	content text landmark
 */

$lang['label_icon_landmark']		= "Landmark Image<br/>(214px X 126px)";
$lang['label_icon_1_landmark']		= "(Not used)";
$lang['label_image_landmark']		= "(Not used)";
$lang['label_image_1_landmark']		= "(Not used)";
$lang['label_teaser_landmark']		= "Landmark Text";
$lang['label_teaser_1_landmark']	= "(Not used)";
$lang['label_content_landmark']		= "(Not used)";
$lang['label_content_1_landmark']	= "(Not used)";

/*	
 *	content text culinary
 */

$lang['label_icon_culinary']		= "Culinary Image<br/>(214px X 126px)";
$lang['label_icon_1_culinary']		= "(Not used)";
$lang['label_image_culinary']		= "(Not used)";
$lang['label_image_1_culinary']		= "(Not used)";
$lang['label_teaser_culinary']		= "Culinary Text";
$lang['label_teaser_1_culinary']	= "(Not used)";
$lang['label_content_culinary']		= "(Not used)";
$lang['label_content_1_culinary']	= "(Not used)";

/*	
 *	content text promo
 */

$lang['label_icon_promo']			= "(Not used)";
$lang['label_icon_1_promo']			= "Image list Promo<br/>(214px X 164px)";
$lang['label_image_promo']			= "(Not used)";
$lang['label_image_1_promo']		= "Image Popup Promo<br/>(max width 288px;)";
$lang['label_teaser_promo']			= "Content List";
$lang['label_teaser_1_promo']		= "(Not used)";
$lang['label_content_promo']		= "Content Popup";
$lang['label_content_1_promo']		= "(Not used)";


/*	
 *	form lable
 */
$lang['label_menu']					= "Menu";
$lang['label_content']				= "Main View";
$lang['label_info']					= "Information";
$lang['label_time']					= "Time";
$lang['label_confirm']				= "Confirm";
$lang['label_close']				= "Close";
$lang['label_more_info']			= "More Info";
$lang['label_module']				= "Module";
$lang['label_class']				= "Class";
$lang['label_function']				= "Function";

$lang['label_hits']					= "Hits";
$lang['label_last_modified']		= "Last Modified";
$lang['label_modified_by']			= "Modified By";
$lang['label_time_created']			= "Time Created";
$lang['label_created_by']			= "Created By";

$lang['label_login']				= "Login";
$lang['label_forget_password']		= "Forgot your password?";
$lang['label_add']					= "Add";
$lang['label_detail']				= "Details";
$lang['label_edit']					= "Edit";
$lang['label_cancel']				= "Cancel";
$lang['label_create']				= "Create";
$lang['label_save']					= "Save";
$lang['label_update']				= "Update";
$lang['label_update_role_perm']		= "Update role permission";
$lang['label_delete']				= "Delete";
$lang['label_remove']				= "Remove";
$lang['label_send']					= "Send";
$lang['label_send_to_leader']		= "Send to leader";
$lang['label_approve']				= "Approve";
$lang['label_reject']				= "Reject";
$lang['label_create_tender']		= "Create Tender";
$lang['label_change_password']		= "Change Password";
$lang['label_reset_password']		= "Reset Password";

$lang['label_personal_info']		= "Personal Information";
$lang['label_person']				= "Person";
$lang['label_company_info']			= "Company Information";
$lang['label_personal_contact']		= "Personal Contact";
$lang['label_contact']				= "Contact";
$lang['label_id']					= "ID";
$lang['label_user_name']			= "User Name";
$lang['label_old_password']			= "Old Password";
$lang['label_old_pass_check']		= "the %s not match";
$lang['label_password']				= "Password";
$lang['label_conf_password']		= "Confirm Password";
$lang['label_user_id']				= "User ID";
$lang['label_role_name']			= "Role Name";
$lang['label_name']					= "Name";
$lang['label_first_name']			= "First Name";
$lang['label_last_name']			= "Last Name";
$lang['label_email']				= "Email";
$lang['label_email_address']		= "Email Address";
$lang['label_email_work']			= "Work Email";
$lang['label_email_other']			= "Other Email";
$lang['label_im']					= "Messenger";
$lang['label_department']			= "Department";
$lang['label_active']				= "Active";
$lang['label_def_lang']				= "Default Language";
$lang['label_last_login']			= "Last Login";
$lang['label_last_login_on']		= "Last Login on";

$lang['label_company_address']		= "Company Address";

$lang['label_from']					= "From";
$lang['label_to']					= "To";
$lang['label_send_to']				= "Send To";
$lang['label_go']					= "Go";
$lang['label_same_dept']			= "Same Dept.";

$lang['label_type']					= "Type";
$lang['label_company']				= "Company";
$lang['label_company_id']			= "Company ID";
$lang['label_company_name']			= "Company Name";
$lang['label_motto']				= "Motto";
$lang['label_phone']				= "Phone";
$lang['label_work']					= "Work";
$lang['label_work_phone']			= "Work Phone";
$lang['label_home']					= "Home";
$lang['label_home_phone']			= "Home Phone";
$lang['label_mobile']				= "Mobile";
$lang['label_mobile_phone']			= "Mobile Phone";
$lang['label_fax']					= "Fax.";
$lang['label_web']					= "Web Site";
$lang['label_longitude']			= "Longitude";
$lang['label_latitude']				= "Latitude";

$lang['label_job_title']			= "Job Title";
$lang['label_communication']		= "Communication";
$lang['label_address']				= "Address";
$lang['label_street']				= "Street";
$lang['label_city']					= "City";
$lang['label_state']				= "State";
$lang['label_country']				= "Country";
$lang['label_zip']					= "Zip";

$lang['label_language']				= "Language";
$lang['label_news_name']			= "News Name";
$lang['label_title']				= "Title";
$lang['label_teaser']				= "Teaser";
$lang['label_content']				= "Content";
$lang['label_parent']				= "Parent";
$lang['label_params']				= "Params";
$lang['label_publish_start']		= "Publish Start";
$lang['label_publish_end']			= "Publish End";

$lang['label_add_category']			= "Add Category";
$lang['label_contact_name']			= "Contact Name";
$lang['label_category_id']			= "Category ID";
$lang['label_category']				= "Category";
$lang['label_sub_category']			= "Sub Category";
$lang['label_category_name']		= "Category name";
$lang['label_desc']					= "Description";

$lang['label_bank']					= "Bank";
$lang['label_bank_account_num']		= "Bank Account #";

$lang['label_location']				= "Location";
$lang['label_location_detail']		= "Location Detail";
$lang['label_storage']				= "Storage";

$lang['label_tree_view']			= "Tree View";
$lang['label_list_view']			= "List View";

$lang['label_consumable']			= "Consumable";
$lang['label_note']					= "Note";
$lang['label_item_id']				= "Item ID";
$lang['label_item_code']			= "Item Code";
$lang['label_item_detail']			= "Item Detail";
$lang['label_item_name']			= "Item Name";
$lang['label_def_ages']				= "Default Ages";
$lang['label_def_residue']			= "Default Residue";
$lang['label_def_depreciation']		= "Default Depreciation";
$lang['label_def_depr_period']		= "Default Depr. Period";
$lang['label_in_month']				= "in Month";
$lang['label_in_year']				= "in Years";
$lang['label_price']				= "Price";
$lang['label_range']				= "Range";
$lang['label_min_value']			= "Min Value";
$lang['label_max_value']			= "Max Value";

$lang['login_error']				= "Wrong user name or password!!";

$lang['label_project_id']			= "Project ID";
$lang['label_project_name']			= "Project Name";
$lang['label_responsible_person']	= "Responsible";
$lang['label_project_note']			= "Note";
$lang['label_back']					= "Back";

$lang['label_order']				= "Order";
$lang['label_quick_info']			= "Quick Info";
$lang['label_product_data']			= "Product Data";
$lang['label_product_id']			= "Product ID";
$lang['label_product_name']			= "Product Name";
$lang['label_product_number']		= "Product Number";
$lang['label_product_price']		= "Product Price";
$lang['label_product_reference']	= "Product Reference";
$lang['label_brand']				= "Brand";
$lang['label_model']				= "Model";
$lang['label_specification_detail']	= "Specification Detail";
$lang['label_specification_key']	= "Specification Key";
$lang['label_specification_value']	= "Specification Value";
$lang['label_image_ref']			= "Image Reference";
$lang['label_image']				= "Image";
$lang['label_icon']					= "Icon";
$lang['label_dimension']			= "Dimension";
$lang['label_dimension_unit']		= "Dimension Unit";
$lang['label_lenght']				= "Lenght";
$lang['label_height']				= "Height";
$lang['label_width']				= "Width";
$lang['label_weight']				= "Weight";
$lang['label_volume']				= "Volume";
$lang['label_currency']				= "Currency";
$lang['label_quantity']				= "Quantity";
$lang['label_unit']					= "Unit";
$lang['label_set_empty']			= "Set Empty";
$lang['label_valid_from']			= "Valid From";
$lang['label_valid_to']				= "Valid To";
$lang['label_link']					= "Link";
$lang['label_tags']					= "Tags";
$lang['label_sort']					= "Sort";
$lang['label_show_stock']			= "Show Stock";
$lang['label_stock']				= "Stock";

$lang['label_files']				= "Files";
$lang['label_images']				= "Images";
$lang['label_quotation']			= "Quotation";
$lang['label_upload_success']		= "Upload file success";

/*	
 *	request
 */
$lang['label_date_required']		= "Date Required";
$lang['label_quantity_request']		= "Quantity Request";
$lang['label_quantity_range']		= "Quantity Range";
$lang['label_status']				= "Status";
$lang['label_comments']				= "Comments";
$lang['label_no_comments']			= "This request have no comments.";
$lang['label_comment']				= "Comment";

$lang['label_hotel']				= "Hotel";
/*	
 *	footer info
 */
$lang['footer_page_render']			= "Page render in ";
$lang['footer_memory_usage']		= "used memory ";

/*	
 *	confirm
 */
$lang['conf_reset_password']		= "Reset Password?";
$lang['conf_delete_user']			= "Are you sure, you want to delete user #";
$lang['conf_delete_company']		= "Are you sure, you want to delete company #";
$lang['conf_delete_project']		= "Are you sure, you want to delete project #";
$lang['conf_delete_category']		= "Are you sure, you want to delete category #";
$lang['conf_delete_product']		= "Are you sure, you want to delete product #";
$lang['conf_delete_request']		= "Are you sure, you want to delete request #";
$lang['conf_delete_tender']			= "Are you sure, you want to delete tender #";
$lang['conf_delete_news']			= "Are you sure, you want to delete news #";
$lang['conf_delete_file']			= "Are you sure, you want to delete this file";
$lang['conf_delete']				= "Delete data?";
$lang['conf_cancel']				= "Cancel change?";
$lang['conf_save']					= "Save data?";

/*	
 *	success
 */
$lang['success_add']				= "Successfully add %s.";
$lang['success_edit']				= "Successfully update %s.";
$lang['success_reset_password']		= "Successfully reset Password! New password has been sent to user";
$lang['success_delete']				= "Successfully delete id %s.";
$lang['success_remove']				= "Successfully remove.";

/*	
 *	alert
 */
$lang['alert_field_required']		= "This field is required!";
$lang['alert_no_selected']			= "No data selected!";
$lang['alert_no_cat_selected']		= "No Category data selected!";
$lang['alert_no_sub_cat_selected']	= "No Sub Category data selected!";
$lang['alert_no_permission']		= "Your level account doesnt reach the requirement.";
$lang['alert_del_own_account']		= "You cannot delete your own account.";
$lang['alert_no_contact_account']	= "No contact in your account, please add contact in your account";
$lang['alert_undefined_data']		= "The data you request is UNDEFINED in the database!";
$lang['alert_user_undefined']		= "Unable to retrieve user details.";
$lang['alert_not_invited']			= "Sory your company is not invited in this tender.";

/*	
 *	email
 */
$lang['email_title_reset_pass']		= "Your password is successfully reset!";
$lang['email_comment']				= "User '%user%' just add a comment : <br>";
$lang['email_status_11']			= "New product request is created by '%user%' and need your approval.<br>";
$lang['email_title_invitation']		= "Invitation from : L`OREAL e-Tender";
$lang['email_invitation']			= "New tender is created in L`OREAL e-Tender <br/>please login using <br/>email : '%email%' <br/> ";
$lang['email_title_set_bid']		= "New tender bid from : ";
$lang['email_set_bid']				= "New tender bid is send in L`OREAL e-Tender <br/>";
$lang['email_title_result']			= "L`OREAL e-Tender %tender_number% result";


/* End of ge_lang.php */
/* Location: ./appl/language/english/ge_lang.php */
