<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource ge_lang.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Aug 28, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */


/*	
 *	login label
 */
$lang['label_login']				= "Login";
$lang['label_forget_password']		= "Forgot your password?";

/*	
 *	header label
 */
$lang['label_welcome']				= "Selamat Datang";
$lang['label_logout']				= "Keluar";
$lang['label_language']				= "Bahasa";

$lang['label_menu']					= "Menu";
$lang['label_content']				= "Main View";
$lang['label_info']					= "Informasi";
$lang['label_time']					= "Waktu";
$lang['label_confirm']				= "Confirm";
$lang['label_close']				= "Close";
$lang['label_more_info']			= "More Info";
$lang['label_module']				= "Module";
$lang['label_class']				= "Class";
$lang['label_function']				= "Function";

$lang['label_personal_info']		= "Informasi Pribadi";
$lang['label_person']				= "Person";
$lang['label_company_info']			= "Informasi Perusahaan";
$lang['label_personal_contact']		= "Kontak Personal";
$lang['label_contact']				= "Kontak";
$lang['label_id']					= "ID";
$lang['label_user_name']			= "Nama Pengguna";
$lang['label_old_password']			= "Password Lama";
$lang['label_old_pass_check']		= "%s tidak cocok";
$lang['label_password']				= "Password";
$lang['label_conf_password']		= "konfirmasi Password";
$lang['label_user_id']				= "ID Pengguna";
$lang['label_role_name']			= "Role Name";
$lang['label_name']					= "Nama";
$lang['label_first_name']			= "Name Depan";
$lang['label_last_name']			= "Nama Belakang";
$lang['label_email']				= "Email";
$lang['label_email_address']		= "Alamat Email";
$lang['label_email_work']			= "Email Kerja";
$lang['label_email_other']			= "Email Lain";
$lang['label_im']					= "Messenger";
$lang['label_department']			= "Departemen";
$lang['label_active']				= "Aktif";
$lang['label_def_lang']				= "Bahasa Standar";
$lang['label_last_login']			= "Login Terakhir";
$lang['label_last_login_on']		= "Login Terakhir pada";

/**
 * End of file ge_lang.php 
 * Location: ./.../.../.../ge_lang.php 
 */