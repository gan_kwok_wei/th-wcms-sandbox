<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2013, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource pages_model.php
 * @copyright Copyright 2011-2013, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Feb 22, 2013
 * @version 
 * @modifiedby 
 * @lastmodified	
 *
 *
 */

/** language **/
$lang['lang_english']		= "english";
$lang['lang_indonesia']		= "indonesia";
$lang['lang_chinese']		= "中国语言";
$lang['lang_thailand']		= "ภาษาไทย";

/** HEADER **/
$lang['home'] = "หน้าแรก";
$lang['our_hotel'] = "โรงแรมของเรา";
$lang['promotions'] = "โปรโมชั่น";
$lang['corporate'] = "องค์กร";
$lang['about_us'] = "เกี่ยวกับเรา";
$lang['help_info'] = "ช่วยเหลือและข้อมูล";

$lang['select_country'] = "เลือกประเทศ";
$lang['select_language'] = "เลือกภาษา";

$lang['search'] = "ค้นหา";
$lang['search_result'] = "ผลการค้นหา";
$lang['search_readmore'] = "อ่านต่อ";

$lang['title_404'] = "404";
$lang['page_not_found'] = "หน้าไม่พบ";
$lang['404_text '] = "ขออภัยหน้าเว็บที่คุณร้องขออาจได้รับการย้ายหรือลบ.";

/** การค้นหาการจอง  **/
$lang['book_hello'] = "สวัสดี.";
$lang['book_city'] = "เมือง";
$lang['book_checkin'] = "เช็คอิน";
$lang['book_ckeckout'] = "เช็คเอาท์";
$lang['book_rooms'] = "ห้อง";
$lang['book_adults'] = "ผู้ใหญ่";
$lang['book_promotion_code'] = "รหัสส่งเสริมการขาย (ตัวเลือก)";
$lang['book_search'] = "ค้นหา";
$lang['booking_managed_confirm'] = "จัดการการจองได้รับการยืนยัน";

/** บนสุดปลายทาง **/
$lang['top_top_destination'] = "ปลายทางยอดนิยม";
$lang['top_from'] = "จาก";
$lang['top_per_Night'] = "คืนต่อ";

/** บันทึกร้อน **/
$lang['whats_whats_hot'] = "มีอะไรน่าสนใจ?";

/** ตามเรา **/
$lang['follow_follow_us'] = "ติดตามเราได้ที่";

/** e-newsletter **/
$lang['e_news_enewsletter'] = "E-จดหมายข่าว";
$lang['e_news_text'] = "สมัครจดหมายข่าวของเราและเป็นคนแรกที่จะได้ยินเกี่ยวกับข้อเสนอล่าสุดของเรา.";
$lang['e_news_your_email'] = "ที่อยู่อีเมลของคุณ";
$lang['e_news_subscribe'] = "สมัครตอนนี้";

$lang['e_news_signup'] = "จดหมายข่าว  ลงชื่อ ขึ้น";

$lang['e_news_email'] = "อีเมล";
$lang['e_news_fname'] = "ชื่อ";
$lang['e_news_lname'] = "นามสกุล";
$lang['e_news_gender'] = "เพศ";
$lang['e_news_gender_m'] = "ชาย";
$lang['e_news_gender_f'] = "หญิง";
$lang['e_news_birth'] = "วันเกิด";
$lang['e_news_phone'] = "โทรศัพท์";
$lang['sign_up'] = "สมัคร";
$lang['invalid_email']= "ส่งอีเมลไม่ถูกต้อง";
$lang['false_field']		= "เลือกอย่างใดอย่างหนึ่ง!";
$lang['false_field_date']	= "วันที่ถูกต้องกรุณา!";
$lang['false_field_room']	= "เลือกประเภทของห้องพัก!";

/** home เท้า **/
$lang['foot_home_find_us'] = "ค้นหาเรา";
$lang['foot_home_see_video'] = "ดูวิดีโอของเรา";
$lang['foot_home_corporate'] = "องค์กร";
$lang['foot_home_guest_review'] = "ทบทวนบุคคล";

/** ส่วนท้าย  **/
$lang['footer_our_hotel'] = "โรงแรมของเรา";
$lang['footer_about_us'] = "เกี่ยวกับเรา";
$lang['footer_help_info'] = "บริการข้อมูล";
$lang['footer_promotion'] = "โปรโมชั่น";
$lang['footer_corporate'] = "องค์กร";
$lang['footer_follow_us'] = "ปฏิบัติตาม สหรัฐ";
$lang['footer_secure_by'] = "ค้ำประกันโดย";
$lang['footer_our_partner'] = "หุ้นส่วนของเรา";
$lang['footer_text'] =  "การใช้งานของ Tune เว็บไซต์โรงแรมฯ ปฏิบัติตามข้อกำหนดและเงื่อนไขของเราและนโยบายความเป็นส่วนตัว <br/> © 2012 โรงแรม Tune สำหรับความละเอียดที่ 1024 x 768. MSIE v.8 หรือ Firefox 3.5 + ";


/** เพจนี้ Hotel  **/

/** แผนที่โรงแรม **/
$lang['map_hotel_tune_hotel'] = "โรงแรม Tune";
$lang['map_hotel_our_hotel'] = "โรงแรมของเรา";
$lang['map_hotel_tune_hotel_so_far'] = "โรงแรม Tune ดังนั้น ไกล";
$lang['map_overview']				= "ภาพรวม";

/** โรงแรมรายการ  **/
$lang['list_hotel_all_hotel'] = "โรงแรมทั้งหมด";
$lang['list_hotel_tune_hotel'] = "โรงแรม Tune";

$lang['book_now'] = "จองตอนนี้";
$lang['see_detail'] = "ดูรายละเอียด";
$lang['read_more'] = "อ่านต่อ";
$lang['lets_go'] = "เถอะ  ไป";

/** โรงแรมรายละเอียด  **/
$lang['det_hotel_hotel_information'] = "ข้อมูลโรงแรม";
$lang['det_hotel_gallery'] = "แกลเลอรี";
$lang['det_hotel_nearby_attraction'] = "สถานที่น่าสนใจใกล้เคียง";
$lang['det_hotel_how_to_here'] = "วิธีการได้รับที่นี่";
$lang['det_hotel_customer_review'] = "ทบทวน ลูกค้า";

/** ข้อมูลโรงแรม  **/
$lang['h_info_address'] = "ที่อยู่";
$lang['h_info_feature'] = "คุณสมบัติ";
$lang['h_info_what_we_provide'] = "สิ่งที่เราให้";
$lang['h_info_addon'] = "เพิ่มเ-มื่อ";
$lang['h_info_package'] = "แพคเกจ";

/** สถานที่ใกล้เคียง **/
$lang['nearby_must_do_think'] = "ต้อง ทำ สิ่งที่อยู่ใน";
$lang['nearby_place_interes'] = "สถานที่ที่น่าสนใจ";
$lang['nearby_appetite'] = "ความอยากอาหาร";

/** โปรโมชั่น  **/
$lang['promo_promotion'] = "โปรโมชั่น";
$lang['promo_sel_cc'] = "กรุณาเลือกประเทศและเมือง";
$lang['promo_sel_country'] = "เลือกประเทศ";
$lang['promo_all_country'] = "ประเทศทั้งหมด";
$lang['promo_sel_city'] = "เลือกเมือง";
$lang['promo_all_city'] = "เลือกเมือง";
$lang['promo_no_found'] = "ไม่มีโปรโมชั่นพบ";
$lang['promo_tune_promo'] = "โรงแรม Tune โปรโมชั่น";

/** คอร์ป **/
$lang['corp_inquiry_form'] = "แบบฟอร์มสอบถาม";
$lang['corp_company_name'] = "ชื่อ บริษัท ";
$lang['corp_contact_person'] = "ผู้ติดต่อ";
$lang['corp_telp'] = "โทรศัพท์";
$lang['corp_mobile'] = "มือถือ";
$lang['corp_addr'] = "ที่อยู่";
$lang['corp_post_code'] = "รหัสไปรษณีย์";
$lang['corp_country'] = "ประเทศ";
$lang['corp_sel_country'] = "เลือกประเทศ";
$lang['corp_fax'] = "หมายเลขโทรสาร";
$lang['corp_email'] = "ที่อยู่ อีเมล์";
$lang['corp_hotel'] = "โรงแรม";
$lang['corp_sel_hotel'] = "เลือกโรงแรม";
$lang['corp_checkin'] = "ตรวจสอบใน วันที่";
$lang['corp_year'] = "ปี";
$lang['corp_month'] = "เดือน";
$lang['corp_day'] = "วัน";
$lang['corp_checkout'] = "วันที่ออก";
$lang['corp_n_night'] = "ไม่มีของคืน";
$lang['corp_no_room'] = "ไม่มีห้อง";
$lang['corp_no_pax'] = "ไม่มีท่าน";
$lang['corp_room_type'] = "ประเภทห้องพัก";
$lang['corp_sel_room'] = "ห้องพักและ";
$lang['corp_remarks'] = "หมายเหตุ";

/** เกี่ยวกับเรา **/
$lang['about_management_team'] = "ทีมผู้บริหาร";
$lang['about_shareholders'] = "ผู้ถือหุ้น";
$lang['about_career'] = "อาชีพ";
$lang['about_concept'] = "แนวคิด";
$lang['about_media'] = "สื่อ";
$lang['about_career_with_tune'] = "ร่วมงานกับโรงแรม Tune";
$lang['about_vacancy'] = "ว่าง";
$lang['about_medi_type'] = "ประเภทสื่อ";

/** ข้อมูลช่วยเหลือ & **/
$lang['help_contact_us'] = "ติดต่อเรา";
$lang['help_booking_cancelation'] = "จองและยกเลิก";
$lang['help_terms_condition'] = "ข้อตกลงและเงื่อนไข";
$lang['help_fee_hotel_schedule'] = "ค่าธรรมเนียมและตาราง";
$lang['help_faq'] = "คำถามที่พบบ่อย";

$lang['help_c_gen_in'] = "เรื่องทั่วไป";
$lang['help_c_adv_tune'] = "โฆษณากับทางโรงแรม Tune";
$lang['help_c_enq_fra'] = "สอบถามในเรื่องของโครงการแฟรนไชส์​​โรงแรม Tune";
$lang['help_c_feed'] = "/ คำติชมแนะนำ";
$lang['help_c_sell'] = "ขาย / เช่าสถานที่ให้บริการกับเรา?";
$lang['help_c_name'] = "ชื่อ";
$lang['help_c_email'] = "ที่อยู่อีเมล";
$lang['help_c_addr'] = "ที่อยู่";
$lang['help_c_post'] = "รหัสไปรษณีย์";
$lang['help_c_countr'] = "ประเทศ";
$lang['help_c_sel_cn'] = "เลือกประเทศ";
$lang['help_c_htl'] = "โรงแรม";
$lang['help_c_sel_htl'] = "เลือกโรงแรม";
$lang['help_c_remarks'] = "หมายเหตุ";
$lang['help_c_submit'] = "ส่ง";

/**
 * End of file pages_lang.php
 */