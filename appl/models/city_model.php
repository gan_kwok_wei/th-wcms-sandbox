<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource city_model.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 9, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class City_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
//		echo "<!--";
//		var_dump($_POST);
//		echo "-->";
	}
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_city_list($country_id = NULL) {
    	if($country_id != NULL) {
    		if(is_array($country_id)) {
    			$this->db->where_in('country.country_id', $country_id);
    		} else {
    			$this->db->where('country.country_id', $country_id);
    		}
    	}
    	
    	$this->db->select('city.*, country.full_name country_name')
    				->from('city')
    				->join('country', 'country.country_id = city.country_id')
    //				->where(array('city.status >=' => 1))
    				->order_by('country.country_id, city.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_city($city_id) {
		return $this->db->get_where('city', array('city_id' => $city_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_city() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('city_id', $id);
	    	$this->db->update('city', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'city'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_city() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('city_name', lang('label_city'), 'trim|required|min_length[5]|xss_clean');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
					$city['city_name'] = $_POST['city_name'];
					$city['country_id'] = $_POST['country_id'];
					$city['tags'] = $_POST['tags'];
					$city['sort'] = $_POST['sort'];
	    			$city['created_id'] = get_user_id();
	    			
					$this->db->insert('city', $city);
					$city_id = $this->db->insert_id();
					
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['content_text_type'] = 'city';
							$text['parent_id'] = $city_id;
							$text['title'] = $_POST['title'][$lang_k];
//							$text['icon'] = $_POST['icon'][$lang_k];
//							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
//							$text['content'] = $_POST['content'][$lang_k];
//							$text['icon_1'] = $_POST['icon_1'][$lang_k];
//							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
						}
					}
					set_success_message(sprintf(lang('success_add'), 'city'));
					redirect('system/city');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			//unset($_POST['content_id']);
	    			$city['city_name'] = $_POST['city_name'];
					$city['country_id'] = $_POST['country_id'];
					$city['tags'] = $_POST['tags'];
					$city['sort'] = $_POST['sort'];
					$city['status'] = $_POST['status'];
					$city['modified_id'] = get_user_id();
	    			$city['modified_time'] = date('Y-m-d H:i:s');	 
	    			
	    			$this->db->where('city_id', $_POST['city_id']);   			
	    			$this->db->update('city', $city);
	    			
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['title'] = $_POST['title'][$lang_k];
//							$text['icon'] = $_POST['icon'][$lang_k];
//							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
//							$text['content'] = $_POST['content'][$lang_k];
//							$text['icon_1'] = $_POST['icon_1'][$lang_k];
//							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
						
							if($_POST['content_text_id'][$lang_k] != '0') {
								$this->db->where(array('content_text_id' => $_POST['content_text_id'][$lang_k]));
								$this->db->update('content_text', $text);
							} else {
								$text['parent_id'] = $_POST['city_id'];
								$text['content_text_type'] = 'city';
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('content_text', $text);
							}
							
						}
					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'content'));
					redirect('system/city/city_edit/' . $_POST['city_id']);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
}

/**
 * End of file city_model.php 
 * Location: ./.../.../.../city_model.php 
 */