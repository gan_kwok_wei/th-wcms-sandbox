<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource culinary_model.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author aron.jahja
 * @package 
 * @subpackage	
 * @since Oct 9, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Culinary_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
//		echo "<!--";
//		var_dump($_POST);
//		echo "-->";
	}
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_culinary_list($city_id = NULL) {
    	if($city_id != NULL) {
    			$this->db->where('city.city_id', $city_id);
    	}
    	
    	$this->db->select('culinary.*, city.city_name, country.full_name country_name')
    				->from('culinary')
    				->join('city', 'city.city_id = culinary.city_id')
					->join('country', 'country.country_id = city.country_id')
    //				->where(array('culinary.status >=' => 1))
    				->order_by('city.city_id, culinary.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_culinary($culinary_id) {
		return $this->db->get_where('culinary', array('culinary_id' => $culinary_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_culinary() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('culinary_id', $id);
	    	$this->db->update('culinary', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'culinary'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_culinary() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		/* bingung di lang label_culinary*/
		$this->form_validation->set_rules('culinary_name', lang('label_culinary'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('city_id', lang('label_city'), 'required');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
					$culinary['culinary_name'] = $_POST['culinary_name'];
					$culinary['city_id'] = $_POST['city_id'];
					$culinary['tags'] = $_POST['tags'];
					$culinary['sort'] = $_POST['sort'];
	    			$culinary['created_id'] = get_user_id();
	    			
					$this->db->insert('culinary', $culinary);
					$culinary_id = $this->db->insert_id();
					
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['content_text_type'] = 'culinary';
							$text['parent_id'] = $culinary_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
//							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
//							$text['content'] = $_POST['content'][$lang_k];
//							$text['icon_1'] = $_POST['icon_1'][$lang_k];
//							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
						}
					}
					set_success_message(sprintf(lang('success_add'), 'culinary'));
					redirect('system/culinary');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			//unset($_POST['content_id']);
	    			$culinary['culinary_name'] = $_POST['culinary_name'];
					$culinary['city_id'] = $_POST['city_id'];
					$culinary['tags'] = $_POST['tags'];
					$culinary['sort'] = $_POST['sort'];
					$culinary['status'] = $_POST['status'];
					$culinary['modified_id'] = get_user_id();
	    			$culinary['modified_time'] = date('Y-m-d H:i:s');	
	    			
	    			$this->db->where('culinary_id', $_POST['culinary_id']);    			
	    			$this->db->update('culinary', $culinary);
	    			
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
//							$this->db->where(array('parent_id' => $_POST['culinary_id'], 'content_text_type' => 'culinary', 'language' => $_POST['language'][$lang_k]));
							$text = array();
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
//							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
//							$text['content'] = $_POST['content'][$lang_k];
//							$text['icon_1'] = $_POST['icon_1'][$lang_k];
//							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							
							if($_POST['content_text_id'][$lang_k] != '0') {
								$this->db->where(array('content_text_id' => $_POST['content_text_id'][$lang_k]));
								$this->db->update('content_text', $text);
							} else {
								$text['parent_id'] = $_POST['culinary_id'];
								$text['content_text_type'] = 'culinary';
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('content_text', $text);
							}
							
						}
					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'culinary'));
					redirect('system/culinary/culinary_edit/' . $_POST['culinary_id']);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
}

/**
 * End of file culinary_model.php 
 * Location: ./.../.../.../culinary_model.php 
 */