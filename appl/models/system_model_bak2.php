<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/********************************************************************/
/* File Name		: ./appl/models/system_model.php				*/
/* Module			: 												*/
/* Revise Number	: 01											*/
/* Created By		: Budi Laxono									*/
/* Created At		: 13-06-2010									*/
/********************************************************************/
/* Modified By		: Unknown										*/
/* Modified At		: Unknown										*/
/* Modification		: Unknown										*/
/********************************************************************/


class System_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $user_id
	 */
	public function get_user_menu($user_id = NULL, $parent_id = 0) {
		$sql = 'SELECT menu_id, menu_name, menu_info, link, enabled, parent_id  FROM menu WHERE enabled=\'y\' AND (permission_params LIKE \'%'.get_role().'%\' OR permission_params = \'*\') AND parent_id = ' . $parent_id . ' ORDER BY sort ASC';
		
		$rows = $this->db->query($sql);
		$results = $rows->result_array();
		if(count($results) > 0) {
			for($i = 0; $i < count($results); $i++) {
				$child = $this->get_user_menu($user_id, $results[$i]['menu_id']);
				if(count($child) > 0) {
					$results[$i]['items'] = $child;
				}
			}
		}
		
		return $results;
	}
	
	/**
	 * @param $user_id
	 * @return Object user data
	 */
	function get_user($user_id = NULL) {
		if($user_id == NULL) {
			$this->db->select("user_id, user.first_name || ' ' || user.last_name as name, job_title, user.email, user.phone_work, user.company_id, security_role.name AS role")
				->from('users')
				->join('security_role', 'security_role.role_id = user.role_id')
				->order_by('first_name');
				
			return $this->db->get();
			
		} else {
			return $this->db->get_where('users', array('user_id' => $user_id));
		}
	}
	
    /**
     * Enter description here ...
     * @return multitype:
     */
    public function get_role($role_id) {
    	return $this->db->get_where('security_role', array('role_id' => $role_id));
		
    }
    
    /**
     * Enter description here ...
     * @return multitype:
     */
    public function get_permission($permission_id) {
    	return $this->db->get_where('security_permission', array('permission_id' => $permission_id));
		
    }
    
    public function _get_user($where) {
    	
		/* @var unknown_type */
    	$this->db->order_by("user_name", "asc");
		return $this->db->get_where('users', $where);
		
    } 
    
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    public function get_forgotten_user($email) {
    	
		$user = $this->db->get_where('users', array('active' => 1, 'email' => $email));
		if($user->num_rows() < 1) {
			return FALSE;
		} else {
			return $user->row();
		}
    } 
    
	/**
	 * Enter description here ...
	 * @return string
	 */
	function suggest_password() {
		$pwchars = "abcdefhjmnpqrstuvwxyz23456789ABCDEFGHJKLMNPQRSTUVWYXZ";
	    $passwordlength = 8;    // do we want that to be dynamic?  no, keep it simple :)
	    $password = '';
	
		for ($i = 0; $i < $passwordlength; $i++) {
			$password .= $pwchars{rand(0, strlen($pwchars))};
		}
		return $password;
	}

	/**
	 * @param $user_id
	 * @return unknown_type
	 */
	function reset_password($user_id) {
		$pass = $this->suggest_password();
		
		$this->db->where('user_id', $user_id);
		$this->db->update('users', array('password' => $this->encrypt->get_key($pass)));
		
		$user = $this->get_user($user_id)->row();
		
		$to = $user->email;
		
		$subject = lang('label_reset_password');
		
		if($this->config->item('mailtype') == 'html') {
			$content = '<table width="100%" border="0" align="center" style="font-size: inherit;">'
				. '<tr><td> ' . lang('label_user_name') . ' : ' . $user->user_name  .' </td></tr>'
				. '<tr><td> ' . lang('label_password') . ' : ' . $pass . '</td></tr></table>'
				. '</body>';
		} else {
			$content = lang('email_title_reset_pass') . "\r\n"
					. lang('label_user_name') . ' : ' . $user->user_name . "\r\n"
					. lang('label_password') . ' : ' . $pass;
		}
		$str_msg = $this->_mail_template(lang('label_reset_password') . ' : ' . $user->user_name, $user->first_name . ' ' . $user->last_name, $content);
		
		$attach = array();
		$this->_send_mail_notification($to, $subject, $str_msg, $attach, lang('label_reset_password'));
		
	}
	
    /**
     * Enter description here ...
     * @return multitype:
     */
    public function get_parent_role() {
    	$return = array();
    	
		/* @var unknown_type */
//    	$this->db->order_by("name", "asc");
		$rows = $this->db->get('security_role');
		
		foreach($rows->result() as $row) {
//			if(get_role() <= $row->role_id) {
				$return[$row->role_id] = $row->name;
//			}
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    public function get_parent_user() {
    	$return = array();
    	
		foreach($this->_get_user(array('active' => 1))->result() as $row) {
			$return[$row->user_id] = $row->first_name.' '.$row->last_name;
		}
		return $return;
    } 

    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    public function get_parent_user_by_role($role_id) {
    	$return = array();
    	
		foreach($this->_get_user(array('active' => 1, 'role_id' => $role_id))->result() as $row) {
			$return[$row->user_id] = $row->first_name.' '.$row->last_name;
		}
		return $return;
    } 
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_parent_company($category = NULL) {
    	$return = array();
    	
		/* @var unknown_type */
    	$this->db->order_by("company_name", "asc");
    	if($category != NULL) {
    		$this->db->where('category', $category);
    	}
    	$this->db->where('status', 1);
		$rows = $this->db->get('company');
		
		foreach($rows->result() as $row) {
			$return[$row->company_id] = $row->company_name;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_parent_country() {
    	$return = array('' => '---');
    	
		/* @var unknown_type */
    	$this->db->order_by("name", "asc");
		$rows = $this->db->get('country');
		
		foreach($rows->result() as $row) {
			$return[$row->country_id] = $row->name;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_parent_city() {
    	$return = array('' => '---');
    	
		/* @var unknown_type */
    	$this->db->select('city.*, country.name country_name')
    			->from('city')
    			->join('country', 'country.country_id = city.country_id')
    			->order_by('country.country_id, city.sort');
		$rows = $this->db->get();
		
		$country = '';
		foreach($rows->result() as $row) {
			if($country != $row->country_name) {
				$country = $row->country_name;
			}
			$return[$country][$row->city_id] = $row->city_name;
		}
		return $return;
    }
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_parent_hotel() {
    	$return = array('' => '---');
    	
		/* @var unknown_type */
    	$this->db->select('hotel.*, country.name country_name')
    			->from('hotel')
    			->join('city', 'city.city_id = hotel.city_id')
    			->join('country', 'country.country_id = city.country_id')
    			->order_by('country.country_id, hotel.sort');
		$rows = $this->db->get();
		
		$country = '';
		foreach($rows->result() as $row) {
			if($country != $row->country_name) {
				$country = $row->country_name;
			}
			$return[$country][$row->hotel_id] = $row->hotel_name;
		}
		return $return;
    }
    
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    public function get_parent_menu() {
    	$return = array();
    	
    	$category = $this->db->get('menu_category');
		foreach($category->result() as $row) {
			$return[$row->menu_category_id] = $row->menu_category_name;
		}
		return $return;
    } 
    
	/**
	 * Enter description here ...
	 */
	function get_system_config($key) {
		$return = array();
		$this->db->order_by("val");
		$rows = $this->db->get_where('system_config', array('key' => $key));
		
		foreach($rows->result() as $row) {
			$return[] = $row->val;
		}
		return $return;
	}
	
	/**
	 * Enter description here ...
	 */
	function get_content_hits() {
		$this->db->limit(3);
		$this->db->order_by('hits', 'desc');
		return $this->db->get_where('content', array('hits >' => 0));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_hotel_hits() {
		$this->db->limit(3);
		$this->db->order_by('hits', 'desc');
		return $this->db->get_where('hotel', array('hits >' => 0));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_promo_hits() {
		$this->db->limit(3);
		$this->db->order_by('hits', 'desc');
		return $this->db->get_where('promo', array('hits >' => 0));
		
	}
	
	/**
	 * Enter description here ...
	 */
	function get_outstanding_content() {
		if(get_role() > 3) {
			$this->db->where('created_id', get_user_id());
		}
		
    	$this->db->select('content.*, content_category.title category_title, users.first_name, users.last_name')
    				->from('content')
    				->join('content_category', 'content_category.content_category_id = content.content_category_id')
    				->join('users', 'users.user_id = content.created_id')
    				->where(array('content.status' => 0))
    				->order_by('content.content_category_id, content.sort');
    	return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 */
	function get_outstanding_hotel() {
		if(get_role() > 3) {
			$this->db->where('created_id', get_user_id());
		}
		
    	$this->db->select('hotel.*, country.full_name country_name, city.city_name, users.first_name, users.last_name')
    				->from('hotel')
    				->join('city', 'city.city_id = hotel.city_id')
    				->join('country', 'country.country_id = city.country_id')
					->join('users', 'users.user_id = hotel.created_id')
    				->where(array('hotel.status' => 0))
    				->order_by('country.country_id, city.sort, hotel.sort');
    	return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 */
	function get_outstanding_promo() {
		if(get_role() > 3) {
			$this->db->where('created_id', get_user_id());
		}
		
    	$this->db->select('promo.*, city.city_name, hotel.hotel_name, users.first_name, users.last_name')
    				->from('promo')
    				->join('city', 'city.city_id = promo.city_id')
    				->join('hotel', 'hotel.hotel_id = promo.hotel_id')
					->join('users', 'users.user_id = promo.created_id')
    				->where(array('promo.status' => 0))
    				->order_by('city.city_id, hotel.hotel_id, hotel.sort');
    	return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 */
	function get_outstanding_banner() {
		if(get_role() > 3) {
			$this->db->where('created_id', get_user_id());
		}
		
    	$this->db->select('banner.*, banner_category.title category_title, users.first_name, users.last_name')
    				->from('banner')
    				->join('banner_category', 'banner_category.banner_category_id = banner.banner_category_id')
					->join('users', 'users.user_id = banner.created_id')
    				->where(array('banner.status' => 0))
    				->order_by('banner.banner_category_id, banner.sort');
    	return $this->db->get();
		
	}
	
	/**
	 * Enter description here ...
	 */
	function get_registered_lang($rev = FALSE) {
		return $this->_registered_language($rev);
	}
	
	/**
	 * Enter description here ...
	 */
	function get_registered_country($rev = FALSE) {
		return $this->_registered_country($rev);
	}
	
	/**
	 * Enter description here ...
	 */
	function get_content_text($content_text_type, $parent_id, $language = "english") {
		return $this->db->get_where('content_text', array('content_text_type' => $content_text_type, 'parent_id' => $parent_id, 'language' => $language));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_feature_ref_text($feature_ref_id, $language = "english") {
		return $this->db->get_where('feature_ref_text', array('feature_ref_id' => $feature_ref_id, 'language' => $language));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_admin_theme() {
		$rows = $this->db->get_where('system_config', array('key' => 'admin_theme'));
		
		if($rows->num_rows > 0) {
			return $rows->row()->val;
		} else {
			return 'default';	
		}
	}
	
	/**
	 * Enter description here ...
	 */
	function get_publish_theme() {
		$rows = $this->db->get_where('system_config', array('key' => 'publish_theme'));
		
		if($rows->num_rows > 0) {
			return $rows->row()->val;
		} else {
			return 'default';	
		}
	}
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    public function update_measurement() {
    	$sql = "TRUNCATE TABLE measurement";
    	$this->db->query($sql);
    	
		/* @var unknown_type */
    	if(isset($_POST['units']))
		foreach($_POST['units'] as $unit) {
			$this->db->insert('measurement', array('unit' => strtolower($unit)));
		}
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    public function get_config() {
    	return $this->db->get('system_config');
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function save_config() {
    	$sql = "TRUNCATE TABLE system_config";
    	$this->db->query($sql);
    	/*
    	echo "<!--";
    	var_dump($_POST);
    	echo "-->";
		 @var unknown_type */
    	if(isset($_POST['key'])) {
			foreach($_POST['key'] as $k => $v) {
				$this->db->insert('system_config', array('type' => $_POST['type'][$k], 'key' => $v, 'val' => $_POST['val'][$k], 'active' => $_POST['active'][$k]));
			}
    	}
    	redirect('system/dashboard/config');
		exit;
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    public function get_country_list() {
    	return $this->db->get('country');
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:
     */
    public function get_country($country_id) {
    	return $this->db->get_where('country', array('country_id' => $country_id));
		
    }
	
    function get_x_country() {
		$sql = "SELECT * "
			.  "FROM country_ref "
			.  "WHERE `letter_code` NOT IN(SELECT letter_code FROM country)";
		$rows = $this->db->query($sql);
		$return = array('' => '---');
		foreach($rows->result() as $row) {
			$return[$row->country_id] = $row->name;
		}
		return $return;
	}
	
	/**
	 * Enter description here ...
	 * @param int $country_id
	 */
	function get_country_language($country_id) {
		$sql = "SELECT * "
			.  "FROM country_language "
			.  "JOIN system_config ON(system_config.type = 'language' AND system_config.key = country_language.language_id) "
			.  "WHERE country_language.country_id = $country_id";
		return $this->db->query($sql);
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 */
	function get_x_language($country_id) {
		$sql = "SELECT * "
			.  "FROM system_config "
			.  "WHERE `type` = 'language' "
			.  "  AND `key` NOT IN(SELECT language_id FROM country_language WHERE country_id = '$country_id')";
		return $this->db->query($sql);
	}
	
	function save_country() {
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		switch ($_POST['mode']) {
			case 'add':
				unset($_POST['mode']);
				
				$this->form_validation->set_rules('x_country_id', 'Country', 'required');
				
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					$sql = "INSERT INTO country(`name`, `full_name`, `type`, `subtype`, `sovereignty`, `capital`, `currency_code`, `currency_name`, `phone_code`, `letter_code`, `letter_code_alt`, `iso_number`, `code`) "
						.  "SELECT `name`, `full_name`, `type`, `subtype`, `sovereignty`, `capital`, `currency_code`, `currency_name`, `phone_code`, `letter_code`, `letter_code_alt`, `iso_number`, `code` FROM `country_ref` WHERE `country_id` = " . $_POST['x_country_id'];
					$this->db->query($sql);
					$id = $this->db->insert_id();
					
					foreach($text['title'] as $lang_k => $lang_v) {
						if($text['title'][$lang_k] != '') {
							$data['country_id'] = $id;
							$data['title'] = $text['title'][$lang_k];
							$data['language'] = $text['language'][$lang_k];
							$this->db->insert('country_text', $data);
						}
					}
					
					set_success_message(sprintf(lang('success_add'), 'country'));
					redirect('system/dashboard/country_edit/' . $id);
					exit;
				}
			break;
			case 'edit':
				unset($_POST['mode']);
				$this->form_validation->set_rules('country_id', 'Country', 'required');
				
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					
					if($_POST['x_country_id'] != '') {
						$result = $this->db->get_where('country_ref', array('country_id' => $_POST['x_country_id']));
						$data = $result->row();
						$update['name'] = $data->name; 
						$update['full_name'] = $data->full_name; 
						$update['type'] = $data->type;
						$update['subtype'] = $data->subtype;
						$update['sovereignty'] = $data->sovereignty;
						$update['capital'] = $data->capital;
						$update['currency_code'] = $data->currency_code;
						$update['currency_name'] = $data->currency_name;
						$update['phone_code'] = $data->phone_code;
						$update['letter_code'] = $data->letter_code;
						$update['letter_code_alt'] = $data->letter_code_alt;
						$update['iso_number'] = $data->iso_number;
						$update['code'] = $data->code;
						$this->db->update('country', $update, 'country_id = ' . $_POST['country_id']);
					}
					
    				$text['language'] = $_POST['language'];
    				$text['country_text_id'] = $_POST['country_text_id'];
    				$text['title'] = $_POST['title'];
    				unset($_POST['language']);
    				unset($_POST['country_text_id']);
    				unset($_POST['title']);
    				
					foreach($text['title'] as $lang_k => $lang_v) {
						$data = array();
						if($text['title'][$lang_k] != '') {
							$data['title'] = $text['title'][$lang_k];
							
							if($text['country_text_id'][$lang_k] != '0') {
								$this->db->where(array('country_text_id' => $text['country_text_id'][$lang_k]));
								$this->db->update('country_text', $data);
							} else {
								$data['country_id'] = $_POST['country_id'];
								$data['language'] = $text['language'][$lang_k];
								$this->db->insert('country_text', $data);
							}
							
						} else {
							$this->db->where(array('country_text_id' => $text['country_text_id'][$lang_k]));
							$this->db->delete('country_text');
						}
						
					}
					
					set_success_message(sprintf(lang('success_add'), 'country'));
					redirect('system/dashboard/country_edit/' . $_POST['country_id']);
					exit;
				}
			break;
		}
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function delete_country() {
		$this->db->where(array('country_id' => $_POST['country_id']));
		$this->db->delete('country');
		
		$this->db->where(array('country_id' => $_POST['country_id']));
		$this->db->delete('country_language');
		echo "{error: '',msg: 'Successful Delete'}";
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function country_add_language($country_id, $language_id, $default = 0) {
		$this->db->insert('country_language', array('country_id' => $country_id, 'language_id' => $language_id, 'default' => $default));
		return $this->db->insert_id();
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function country_remove_language($country_id, $language_id) {
		$this->db->where(array('country_id' => $country_id, 'language_id' => $language_id));
		return $this->db->delete('country_language');
	}
	
	function update_country_language() {
		$this->db->update('country_language', array('default' => 0), 'country_id = ' . $_POST['country_id']);
		
		$this->db->where(array('country_id' => $_POST['country_id'], 'language_id' => $_POST['default']));
		$this->db->update('country_language', array('default' => 1));
	}
	
    /**
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    public function get_meta_tags() {
    	return $this->db->get('meta_tags');
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_available_tags($type = NULL) {
    	$tags = array();
    	if($type == NULL) {
			$result = $this->db->get('meta_tags');
    	} else {
			$result = $this->db->get_where('meta_tags', array('tag_type' => $type));
    	}
		foreach($result->result() as $row) {
			$tags[] = '"' . $row->tag_name . '"';
		}
		
		return implode(', ', $tags);
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function save_meta_tags() {
		$sql = "TRUNCATE TABLE meta_tags";
		$this->db->query($sql);
/*     	echo "<!--";
    	var_dump($_POST);
    	echo "-->";
*/		/* @var unknown_type */
		if(isset($_POST['tag_name'])) {
		$value = array();
			foreach($_POST['tag_name'] as $k => $v) {
				if($_POST['active'][$k] == 1) {
					$value[] = "('" . $_POST['tag_type'][$k] . "', '" . $v . "')";
				}
			}
			
			echo $sql = 'INSERT INTO meta_tags(tag_type, tag_name) VALUES ' . implode(', ', $value);
			$this->db->query($sql);
		}
	} 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    public function get_menu_list() {
/*    	$this->db->select('menu.*, menu_category.title category_title')
    				->from('menu')
    				->join('menu_category', 'menu_category.menu_category_id = menu.menu_category_id')
    				->where(array('menu_category.status <=' => 1, 'menu.status <=' => 1))
    				->order_by('menu.menu_category_id, menu.sort');
*/		return $this->db->get('menu');
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    public function get_menu_category_list() {
    	return $this->db->get('menu_category');
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_menu($menu_id) {
		return $this->db->get_where('menu', array('menu_id' => $menu_id));
   	
    } 
    
	/**
	 * Enter description here ...
	 */
	function get_menu_text($menu_id, $language = "english") {
		return $this->db->get_where('menu_text', array('menu_id' => $menu_id, 'language' => $language));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_country_text($country_id, $language = "english") {
		return $this->db->get_where('country_text', array('country_id' => $country_id, 'language' => $language));
	}
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_menu_category($menu_category_id) {
		return $this->db->get_where('menu_category', array('menu_category_id' => $menu_category_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_menu() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('menu_id', $id);
	    	$this->db->update('menu', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'menu'));
    	
    }
    
	/**
	 * Enter description here ...
	 */
	function save_list_menu_category() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('menu_category_id', $id);
	    	$this->db->update('menu_category', array('sort' => $val, 'parent_id' => $_POST['parent_id'][$id]));
		}
		set_success_message(sprintf(lang('success_edit'), 'menu category'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_menu() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('menu_name', lang('label_menu_name'), 'trim|required|xss_clean');
//		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		
//		$category = $this->db->get_where('menu_category', array('menu_category_id' => $_POST['menu_category_id']));
//		$res_category = $category->row();
//		$_POST['language'] = $res_category->language;
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				$text['language'] = $_POST['language'];
    				$text['title'] = $_POST['title'];
    				unset($_POST['language']);
    				unset($_POST['title']);
    				
					$this->db->insert('menu', $_POST);
					$parent_id = $this->db->insert_id();
					
					foreach($text['title'] as $lang_k => $lang_v) {
						if($text['title'][$lang_k] != '') {
							$data['menu_id'] = $parent_id;
							$data['title'] = $text['title'][$lang_k];
							$data['language'] = $text['language'][$lang_k];
							$this->db->insert('menu_text', $data);
						}
					}
					
    				set_success_message(sprintf(lang('success_add'), 'menu'));
					$ref = 'system/dashboard/menu_edit/' . $parent_id;
					redirect($ref);
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
    				$text['language'] = $_POST['language'];
    				$text['menu_text_id'] = $_POST['menu_text_id'];
    				$text['title'] = $_POST['title'];
    				unset($_POST['language']);
    				unset($_POST['menu_text_id']);
    				unset($_POST['title']);
    				
					foreach($text['title'] as $lang_k => $lang_v) {
						$data = array();
						if($text['title'][$lang_k] != '') {
							$data['title'] = $text['title'][$lang_k];
							
							if($text['menu_text_id'][$lang_k] != '0') {
								$this->db->where(array('menu_text_id' => $text['menu_text_id'][$lang_k]));
								$this->db->update('menu_text', $data);
							} else {
								$data['menu_id'] = $_POST['menu_id'];
								$data['language'] = $text['language'][$lang_k];
								$this->db->insert('menu_text', $data);
							}
							
						} else {
							$this->db->where(array('menu_text_id' => $text['menu_text_id'][$lang_k]));
							$this->db->delete('menu_text');
						}
						
					}
					
	    			$this->db->where('menu_id', $_POST['menu_id']);
	    			$ref = 'system/dashboard/menu_edit/' . $_POST['menu_id'];
	    			unset($_POST['menu_id']);
	    			$this->db->update('menu', $_POST);
	    			
	    			
	    			set_success_message(sprintf(lang('success_edit'), 'menu'));
					redirect($ref);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function delete_menu() {
		$this->db->where('menu_id', $_POST['menu_id']);
		$this->db->delete('menu');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'menu') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_menu_category() {
		$this->db->where('menu_category_id', $_POST['menu_category_id']);
		$this->db->delete('menu_category');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'menu category') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function save_menu_category() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('menu_category_name', lang('label_name'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('menu_category', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'menu category'));
    				redirect('system/dashboard/menu_category');
					exit;
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('menu_category_id', $_POST['menu_category_id']);
	    			unset($_POST['menu_category_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('menu_category', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'menu category'));
					
				}
    		break;
    	}
    }
    
	/**
	 * Enter description here ...
	 */
	function upload_file($table, $id) {
		
		$config_file['allowed_types'] = '*';
		$config_file['upload_path'] = $this->config->item('ge_upload_path') . "files/$table/";
		$config_file['max_size']	= '4000';
		$this->load->library('upload', $config_file);
		$this->upload->display_errors('','');
		
		if (!$this->upload->do_upload('file_ref')) {
			return "{file: '', id: '', error: '" . $this->upload->display_errors() . "',msg: ''}";
		} else {
			$file = $this->upload->data();
			$this->db->insert($table . '_file', array($table . '_id' => $id, 'file_name' => $file['file_name'], 'created_id' => get_user_id()));
			$pk = $this->db->insert_id();
			
			return "{file: '" . $file['file_name'] . "', id: '$pk', error: '',msg: '" . lang('label_upload_success') . "'}";
		}
	}
	
	/**
	 * Enter description here ...
	 */
	function delete_file() {
//		var_dump($_POST);
		
		if(!isset($_POST['table'])) {
			return "{error: 'failed', msg: ''}";
		}
		
		$file = $this->db->get_where($_POST['table'] . '_file', array($_POST['table'] . '_file_id' => $_POST['id']));

		if($file->num_rows() == 0) {
			return "{error: '" . lang('alert_undefined_data') . "',msg: ''}";
		}
		if (!unlink($this->config->item('ge_upload_path') . "files/" . $_POST['table'] . "/" . $file->row()->file_name)) {
			return "{error: 'failed', msg: ''}";
		} else {
			$this->db->where(array($_POST['table'] . '_file_id' => $_POST['id']));
			$this->db->delete($_POST['table'] . '_file');
			
			return "{error: '', msg: '" . lang('success_remove') . "'}";
		}
	}
	
	
	
}

/* End of file layout_model.php */
/* Location: ./appl/models/layout_model.php */