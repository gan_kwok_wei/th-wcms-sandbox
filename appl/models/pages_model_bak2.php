<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource pages_model.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author syaiful.isnaini
 * @package 
 * @subpackage	
 * @since Jul 10, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */


class Pages_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	
	var $def_lang;
	var $lang;
		
	function __construct() {
		parent::__construct();
		$array_lang = $this->_registered_language();
		$this->def_lang = $array_lang[$this->_get_def_language()];
		$this->lang = $array_lang[$this->_get_language()];
		
//		echo "<!--";
//		var_dump($_POST);
//		echo "-->";
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $country_id
	 */
	function get_country_all(){
		$this->db->order_by('name');
		return $this->db->get('country_ref');
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $country_id
	 */
	function get_country($country_id = NULL){
		if($country_id == NULL){
			$sql = "SELECT country_id, name FROM country ORDER BY country_id";
		}
		else {
			$sql = "SELECT country_id, name FROM country WHERE country_id='" . $country_id . "'";
			}
		return $this->db->query($sql);
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $country_id
	 */
	function get_city_by_country($country_id){
		$sql = "SELECT city_id, city_name, title FROM city WHERE country_id='" . $country_id . "' AND status='1' ORDER BY sort";
		return $this->db->query($sql);
	}
	
	/**
	 *	HOTEL
	 */
	function get_hotel_by_country($country_id = NULL){
		$where = '';
		if($country_id != NULL) {
			$where = "city.country_id = $country_id AND ";
		}
		
		$this->db->select('hotel.hotel_id, city.country_id, city.city_id, hotel_name, text_def.title def_title, text_def.icon def_icon, text.title text_title, text.icon text_icon')
				->from('hotel')
				->join('city', 'city.city_id = hotel.city_id')
				->join('hotel_text text_def', "text_def.hotel_id = hotel.hotel_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('hotel_text text', "text.hotel_id = hotel.hotel_id AND text.language = '" . $this->lang . "'", 'left')
				->where($where . "hotel.status >= 1 AND (hotel.publish_date_start <= '" . date('Y-m-d') . "' OR hotel.publish_date_start = '0000-00-00') AND (hotel.publish_date_end >= '" . date('Y-m-d') . "' OR hotel.publish_date_end = '0000-00-00')");
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel_by_id($hotel_id = NULL){
		$sql = "SELECT A.hotel_id,
					A.country_id,
					A.city_id,
					A.hotel_name,
					B.title,
					B.icon,
					B.address,
					B.feature_text,
					B.note,
					A.phone1,
					A.phone2,
					A.fax,
					A.latitude,
					A.longitude
					FROM hotel A
					LEFT JOIN hotel_text B ON A.hotel_id=B.hotel_id
				WHERE A.hotel_id='" . $hotel_id . "'
					AND A.language='" . $this->lang . "'";
		return $this->db->query($sql);
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel($hotel_id) {
		$this->db->select('hotel.*,
							text_def.title def_title, text_def.icon def_icon, text_def.address def_address, text_def.feature_text def_feature_text, text_def.note def_note, text_def.teaser def_teaser, 
							text.title text_title, text.icon text_icon, text.address text_address, text.feature_text text_feature_text, text.note text_note, text.teaser text_teaser')
				->from('hotel')
				->join('hotel_text text_def', "text_def.hotel_id = hotel.hotel_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('hotel_text text', "text.hotel_id = hotel.hotel_id AND text.language = '" . $this->lang . "'", 'left')
				->where("hotel.status >= 1 AND (hotel.publish_date_start <= '" . date('Y-m-d') . "' OR hotel.publish_date_start = '0000-00-00') AND (hotel.publish_date_end >= '" . date('Y-m-d') . "' OR hotel.publish_date_end = '0000-00-00') AND hotel.hotel_id='" . $hotel_id . "'");
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel_hits($hotel_id){
		$this->db->select('hits')
				->from('hotel')
				->where("hotel_id = '" . $hotel_id . "'");
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function set_hotel_hits($hotel_id){
		$sql = "UPDATE hotel SET hits = hits+1 WHERE hotel_id = '$hotel_id'";
		$this->db->query($sql);
//		return $this->db->update('hotel', array('hits' => $new_hits), array('hotel_id' => $hotel_id));
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel_top_destination() {
		$this->db->select('hotel.*,
							destination.price, destination.package,
							text_def.title def_title, text_def.icon def_icon, text_def.address def_address, text_def.feature_text def_feature_text, text_def.feature_text_1 def_feature_text_1, text_def.note def_note, 
							text.title text_title, text.icon text_icon, text.address text_address, text.feature_text text_feature_text, text.feature_text_1 text_feature_text_1, text.note text_note,
							country.currency_code currency_code')
				->from('destination')
				->join('hotel', "destination.hotel_id = hotel.hotel_id")
				->join('hotel_text text_def', "text_def.hotel_id = hotel.hotel_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('hotel_text text', "text.hotel_id = hotel.hotel_id AND text.language = '" . $this->lang . "'", 'left')
				->join('city', "hotel.city_id = city.city_id", 'left')
				->join('country', "city.country_id = country.country_id", 'left')
				->where("destination.status >= 1 AND (destination.publish_date_start <= '" . date('Y-m-d') . "' OR destination.publish_date_start = '0000-00-00') AND (destination.publish_date_end >= '" . date('Y-m-d') . "' OR destination.publish_date_end = '0000-00-00')")
				->limit(5);
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel_gallery($hotel_id) {
		$this->db->order_by('sort');
		return $this->db->get_where('hotel_gallery', array('hotel_id' => $hotel_id, 'status >=' => 1));
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel_room($hotel_id) {
		$this->db->order_by('sort');
		return $this->db->get_where('room', array('hotel_id' => $hotel_id, 'status >=' => 1, 'language' => $this->lang));
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $room_id
	 */
	function get_room_detail($room_id) {
		
		return $this->db->get_where('room', array('room_id' => $room_id));
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 * @param unknown_type $type
	 */
	function get_hotel_feature($hotel_id, $type = NULL) {
		if($type != NULL) {
			$this->db->where('feature_type', $type);
		}
		$this->db->select('feature_ref.*, text_def.title def_title, text_def.icon def_icon, text_def.content def_content, text.title text_title, text.icon text_icon, text.content text_content, hotel_feature.status available')
				->from('feature_ref')
				->join('feature_ref_text text_def', "text_def.feature_ref_id = feature_ref.feature_ref_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('feature_ref_text text', "text.feature_ref_id = feature_ref.feature_ref_id AND text.language = '" . $this->lang . "'", 'left')
				->join('hotel_feature', "hotel_feature.feature_ref_id = feature_ref.feature_ref_id AND hotel_feature.hotel_id = $hotel_id", 'left')
//				->where('hotel_id', $hotel_id)
				->where('feature_ref.status', 1)
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $city_id
	 */
	function get_city_list($country_id = NULL) {
		if($country_id != NULL) {
			$this->db->where('country_id', $country_id);
		}
		
		$this->db->select('city.*, text_def.title def_title, text_def.icon def_icon, text.title text_title, text.icon text_icon')
				->from('city')
				->join('content_text text_def', "text_def.parent_id = city.city_id AND text_def.content_text_type = 'city' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = city.city_id AND text.content_text_type = 'city' AND text.language = '" . $this->lang . "'", 'left')
				->where('city.status', 1)
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $city_id
	 */
	function get_city($city_id) {
		$this->db->select('city.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.image def_image, text_def.teaser def_teaser, text_def.content def_content, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.image text_image, text.teaser text_teaser, text.content text_content, text.link text_link')
				->from('city')
				->join('content_text text_def', "text_def.parent_id = city.city_id AND text_def.content_text_type = 'city' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = city.city_id AND text.content_text_type = 'city' AND text.language = '" . $this->lang . "'", 'left')
				->where('city_id', $city_id)
				->where('city.status', 1)
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $city_id
	 */
	function get_landmark_list($city_id) {
		$this->db->select('landmark.*, 
				text_def.title def_title, text_def.icon def_icon, text_def.teaser def_teaser, text_def.link def_link, 
				text.title text_title, text.icon text_icon, text.teaser text_teaser, text.link text_link')
				->from('landmark')
				->join('content_text text_def', "text_def.parent_id = landmark.landmark_id AND text_def.content_text_type = 'landmark' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = landmark.landmark_id AND text.content_text_type = 'landmark' AND text.language = '" . $this->lang . "'", 'left')
				->where('city_id', $city_id)
				->where('landmark.status', 1)
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $city_id
	 */
	function get_landmark($landmark_id) {
		$this->db->select('landmark.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.image def_image, text_def.teaser def_teaser, text_def.content def_content, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.image text_image, text.teaser text_teaser, text.content text_content, text.link text_link')
				->from('landmark')
				->join('content_text text_def', "text_def.parent_id = landmark.landmark_id AND text_def.content_text_type = 'landmark' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = landmark.landmark_id AND text.content_text_type = 'landmark' AND text.language = '" . $this->lang . "'", 'left')
				->where('landmark_id', $landmark_id)
				->where('landmark.status', 1)
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $page_id
	 * @param unknown_type $position
	 */
	function get_page_banner($page_id, $position) {
		$this->db->select('banner.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.image def_image, text_def.teaser def_teaser, text_def.content def_content, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.image text_image, text.teaser text_teaser, text.content text_content, text.link text_link')
				->from('banner')
				->join('content_text text_def', "text_def.parent_id = banner.banner_id AND text_def.content_text_type = 'banner' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = banner.banner_id AND text.content_text_type = 'banner' AND text.language = '" . $this->lang . "'", 'left')
				->where("banner.status >= 1 AND (banner.publish_date_start <= '" . date('Y-m-d') . "' OR banner.publish_date_start = '0000-00-00') AND (banner.publish_date_end >= '" . date('Y-m-d') . "' OR banner.publish_date_end = '0000-00-00') AND banner.page_id = $page_id AND banner.display_pos = '$position' ")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $page_id
	 * @param unknown_type $position
	 */
	function get_page_banner_pos($page_id, $position, $hotel_id = 0) {
		$this->db->select('banner.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.image def_image, text_def.teaser def_teaser, text_def.content def_content, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.image text_image, text.teaser text_teaser, text.content text_content, text.link text_link')
				->from('banner_display')
				->join('banner', "banner_display.banner_id = banner.banner_id", 'left')
				->join('content_text text_def', "text_def.parent_id = banner.banner_id AND text_def.content_text_type = 'banner' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = banner.banner_id AND text.content_text_type = 'banner' AND text.language = '" . $this->lang . "'", 'left')
				->where("banner.status >= 1 AND (banner.publish_date_start <= '" . date('Y-m-d') . "' OR banner.publish_date_start = '0000-00-00') AND (banner.publish_date_end >= '" . date('Y-m-d') . "' OR banner.publish_date_end = '0000-00-00') AND banner_display.page_id = $page_id AND banner_display.hotel_id = $hotel_id AND banner_display.display_pos = '$position' ")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $page_id
	 * @param unknown_type $position
	 */
	function get_page_banner_page($page_id) {
		$this->db->select('banner.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.image def_image, text_def.teaser def_teaser, text_def.content def_content, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.image text_image, text.teaser text_teaser, text.content text_content, text.link text_link')
				->from('banner')
				->join('content_text text_def', "text_def.parent_id = banner.banner_id AND text_def.content_text_type = 'banner' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = banner.banner_id AND text.content_text_type = 'banner' AND text.language = '" . $this->lang . "'", 'left')
				->where("banner.status >= 1 AND (banner.publish_date_start <= '" . date('Y-m-d') . "' OR banner.publish_date_start = '0000-00-00') AND (banner.publish_date_end >= '" . date('Y-m-d') . "' OR banner.publish_date_end = '0000-00-00') AND banner.page_id = $page_id ")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $page_id
	 * @param unknown_type $position
	 */
	function get_page_promo($page_id, $position, $hotel_id = 0) {
		$this->db->select('promo.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('promo_display')
				->join('promo', "promo_display.promo_id = promo.promo_id", 'left')
				->join('content_text text_def', "text_def.parent_id = promo.promo_id AND text_def.content_text_type = 'promo' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = promo.promo_id AND text.content_text_type = 'promo' AND text.language = '" . $this->lang . "'", 'left')
				->where("promo.status >= 1 AND (promo.valid_from <= '" . date('Y-m-d') . "' OR promo.valid_from = '0000-00-00') AND (promo.valid_to >= '" . date('Y-m-d') . "' OR promo.valid_to = '0000-00-00') AND promo_display.page_id = $page_id AND promo_display.hotel_id = $hotel_id AND promo_display.display_pos = '$position' ")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	function get_page_promo_hot($page_id, $position, $hotel_id = 0) {
		$this->db->select('promo.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('promo_display')
				->join('promo', "promo_display.promo_id = promo.promo_id", 'left')
				->join('content_text text_def', "text_def.parent_id = promo.promo_id AND text_def.content_text_type = 'promo' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = promo.promo_id AND text.content_text_type = 'promo' AND text.language = '" . $this->lang . "'", 'left')
				->where("promo.status >= 1 AND (promo.valid_from <= '" . date('Y-m-d') . "' OR promo.valid_from = '0000-00-00') AND (promo.valid_to >= '" . date('Y-m-d') . "' OR promo.valid_to = '0000-00-00') AND promo_display.page_id = $page_id AND promo_display.hotel_id = $hotel_id AND promo_display.display_pos = '$position' AND promo.hot='1' ")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/* 
	 * Enter description here ...
	 */
	 function get_all_promo(){
		$this->db->select('promo.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('promo')
				->join('content_text text_def', "text_def.parent_id = promo.promo_id AND text_def.content_text_type = 'promo' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = promo.promo_id AND text.content_text_type = 'promo' AND text.language = '" . $this->lang . "'", 'left')
				->where("promo.status >= 1 AND (promo.valid_from <= '" . date('Y-m-d') . "' OR promo.valid_from = '0000-00-00') AND (promo.valid_to >= '" . date('Y-m-d') . "' OR promo.valid_to = '0000-00-00')")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	} 
	
	/**
	 * Enter description here ...
	 */
	function get_condition_promo($country_id){
		$this->db->select('promo.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('promo')
				->join('content_text text_def', "text_def.parent_id = promo.promo_id AND text_def.content_text_type = 'promo' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = promo.promo_id AND text.content_text_type = 'promo' AND text.language = '" . $this->lang . "'", 'left')
				->join('city', "city.city_id = promo.city_id AND city.country_id = '$country_id'")
				->where("promo.status >= 1 AND (promo.valid_from <= '" . date('Y-m-d') . "' OR promo.valid_from = '0000-00-00') AND (promo.valid_to >= '" . date('Y-m-d') . "' OR promo.valid_to = '0000-00-00')")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $page_id
	 * @param unknown_type $position
	 */
	function get_promo_by_id($promo_id){
		$this->db->select('promo.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('promo')
				->join('content_text text_def', "text_def.parent_id = promo.promo_id AND text_def.content_text_type = 'promo' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = promo.promo_id AND text.content_text_type = 'promo' AND text.language = '" . $this->lang . "'", 'left')
				->where("promo.status >= 1 AND (promo.valid_from <= '" . date('Y-m-d') . "' OR promo.valid_from = '0000-00-00') AND (promo.valid_to >= '" . date('Y-m-d') . "' OR promo.valid_to = '0000-00-00') AND promo_id='" . $promo_id . "'")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $page_id
	 * @param unknown_type $position
	 */
	function get_promo_by_city($city_id){
		$this->db->select('promo.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('promo')
				->join('content_text text_def', "text_def.parent_id = promo.promo_id AND text_def.content_text_type = 'promo' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = promo.promo_id AND text.content_text_type = 'promo' AND text.language = '" . $this->lang . "'", 'left')
				->where("promo.status >= 1 AND (promo.valid_from <= '" . date('Y-m-d') . "' OR promo.valid_from = '0000-00-00') AND (promo.valid_to >= '" . date('Y-m-d') . "' OR promo.valid_to = '0000-00-00') AND city_id='" . $city_id . "'")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel_top_promo($hotel_id) {
		$this->db->select('promo.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.image def_image, text_def.teaser def_teaser, text_def.content def_content, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.image text_image, text.teaser text_teaser, text.content text_content, text.link text_link')
				->from('promo')
				->join('content_text text_def', "text_def.parent_id = promo.promo_id AND text_def.content_text_type = 'promo' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = promo.promo_id AND text.content_text_type = 'promo' AND text.language = '" . $this->lang . "'", 'left')
				->where("promo.status >= 1 AND (promo.publish_date_start <= '" . date('Y-m-d') . "' OR promo.publish_date_start = '0000-00-00') AND (promo.publish_date_end >= '" . date('Y-m-d') . "' OR promo.publish_date_end = '0000-00-00') AND promo.hotel_id = $hotel_id AND promo.display_pos = 'top' ")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $promo_id
	 */
	function set_promo_hits($promo_id){
		$sql = "UPDATE promo SET hits = hits+1 WHERE promo_id = '$promo_id'";
		$this->db->query($sql);
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $content_category_id
	 */
	function get_content_category($parent_id = NULL) {
		$this->db->order_by('sort');
		return $this->db->get_where('content_category', array('parent_id' => $parent_id, 'status >=' => 1));
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $content_category_id
	 */
	function get_content_category_by_id($content_category_id = NULL) {
		return $this->db->get_where('content_category', array('content_category_id' => $content_category_id, 'status >=' => 1));
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $content_category_id
	 */
	function get_content_list($content_category_id) {
		$this->db->select('content.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('content')
				->join('content_text text_def', "text_def.parent_id = content.content_id AND text_def.content_text_type = 'content' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = content.content_id AND text.content_text_type = 'content' AND text.language = '" . $this->lang . "'", 'left')
				->where("content.status >= 1 AND (content.publish_date_start <= '" . date('Y-m-d') . "' OR content.publish_date_start = '0000-00-00') AND (content.publish_date_end >= '" . date('Y-m-d') . "' OR content.publish_date_end = '0000-00-00') AND content.content_category_id='" . $content_category_id . "'")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $content_category_id
	 */
	function get_content($content_id) {
		$this->db->select('content.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.icon_1 def_icon_1, text_def.image def_image, text_def.image_1 def_image_1, text_def.teaser def_teaser, text_def.teaser_1 def_teaser_1, text_def.content def_content, text_def.content_1 def_content_1, text_def.link def_link, 
							text.title text_title, text.icon text_icon, text.icon_1 text_icon_1, text.image text_image, text.image_1 text_image_1, text.teaser text_teaser, text.teaser_1 text_teaser_1, text.content text_content, text.content_1 text_content_1, text.link text_link')
				->from('content')
				->join('content_text text_def', "text_def.parent_id = content.content_id AND text_def.content_text_type = 'content' AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = content.content_id AND text.content_text_type = 'content' AND text.language = '" . $this->lang . "'", 'left')
				->where("content.status >= 1 AND (content.publish_date_start <= '" . date('Y-m-d') . "' OR content.publish_date_start = '0000-00-00') AND (content.publish_date_end >= '" . date('Y-m-d') . "' OR content.publish_date_end = '0000-00-00') AND content.content_id='" . $content_id . "'")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country())
				->order_by('sort');
		return $this->db->get();
		
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $promo_id
	 */
	function set_content_hits($content_id){
		$sql = "UPDATE content SET hits = hits+1 WHERE content_id = '$content_id'";
		$this->db->query($sql);
	}
		
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	
	function get_hotel_package($hotel_id) {
		$this->db->select('hotel_package.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.note def_note, 
							text.title text_title, text.icon text_icon, text.note text_note')
				->from('hotel_package')
				->join('hotel_package_text text_def', "text_def.hotel_package_id = hotel_package.hotel_package_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('hotel_package_text text', "text.hotel_package_id = hotel_package.hotel_package_id AND text.language = '" . $this->lang . "'", 'left')
				->where("hotel_package.status >= 1 AND (hotel_package.publish_date_start <= '" . date('Y-m-d') . "' OR hotel_package.publish_date_start = '0000-00-00') AND (hotel_package.publish_date_end >= '" . date('Y-m-d') . "' OR hotel_package.publish_date_end = '0000-00-00') AND hotel_package.hotel_id='" . $hotel_id . "'")
				->order_by('sort');
		return $this->db->get();
	}
	 */
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_hotel_package($hotel_id) {
		$this->db->select('package.*, 
							text_def.title def_title, text_def.icon def_icon, text_def.note def_note, 
							text.title text_title, text.icon text_icon, text.note text_note')
				->from('hotel_package')
				->join('package', 'package.package_id = hotel_package.package_id')
				->join('package_text text_def', "text_def.package_id = package.package_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('package_text text', "text.package_id = package.package_id AND text.language = '" . $this->lang . "'", 'left')
				->where("package.status >= 1 AND (package.publish_date_start <= '" . date('Y-m-d') . "' OR package.publish_date_start = '0000-00-00') AND (package.publish_date_end >= '" . date('Y-m-d') . "' OR package.publish_date_end = '0000-00-00') AND hotel_package.hotel_id='" . $hotel_id . "'")
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 * @param unknown_type $type
	
	function get_package_feature($package_id) {
		$this->db->select('hotel_package_feature.*, 
				text_def.title def_title, text_def.icon def_icon, text_def.content def_content,
				text.title text_title, text.icon text_icon, text.content text_content')
				->from('hotel_package_feature')
				->join('feature_ref', "hotel_package_feature.feature_ref_id = feature_ref.feature_ref_id", 'left')
				->join('feature_ref_text text_def', "text_def.feature_ref_id = feature_ref.feature_ref_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('feature_ref_text text', "text.feature_ref_id = feature_ref.feature_ref_id AND text.language = '" . $this->lang . "'", 'left')
				->where("hotel_package_feature.status = '1' AND hotel_package_feature.hotel_package_id = '" . $package_id . "'")
				->order_by('sort');
		return $this->db->get();
	}
	 */
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 * @param unknown_type $type
	 */
	function get_package_feature($package_id) {
		$this->db->select('package_feature.*, 
				text_def.title def_title, text_def.icon def_icon, text_def.content def_content,
				text.title text_title, text.icon text_icon, text.content text_content')
				->from('package_feature')
				->join('feature_ref', "package_feature.feature_ref_id = feature_ref.feature_ref_id", 'left')
				->join('feature_ref_text text_def', "text_def.feature_ref_id = feature_ref.feature_ref_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('feature_ref_text text', "text.feature_ref_id = feature_ref.feature_ref_id AND text.language = '" . $this->lang . "'", 'left')
				->where("package_feature.status = '1' AND package_feature.package_id = '" . $package_id . "'")
				->order_by('sort');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $hotel_id
	 */
	function get_culinary($city_id) {
		$this->db->select('culinary.*,
							text_def.title def_title, text_def.icon def_icon, text_def.teaser def_teaser, text_def.content_text_type def_content_text_type,
							text.title text_title, text.icon text_icon, text.teaser text_teaser, text.content_text_type text_content_text_type,')
				->from('culinary')
				->join('content_text text_def', "text_def.parent_id = culinary.culinary_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('content_text text', "text.parent_id = culinary.culinary_id AND text.language = '" . $this->lang . "'", 'left')
				->where("culinary.status >= 1 AND culinary.city_id = '" . $city_id . "' AND text.content_text_type = 'culinary' AND text_def.content_text_type = 'culinary'")
				->like('text_def.country', $this->_get_country())
				->like('text.country', $this->_get_country());
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $parent_id
	 */
	/* function get_menu_list($parent_id = '0') {
		$this->db->order_by('sort');
		return $this->db->get_where('menu', array('parent_id' => $parent_id, 'status >=' => 1, 'language' => $this->lang));
	} */
	
	function get_menu_list($parent_id = '0') {
		$this->db->select('menu.*, text_def.title as def_title, text.title as text_title')
				->from('menu')
				->join('menu_text text_def', "text_def.menu_id = menu.menu_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('menu_text text', "text.menu_id = menu.menu_id AND text.language = '" . $this->lang . "'", 'left')
				->where("menu.parent_id = '" . $parent_id . "' AND menu.status = '1'")
				->order_by('menu.sort', 'asc');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $parent_id
	 */
	/* function get_menu($menu_id) {
		return $this->db->get_where('menu', array('menu_id' => $menu_id, 'language' => $this->lang));
	} */
	function get_menu($menu_id = '0') {
		$this->db->select('menu.*, text_def.title as def_title, text.title as text_title')
				->from('menu')
				->join('menu_text text_def', "text_def.menu_id = menu.menu_id AND text_def.language = '" . $this->def_lang . "'", 'left')
				->join('menu_text text', "text.menu_id = menu.menu_id AND text.language = '" . $this->lang . "'", 'left')
				->where("menu.menu_id = '" . $menu_id . "' AND menu.status = '1'")
				->order_by('menu.sort', 'asc');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $text_search
	 */
	function set_search($text_search = NULL, $remote_ip = NULL) {
		$sql = "INSERT INTO search(search_text, remote_ip) VALUES('" . $text_search . "', '" . $remote_ip . "')";
		$this->db->query($sql);
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $parent_id
	 */
	function get_search($search) {
		$search = str_replace(' ', '%', $search);
		$this->db->select('promo.*, content_text.title as text_title, content_text.teaser as text_teaser, content_text.content as text_content')
				->from('promo')
				->join('content_text', "content_text.parent_id = promo.promo_id")
				->where("promo.status >= 1 AND (promo.valid_from <= '" . date('Y-m-d') . "' OR promo.valid_from = '0000-00-00') AND (promo.valid_to >= '" . date('Y-m-d') . "' OR promo.valid_to = '0000-00-00') AND content_text.content_text_type = 'promo' AND (content_text.title LIKE '%" . $search . "%' OR content_text.teaser LIKE '%" . $search . "%' OR content_text.content LIKE '%" . $search . "%') AND content_text.language = '" . $this->lang . "' ")
				->like('content_text.country', $this->_get_country())
				->order_by('sort')
				->group_by('promo.promo_id');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type $parent_id
	 */
	function get_search_media($search) {
		$search = str_replace(' ', '%', $search);
		$this->db->select('content.*, content_text.title as text_title, content_text.teaser as text_teaser, content_text.content as text_content')
				->from('content')
				->join('content_category', "content.content_category_id = content_category.content_category_id")
				->join('content_text', "content_text.parent_id = content.content_id")
				->where("content.status >= 1 AND (content.publish_date_start <= '" . date('Y-m-d') . "' OR content.publish_date_start = '0000-00-00') AND (content.publish_date_end >= '" . date('Y-m-d') . "' OR content.publish_date_end = '0000-00-00') AND content_text.content_text_type = 'content' AND (content_text.title LIKE '%" . $search . "%' OR content_text.teaser LIKE '%" . $search . "%' OR content_text.content LIKE '%" . $search . "%') AND content_text.language = '" . $this->lang . "' AND (content_category.content_category_id = '13' OR content_category.content_category_id = '14') ")
				->like('content_text.country', $this->_get_country())
				->order_by('content.sort')
				->group_by('content.content_id');
		return $this->db->get();
	}
	
	/**
	 * Enter description here ...
	 * @param unknown_type 
	 */
	function get_social(){
		return $this->db->get('social');
	} 
	
	
}

/**
 * End of file pages_model.php 
 * Location: ./.../.../.../pages_model.php 
 */