<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource hotel_model.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Oct 9, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified
 *
 *
 */

class Hotel_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
//		echo "<!--";
//		var_dump($_POST);
//		echo "-->";
	}
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_hotel_list($city_id = NULL, $country_id = NULL) {
    	
    	if($city_id != NULL) {
    		$this->db->where('city.city_id', $city_id);
    	}
    	if($country_id != NULL) {
    		$this->db->where('country.country_id', $country_id);
    	}
    	
    	$this->db->select('hotel.*, country.full_name country_name, city.city_name')
    				->from('hotel')
    				->join('city', 'city.city_id = hotel.city_id')
    				->join('country', 'country.country_id = city.country_id')
//    				->where(array('city.status >=' => 1))
    				->order_by('country.country_id, city.sort, hotel.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_hotel($hotel_id) {
		return $this->db->get_where('hotel', array('hotel_id' => $hotel_id));
   	}
    
	/**
	 * Enter description here ...
	 */
	function get_hotel_text($hotel_id, $language = "english") {
		return $this->db->get_where('hotel_text', array('hotel_id' => $hotel_id, 'language' => $language));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_room_list($hotel_id, $language = "english") {
		return $this->db->get_where('room', array('hotel_id' => $hotel_id, 'language' => $language));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_hotel_gallery($hotel_id) {
		return $this->db->get_where('hotel_gallery', array('hotel_id' => $hotel_id));
	}
	
	/**
	 * Enter description here ...
	 */
	function get_hotel_feature($hotel_id) {
		$list = $this->db->get_where('hotel_feature', array('hotel_id' => $hotel_id));
		$return = array();
		if($list->num_rows() == 0) {
			return $return;
		}
		foreach($list->result() as $row) {
			$return[] = $row->feature_ref_id;
		}
		return $return;
	}
	
	/**
	 * Enter description here ...
	 */
	function get_hotel_package_list($hotel_id) {
		return $this->db->get_where('hotel_package', array('hotel_id' => $hotel_id));
		
	}
	
	/**
	 * Enter description here ...
	 */
	function get_package_list() {
		return $this->db->get('package');
		
	}
	
	/**
	 * Enter description here ...
	
	function get_hotel_package($hotel_package_id) {
		return $this->db->get_where('hotel_package', array('hotel_package_id' => $hotel_package_id));
		
	}
	 */
	
	/**
	 * Enter description here ...
	 */
	function get_package($package_id) {
		return $this->db->get_where('package', array('package_id' => $package_id));
		
	}
	
	/**
	 * Enter description here ...
	
	function get_hotel_package_text($hotel_package_id, $language = "english") {
		return $this->db->get_where('hotel_package_text', array('hotel_package_id' => $hotel_package_id, 'language' => $language));
	}
	 */
	
	/**
	 * Enter description here ...
	 */
	function get_package_text($package_id, $language = "english") {
		return $this->db->get_where('package_text', array('package_id' => $package_id, 'language' => $language));
	}
	
	/**
	 * Enter description here ...
	
	function get_hotel_package_feature($hotel_package_id) {
		$this->db->join('feature_ref', 'feature_ref.feature_ref_id = hotel_package_feature.feature_ref_id');
		return $this->db->get_where('hotel_package_feature', array('hotel_package_id' => $hotel_package_id));
	}
	 */
	
	/**
	 * Enter description here ...
	 */
	function get_package_feature($package_id) {
		$this->db->join('feature_ref', 'feature_ref.feature_ref_id = package_feature.feature_ref_id');
		return $this->db->get_where('package_feature', array('package_id' => $package_id));
	}
	
	/**
	 * Enter description here ...
	
	function get_array_hotel_package_feature($hotel_package_id) {
		$list = $this->db->get_where('hotel_package_feature', array('hotel_package_id' => $hotel_package_id));
		$return = array();
		if($list->num_rows() == 0) {
			return $return;
		}
		foreach($list->result() as $row) {
			$return[] = $row->feature_ref_id;
		}
		return $return;
	}
	 */
	
	/**
	 * Enter description here ...
	 */
	function get_array_package_feature($package_id) {
		$list = $this->db->get_where('package_feature', array('package_id' => $package_id));
		$return = array();
		if($list->num_rows() == 0) {
			return $return;
		}
		foreach($list->result() as $row) {
			$return[] = $row->feature_ref_id;
		}
		return $return;
	}
	
	/**
	 * Enter description here ...
	 */
	function get_array_hotel_package($hotel_id) {
		$list = $this->db->get_where('hotel_package', array('hotel_id' => $hotel_id));
		$return = array();
		if($list->num_rows() == 0) {
			return $return;
		}
		foreach($list->result() as $row) {
			$return[] = $row->package_id;
		}
		return $return;
	}
	
	/**
	 * Enter description here ...
	 */
	function get_inquiry_list($status = 0) {
		$this->db->select('hotel_inquiry.*, hotel.hotel_name')
				->from('hotel_inquiry')
				->join('hotel', 'hotel.hotel_id = hotel_inquiry.hotel_id')
				->where(array('hotel_inquiry.status' => $status));
		return $this->db->get();
		
	}
	
	/**
	 * Enter description here ...
	 */
	function get_inquiry($hotel_inquiry_id) {
	$this->db->select('hotel_inquiry.*, hotel.hotel_name, country_ref.name country_name, room.room_name')
				->from('hotel_inquiry')
				->join('country_ref', 'country_ref.country_id = hotel_inquiry.country_id')
				->join('hotel', 'hotel.hotel_id = hotel_inquiry.hotel_id')
				->join('room', 'room.room_id = hotel_inquiry.room_type')
				->where(array('hotel_inquiry_id' => $hotel_inquiry_id));
	
/*			$this->db->select('hotel_inquiry.*, hotel.hotel_name, country_ref.name country_name')
				->from('hotel_inquiry')
				->join('country_ref', 'country_ref.country_id = hotel_inquiry.country_id')
				->join('hotel', 'hotel.hotel_id = hotel_inquiry.hotel_id')
				->where(array('hotel_inquiry_id' => $hotel_inquiry_id));
*/			return $this->db->get();
		
	}
	
    /**
     * Enter description here ...
     */
    function save_list_hotel() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('hotel_id', $id);
	    	$this->db->update('hotel', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'hotel'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function add_hotel() {
    	$this->load->library('form_validation');
		
		$this->form_validation->set_rules('hotel_name', lang('label_hotel'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('city_id', lang('label_city'), 'trim|required');
		
		if ($this->form_validation->run() != FALSE){//	echo 'valid';
			if(!isset($_POST['top_destination'])) {
				$_POST['top_destination'] = 0;
			}
			$hotel['city_id'] = $_POST['city_id'];
			$hotel['hotel_name'] = $this->_set_clean_name($_POST['hotel_name']);
			$hotel['phone1'] = $_POST['phone1'];
			$hotel['phone2'] = $_POST['phone2'];
			$hotel['fax'] = $_POST['fax'];
			$hotel['email'] = $_POST['email'];
			$hotel['latitude'] = $_POST['latitude'];
			$hotel['longitude'] = $_POST['longitude'];
			$hotel['tags'] = $_POST['tags'];
			$hotel['icon'] = $_POST['google_icon'];
			$hotel['note'] = $_POST['google_note'];
			$hotel['top_destination'] = $_POST['top_destination'];
			$hotel['sort'] = $_POST['sort'];
			$hotel['publish_date_start'] = $_POST['publish_date_start'];
			$hotel['publish_date_end'] = $_POST['publish_date_end'];
			$hotel['created_id'] = get_user_id();
			$this->db->insert('hotel', $hotel);
			$hotel_id = $this->db->insert_id();
			//var_dump($_POST['title']['en']);
			foreach($_POST['language'] as $lang_k => $lang_v) {
				if($_POST['title'][$lang_k] != '') {
					$text = array();
					$text['hotel_id'] = $hotel_id;
					$text['title'] = $_POST['title'][$lang_k];
					$text['icon'] = $_POST['icon'][$lang_k];
					$text['image'] = $_POST['image'][$lang_k];
					$text['address'] = $_POST['address'][$lang_k];
//					$text['feature_text'] = $_POST['feature_text'][$lang_k];
//					$text['feature_text_1'] = $_POST['feature_text_1'][$lang_k];
					$text['note'] = $_POST['note'][$lang_k];
					$text['language'] = $lang_v;
					$this->db->insert('hotel_text', $text);
				}
			}

			set_success_message(sprintf(lang('success_add'), 'hotel'));
			redirect('system/hotel/hotel_edit/' . $hotel_id);
			exit;
			
		}
		
    }
    
    /**
     * Enter description here ...
     */
    function update_hotel() {
    	$this->load->library('form_validation');
		
		$this->form_validation->set_rules('hotel_name', lang('label_hotel'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('city_id', lang('label_city'), 'trim|required');
		
		if ($this->form_validation->run() != FALSE){//	echo 'valid';
			$this->db->where(array('hotel_id' => $_POST['hotel_id']));
			if(!isset($_POST['status'])) {
				$_POST['status'] = 0;
			}
			if(!isset($_POST['top_destination'])) {
				$_POST['top_destination'] = 0;
			}
			
			$hotel['city_id'] = $_POST['city_id'];
			$hotel['hotel_name'] = $this->_set_clean_name($_POST['hotel_name']);
			$hotel['phone1'] = $_POST['phone1'];
			$hotel['phone2'] = $_POST['phone2'];
			$hotel['fax'] = $_POST['fax'];
			$hotel['email'] = $_POST['email'];
			$hotel['latitude'] = $_POST['latitude'];
			$hotel['longitude'] = $_POST['longitude'];
			$hotel['icon'] = $_POST['google_icon'];
			$hotel['note'] = $_POST['google_note'];
			$hotel['tags'] = $_POST['tags'];
			$hotel['top_destination'] = $_POST['top_destination'];
			$hotel['sort'] = $_POST['sort'];
			$hotel['status'] = $_POST['status'];
			$hotel['publish_date_start'] = $_POST['publish_date_start'];
			$hotel['publish_date_end'] = $_POST['publish_date_end'];
			$hotel['modified_id'] = get_user_id();
			$hotel['modified_time'] = date('Y-m-d H:i:s');
			$this->db->update('hotel', $hotel);
			foreach($_POST['language'] as $lang_k => $lang_v) {
				if($_POST['title'][$lang_k] != '') {
					$text = array();
					$text['title'] = $_POST['title'][$lang_k];
					$text['icon'] = $_POST['icon'][$lang_k];
					$text['image'] = $_POST['image'][$lang_k];
					$text['address'] = $_POST['address'][$lang_k];
//					$text['feature_text'] = $_POST['feature_text'][$lang_k];
//					$text['feature_text_1'] = $_POST['feature_text_1'][$lang_k];
					$text['note'] = $_POST['note'][$lang_k];

					if($_POST['hotel_text_id'][$lang_k] != '0') {
						$this->db->where(array('hotel_text_id' => $_POST['hotel_text_id'][$lang_k]));
						$this->db->update('hotel_text', $text);
					} else {
						$text['hotel_id'] = $_POST['hotel_id'];
						$text['language'] = $_POST['language'][$lang_k];
						$this->db->insert('hotel_text', $text);
					}

				}
			}

			set_success_message(sprintf(lang('success_edit'), 'hotel'));
			redirect('system/hotel/hotel_edit/' . $_POST['hotel_id']);
			exit;
			
		}
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_hotel() {
		unset($_POST['mode']);
		$result = $this->db->get_where('hotel', array('hotel_id' => $_POST['hotel_id']));
		if($result->num_rows() == 0) {
			$this->output->set_content_type('application/json')
				->set_output(json_encode(array('error' => '', 'msg' => 'undefine Hotel ' . humanize($_POST['hotel_name']))));
	    } else {
	    	$hotel = $result->row();
	    	
	    	$this->db->delete('hotel', array('hotel_id' => $_POST['hotel_id']));
	    	$this->db->delete('hotel_text', array('hotel_id' => $_POST['hotel_id']));
	    	$this->db->delete('room', array('hotel_id' => $_POST['hotel_id']));
	    	$this->db->delete('hotel_gallery', array('hotel_id' => $_POST['hotel_id']));
	    	$this->db->delete('hotel_feature', array('hotel_id' => $_POST['hotel_id']));
	    	$this->db->delete('hotel_package', array('hotel_id' => $_POST['hotel_id']));
	    	$this->db->delete('hotel_inquiry', array('hotel_id' => $_POST['hotel_id']));
	    	
	    	$this->db->delete('banner_display', array('hotel_id' => $_POST['hotel_id']));
	    	
			$this->output->set_content_type('application/json')
				->set_output(json_encode(array('error' => '', 'msg' => humanize($_POST['hotel_name']) . ' successfully deleted!')));
	    }
    	
    }
    
    /**
     * Enter description here ...
     */
    function update_hotel_room() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		foreach($_POST['language'] as $lang_k => $lang_v) {
			if(isset($_POST['room_title'])) {
				foreach($_POST['room_title'][$lang_k] as $row_k => $row_v) {
					if($row_v != '') {
						$room = array();
//						$room['room_type'] = $_POST['room_type'][$lang_k][$row_k];
//						$room['room_name'] = $_POST['room_name'][$lang_k][$row_k];
						$room['title'] = $_POST['room_title'][$lang_k][$row_k];
						$room['room_type'] = $room['room_name'] = $this->_set_clean_name($room['title']);
						$room['quantity'] = $_POST['room_quantity'][$lang_k][$row_k];
						$room['image'] = $_POST['image'][$lang_k][$row_k];
						$room['feature_text'] = $_POST['room_feature_text'][$lang_k][$row_k];
//						echo $_POST['room_id'][$lang_k][$row_k] . '<br>';
						if($_POST['room_id'][$lang_k][$row_k] != '0') {
							$this->db->where(array('room_id' => $_POST['room_id'][$lang_k][$row_k]));
							$this->db->update('room', $room);
						} else {
							$room['language'] = $lang_v;
							$room['hotel_id'] = $_POST['hotel_id'];
							$this->db->insert('room', $room);
						}
					} else {
						$this->db->where(array('room_id' => $_POST['room_id'][$lang_k][$row_k]));
						$this->db->delete('room');
					}
				}
			}
			
			if($_POST['feature_text'][$lang_k] != '') {
				if($_POST['hotel_text_id'][$lang_k] != '0') {
					$this->db->where(array('hotel_text_id' => $_POST['hotel_text_id'][$lang_k]));
					$this->db->update('hotel_text', array('feature_text' => $_POST['feature_text'][$lang_k]));
				} else {
					$text['hotel_id'] = $_POST['hotel_id'];
					$text['language'] = $_POST['language'][$lang_k];
					$text['feature_text'] = $_POST['feature_text'][$lang_k];
					$this->db->insert('hotel_text', $text);
				}
			
			}
			
		}
		
		set_success_message(sprintf(lang('success_edit'), 'hotel room'));
		redirect('system/hotel/hotel_room_edit/' . $_POST['hotel_id']);
		exit;
		
    }
    
    /**
     * Enter description here ...
     */
    function update_hotel_gallery() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		foreach($_POST['gallery_image'] as $gallery_k => $gallery_v) {
			if($gallery_v != '') {
				$gallery = array();
				$gallery['hotel_id'] = $_POST['hotel_id'];
				$gallery['image'] = $_POST['gallery_image'][$gallery_k];
				$gallery['title'] = $_POST['gallery_title'][$gallery_k];
				$gallery['gallery_name'] = $this->_set_clean_name($gallery['title']);
				if((isset($_POST['gallery_id'])) && $_POST['gallery_id'][$gallery_k] != '0') {
					$this->db->where(array('gallery_id' => $_POST['gallery_id'][$gallery_k]));
					$this->db->update('hotel_gallery', $gallery);
				} else {
					$this->db->insert('hotel_gallery', $gallery);
				}
				$this->_create_gallery_thumb($_POST['gallery_image'][$gallery_k]);
			} else {
				$this->db->where(array('gallery_id' => $_POST['gallery_id'][$gallery_k]));
				$this->db->delete('hotel_gallery');
			}
		}
		
		set_success_message(sprintf(lang('success_edit'), 'hotel gallery'));
		redirect('system/hotel/hotel_gallery_edit/' . $_POST['hotel_id']);
		exit;
    }
    
    /**
     * Enter description here ...
     * @param unknown_type $file
     */
    function _create_gallery_thumb($file) {
    	$this->load->library('image_lib');
    	$image = pathinfo($file);
    	$src_dir =  BASEPATH . str_replace('/tune_media/', '../assets/media/', $file);
		$thumb_dir =  BASEPATH . '../assets/media/_thumbs/tune_media/images/Hotel/' . $image['basename'];
		//set_error_message($thumb_dir);
		list($width, $height, $type, $attr) = getimagesize($src_dir);
		
		$this->image_lib->clear();
    	$config_resize['source_image'] = $src_dir;
		$config_resize['width'] = 130;
		$config_resize['height'] = 130;
  		$config_resize['maintain_ratio'] = true;
		$config_resize['master_dim'] = ($width > $height)? 'height' : 'width';
		$config_resize['new_image'] = $thumb_dir;
		$this->image_lib->initialize($config_resize); 
		if(!$this->image_lib->resize()){
			set_error_message($this->image_lib->display_errors());
		} else {
			
			$this->image_lib->clear();
			$config['source_image'] = $thumb_dir;
			$config['width'] = 130;
			$config['height'] = 130;
			$config['x_axis'] = 0;
  			$config['y_axis'] = 0;
  			$config['maintain_ratio'] = false;
//			copy($config['source_image'], $config['source_image'] . '.bak');
//			$config['new_image'] = $cthumb_dir;
			$this->image_lib->initialize($config);
	    	if(!$this->image_lib->crop()) {
			    set_error_message($this->image_lib->display_errors());
			}
		}
    	
    }
    
    /**
     * Enter description here ...
     */
    function update_hotel_feature() {
    
		$this->db->delete('hotel_feature', array('hotel_id' => $_POST['hotel_id']));
		if(isset($_POST['feature_ref'])) {
			foreach($_POST['feature_ref'] as $feat_k => $feat_v) {
				$feat = array();
				$feat['hotel_id'] = $_POST['hotel_id'];
				$feat['feature_ref_id'] = $feat_k;
				$this->db->insert('hotel_feature', $feat);
			}
		}
		
		set_success_message(sprintf(lang('success_edit'), 'hotel feature'));
		redirect('system/hotel/hotel_feature_edit/' . $_POST['hotel_id']);
		exit;
    }
    
    /**
     * Enter description here ...
     */
    function update_hotel_package() {
    
		$this->db->delete('hotel_package', array('hotel_id' => $_POST['hotel_id']));
		if(isset($_POST['package_id'])) {
			foreach($_POST['package_id'] as $pack_k => $pack_v) {
				$pack = array();
				$pack['hotel_id'] = $_POST['hotel_id'];
				$pack['package_id'] = $pack_k;
				$this->db->insert('hotel_package', $pack);
			}
		}
		
		set_success_message(sprintf(lang('success_edit'), 'hotel package'));
		redirect('system/hotel/hotel_package_edit/' . $_POST['hotel_id']);
		exit;
    }
    
    /**
     * Enter description here ...
     */
	function update_hotel_meta() {
	
		$hotel['meta_keywords'] = strip_tags($_POST['meta_keywords']);
		$hotel['meta_description'] = strip_tags($_POST['meta_description']);
		$hotel['modified_id'] = get_user_id();
		$hotel['modified_time'] = date('Y-m-d H:i:s');
		$this->db->update('hotel', $hotel);
		
		
		set_success_message(sprintf(lang('success_edit'), 'hotel'));
		redirect('system/hotel/hotel_meta_edit/' . $_POST['hotel_id']);
		exit;
		
    }
    
    /**
     * Enter description here ...

    function update_hotel_package() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
    	
		$this->form_validation->set_rules('package_name', 'Package Name', 'trim|required|xss_clean');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
    				$package['package_name'] = $_POST['package_name'];
					$package['hotel_id'] = $_POST['hotel_id'];
					$package['sort'] = $_POST['sort'];
					$package['status'] = $_POST['status'];
					$package['publish_date_start'] = $_POST['publish_date_start'];
					$package['publish_date_end'] = $_POST['publish_date_end'];
					$package['created_id'] = get_user_id();
					$this->db->insert('hotel_package', $package);
					$hotel_package_id = $this->db->insert_id();
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['hotel_package_id'] = $hotel_package_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['note'] = $_POST['note'][$lang_k];
							$text['language'] = $lang_v;
							$this->db->insert('hotel_package_text', $text);
						}
					}
					
					foreach($_POST['feature_ref'] as $feat_k => $feat_v) {
						$feat = array();
						$feat['hotel_package_id'] = $hotel_package_id;
						$feat['feature_ref_id'] = $feat_k;
						$this->db->insert('hotel_package_feature', $feat);
					}
					
					set_success_message(sprintf(lang('success_add'), 'hotel package'));
					redirect('system/hotel/hotel_package_detail_edit/' . $_POST['hotel_id'] . '/' . $hotel_package_id);
					exit;
					
    			}
    		break;
    		case 'edit':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
    				$package['package_name'] = $_POST['package_name'];
					$package['hotel_id'] = $_POST['hotel_id'];
					$package['sort'] = $_POST['sort'];
					$package['status'] = $_POST['status'];
					$package['publish_date_start'] = $_POST['publish_date_start'];
					$package['publish_date_end'] = $_POST['publish_date_end'];
					$package['created_id'] = get_user_id();
					$this->db->where(array('hotel_package_id' => $_POST['hotel_package_id']));
					$this->db->update('hotel_package', $package);
//					echo $this->db->last_query();
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$this->db->where(array('hotel_package_text_id' => $_POST['hotel_package_text_id'][$lang_k]));
							$text = array();
//							$text['hotel_package_id'] = $hotel_package_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['note'] = $_POST['note'][$lang_k];
//							$text['language'] = $lang_v;
//							$this->db->update('hotel_package_text', $text);
							if($_POST['hotel_package_text_id'][$lang_k] != '0') {
								$this->db->update('hotel_package_text', $text);
							} else {
								$text['hotel_package_id'] = $_POST['hotel_package_id'][$lang_k];
								$text['language'] = $lang_v;
								$this->db->insert('hotel_package_text', $text);
							}
							
						}
					}
					
					$this->db->delete('hotel_package_feature', array('hotel_package_id' => $_POST['hotel_package_id']));
					if(isset($_POST['feature_ref'])) {
						foreach($_POST['feature_ref'] as $feat_k => $feat_v) {
							$feat = array();
							$feat['hotel_package_id'] = $_POST['hotel_package_id'];
							$feat['feature_ref_id'] = $feat_k;
							$this->db->insert('hotel_package_feature', $feat);
						}
					}
					
					set_success_message(sprintf(lang('success_edit'), 'hotel package'));
					redirect('system/hotel/hotel_package_detail_edit/' . $_POST['hotel_id'] . '/' . $_POST['hotel_package_id']);
					exit;
					
    			}
    		break;
    	}
    		
    }
    */
    
    /**
     * Enter description here ...
    
    function save_package() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
    	
		$this->form_validation->set_rules('package_name', 'Package Name', 'trim|required|xss_clean');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
    				$package['package_name'] = $_POST['package_name'];
//					$package['hotel_id'] = $_POST['hotel_id'];
					$package['sort'] = $_POST['sort'];
					$package['status'] = $_POST['status'];
					$package['publish_date_start'] = $_POST['publish_date_start'];
					$package['publish_date_end'] = $_POST['publish_date_end'];
					$package['created_id'] = get_user_id();
					$this->db->insert('hotel_package', $package);
					$hotel_package_id = $this->db->insert_id();
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['hotel_package_id'] = $hotel_package_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['note'] = $_POST['note'][$lang_k];
							$text['language'] = $lang_v;
							$this->db->insert('hotel_package_text', $text);
						}
					}
					
					foreach($_POST['feature_ref'] as $feat_k => $feat_v) {
						$feat = array();
						$feat['hotel_package_id'] = $hotel_package_id;
						$feat['feature_ref_id'] = $feat_k;
						$this->db->insert('hotel_package_feature', $feat);
					}
					
					set_success_message(sprintf(lang('success_add'), 'hotel package'));
					redirect('system/hotel/package_detail_edit/' . $hotel_package_id);
					exit;
					
    			}
    		break;
    		case 'edit':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
    				$package['package_name'] = $_POST['package_name'];
//					$package['hotel_id'] = $_POST['hotel_id'];
					$package['sort'] = $_POST['sort'];
					$package['status'] = $_POST['status'];
					$package['publish_date_start'] = $_POST['publish_date_start'];
					$package['publish_date_end'] = $_POST['publish_date_end'];
					$package['created_id'] = get_user_id();
					$this->db->where(array('hotel_package_id' => $_POST['hotel_package_id']));
					$this->db->update('hotel_package', $package);
//					echo $this->db->last_query();
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$this->db->where(array('hotel_package_text_id' => $_POST['hotel_package_text_id'][$lang_k]));
							$text = array();
//							$text['hotel_package_id'] = $hotel_package_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['note'] = $_POST['note'][$lang_k];
//							$text['language'] = $lang_v;
//							$this->db->update('hotel_package_text', $text);
							if($_POST['hotel_package_text_id'][$lang_k] != '0') {
								$this->db->update('hotel_package_text', $text);
							} else {
								$text['hotel_package_id'] = $_POST['hotel_package_id'][$lang_k];
								$text['language'] = $lang_v;
								$this->db->insert('hotel_package_text', $text);
							}
							
						}
					}
					
					$this->db->delete('hotel_package_feature', array('hotel_package_id' => $_POST['hotel_package_id']));
					if(isset($_POST['feature_ref'])) {
						foreach($_POST['feature_ref'] as $feat_k => $feat_v) {
							$feat = array();
							$feat['hotel_package_id'] = $_POST['hotel_package_id'];
							$feat['feature_ref_id'] = $feat_k;
							$this->db->insert('hotel_package_feature', $feat);
						}
					}
					
					set_success_message(sprintf(lang('success_edit'), 'hotel package'));
					redirect('system/hotel/package_detail_edit/' . $_POST['hotel_package_id']);
					exit;
					
    			}
    		break;
    	}
    		
    }
     */
    
    /**
     * Enter description here ...
     */
    function save_package() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
    	
		$this->form_validation->set_rules('package_name', 'Package Name', 'trim|required|xss_clean');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
    				$package['package_name'] = $this->_set_clean_name($_POST['package_name']);
//					$package['hotel_id'] = $_POST['hotel_id'];
					$package['sort'] = $_POST['sort'];
					$package['status'] = $_POST['status'];
					$package['publish_date_start'] = $_POST['publish_date_start'];
					$package['publish_date_end'] = $_POST['publish_date_end'];
					$package['created_id'] = get_user_id();
					$this->db->insert('package', $package);
					$package_id = $this->db->insert_id();
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['package_id'] = $package_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['note'] = $_POST['note'][$lang_k];
							$text['language'] = $lang_v;
							$this->db->insert('package_text', $text);
						}
					}
					
					foreach($_POST['feature_ref'] as $feat_k => $feat_v) {
						$feat = array();
						$feat['package_id'] = $package_id;
						$feat['feature_ref_id'] = $feat_k;
						$this->db->insert('package_feature', $feat);
					}
					
					set_success_message(sprintf(lang('success_add'), 'hotel package'));
					redirect('system/hotel/package_detail_edit/' . $package_id);
					exit;
					
    			}
    		break;
    		case 'edit':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
    				$package['package_name'] = $this->_set_clean_name($_POST['package_name']);
//					$package['hotel_id'] = $_POST['hotel_id'];
					$package['sort'] = $_POST['sort'];
					$package['status'] = $_POST['status'];
					$package['publish_date_start'] = $_POST['publish_date_start'];
					$package['publish_date_end'] = $_POST['publish_date_end'];
					$package['created_id'] = get_user_id();
					$this->db->where(array('package_id' => $_POST['package_id']));
					$this->db->update('package', $package);
//					echo $this->db->last_query();
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$this->db->where(array('package_text_id' => $_POST['package_text_id'][$lang_k]));
							$text = array();
//							$text['hotel_package_id'] = $hotel_package_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['note'] = $_POST['note'][$lang_k];
//							$text['language'] = $lang_v;
//							$this->db->update('hotel_package_text', $text);
							if($_POST['package_text_id'][$lang_k] != '0') {
								$this->db->update('package_text', $text);
							} else {
								$text['package_id'] = $_POST['package_id'][$lang_k];
								$text['language'] = $lang_v;
								$this->db->insert('package_text', $text);
							}
							
						}
					}
					
					$this->db->delete('package_feature', array('package_id' => $_POST['package_id']));
					if(isset($_POST['feature_ref'])) {
						foreach($_POST['feature_ref'] as $feat_k => $feat_v) {
							$feat = array();
							$feat['package_id'] = $_POST['package_id'];
							$feat['feature_ref_id'] = $feat_k;
							$this->db->insert('package_feature', $feat);
						}
					}
					
					set_success_message(sprintf(lang('success_edit'), 'package'));
					redirect('system/hotel/package_detail_edit/' . $_POST['package_id']);
					exit;
					
    			}
    		break;
    	}
    		
    }
    
    /**
     * Enter description here ...
     */
    function save_hotel() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('hotel_name', lang('label_hotel'), 'trim|required|min_length[5]|xss_clean');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
					$hotel['city_name'] = $this->_set_clean_name($_POST['city_name']);
					$hotel['country_id'] = $_POST['country_id'];
					$hotel['tags'] = $_POST['tags'];
					$hotel['sort'] = $_POST['sort'];
	    			$hotel['created_id'] = get_user_id();
					$this->db->insert('hotel', $hotel);
					$city_id = $this->db->insert_id();
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text['content_text_type'] = 'city';
							$text['parent_id'] = $city_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
						}
					}
					set_success_message(sprintf(lang('success_add'), 'city'));
					redirect('system/city');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('city_id', $_POST['city_id']);
	    			//unset($_POST['content_id']);
	    			$hotel['city_name'] = $this->_set_clean_name($_POST['city_name']);
					$hotel['country_id'] = $_POST['country_id'];
					$hotel['tags'] = $_POST['tags'];
					$hotel['sort'] = $_POST['sort'];
					$hotel['status'] = $_POST['status'];
					$hotel['modified_id'] = get_user_id();
	    			$hotel['modified_time'] = date('Y-m-d H:i:s');	    			
	    			$this->db->update('hotel', $hotel);
	    			
					foreach($_POST['title'] as $lang_k => $lang_v) {
						$this->db->where(array('parent_id' => $_POST['city_id'], 'content_text_type' => 'city', 'language' => $_POST['language'][$lang_k]));
						if($_POST['title'][$lang_k] != '') {
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->update('content_text', $text);
							
						} else {
							$this->db->delete('content_text');
						}
					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'content'));
					redirect('system/city/city_edit/' . $_POST['city_id']);
					exit;
				}
    		break;
    		case 'delete':
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_feature_ref_list() {
    	return $this->db->get('feature_ref');
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_feature_ref($feature_ref_id) {
		return $this->db->get_where('feature_ref', array('feature_ref_id' => $feature_ref_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_feature_ref() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('feature_ref_id', $id);
	    	$this->db->update('feature_ref', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'feature ref'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_feature_ref() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
//		$this->load->library('form_validation');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
//    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
					$feature['feature_type'] = $_POST['feature_type'];
					$feature['feature_name'] = $this->_set_clean_name($_POST['feature_name']);
					$feature['sort'] = $_POST['sort'];
					$this->db->insert('feature_ref', $feature);
					$feature_ref_id = $this->db->insert_id();
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text['feature_ref_id'] = $feature_ref_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							$this->db->insert('feature_ref_text', $text);
							
						}
					}
					set_success_message(sprintf(lang('success_add'), 'feature ref'));
					redirect('system/hotel/feature_ref_list');
					exit;
//				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
//				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('feature_ref_id', $_POST['feature_ref_id']);
	    			//unset($_POST['content_id']);
					$feature['feature_type'] = $_POST['feature_type'];
					$feature['feature_name'] = $this->_set_clean_name($_POST['feature_name']);
					$feature['sort'] = $_POST['sort'];
	    			$feature['status'] = $_POST['status'];
	    			$this->db->update('feature_ref', $feature);
	    			
					foreach($_POST['title'] as $lang_k => $lang_v) {
//						echo $lang_k;
						$this->db->where(array('feature_ref_id' => $_POST['feature_ref_id'], 'language' => $_POST['language'][$lang_k]));
						if($_POST['title'][$lang_k] != '') {
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$this->db->update('feature_ref_text', $text);
//							echo $this->db->last_query() . '<br>';
							if($this->db->affected_rows() == 0) {
								$text['feature_ref_id'] = $_POST['feature_ref_id'];
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('feature_ref_text', $text);
							}
							
						} else {
							$this->db->delete('feature_ref_text');
						}
//					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'feature ref'));
					redirect('system/hotel/feature_ref_edit/' . $_POST['feature_ref_id']);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_destination_list() {
    	$this->db->select('destination.*, hotel.hotel_name')
    			->from('destination')
    			->join('hotel', 'hotel.hotel_id = destination.hotel_id');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_destination($destination_id) {
		return $this->db->get_where('destination', array('destination_id' => $destination_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_destination() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('destination_id', $id);
	    	$this->db->update('destination', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'destination'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_destination() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
/*		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['content_name'])) {
			$_POST['content_name'] = underscore($_POST['title']);
		}
*/		$this->form_validation->set_rules('hotel_id', lang('label_hotel'), 'required');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
    				$_POST['status'] = 1;
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('destination', $_POST);
					
    				set_success_message(sprintf(lang('success_add'), 'destination'));
    				redirect('system/hotel/destination_edit/' . $this->db->insert_id());
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$destination_id = $_POST['destination_id'];
	    			unset($_POST['destination_id']);
	    			
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->where('destination_id', $destination_id);
	    			$this->db->update('destination', $_POST);
	    			
	    			set_success_message(sprintf(lang('success_edit'), 'destination'));
    				redirect('system/hotel/destination_edit/' . $destination_id);
					exit;
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
     
}

/**
 * End of file city_model.php 
 * Location: ./.../.../.../city_model.php 
 */