<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource cms_model.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Apr 2, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Cms_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
//		echo "<!--";
//		var_dump($_POST);
//		echo "-->";
	}
	
	/**
	 * Enter description here ...
	 * @param String $table
	 * @param int $category_id
	 */
	function get_child_category($table, $parent_id) {
		$sql = "SELECT " . $table . "_category_id AS id FROM " . $table . "_category WHERE parent_id = $parent_id AND status = 1 ";
		$list = $this->db->query($sql);
		$return = array();
    	
    	foreach($list->result() as $row) {
			$return[] = $row->id;
		}
		return $return;
	}

	/**
	 * Enter description here ...
	 */
	function save_list_content_category() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('content_category_id', $id);
	    	$this->db->update('content_category', array('sort' => $val, 'parent_id' => $_POST['parent_id'][$id]));
		}
		set_success_message(sprintf(lang('success_edit'), 'content category'));
    	
    }
    
	/**
	 * Enter description here ...
	 */
	function save_list_gallery_category() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('gallery_category_id', $id);
	    	$this->db->update('gallery_category', array('sort' => $val, 'parent_id' => $_POST['parent_id'][$id]));
		}
		set_success_message(sprintf(lang('success_edit'), 'gallery category'));
    	
    }
    
	/**
	 * Enter description here ...
	 */
	function save_list_banner_category() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('banner_category_id', $id);
	    	$this->db->update('banner_category', array('sort' => $val, 'parent_id' => $_POST['parent_id'][$id]));
		}
		set_success_message(sprintf(lang('success_edit'), 'banner category'));
    	
    }
    
	
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    function get_parent_content() {
    	$return = array();
    	
    	$category = $this->db->get_where('content_category', array('parent_id !=' => 0));
		foreach($category->result() as $row) {
			$return[$row->content_category_id] = $row->title;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    function get_parent_gallery() {
    	$return = array();
    	
    	$category = $this->db->get('gallery_category');
		foreach($category->result() as $row) {
			$return[$row->gallery_category_id] = $row->title;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    function get_parent_banner() {
    	$return = array();
    	
    	$category = $this->db->get('banner_category');
		foreach($category->result() as $row) {
			$return[$row->banner_category_id] = $row->title;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_content_category_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('content_category', array('parent_id' => $parent_id));
    	} else {
    		return $this->db->get('content_category');
    	}
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_gallery_category_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('gallery_category', array('parent_id' => $parent_id));
    	} else {
    		return $this->db->get('gallery_category');
    	}
    }
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_banner_category_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('banner_category', array('parent_id' => $parent_id));
    	} else {
    		return $this->db->get('banner_category');
    	}
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_content_list($content_category_id = NULL) {
    	if($content_category_id != NULL) {
    		if(is_array($content_category_id)) {
    			$this->db->where_in('content_category.content_category_id', $content_category_id);
    		} else {
    			$this->db->where('content_category.content_category_id', $content_category_id);
    		}
    	}
    	
    	$this->db->select('content.*, content_category.title category_title')
    				->from('content')
    				->join('content_category', 'content_category.content_category_id = content.content_category_id')
    				->where(array('content_category.status <=' => 1, 'content.status <=' => 1))
    				->order_by('content.content_category_id, content.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_gallery_list($gallery_category_id = NULL) {
    	if($gallery_category_id != NULL) {
    		if(is_array($gallery_category_id)) {
    			$this->db->where_in('gallery_category.gallery_category_id', $gallery_category_id);
    		} else {
    			$this->db->where('gallery_category.gallery_category_id', $gallery_category_id);
    		}
    	}
    	
    	$this->db->select('gallery.*, gallery_category.title category_title')
    				->from('gallery')
    				->join('gallery_category', 'gallery_category.gallery_category_id = gallery.gallery_category_id')
 //   				->where(array('gallery_category.status <=' => 1, 'gallery.status <=' => 1))
    				->order_by('gallery.gallery_category_id, gallery.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_banner_list($banner_category_id = NULL) {
    	if($banner_category_id != NULL) {
    		if(is_array($banner_category_id)) {
    			$this->db->where_in('banner_category.banner_category_id', $banner_category_id);
    		} else {
    			$this->db->where('banner_category.banner_category_id', $banner_category_id);
    		}
    	}
    	
    	$this->db->select('banner.*, banner_category.title category_title')
    				->from('banner')
    				->join('banner_category', 'banner_category.banner_category_id = banner.banner_category_id')
    				->where(array('banner_category.status <=' => 1, 'banner.status <=' => 1))
    				->order_by('banner.banner_category_id, banner.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_contact_list() {
    	$this->db->select('contact.*, users.first_name, users.last_name, users.email')
    				->from('contact')
    				->join('users', 'users.user_id = contact.user_id');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_content($content_id) {
		return $this->db->get_where('content', array('content_id' => $content_id));
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_gallery($gallery_id) {
		return $this->db->get_where('gallery', array('gallery_id' => $gallery_id));
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_banner($banner_id) {
		return $this->db->get_where('banner', array('banner_id' => $banner_id));
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_contact($contact_id) {
		return $this->db->get_where('contact', array('contact_id' => $contact_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_content() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('content_id', $id);
	    	$this->db->update('content', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'content'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_list_gallery() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('gallery_id', $id);
	    	$this->db->update('gallery', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'gallery'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_list_banner() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('banner_id', $id);
	    	$this->db->update('banner', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'banner'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_content_category() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
//		$this->form_validation->set_rules('content_category_name', lang('label_name'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['content_category_name'])) {
			$_POST['content_category_name'] = underscore($_POST['title']);
		}
		
		$redirect = 'system/cms/content_category';
		if(isset($_POST['redirect'])) {
			$redirect = $_POST['redirect'];
			unset($_POST['redirect']);
		}
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('content_category', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'content_category'));
    				redirect($redirect);
					exit;
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('content_category_id', $_POST['content_category_id']);
	    			unset($_POST['content_category_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('content_category', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'content category'));
					
				}
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_content() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
//		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['content_name'])) {
			$_POST['content_name'] = underscore($_POST['title']);
		}
		$this->form_validation->set_rules('content_name', lang('label_content_name'), 'trim|required|min_length[5]|xss_clean');
		
		$category = $this->db->get_where('content_category', array('content_category_id' => $_POST['content_category_id']));
		$res_category = $category->row();
		$_POST['language'] = $res_category->language;
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('content', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'content'));
    				redirect('system/cms/content');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('content_id', $_POST['content_id']);
	    			unset($_POST['content_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('content', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'content'));
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
     
    /**
     * Enter description here ...
     */
    function save_gallery_category() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['gallery_category_name'])) {
			$_POST['gallery_category_name'] = underscore($_POST['title']);
		}
//		$this->form_validation->set_rules('gallery_category_name', lang('label_name'), 'trim|required|xss_clean');
		
		$redirect = 'system/cms/gallery_category';
		if(isset($_POST['redirect'])) {
			$redirect = $_POST['redirect'];
			unset($_POST['redirect']);
		}
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('gallery_category', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'gallery category'));
    				redirect($redirect);
					exit;
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('gallery_category_id', $_POST['gallery_category_id']);
	    			unset($_POST['gallery_category_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('gallery_category', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'gallery category'));
					
				}
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_gallery() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
//		$this->form_validation->set_rules('gallery_name', lang('label_name'), 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['gallery_name'])) {
			$_POST['gallery_name'] = underscore($_POST['title']);
		}
		
		$category = $this->db->get_where('gallery_category', array('gallery_category_id' => $_POST['gallery_category_id']));
		$res_category = $category->row();
		$_POST['language'] = $res_category->language;
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('gallery', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'gallery'));
    				redirect('system/cms/gallery');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('gallery_id', $_POST['gallery_id']);
	    			unset($_POST['gallery_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('gallery', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'gallery'));
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_banner_category() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('banner_category_name', lang('label_name'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('banner_category', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'banner category'));
    				redirect('system/cms/banner_category');
					exit;
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('banner_category_id', $_POST['banner_category_id']);
	    			unset($_POST['banner_category_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('banner_category', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'banner category'));
					
				}
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_banner() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('banner_name', lang('label_name'), 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		
		$category = $this->db->get_where('banner_category', array('banner_category_id' => $_POST['banner_category_id']));
		$res_category = $category->row();
		$_POST['language'] = $res_category->language;
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('banner', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'banner'));
    				redirect('system/cms/banner');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('banner_id', $_POST['banner_id']);
	    			unset($_POST['banner_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('banner', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'banner'));
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_contact() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		$this->form_validation->set_rules('contact_type', lang('label_type'), 'trim|required');
		$this->form_validation->set_rules('user_id', lang('label_user_name'), 'trim|required');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('contact', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'contact'));
    				redirect('system/cms/contact');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('contact_id', $_POST['contact_id']);
	    			unset($_POST['contact_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('contact', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'contact'));
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function delete_content_category() {
		$this->db->where('content_category_id', $_POST['content_category_id']);
		$this->db->delete('content_category');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'content category') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_content() {
		$this->db->where('content_id', $_POST['content_id']);
		$this->db->delete('content');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'content') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_gallery_category() {
		$this->db->where('gallery_category_id', $_POST['gallery_category_id']);
		$this->db->delete('gallery_category');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'gallery category') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_gallery() {
		$this->db->where('gallery_id', $_POST['gallery_id']);
		$this->db->delete('gallery');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'gallery') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_banner_category() {
		$this->db->where('banner_category_id', $_POST['banner_category_id']);
		$this->db->delete('banner_category');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'banner category') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_banner() {
		$this->db->where('banner_id', $_POST['banner_id']);
		$this->db->delete('banner');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'banner') . "'}";
		
    }
    
	/**
	 * Enter description here ...
	 * @param int $content_category_id
	 */
	function get_content_category($content_category_id) {
		return $this->db->get_where('content_category', array('content_category_id' => $content_category_id));
   	
    } 
    
	/**
	 * Enter description here ...
	 * @param int $gallery_category_id
	 */
	function get_gallery_category($gallery_category_id) {
		return $this->db->get_where('gallery_category', array('gallery_category_id' => $gallery_category_id));
   	
    } 
    
	/**
	 * Enter description here ...
	 * @param int $banner_category_id
	 */
	function get_banner_category($banner_category_id) {
		return $this->db->get_where('banner_category', array('banner_category_id' => $banner_category_id));
   	
    } 
    
/*
 * ADDITIONAL FUNCTION
 */	
	
    /**
     * Enter description here ...
     */
    function order_gallery() {
    	$list = $this->get_gallery_list($_POST['category_id']);
    	
    	switch ($_POST['type']) {
    		case 'First' :
    			$i = 0;
    			foreach($list->result() as $row) {
    				$this->db->where('gallery_id', $row->gallery_id);
		    		if($row->gallery_id == $_POST['gallery_id']) {
    					$this->db->update('gallery', array('sort' => 0));
		    		} else {
    					$this->db->update('gallery', array('sort' => $i+=10));
    				}
    			}
    			
    			break;
    		case 'Up' :
    			$key_list = array();
    			$val_list = array();
    			foreach($list->result() as $row) {
    				$key_list[] = $row->gallery_id;
    				$val_list[$row->gallery_id] = $row->sort;
    			}
    			$key = array_search($_POST['gallery_id'], $key_list); 
    			$up_key = $key_list[$key - 1];
    			
    			$this->db->where('gallery_id', $_POST['gallery_id']);
    			$this->db->update('gallery', array('sort' => $val_list[$up_key]));
    			
    			$this->db->where('gallery_id', $up_key);
    			$this->db->update('gallery', array('sort' => $val_list[$_POST['gallery_id']]));
    			
    			break;
    		case 'Down' :
    			$key_list = array();
    			$val_list = array();
    			foreach($list->result() as $row) {
    				$key_list[] = $row->gallery_id;
    				$val_list[$row->gallery_id] = $row->sort;
    			}
    			$key = array_search($_POST['gallery_id'], $key_list); 
    			$down_key = $key_list[$key + 1];
    			
    			$this->db->where('gallery_id', $_POST['gallery_id']);
    			$this->db->update('gallery', array('sort' => $val_list[$down_key]));
    			
    			$this->db->where('gallery_id', $down_key);
    			$this->db->update('gallery', array('sort' => $val_list[$_POST['gallery_id']]));
    			
    			break;
    		case 'Last' :
    			$i = 0;
    			foreach($list->result() as $row) {
		    		if($row->gallery_id != $_POST['gallery_id']) {
    					$this->db->where('gallery_id', $row->gallery_id);
    					$this->db->update('gallery', array('sort' => $i+=10));
    				}
    			}
    			$this->db->where('gallery_id', $_POST['gallery_id']);
				$this->db->update('gallery', array('sort' => $i+=10));
    			
    			break;
    				
    	}
    	echo "{error: '',msg: '" . sprintf(lang('success_edit'), 'gallery order') . "'}";
		
    }
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_page_gallery_list($gallery_category_id = NULL) {
    	if($gallery_category_id != NULL) {
    		if(is_array($gallery_category_id)) {
    			$this->db->where_in('gallery_category.gallery_category_id', $gallery_category_id);
    		} else {
    			$this->db->where('gallery_category.gallery_category_id', $gallery_category_id);
    		}
    	}
    	
    	$this->db->select('gallery.*, gallery_category.title category_title')
    				->from('gallery')
    				->join('gallery_category', 'gallery_category.gallery_category_id = gallery.gallery_category_id')
    				->where(array('gallery_category.status >=' => 1, 'gallery.status >=' => 1))
    				->order_by('gallery.gallery_category_id, gallery.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_page_gallery_category_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('gallery_category', array('parent_id' => $parent_id, 'gallery_category.status >=' => 1));
    	} else {
    		return $this->db->get_where('gallery_category', array('gallery_category.status >=' => 1));
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_pages_gallery() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
//		$this->form_validation->set_rules('gallery_name', lang('label_name'), 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['gallery_name'])) {
			$_POST['gallery_name'] = underscore($_POST['title']);
		}
		
		$category = $this->db->get_where('gallery_category', array('gallery_category_id' => $_POST['gallery_category_id']));
		$res_category = $category->row();
		$_POST['language'] = $res_category->language;
		
		if($_FILES["image"]["name"] != "") {
			$this->load->library(array('upload', 'image_lib'));
			
			$config_image['upload_path'] = $this->config->item('ge_upload_path') . "images/" . $_POST['type'] . "/";
			$config_image['allowed_types'] = 'gif|jpg|png';
			$this->upload->initialize($config_image);
			if ($this->upload->do_upload('image')) {
				$this->load->library('image_lib');
				$data = $this->upload->data();
				$config_resize['source_image'] = $config_image['upload_path'] . $data['file_name'];
				$config_resize['width'] = 150;
				$config_resize['height'] = 99;
				$this->image_lib->initialize($config_resize); 
				
				if(!$this->image_lib->resize()){
					echo $this->image_lib->display_errors();
				} else {
//					echo "Ok..";
				}
				$_POST['image'] = '/lynx_media/images/' . $_POST['type'] . "/" . $data['file_name'];
			}
		}
		unset($_POST['type']);
		
		$redirect = 'system/cms/gallery';
		if(isset($_POST['redirect'])) {
			$redirect = $_POST['redirect'];
			unset($_POST['redirect']);
		}
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('gallery', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'gallery'));
    				redirect($redirect);
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('gallery_id', $_POST['gallery_id']);
	    			unset($_POST['gallery_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('gallery', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'gallery'));
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
	function update_map() {
	//	var_dump($_FILES);
		if($_FILES["image"]["name"] != "") {
			$this->load->library(array('upload', 'image_lib'));
			
			$config_image['upload_path'] = $this->config->item('ge_upload_path') . "images/maps/";
			$config_image['allowed_types'] = 'jpg';
			$config_image['file_name'] = 'maps.jpg';
			$this->upload->initialize($config_image);
			if ($this->upload->do_upload('image')) {
				$this->load->library('image_lib');
				$data = $this->upload->data();
				$config_resize['source_image'] = $config_image['upload_path'] . $data['file_name'];
				$config_resize['width'] = 755;
				$config_resize['height'] = 426;
				$this->image_lib->initialize($config_resize); 
				
				if(!$this->image_lib->resize()){
					echo $this->image_lib->display_errors();
				} else {
					rename($this->config->item('ge_upload_path') . "images/maps/maps.jpg", $this->config->item('ge_upload_path') . "images/maps/maps_bak" . date('Ymdhis') . ".jpg");
					rename($this->config->item('ge_upload_path') . "images/maps/" . $data['file_name'], $this->config->item('ge_upload_path') . "images/maps/maps.jpg");
//					echo "Ok..";
				}
//				$_POST['image'] = '/lynx_media/images/maps/' . $data['file_name'];
			}
		}
	}
	
	/**
	 * Enter description here ...
	 */
	function update_map_list() {
		if(isset($_POST['title'])) {
			foreach ($_POST['title'] as $key => $val) {
				$this->db->where('gallery_category_id', $key);
	    		$this->db->update('gallery_category', array('title' => $val, 'gallery_category_name' => underscore($val), 'note' => $_POST['top'][$key] . ' ' . $_POST['left'][$key], 'status' => $_POST['status'][$key]));
			}
		}
		if(isset($_POST['new_title'])) {
			foreach ($_POST['new_title'] as $key => $val) {
				$this->db->insert('gallery_category', array('parent_id' => 11, 'title' => $val, 'gallery_category_name' => underscore($val), 'note' => $_POST['new_top'][$key] . ' ' . $_POST['new_left'][$key]));
			}
		}
		set_success_message(sprintf(lang('success_edit'), 'map point'));
		
	}
	
}

/**
 * End of file cms_model.php 
 * Location: ./.../.../.../cms_model.php 
 */