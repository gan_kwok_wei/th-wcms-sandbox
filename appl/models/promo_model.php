<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource promo_model.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author nico
 * @package 
 * @subpackage	
 * @since Oct 9, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Promo_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
//		echo "<!--";
//		var_dump($_POST);
//		echo "-->";
	}
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_promo_list($hotel_id = NULL) {
    	if($hotel_id != NULL) {
    		$this->db->where('hotel.hotel_id', $hotel_id);
    	}
    	
    	$this->db->select('promo.*, city.city_name, hotel.hotel_name')
    				->from('promo')
    				->join('hotel', 'hotel.hotel_id = promo.hotel_id')
    				->join('city', 'city.city_id = hotel.city_id')
    				->order_by('hotel.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_promo($promo_id) {
		return $this->db->get_where('promo', array('promo_id' => $promo_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_promo() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('promo_id', $id);
	    	$this->db->update('promo', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'promo'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_promo() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('promo_name', lang('label_promo'), 'trim|required|xss_clean');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
					$promo['promo_name'] = $_POST['promo_name'];
//					$promo['city_id'] = $_POST['city_id'];
					$promo['hotel_id'] = $_POST['hotel_id'];
//					$promo['display_pos'] = $_POST['display_pos'];
//					$promo['page_id'] = $_POST['page_id'];
					$promo['tags'] = $_POST['tags'];
					$promo['valid_from'] = $_POST['valid_from'];
					$promo['valid_to'] = $_POST['valid_to'];
					$promo['sort'] = $_POST['sort'];
	    			$promo['created_id'] = get_user_id();
	    			
					$this->db->insert('promo', $promo);
					$promo_id = $this->db->insert_id();
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['content_text_type'] = 'promo';
							$text['parent_id'] = $promo_id;
							$text['title'] = $_POST['title'][$lang_k];
//							$text['icon'] = $_POST['icon'][$lang_k];
//							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$text['icon_1'] = $_POST['icon_1'][$lang_k];
							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
						}
					}
					
					foreach($_POST['page_id'] as $display_k => $display_v) {
						if($display_v != '') {
							$display = array();
							$display['promo_id'] = $promo_id;
							$display['page_id'] = $_POST['page_id'][$display_k];
							$display['hotel_id'] = ($_POST['page_id'][$display_k] != 2) ? 0 : $_POST['hotel_row'][$display_k];
//							$display['display_pos'] = $_POST['display_pos'][$display_k];
							$this->db->insert('promo_display', $display);
						} 
					}
					
					set_success_message(sprintf(lang('success_add'), 'promo'));
					redirect('system/promo');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['hot'])) {
						$_POST['hot'] = 0;
					}
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$promo['promo_name'] = $_POST['promo_name'];
					$promo['hotel_id'] = $_POST['hotel_id'];
//					$promo['display_pos'] = $_POST['display_pos'];
//					$promo['page_id'] = $_POST['page_id'];
					$promo['tags'] = $_POST['tags'];
					$promo['valid_from'] = $_POST['valid_from'];
					$promo['valid_to'] = $_POST['valid_to'];
					$promo['hot'] = $_POST['hot'];
					$promo['status'] = $_POST['status'];
					$promo['sort'] = $_POST['sort'];
					$promo['modified_id'] = get_user_id();
	    			$promo['modified_time'] = date('Y-m-d H:i:s');
	    			
	    			$this->db->where('promo_id', $_POST['promo_id']); 			
	    			$this->db->update('promo', $promo);
					
					foreach($_POST['language'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
//							$this->db->where(array('parent_id' => $_POST['promo_id'], 'content_text_type' => 'promo', 'language' => $_POST['language'][$lang_k]));
							$text = array();
							$text['title'] = $_POST['title'][$lang_k];
//							$text['icon'] = $_POST['icon'][$lang_k];
//							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$text['icon_1'] = $_POST['icon_1'][$lang_k];
							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);

							if($_POST['content_text_id'][$lang_k] != '0') {
								$this->db->where(array('content_text_id' => $_POST['content_text_id'][$lang_k]));
								$this->db->update('content_text', $text);
							} else {
								$text['parent_id'] = $_POST['promo_id'];
								$text['content_text_type'] = 'promo';
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('content_text', $text);
							}
							
						}
					}
					
					foreach($_POST['page_id'] as $display_k => $display_v) {
						if($display_v != '') {
							$display = array();
							$display['promo_id'] = $_POST['promo_id'];
							$display['page_id'] = $_POST['page_id'][$display_k];
							$display['hotel_id'] = ($_POST['page_id'][$display_k] != 2) ? 0 : $_POST['hotel_row'][$display_k];
//							$display['display_pos'] = $_POST['display_pos'][$display_k];
							if((isset($_POST['promo_display_id'])) && $_POST['promo_display_id'][$display_k] != '0') {
								$this->db->where(array('promo_display_id' => $_POST['promo_display_id'][$display_k]));
								$this->db->update('promo_display', $display);
							} else {
								$this->db->insert('promo_display', $display);
							}
						} else {
							$this->db->where(array('promo_display_id' => $_POST['promo_display_id'][$display_k]));
							$this->db->delete('promo_display');
						}
					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'content'));
					redirect('system/promo/promo_edit/' . $_POST['promo_id']);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    function get_promo_display_list($promo_id) {
    	return $this->db->get_where('promo_display', array('promo_id' => $promo_id));
    }
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_campaign_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('campaign', array('parent_id' => $parent_id));
    	} else {
    		return $this->db->get('campaign');
    	}
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_campaign($campaign_id) {
		return $this->db->get_where('campaign', array('campaign_id' => $campaign_id));
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_banner_list() {
    	$this->db->select('banner.*, banner_category.title category_title')
    				->from('banner')
    				->join('banner_category', 'banner_category.banner_category_id = banner.banner_category_id')
    				->order_by('banner.banner_category_id, banner.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_campaign_banner_list($campaign_id) {
    	$this->db->select('banner.*, banner_category.title category_title')
    				->from('banner')
    				->join('banner_category', 'banner_category.banner_category_id = banner.banner_category_id')
    				->where('campaign_id', $campaign_id)
    				->order_by('banner.banner_category_id, banner.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_campaign_promo_list($campaign_id) {
    	$this->db->select('promo.*, hotel.hotel_name')
    				->from('promo')
    				->join('hotel', 'hotel.hotel_id = promo.hotel_id')
    				->where('promo.campaign_id', $campaign_id)
    				->order_by('hotel.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_campaign() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['campaign_name'])) {
			$campaign['campaign_name'] = underscore($_POST['title']);
		}

    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			
	    			$campaign['title'] = $_POST['title'];
					$campaign['note'] = $_POST['note'];
					$campaign['valid_from'] = $_POST['valid_from'];
					$campaign['valid_to'] = $_POST['valid_to'];
					$campaign['status'] = 0;
	    			$campaign['created_id'] = get_user_id();
	    			
					if($this->db->insert('campaign', $campaign)) {
						$campaign_id = $this->db->insert_id();
						
						if(isset($_POST['banner_id'])){
							foreach($_POST['banner_id'] as $banner_id) {
								$banner = array();
								$banner['campaign_id'] = $campaign_id;
								$banner['publish_date_start'] = $_POST['valid_from'];
								$banner['publish_date_end'] = $_POST['valid_to'];
								$banner['status'] = 0;
	    						
								$this->db->where(array('banner_id' => $banner_id));
								$this->db->update('banner', $banner);
							}
						}
						
						if(isset($_POST['promo_id'])){
							foreach($_POST['promo_id'] as $promo_id) {
								$promo = array();
								$promo['campaign_id'] = $campaign_id;
								$promo['valid_from'] = $_POST['valid_from'];
								$promo['valid_to'] = $_POST['valid_to'];
								$promo['status'] = 0;
	    						
								$this->db->where(array('promo_id' => $promo_id));
								$this->db->update('promo', $promo);
							}
						}
						
	    				set_success_message(sprintf(lang('success_add'), 'campaign'));
	    				redirect('system/promo/campaign_edit/' . $campaign_id);
						exit;
					} else {
						set_error_message('Insert Failed!');
					}
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$campaign['title'] = $_POST['title'];
					$campaign['note'] = $_POST['note'];
					$campaign['valid_from'] = $_POST['valid_from'];
					$campaign['valid_to'] = $_POST['valid_to'];
					$campaign['status'] = $_POST['status'];
	    			$campaign['modified_id'] = get_user_id();
	    			$campaign['modified_time'] = date('Y-m-d H:i:s');
	    			
	    			$this->db->where('campaign_id', $_POST['campaign_id']);
	    			if($this->db->update('campaign', $campaign)) {
	    				
						$this->db->where(array('campaign_id' => $_POST['campaign_id']));
						$this->db->update('banner', array('campaign_id' => 0));
						if(isset($_POST['banner_id'])){
							foreach($_POST['banner_id'] as $banner_id) {
								$banner = array();
								$banner['campaign_id'] = $_POST['campaign_id'];
								$banner['publish_date_start'] = $_POST['valid_from'];
								$banner['publish_date_end'] = $_POST['valid_to'];
								$banner['status'] = $_POST['status'];
	    						
								$this->db->where(array('banner_id' => $banner_id));
								$this->db->update('banner', $banner);
							}
						}
						
						$this->db->where(array('campaign_id' => $_POST['campaign_id']));
						$this->db->update('promo', array('campaign_id' => 0));
						if(isset($_POST['promo_id'])){
							foreach($_POST['promo_id'] as $promo_id) {
								$promo = array();
								$promo['campaign_id'] = $_POST['campaign_id'];
								$promo['valid_from'] = $_POST['valid_from'];
								$promo['valid_to'] = $_POST['valid_to'];
								$promo['status'] = $_POST['status'];
	    						
								$this->db->where(array('promo_id' => $promo_id));
								$this->db->update('promo', $promo);
							}
						}
						
	    				set_success_message(sprintf(lang('success_edit'), 'campaign'));
	    				redirect('system/promo/campaign_edit/' . $_POST['campaign_id']);
						exit;
	    			} else {
	    				set_error_message('Update Failed!');
	    			}
					
    				
				}
    		break;
    	}
    }
    
}

/**
 * End of file promo_model.php 
 * Location: ./.../.../.../promo_model.php 
 */