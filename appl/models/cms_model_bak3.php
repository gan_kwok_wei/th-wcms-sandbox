<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * PHP 5
 *
 * GreenLabGroup Application System Environment (GreASE)
 * GreenLabGroup(tm) :  Rapid Development Framework (http://www.greenlabgroup.com)
 * Copyright 2011-2012, P.T. Green Lab Group.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource cms_model.php
 * @copyright Copyright 2011-2012, P.T. Green Lab Group.
 * @author budi.lx
 * @package 
 * @subpackage	
 * @since Apr 2, 2012
 * @version 
 * @modifiedby budi.lx
 * @lastmodified	
 *
 *
 */

class Cms_model extends GE_Model {

	/**
	 * Enter description here ...
	 */
	function __construct() {
		parent::__construct();
//		echo "<!--";
//		var_dump($_POST);
//		echo "-->";
	}
	
	/**
	 * Enter description here ...
	 * @param String $table
	 * @param int $category_id
	 */
	function get_child_category($table, $parent_id) {
		$sql = "SELECT " . $table . "_category_id AS id FROM " . $table . "_category WHERE parent_id = $parent_id AND status = 1 ";
		$list = $this->db->query($sql);
		$return = array();
    	
    	foreach($list->result() as $row) {
			$return[] = $row->id;
		}
		return $return;
	}

	/**
	 * Enter description here ...
	 */
	function save_list_content_category() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('content_category_id', $id);
	    	$this->db->update('content_category', array('sort' => $val, 'parent_id' => $_POST['parent_id'][$id]));
		}
		set_success_message(sprintf(lang('success_edit'), 'content category'));
    	
    }
    
	/**
	 * Enter description here ...
	 */
	function save_list_gallery_category() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('gallery_category_id', $id);
	    	$this->db->update('gallery_category', array('sort' => $val, 'parent_id' => $_POST['parent_id'][$id]));
		}
		set_success_message(sprintf(lang('success_edit'), 'gallery category'));
    	
    }
    
	/**
	 * Enter description here ...
	 */
	function save_list_banner_category() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('banner_category_id', $id);
	    	$this->db->update('banner_category', array('sort' => $val, 'parent_id' => $_POST['parent_id'][$id]));
		}
		set_success_message(sprintf(lang('success_edit'), 'banner category'));
    	
    }
    
	
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    function get_parent_content() {
    	$return = array();
    	
    	$category = $this->db->get_where('content_category', array('parent_id !=' => 0));
		foreach($category->result() as $row) {
			$return[$row->content_category_id] = $row->title;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    function get_parent_gallery() {
    	$return = array();
    	
    	$category = $this->db->get('gallery_category');
		foreach($category->result() as $row) {
			$return[$row->gallery_category_id] = $row->title;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return ArrayIterator
     */
    function get_parent_banner() {
    	$return = array();
    	
    	$category = $this->db->get('banner_category');
		foreach($category->result() as $row) {
			$return[$row->banner_category_id] = $row->title;
		}
		return $return;
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_content_category_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('content_category', array('parent_id' => $parent_id));
    	} else {
    		return $this->db->get('content_category');
    	}
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_gallery_category_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('gallery_category', array('parent_id' => $parent_id));
    	} else {
    		return $this->db->get('gallery_category');
    	}
    }
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_banner_category_list($parent_id = NULL) {
    	if($parent_id != NULL) {
    		return $this->db->get_where('banner_category', array('parent_id' => $parent_id));
    	} else {
    		return $this->db->get('banner_category');
    	}
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_content_list($content_category_id = NULL) {
    	if(get_role() > 3) {
    		$this->db->where('created_id', get_user_id());
    	}
    	
    	if($content_category_id != NULL) {
    		if(is_array($content_category_id)) {
    			$this->db->where_in('content_category.content_category_id', $content_category_id);
    		} else {
    			$this->db->where('content_category.content_category_id', $content_category_id);
    		}
    	}
    	
    	$this->db->select('content.*, content_category.title category_title')
    				->from('content')
    				->join('content_category', 'content_category.content_category_id = content.content_category_id')
//					->where(array('content_category.status <=' => 1, 'content.status <=' => 1))
    				->order_by('content.content_category_id, content.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_gallery_list($gallery_category_id = NULL) {
    	if(get_role() > 3) {
    		$this->db->where('created_id', get_user_id());
    	}
    	
    	if($gallery_category_id != NULL) {
    		if(is_array($gallery_category_id)) {
    			$this->db->where_in('gallery_category.gallery_category_id', $gallery_category_id);
    		} else {
    			$this->db->where('gallery_category.gallery_category_id', $gallery_category_id);
    		}
    	}
    	
    	$this->db->select('gallery.*, gallery_category.title category_title')
    				->from('gallery')
    				->join('gallery_category', 'gallery_category.gallery_category_id = gallery.gallery_category_id')
 //   				->where(array('gallery_category.status <=' => 1, 'gallery.status <=' => 1))
    				->order_by('gallery.gallery_category_id, gallery.sort');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_banner_list($banner_category_id = NULL) {
    	if(get_role() > 3) {
    		$this->db->where('created_id', get_user_id());
    	}
    	
    	if($banner_category_id != NULL) {
    		if(is_array($banner_category_id)) {
    			$this->db->where_in('banner_category.banner_category_id', $banner_category_id);
    		} else {
    			$this->db->where('banner_category.banner_category_id', $banner_category_id);
    		}
    	}
    	
    	$this->db->select('banner.*, banner_category.title category_title')
    				->from('banner')
    				->join('banner_category', 'banner_category.banner_category_id = banner.banner_category_id')
//					->where(array('banner_category.status <=' => 1, 'banner.status <=' => 1))
    				->order_by('banner.banner_category_id, banner.sort');
    	return $this->db->get();
   	
    } 
    
    function get_banner_display_list($banner_id) {
    	return $this->db->get_where('banner_display', array('banner_id' => $banner_id));
    }
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_contact_list() {
    	$this->db->select('contact.*, users.first_name, users.last_name, users.email')
    				->from('contact')
    				->join('users', 'users.user_id = contact.user_id');
    	return $this->db->get();
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_content($content_id) {
		return $this->db->get_where('content', array('content_id' => $content_id));
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_gallery($gallery_id) {
		return $this->db->get_where('gallery', array('gallery_id' => $gallery_id));
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_banner($banner_id) {
		return $this->db->get_where('banner', array('banner_id' => $banner_id));
   	
    } 
    
    /**
     * Enter description here ...
     * @return multitype:string 
     */
	function get_contact($contact_id) {
		return $this->db->get_where('contact', array('contact_id' => $contact_id));
   	
    } 
    
    /**
     * Enter description here ...
     */
    function save_list_content() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('content_id', $id);
	    	$this->db->update('content', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'content'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_list_gallery() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('gallery_id', $id);
	    	$this->db->update('gallery', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'gallery'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_list_banner() {
		unset($_POST['btnSave']);
		foreach($_POST['sort'] as $id => $val) {
			$this->db->where('banner_id', $id);
	    	$this->db->update('banner', array('sort' => $val));
		}
		set_success_message(sprintf(lang('success_edit'), 'banner'));
    	
    }
    
    /**
     * Enter description here ...
     */
    function save_content_category() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
//		$this->form_validation->set_rules('content_category_name', lang('label_name'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['content_category_name'])) {
			$_POST['content_category_name'] = underscore($_POST['title']);
		}
		
		$redirect = 'system/cms/content_category';
		if(isset($_POST['redirect'])) {
			$redirect = $_POST['redirect'];
			unset($_POST['redirect']);
		}
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('content_category', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'content_category'));
    				redirect($redirect);
					exit;
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('content_category_id', $_POST['content_category_id']);
	    			unset($_POST['content_category_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('content_category', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'content category'));
					
				}
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_content() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
//		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
//		if(!isset($_POST['content_name'])) {
//			$_POST['content_name'] = underscore($_POST['title']);
//		}
		$this->form_validation->set_rules('content_name', lang('label_content_name'), 'trim|required|xss_clean');
		
//		$category = $this->db->get_where('content_category', array('content_category_id' => $_POST['content_category_id']));
//		$res_category = $category->row();
//		$_POST['language'] = $res_category->language;
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
					$content['content_name'] = $_POST['content_name'];
					$content['content_category_id'] = $_POST['content_category_id'];
					$content['tags'] = $_POST['tags'];
					$content['sort'] = $_POST['sort'];
	    			$content['created_id'] = get_user_id();
					$content['publish_date_start'] = $_POST['publish_date_start'];
					$content['publish_date_end'] = $_POST['publish_date_end'];
					$this->db->insert('content', $content);
					$parent_id = $this->db->insert_id();
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['content_text_type'] = 'content';
							$text['parent_id'] = $parent_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = (isset($_POST['icon'][$lang_k])) ? $_POST['icon'][$lang_k] : NULL;
							$text['image'] = (isset($_POST['image'][$lang_k])) ? $_POST['image'][$lang_k] : NULL;
							$text['teaser'] = (isset($_POST['teaser'][$lang_k])) ? $_POST['teaser'][$lang_k] : NULL;
							$text['content'] = (isset($_POST['content'][$lang_k])) ? $_POST['content'][$lang_k] : NULL;
							$text['icon_1'] = (isset($_POST['icon_1'][$lang_k])) ? $_POST['icon_1'][$lang_k] : NULL;
							$text['image_1'] = (isset($_POST['image_1'][$lang_k])) ? $_POST['image_1'][$lang_k] : NULL;
							$text['teaser_1'] = (isset($_POST['teaser_1'][$lang_k])) ? $_POST['teaser_1'][$lang_k] : NULL;
							$text['content_1'] = (isset($_POST['content_1'][$lang_k])) ? $_POST['content_1'][$lang_k] : NULL;
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
						}
					}
					set_success_message(sprintf(lang('success_add'), 'content'));
					$ref = 'system/cms/content_edit/' . $parent_id;
					if(isset($_POST['ref'])) {
						$ref = $_POST['ref'] . $parent_id;
					}
					redirect($ref);
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			//unset($_POST['content_id']);
	    			$content['content_name'] = $_POST['content_name'];
					$content['content_category_id'] = $_POST['content_category_id'];
					$content['tags'] = $_POST['tags'];
					$content['sort'] = $_POST['sort'];
					$content['status'] = $_POST['status'];
					$content['publish_date_start'] = $_POST['publish_date_start'];
					$content['publish_date_end'] = $_POST['publish_date_end'];
					$content['modified_id'] = get_user_id();
	    			$content['modified_time'] = date('Y-m-d H:i:s');	
	    			
	    			$this->db->where('content_id', $_POST['content_id']);    			
	    			$this->db->update('content', $content);
	    			
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = (isset($_POST['icon'][$lang_k])) ? $_POST['icon'][$lang_k] : NULL;
							$text['image'] = (isset($_POST['image'][$lang_k])) ? $_POST['image'][$lang_k] : NULL;
							$text['teaser'] = (isset($_POST['teaser'][$lang_k])) ? $_POST['teaser'][$lang_k] : NULL;
							$text['content'] = (isset($_POST['content'][$lang_k])) ? $_POST['content'][$lang_k] : NULL;
							$text['icon_1'] = (isset($_POST['icon_1'][$lang_k])) ? $_POST['icon_1'][$lang_k] : NULL;
							$text['image_1'] = (isset($_POST['image_1'][$lang_k])) ? $_POST['image_1'][$lang_k] : NULL;
							$text['teaser_1'] = (isset($_POST['teaser_1'][$lang_k])) ? $_POST['teaser_1'][$lang_k] : NULL;
							$text['content_1'] = (isset($_POST['content_1'][$lang_k])) ? $_POST['content_1'][$lang_k] : NULL;
							$text['link'] = $_POST['link'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							
							if($_POST['content_text_id'][$lang_k] != '0') {
								$this->db->where(array('content_text_id' => $_POST['content_text_id'][$lang_k]));
								$this->db->update('content_text', $text);
							} else {
								$text['parent_id'] = $_POST['content_id'];
								$text['content_text_type'] = 'content';
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('content_text', $text);
							}
						}
					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'content'));
	    			$ref = 'system/cms/content_edit/' . $_POST['content_id'];
					if(isset($_POST['ref'])) {
						$ref = $_POST['ref'];
					}
					redirect($ref);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
	
    /**
     * Enter description here ...
     */
    function save_page() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('content_name', lang('label_content_name'), 'trim|required|xss_clean');
		
		$category = $this->db->get_where('content_category', array('content_category_id' => $_POST['content_category_id']));
		$res_category = $category->row();
//		$_POST['language'] = $res_category->language;
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
					$content['content_name'] = $_POST['content_name'];
					$content['content_category_id'] = $_POST['content_category_id'];
					$content['tags'] = $_POST['tags'];
					$content['sort'] = $_POST['sort'];
	    			$content['created_id'] = get_user_id();
					$content['publish_date_start'] = $_POST['publish_date_start'];
					$content['publish_date_end'] = $_POST['publish_date_end'];
					$this->db->insert('content', $content);
					$parent_id = $this->db->insert_id();
					
					$link = $res_category->content_category_name . '/page/content_id_' . $parent_id . '/' . $_POST['content_name'];
						
					if(in_array($_POST['content_category_id'], array(3, 4, 5, 6))) {
						$menu['menu_name'] = $_POST['content_name'];
//						$menu['link'] = $parent_id . '/' . $_POST['content_name'];
						$menu['link'] = $link;
						$menu['sort'] = $_POST['sort'];
						$menu['parent_id'] = $_POST['content_category_id'];
						$this->db->insert('menu', $menu);
						$menu_id = $this->db->insert_id();
					}
					
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['content_text_type'] = 'content';
							$text['parent_id'] = $parent_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = (isset($_POST['icon'][$lang_k])) ? $_POST['icon'][$lang_k] : NULL;
							$text['image'] = (isset($_POST['image'][$lang_k])) ? $_POST['image'][$lang_k] : NULL;
							$text['teaser'] = (isset($_POST['teaser'][$lang_k])) ? $_POST['teaser'][$lang_k] : NULL;
							$text['content'] = (isset($_POST['content'][$lang_k])) ? $_POST['content'][$lang_k] : NULL;
							$text['icon_1'] = (isset($_POST['icon_1'][$lang_k])) ? $_POST['icon_1'][$lang_k] : NULL;
							$text['image_1'] = (isset($_POST['image_1'][$lang_k])) ? $_POST['image_1'][$lang_k] : NULL;
							$text['teaser_1'] = (isset($_POST['teaser_1'][$lang_k])) ? $_POST['teaser_1'][$lang_k] : NULL;
							$text['content_1'] = (isset($_POST['content_1'][$lang_k])) ? $_POST['content_1'][$lang_k] : NULL;
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
							if(in_array($_POST['content_category_id'], array(3, 4, 5, 6))) {
								$menu_text = array();
								$menu_text['menu_id'] = $menu_id;
								$menu_text['title'] = $_POST['title'][$lang_k];
								$menu_text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('menu_text', $menu_text);
							}
							
						}
					}
					$this->db->update('content', array('link' => $link), "content_id = $parent_id");
					
					set_success_message(sprintf(lang('success_add'), 'content'));
					$ref = 'system/cms/content_edit/' . $parent_id;
					if(isset($_POST['ref'])) {
						$ref = $_POST['ref'] . $parent_id;
					}
					redirect($ref);
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			//unset($_POST['content_id']);
	    			$content['content_name'] = $_POST['content_name'];
					$content['content_category_id'] = $_POST['content_category_id'];
					$content['tags'] = $_POST['tags'];
					$content['link'] = $res_category->content_category_name . '/page/content_id_' . $_POST['content_id'] . '/' . $_POST['content_name'];
					$content['sort'] = $_POST['sort'];
					$content['status'] = $_POST['status'];
					$content['publish_date_start'] = $_POST['publish_date_start'];
					$content['publish_date_end'] = $_POST['publish_date_end'];
					$content['modified_id'] = get_user_id();
	    			$content['modified_time'] = date('Y-m-d H:i:s');	
	    			
	    			$this->db->where('content_id', $_POST['content_id']);    			
	    			$this->db->update('content', $content);
	    			
					$this->db->like('link', '/content_id_' . $_POST['content_id'] . '/');
					$result_menu = $this->db->get('menu');
					if($result_menu->num_rows() > 0) {
		    			$menu['menu_name'] = $_POST['content_name'];
						$menu['link'] = $content['link'];
						$menu['sort'] = $_POST['sort'];
						$menu['parent_id'] = $_POST['content_category_id'];
						$menu['status'] = $_POST['status'];
						
						$menu_id = $result_menu->row()->menu_id;
						$this->db->where('menu_id', $menu_id);    			
		    			$this->db->update('menu', $menu);
		    			
		    			$this->db->where('menu_id', $menu_id);
		    			$this->db->delete('menu_text');
		    		} else {
		    			if(in_array($_POST['content_category_id'], array(3, 4, 5, 6))) {
		    				$menu['menu_name'] = $_POST['content_name'];
							$menu['link'] = $res_category->content_category_name . '/page/content_id_' . $_POST['content_id'] . '/' . $_POST['content_name'];
							$menu['sort'] = $_POST['sort'];
							$menu['parent_id'] = $_POST['content_category_id'];
							$this->db->insert('menu', $menu);
							$menu_id = $this->db->insert_id();
		    			}
		    		}
					
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = (isset($_POST['icon'][$lang_k])) ? $_POST['icon'][$lang_k] : NULL;
							$text['image'] = (isset($_POST['image'][$lang_k])) ? $_POST['image'][$lang_k] : NULL;
							$text['teaser'] = (isset($_POST['teaser'][$lang_k])) ? $_POST['teaser'][$lang_k] : NULL;
							$text['content'] = (isset($_POST['content'][$lang_k])) ? $_POST['content'][$lang_k] : NULL;
							$text['icon_1'] = (isset($_POST['icon_1'][$lang_k])) ? $_POST['icon_1'][$lang_k] : NULL;
							$text['image_1'] = (isset($_POST['image_1'][$lang_k])) ? $_POST['image_1'][$lang_k] : NULL;
							$text['teaser_1'] = (isset($_POST['teaser_1'][$lang_k])) ? $_POST['teaser_1'][$lang_k] : NULL;
							$text['content_1'] = (isset($_POST['content_1'][$lang_k])) ? $_POST['content_1'][$lang_k] : NULL;
							$text['link'] = $_POST['link'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							
							if($_POST['content_text_id'][$lang_k] != '0') {
								$this->db->where(array('content_text_id' => $_POST['content_text_id'][$lang_k]));
								$this->db->update('content_text', $text);
							} else {
								$text['parent_id'] = $_POST['content_id'];
								$text['content_text_type'] = 'content';
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('content_text', $text);
							}
							
							if(in_array($_POST['content_category_id'], array(3, 4, 5, 6))) {
								$menu_text = array();
								$menu_text['menu_id'] = $menu_id;
								$menu_text['title'] = $_POST['title'][$lang_k];
								$menu_text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('menu_text', $menu_text);
							}
							
						}
					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'content'));
	    			$ref = 'system/cms/content_edit/' . $_POST['content_id'];
					if(isset($_POST['ref'])) {
						$ref = $_POST['ref'];
					}
					redirect($ref);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
	
    /**
     * Enter description here ...
     * @param unknown_type $param
     */
    function _save_menu($param) {
    	
    }
     
    /**
     * Enter description here ...
     */
    function save_gallery_category() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		if(!isset($_POST['gallery_category_name'])) {
			$_POST['gallery_category_name'] = underscore($_POST['title']);
		}
//		$this->form_validation->set_rules('gallery_category_name', lang('label_name'), 'trim|required|xss_clean');
		
		$redirect = 'system/cms/gallery_category';
		if(isset($_POST['redirect'])) {
			$redirect = $_POST['redirect'];
			unset($_POST['redirect']);
		}
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('gallery_category', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'gallery category'));
    				redirect($redirect);
					exit;
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('gallery_category_id', $_POST['gallery_category_id']);
	    			unset($_POST['gallery_category_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('gallery_category', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'gallery category'));
					
				}
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_gallery() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('gallery_name', lang('label_name'), 'trim|required|xss_clean');		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
                
	    			$gallery['gallery_name'] = $_POST['gallery_name'];
					$gallery['gallery_category_id'] = $_POST['gallery_category_id'];
					$gallery['sort'] = $_POST['sort'];
	    			$gallery['created_id'] = get_user_id();
					$this->db->insert('gallery', $gallery);
					$gallery_id = $this->db->insert_id();
					foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['content_text_type'] = 'gallery';
							$text['parent_id'] = $gallery_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$text['icon_1'] = $_POST['icon_1'][$lang_k];
							$text['image_1'] = $_POST['image_1'][$lang_k];
							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
						}
					}
    				set_success_message(sprintf(lang('success_add'), 'gallery'));
    				redirect('system/cms/gallery_edit/' . $gallery_id);
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$gallery['gallery_name'] = $_POST['gallery_name'];
					$gallery['gallery_category_id'] = $_POST['gallery_category_id'];
					$gallery['sort'] = $_POST['sort'];
					$gallery['status'] = $_POST['status'];
					$gallery['modified_id'] = get_user_id();
	    			$gallery['modified_time'] = date('Y-m-d H:i:s');
	    				
	    			$this->db->where('gallery_id', $_POST['gallery_id']);    			
	    			$this->db->update('gallery', $gallery);
	    				
                    foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
							$text['content'] = $_POST['content'][$lang_k];
							$text['icon_1'] = $_POST['icon_1'][$lang_k];
							$text['image_1'] = $_POST['image_1'][$lang_k];
							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
						
							if($_POST['content_text_id'][$lang_k] != '0') {
								$this->db->where(array('content_text_id' => $_POST['content_text_id'][$lang_k]));
								$this->db->update('content_text', $text);
							} else {
								$text['parent_id'] = $_POST['gallery_id'];
								$text['content_text_type'] = 'gallery';
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('content_text', $text);
							}
						}
					}
                
              		set_success_message(sprintf(lang('success_edit'), 'gallery'));
					redirect('system/cms/gallery_edit/' . $_POST['gallery_id']);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_banner_category() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('banner_category_name', lang('label_name'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('banner_category', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'banner category'));
    				redirect('system/cms/banner_category');
					exit;
				}
    			
    		break;
    		case 'edit':
    			unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('banner_category_id', $_POST['banner_category_id']);
	    			unset($_POST['banner_category_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('banner_category', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'banner category'));
					
				}
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_banner() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('banner_name', lang('label_name'), 'trim|required|xss_clean');		
    	$this->form_validation->set_rules('position', 'Position', 'trim|required');		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    		
                    $banner['banner_name'] = $_POST['banner_name'];
					$banner['banner_category_id'] = $_POST['banner_category_id'];
					$banner['display_pos'] = $_POST['position'];
//					$banner['page_id'] = $_POST['page_id'];
					$banner['sort'] = $_POST['sort'];
					$banner['publish_date_start'] = $_POST['publish_date_start'];
					$banner['publish_date_end'] = $_POST['publish_date_end'];
	    			$banner['created_id'] = get_user_id();
					$this->db->insert('banner', $banner);
					$banner_id = $this->db->insert_id();
                	foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['content_text_type'] = 'banner';
							$text['parent_id'] = $banner_id;
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
//							$text['content'] = $_POST['content'][$lang_k];
//							$text['icon_1'] = $_POST['icon_1'][$lang_k];
//							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							$text['language'] = $_POST['language'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
							$this->db->insert('content_text', $text);
							
						}
					}
					
					foreach($_POST['page_id'] as $display_k => $display_v) {
						if($display_v != '') {
							$display = array();
							$display['banner_id'] = $banner_id;
							$display['page_id'] = $_POST['page_id'][$display_k];
							$display['hotel_id'] = ($_POST['page_id'][$display_k] != 2) ? 0 : $_POST['hotel_id'][$display_k];
							$display['display_pos'] = $_POST['display_pos'][$display_k];
							$this->db->insert('banner_display', $display);
						} 
					}
					
                    set_success_message(sprintf(lang('success_add'), 'banner'));
    				redirect('system/cms/banner_edit/' . $banner_id);
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$banner['banner_name'] = $_POST['banner_name'];
					$banner['banner_category_id'] = $_POST['banner_category_id'];
					$banner['display_pos'] = $_POST['position'];
//					$banner['page_id'] = $_POST['page_id'];
					$banner['sort'] = $_POST['sort'];
					$banner['status'] = $_POST['status'];
					$banner['publish_date_start'] = $_POST['publish_date_start'];
					$banner['publish_date_end'] = $_POST['publish_date_end'];
					$banner['modified_id'] = get_user_id();
	    			$banner['modified_time'] = date('Y-m-d H:i:s');	  
	    			 
                    $this->db->where('banner_id', $_POST['banner_id']); 			
	    			$this->db->update('banner', $banner);
	    			
                    foreach($_POST['title'] as $lang_k => $lang_v) {
						if($_POST['title'][$lang_k] != '') {
							$text = array();
							$text['title'] = $_POST['title'][$lang_k];
							$text['icon'] = $_POST['icon'][$lang_k];
							$text['image'] = $_POST['image'][$lang_k];
							$text['teaser'] = $_POST['teaser'][$lang_k];
//							$text['content'] = $_POST['content'][$lang_k];
//							$text['icon_1'] = $_POST['icon_1'][$lang_k];
//							$text['image_1'] = $_POST['image_1'][$lang_k];
//							$text['teaser_1'] = $_POST['teaser_1'][$lang_k];
//							$text['content_1'] = $_POST['content_1'][$lang_k];
							$text['link'] = $_POST['link'][$lang_k];
							if(!isset($_POST['country'][$lang_k])) {
								$_POST['country'][$lang_k] = $this->config->item('ge_def_country');
							}
							$text['country'] = implode('|', $_POST['country'][$lang_k]);
						
							if($_POST['content_text_id'][$lang_k] != '0') {
								$this->db->where(array('content_text_id' => $_POST['content_text_id'][$lang_k]));
								$this->db->update('content_text', $text);
							} else {
								$text['parent_id'] = $_POST['banner_id'];
								$text['content_text_type'] = 'banner';
								$text['language'] = $_POST['language'][$lang_k];
								$this->db->insert('content_text', $text);
							}
						}
					}
                
					foreach($_POST['page_id'] as $display_k => $display_v) {
						if($display_v != '') {
							$display = array();
							$display['banner_id'] = $_POST['banner_id'];
							$display['page_id'] = $_POST['page_id'][$display_k];
							$display['hotel_id'] = ($_POST['page_id'][$display_k] != 2) ? 0 : $_POST['hotel_id'][$display_k];
							$display['display_pos'] = $_POST['display_pos'][$display_k];
							if((isset($_POST['banner_display_id'])) && $_POST['banner_display_id'][$display_k] != '0') {
								$this->db->where(array('banner_display_id' => $_POST['banner_display_id'][$display_k]));
								$this->db->update('banner_display', $display);
							} else {
								$this->db->insert('banner_display', $display);
							}
						} else {
							$this->db->where(array('banner_display_id' => $_POST['banner_display_id'][$display_k]));
							$this->db->delete('banner_display');
						}
					}
					
	    			set_success_message(sprintf(lang('success_edit'), 'banner'));
					redirect('system/cms/banner_edit/' . $_POST['banner_id']);
					exit;
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
     
    /**
     * Enter description here ...
     */
    function save_page_banner() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('banner_name', lang('label_name'), 'trim|required|xss_clean');
		
		$category = $this->db->get_where('banner_category', array('banner_category_id' => $_POST['banner_category_id']));
		$res_category = $category->row();
		$_POST['language'] = $res_category->language;
		
		$redirect = 'system/pages/banner';
		if(isset($_POST['ref'])) {
			list($ref_mod, $ref_func) = explode('.', $_POST['ref']);
			$redirect = "system/$ref_mod/$ref_func";
			unset($_POST['ref']);
		}
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
    				
    				if($_FILES["image"]["name"] != "") {
						$this->load->library(array('upload', 'image_lib'));
						
						$config_image['upload_path'] = $this->config->item('ge_upload_path') . "images/banner/";
						$config_image['allowed_types'] = 'jpg';
						$config_image['file_name'] = 'banner.jpg';
						$this->upload->initialize($config_image);
						if ($this->upload->do_upload('image')) {
							$data = $this->upload->data();
							$_POST['image'] = '/ace_media/images/banner/' . $data['file_name'];
						}
					}
    				
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('banner', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'banner'));
    				redirect($redirect);
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
					if($_FILES["image"]["name"] != "") {
						$this->load->library(array('upload', 'image_lib'));
						
						$config_image['upload_path'] = $this->config->item('ge_upload_path') . "images/banner/";
						$config_image['allowed_types'] = 'jpg';
						$config_image['file_name'] = 'banner.jpg';
						$this->upload->initialize($config_image);
						if ($this->upload->do_upload('image')) {
							$data = $this->upload->data();
							$_POST['image'] = '/ace_media/images/banner/' . $data['file_name'];
						}
					}
					
	    			$this->db->where('banner_id', $_POST['banner_id']);
	    			unset($_POST['banner_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('banner', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'banner'));
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function save_contact() {
		unset($_POST['action']);
		unset($_POST['btnSave']);
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('title', lang('label_title'), 'trim|required');
		$this->form_validation->set_rules('contact_type', lang('label_type'), 'trim|required');
		$this->form_validation->set_rules('user_id', lang('label_user_name'), 'trim|required');
		
    	switch ($_POST['mode']) {
    		case 'add':
    			unset($_POST['mode']);
    			if ($this->form_validation->run() != FALSE){//	echo 'valid';
	    			$_POST['created_id'] = get_user_id();
					$this->db->insert('contact', $_POST);
    				set_success_message(sprintf(lang('success_add'), 'contact'));
    				redirect('system/cms/contact');
					exit;
				}
    			
    		break;
    		case 'edit':
	    		unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					if(!isset($_POST['status'])) {
						$_POST['status'] = 0;
					}
					
	    			$this->db->where('contact_id', $_POST['contact_id']);
	    			unset($_POST['contact_id']);
	    			$_POST['modified_id'] = get_user_id();
	    			$_POST['modified_time'] = date('Y-m-d H:i:s');
	    			$this->db->update('contact', $_POST);
	    			set_success_message(sprintf(lang('success_edit'), 'contact'));
					
				}
    		break;
    		case 'delete':
				
    		break;
    	}
    }
        
    /**
     * Enter description here ...
     */
    function image_edit() {
    	$this->load->library('image_lib');
    	$image = pathinfo($_POST['image']);
    	
    	$config['source_image'] = BASEPATH . str_replace('/tune_media/', '../assets/media/', $_POST['image']);
    	
    	switch ($_POST['btnSave']) {
    		case 'Crop Image' :
    			
    			$config['width'] = $_POST['w'];
    			$config['height'] = $_POST['h'];
    			$config['x_axis'] = $_POST['x'];
    			$config['y_axis'] = $_POST['y'];
  				$config['maintain_ratio'] = false;
    			copy($config['source_image'], $config['source_image'] . '.bak');
				$this->image_lib->initialize($config);
		    	if(!$this->image_lib->crop()) {
				    set_error_message($this->image_lib->display_errors());
				} else {
/*					$config_resize_l['source_image'] = $config['source_image'];
					$config_resize_l['width'] = 370;
					$config_resize_l['height'] = 280;
					$config_resize_l['new_image'] = realpath($this->image_dir) . '/large/' . $image['basename'];
					$this->image_lib->initialize($config_resize_l); 
					
					if(!$this->image_lib->resize()){
						echo $this->image_lib->display_errors();
					} else {
	//					echo "Ok..";
					}
					$config_resize_lx['source_image'] = $config['source_image'];
					$config_resize_lx['width'] = 125;
					$config_resize_lx['height'] = 135;
					$config_resize_lx['new_image'] = realpath($this->image_dir) . '/thumb/' . $image['basename'];
					$this->image_lib->initialize($config_resize_lx); 
					
					if(!$this->image_lib->resize()){
	//					echo $this->image_lib->display_errors();
					} else {
	//					echo "Ok..";
					}
*/				}
    			
    			break;
    		case 'Resize Image' :
    			$config['width'] = $_POST['width'];
    			$config['height'] = $_POST['height'];
    			copy($config['source_image'], $config['source_image'] . '.bak');
				$this->image_lib->initialize($config);
		    	if(!$this->image_lib->resize()) {
				    set_error_message($this->image_lib->display_errors());
				} else {
/*					$config_resize_l['source_image'] = $config['source_image'];
					$config_resize_l['width'] = 370;
					$config_resize_l['height'] = 280;
					$config_resize_l['new_image'] = realpath($this->image_dir) . '/large/' . $image['basename'];
					$this->image_lib->initialize($config_resize_l); 
					
					if(!$this->image_lib->resize()){
						echo $this->image_lib->display_errors();
					} else {
	//					echo "Ok..";
					}
					$config_resize_lx['source_image'] = $config['source_image'];
					$config_resize_lx['width'] = 125;
					$config_resize_lx['height'] = 135;
					$config_resize_lx['new_image'] = realpath($this->image_dir) . '/thumb/' . $image['basename'];
					$this->image_lib->initialize($config_resize_lx); 
					
					if(!$this->image_lib->resize()){
	//					echo $this->image_lib->display_errors();
					} else {
	//					echo "Ok..";
					}
*/				}
    			
    			break;
    	}
    }
    
    /**
     * Enter description here ...
     */
    function delete_content_category() {
		$this->db->where('content_category_id', $_POST['content_category_id']);
		$this->db->delete('content_category');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'content category') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_content() {
		$this->db->where('content_id', $_POST['content_id']);
		$this->db->delete('content');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'content') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_gallery_category() {
		$this->db->where('gallery_category_id', $_POST['gallery_category_id']);
		$this->db->delete('gallery_category');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'gallery category') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_gallery() {
		$this->db->where('gallery_id', $_POST['gallery_id']);
		$this->db->delete('gallery');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'gallery') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_banner_category() {
		$this->db->where('banner_category_id', $_POST['banner_category_id']);
		$this->db->delete('banner_category');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'banner category') . "'}";
		
    }
    
    /**
     * Enter description here ...
     */
    function delete_banner() {
		$this->db->where('banner_id', $_POST['banner_id']);
		$this->db->delete('banner');
		echo "{error: '',msg: '" . sprintf(lang('success_delete'), 'banner') . "'}";
		
    }
    
	/**
	 * Enter description here ...
	 * @param int $content_category_id
	 */
	function get_content_category($content_category_id) {
		return $this->db->get_where('content_category', array('content_category_id' => $content_category_id));
   	
    } 
    
	/**
	 * Enter description here ...
	 * @param int $gallery_category_id
	 */
	function get_gallery_category($gallery_category_id) {
		return $this->db->get_where('gallery_category', array('gallery_category_id' => $gallery_category_id));
   	
    } 
    
	/**
	 * Enter description here ...
	 * @param int $banner_category_id
	 */
	function get_banner_category($banner_category_id) {
		return $this->db->get_where('banner_category', array('banner_category_id' => $banner_category_id));
   	
    } 
    
    
    
/*
 * ADDITIONAL FUNCTION
 */	
	
    /**
     * Enter description here ...
     * @return multitype:string 
     */
    function get_page_list() {
    	if(get_role() > 3) {
    		$this->db->where('created_id', get_user_id());
    	}
    	
    	$this->db->select('content.*, content_category.title category_title')
    				->from('content')
    				->join('content_category', 'content_category.content_category_id = content.content_category_id')
					->where("content_category.content_category_id = 1 OR (content_category.content_category_id IN(3, 4, 5, 6) AND NOT ISNULL(content.link)) ")
    				->order_by('content.content_category_id, content.sort');
    	return $this->db->get();
   	
    } 
    
}

/**
 * End of file cms_model.php 
 * Location: ./.../.../.../cms_model.php 
 */