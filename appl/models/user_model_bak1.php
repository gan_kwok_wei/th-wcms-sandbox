<?php	if (!defined('BASEPATH')) exit('No direct script access allowed');
/********************************************************************/
/* File Name		: ./appl/models/user_model.php					*/
/* Module			: 												*/
/* Revise Number	: 01											*/
/* Created By		: Budi Laxono									*/
/* Created At		: 20-06-2010									*/
/********************************************************************/
/* Modified By		: Unknown										*/
/* Modified At		: Unknown										*/
/* Modification		: Unknown										*/
/********************************************************************/


class User_model extends GE_Model {

	function __construct() {
		parent::__construct();
	}
	
	/**
	 * @param $user_id
	 * @return Object user data
	 */
	function get_user_list() {
		$this->db->select("user_id, external_id, users.first_name, users.last_name, job_title, users.email, users.phone_work, security_role.name AS role")
			->from('users')
			->join('security_role', 'security_role.role_id = users.role_id')
			->order_by('first_name');
			
		return $this->db->get();
		
	}
	
	/**
	 * Enter description here ...
	 */
	function get_role_list() {
		return $this->db->get('security_role');
		
	}
	
	/**
	 * Enter description here ...
	 */
	function get_permission_list() {
		return $this->db->get('security_permission');
		
	}
	
	/**
	 * Enter description here ...
	 */
	function save_role() {
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name', lang('label_name'), 'trim|required');
			
		switch ($_POST['mode']) {
			case 'add':
				unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					$this->add_role();
					set_success_message(sprintf(lang('success_add'), 'role'));
					redirect('system/security');
					exit;
				}
			break;
			case 'edit':
				unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					$this->update_role();
					set_success_message(sprintf(lang('success_edit'), 'role'));
				}
			break;
		}
	}
	
	/**
	 * @return unknown_type
	 */
	function add_role(){
		
		if(isset($_POST['btnSave'])) {
			unset($_POST['btnSave']);
		}
		
		$this->db->insert('security_role', $_POST);
		return $this->db->insert_id();
//		var_dump($_POST);
	}
	
	/**
	 * @return unknown_type
	 */
	function update_role(){
		
		if(isset($_POST['btnSave'])) {
			unset($_POST['btnSave']);
		}
		
		$this->db->where('role_id', $_POST['role_id']);
		unset($_POST['role_id']);
		$this->db->update('security_role', $_POST);
//		var_dump($_POST);
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 */
	function get_role_permission($role_id) {
		$sql = "SELECT security_permission.*, security_role_permission.entry_id, security_role_permission.allow_deny "
			.  "FROM security_permission "
			.  "JOIN security_role_permission ON(security_role_permission.permission_id = security_permission.permission_id) "
			.  "WHERE security_role_permission.role_id = $role_id";
		return $this->db->query($sql);
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 */
	function get_x_role_permission($role_id) {
		$sql = "SELECT * "
			.  "FROM security_permission "
			.  "WHERE permission_id NOT IN(SELECT permission_id FROM security_role_permission WHERE role_id = $role_id)";
		return $this->db->query($sql);
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function role_add_permission($role_id, $permission_id, $allow = 1) {
		$this->db->insert('security_role_permission', array('role_id' => $role_id, 'permission_id' => $permission_id, 'allow_deny' => $allow));
		return $this->db->insert_id();
	}
	
	/**
	 * Enter description here ...
	 * @param int $role_id
	 * @param int $permission_id
	 * @param int $allow
	 */
	function update_role_permision() {
		$user_role = $this->get_role_permission($_POST['role_id']);
		foreach ($user_role->result() as $role) {
			
			$this->db->where('entry_id', $role->entry_id);
			$list = $_POST['role_permission'];
			$allow = isset($list[$role->entry_id]) ? 1 : 0;
			$this->db->update('security_role_permission', array('allow_deny' => $allow));
		}
	}
	
	/**
	 * Enter description here ...
	 */
	function save_permission() {
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name', lang('label_name'), 'trim|required');
		$this->form_validation->set_rules('module', lang('label_module'), 'trim|required');
		$this->form_validation->set_rules('class', lang('label_class'), 'trim|required');
		$this->form_validation->set_rules('function', lang('label_function'), 'trim|required');
			
		switch ($_POST['mode']) {
			case 'add':
				unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					$this->add_permission();
					set_success_message(sprintf(lang('success_add'), 'permission'));
					redirect('system/security');
					exit;
				}
			break;
			case 'edit':
				unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					$this->update_permission();
					set_success_message(sprintf(lang('success_edit'), 'permission'));
				}
			break;
		}
	}
	
	/**
	 * @return unknown_type
	 */
	function add_permission(){
		
		if(isset($_POST['btnSave'])) {
			unset($_POST['btnSave']);
		}
		
		$this->db->insert('security_permission', $_POST);
		return $this->db->insert_id();
//		var_dump($_POST);
	}
	
	/**
	 * @return unknown_type
	 */
	function update_permission(){
		
		if(isset($_POST['btnSave'])) {
			unset($_POST['btnSave']);
		}
		
		$this->db->where('permission_id', $_POST['permission_id']);
		unset($_POST['permission_id']);
		$this->db->update('security_permission', $_POST);
//		var_dump($_POST);
	}
	
	/**
	 * @param $user_id
	 * @return Object user data
	 */
	function get_user($user_id) {
		return $this->db->get_where('users', array('user_id' => $user_id));
	}
	
	/**
	 * Enter description here ...
	 */
	function save_user() {
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('user_name', lang('label_user_id'), 'trim|required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('email', lang('label_email'), 'trim|required|valid_email');
		$this->form_validation->set_rules('email_other', lang('label_email_other'), 'trim|valid_email');
		$this->form_validation->set_rules('role_id', lang('label_role_id'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('first_name', lang('label_first_name'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('last_name', lang('label_last_name'), 'trim|xss_clean');
			
		switch ($_POST['mode']) {
			case 'add':
				unset($_POST['mode']);
				$this->form_validation->set_rules('password', lang('label_password'), 'trim|required|matches[conf_password]|min_length[5]|md5');
				$this->form_validation->set_rules('conf_password', lang('label_conf_password'), 'trim|required|md5');
				
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					$this->add_user();
					set_success_message(sprintf(lang('success_add'), 'user'));
    				redirect('system/user');
					exit;
				}
			break;
			case 'edit':
				unset($_POST['mode']);
				if ($this->form_validation->run() != FALSE){//	echo 'valid';
					$this->update_user();
					set_success_message(sprintf(lang('success_edit'), 'user'));
				}
			break;
		}
	}
	
	/**
	 * @return unknown_type
	 */
	function add_user(){
		
		if(isset($_POST['btnSave'])) {
			unset($_POST['btnSave']);
		}
		
		if(isset($_POST['conf_password'])) {
			unset($_POST['conf_password']);
		}
		$_POST['created_id'] = get_user_id();
		
		$this->db->insert('users', $_POST);
		return $this->db->insert_id();
//		var_dump($_POST);
	}
	
	/**
	 * @return unknown_type
	 */
	function add_contact_user(){
		
		$_POST['created_id'] = get_user_id();
		
		$this->db->insert('users', $_POST);
		echo "{error: '',id: '" . $this->db->insert_id() . "',msg: '" . sprintf(lang('success_insert'), 'contact user') . "'}";
//		var_dump($_POST);
	}
	
	/**
	 * @return unknown_type
	 */
	function update_user(){
		
		if(isset($_POST['btnSave'])) {
			unset($_POST['btnSave']);
		}
		
		$this->db->where('user_id', $_POST['user_id']);
		unset($_POST['user_id']);
		$_POST['modified_id'] = get_user_id();
		$_POST['modified_time'] = date('Y-m-d H:i:s');

		$this->db->update('users', $_POST);
//		var_dump($_POST);
	}
	
	/**
	 * @param $user_id
	 * @return unknown_type
	 */
	function delete_user($user_id) {
		$this->db->where('user_id', $user_id);
		$this->db->delete('users');
		
	}
	
	/**
	 * @return Process
	 */
	function update_password() {
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('old_password', lang('label_old_password'), 'trim|required|md5|callback_old_password_check');
		$this->form_validation->set_rules('password', lang('label_password'), 'trim|required|min_length[5]|matches[conf_password]|md5');
		$this->form_validation->set_rules('conf_password', lang('label_conf_password'), 'trim|required|md5');
		
		if ($this->form_validation->run() != FALSE){//	echo 'valid';
			$user_id = $this->input->post('user_id');
			unset($_POST['old_password']);
			unset($_POST['conf_password']);
			$this->update_user();
			
			set_success_message(sprintf(lang('success_edit'), 'password user'));
		}
	
	}
	
}
	
/* End of file user_model.php */
/* Location: ./appl/models/user_model.php */